<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Driving_license_block extends Model
    {
        protected $fillable = ['driving_license'];
    }
