<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class User_relation extends Model
    {
        protected $fillable = [
            'name_en',
            'name_ar',
            'image',
        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['name'] = $this->Serv_name;
            $data["image"] = $this->serv_image;
            return $data;
        }

        public function getServNameAttribute()
        {
            if (\request()->lang == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getDashNameAttribute()
        {
            if (app()->getLocale() == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getServImageAttribute()
        {
            if ($this->image)
                return $attribute = $this->image;
            else
                return asset('assets/admin/images/childImage.png');
        }

    }
