<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip_order_receipt_status extends Model
{
    protected $fillable = [
        'name_ar', 'name_en'
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->serv_name;
        return $data;
    }

    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getDashNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

}
