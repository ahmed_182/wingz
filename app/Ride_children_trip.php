<?php

namespace App;

use Composer\XdebugHandler\Status;
use Illuminate\Database\Eloquent\Model;
use TripRideStatus;

class Ride_children_trip extends Model
{
    protected $fillable = [
        'ride_id',
        'child_id',
        'distance_in_kilo',
        'status_id',
        'sort_by_distance'
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['child'] = $this->child->profile();
        $data['is_finished_status'] = $this->child_finish_status();
        $data['child_absences_status'] = $this->child_status();
        $data['is_absences_status'] = $this->child_absences_bool();
        $data['status'] = $this->status;
        return $data;
    }



    public function child()
    {
        return $this->belongsTo(Child::class, 'child_id');
    }

    public function status()
    {
        return $this->belongsTo(Trip_ride_status::class, 'status_id');
    }

    public function child_finish_status()
    {
        $ride = Trip_ride::find($this->ride_id);
        $child_finish_status_count = Trip_ride_life_cycle::where('trip_ride_id', $ride->id)->where('child_id', $this->child_id)->count();
        if ($child_finish_status_count >= 3) {
            $att = true;
        } else {
            $att = false;
        }
        return $att;
    }

    public function child_status()
    {
        // check if this child will absence today or not !
        $item = Child_absence::where('child_id', $this->child_id)->first();
        if ($item) {
            // start of day
            $startOfToday = date('Y/m/d 00:00:00');
            $endOfToday = date('Y/m/d 23:59:59');
            $startOfToday_long = strtotime($startOfToday) * 1000;
            $endOfToday_long = strtotime($endOfToday) * 1000;

            $check_if_exist = $days_date = Child_absence_days::where('child_absences_id', $item->id)
                ->where('day', '<=', $endOfToday_long)
                ->where('day', '>=', $startOfToday_long)
                ->count();
            if ($check_if_exist > 0) {
                return trans('language.child_is_absence');
            }
        }
        return null;
    }

    public function child_absences_bool()
    {
        // check if this child will absence today or not !
        $item = Child_absence::where('child_id', $this->child_id)->first();
        if ($item) {
            // start of day
            $startOfToday = date('Y/m/d 00:00:00');
            $endOfToday = date('Y/m/d 23:59:59');
            $startOfToday_long = strtotime($startOfToday) * 1000;
            $endOfToday_long = strtotime($endOfToday) * 1000;

            $check_if_exist = $days_date = Child_absence_days::where('child_absences_id', $item->id)
                ->where('day', '<=', $endOfToday_long)
                ->where('day', '>=', $startOfToday_long)
                ->count();
            if ($check_if_exist > 0) {
                return true;
            }
        }
        return null;
    }


}
