<?php

    namespace App;

    use Carbon\Carbon;
    use Illuminate\Database\Eloquent\Model;

    class School_time extends Model
    {

        protected $fillable = [
            'stage_id',
            'school_level_id',
            'start_at',
            'end_at',
            'year_start_at',
            'year_end_at',
        ];

        public function toArray()
        {
            $data["school_time_stage_id"] = $this->id;
            $data["id"] = $this->stage_id;
            $data["name"] = $this->serv_stage_name;
            $data["start_at"] = date('h:i A', strtotime($this->start_at));
            $data["end_at"] = date('h:i A', strtotime($this->end_at)) ;
            $data["year_start_at"] = date('d M Y', strtotime($this->year_start_at));
            $data["year_end_at"] = date('d M Y ', strtotime($this->year_end_at));
            return $data;
        }


        public function schoolLevel()
        {
            return $this->belongsTo(School_level::class, 'school_level_id');
        }

        public function stage()
        {
            return $this->belongsTo(Stage::class, 'stage_id');
        }

        public function getServStageNameAttribute()
        {
            $att = null;
            if ($this->stage)
                $att = $this->stage->serv_name;
            return $att;
        }

        public function getDashStageNameAttribute()
        {
            $att = null;
            if ($this->stage)
                $att = $this->stage->serv_name;
            return $att;
        }

        public function getDashSchoolNameAttribute()
        {
            $att = "";
            if ($this->schoolLevel)
                $att = $this->schoolLevel->dash_school_name;
            return $att;
        }

        public function getDashLevelNameAttribute()
        {
            $att = "";
            if ($this->level)
                $att = $this->level->dash_name;
            return $att;
        }

        public function getDashStartAtAttribute()
        {
            $att = "";
            if ($this->start_at)
                $att = Carbon::parse($this->start_at)->format('H:i A');
            return $att;
        }

        public function getDashEndAtAttribute()
        {
            $att = "";
            if ($this->start_at)
                $att = Carbon::parse($this->end_at)->format('H:i A');
            return $att;
        }

        public function getStartTimeAttribute()
        {
            $attribute = date("H:s");
            if ($this->start_at)
                $attribute = Carbon::parse($this->start_at)->format('H:i');
            return $attribute;
        }

        public function getEndTimeAttribute()
        {
            $attribute = date("H:s");
            if ($this->end_at)
                $attribute = Carbon::parse($this->end_at)->format('H:i');
            return $attribute;
        }

        public function getYearStartTimeAttribute()
        {
            $attribute = date("D , M , Y");
            if ($this->start_at)
                $attribute = Carbon::parse($this->start_at)->format("D , M , Y");
            return $attribute;
        }

        public function getYearEndTimeAttribute()
        {
            $attribute = date("D , M , Y");
            if ($this->end_at)
                $attribute = Carbon::parse($this->end_at)->format("D , M , Y");
            return $attribute;
        }


        public function getDashNameAttribute()
        {
            $att = "";
            if ($this->school)
                $school_name = $this->school->dash_name;

            if ($this->level)
                $level_name = $this->level->dash_name;

            if ($this->school and $this->level)
                return $school_name . ' ( ' . $level_name . ' ) ';
        }

        public function getFormatStartTimeAttribute()
        {
            $attribute = date("H:s");
            if ($this->start_at)
                $attribute = Carbon::parse($this->start_at)->format('H:i');
            return $attribute;
        }

        public function getFormatEndTimeAttribute()
        {
            $attribute = date("H:s");
            if ($this->end_at)
                $attribute = Carbon::parse($this->end_at)->format('H:i');
            return $attribute;
        }

        public function getFormatStartTimeYearAttribute()
        {
            $attribute = date("Y-m-d");
            if ($this->year_start_at)
                $attribute = Carbon::parse($this->year_start_at)->format('Y-m-d');
            return $attribute;
        }

        public function getFormatEndTimeYearAttribute()
        {
            $attribute = date("Y-m-d");
            if ($this->year_end_at)
                $attribute = Carbon::parse($this->year_end_at)->format('Y-m-d');
            return $attribute;
        }

    }
