<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class User_review extends Model
    {
        protected $fillable = [
            'reviewer_id',
            'user_id',
            'rate',
            'feedBack'
        ];

        public function toArray()
        {
            $data['reviewer'] = $this->reviewer;
            $data['user'] = $this->user;
            $data['rate'] = $this->rate;
            $data['feedBack'] = $this->feedBack;
            return $data;
        }


        public function reviewer()
        {
            return $this->belongsTo(User::class, 'reviewer_id');
        }

        public function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }
    }
