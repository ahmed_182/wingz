<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class After_school extends Model
    {
        protected $fillable = [
            'child_id',
            'start_date',
            'end_date',
            'time'
        ];
    }
