<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver_car_images extends Model
{
    protected $fillable = ["driver_id", "image"];

    public function toArray()
    {
        $data["id"] = $this->id;
        $data["image"] = $this->image;
        return $data;
    }
}
