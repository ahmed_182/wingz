<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SingleRideOperation extends Model
{
    protected $fillable =[
        'order_id',
        'user_id',
        'description',
        'paid', // total paid
        'cost', // total cost
        'balance', // current balance
        'debit' // debit balance
    ];


    protected function user()
    {
        return $this->belongsTo(User::class,  'user_id');
    }

    public function getUserAttribute()
    {
        return $this->user()->first();
    }

    protected function order()
    {
        return $this->belongsTo(Order::class,  'order_id');
    }

    public function getOrderAttribute()
    {
        return $this->order()->first();
    }
}
