<?php

    namespace App\ModulesConst;
    class TripStatus
    {
        public const pending = '1';
        public const accepted = '2';
        public const onway = '3';
        public const rejected = '4';
        public const finished = '5';

    }
