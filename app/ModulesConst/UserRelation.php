<?php

    namespace App\ModulesConst;
    class UserRelation
    {
        const father = '1';
        const mother = '2';
        const grandFather = '3';
        const grandMother = '4';
        const other = '5';
    }
