<?php

    namespace App\ModulesConst;
    class ChildStatus
    {
        public const home = '1';
        public const way_to_school = '2';
        public const school = '3';
        public const way_to_home = '4';
        public const after_school = '5';
        public const car_problem = '6';
    }
