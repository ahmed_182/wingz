<?php

    namespace App\ModulesConst;
    class AssignedStatus
    {
        public const pending = '1';
        public const meeting = '2';
        public const accepted = '3';
        public const rejected = '4';
        public const expired = '5';

    }
