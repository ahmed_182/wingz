<?php

namespace App;

use App\ModulesConst\NotificationTyps;
use App\ModulesConst\PlanTyps;
use Illuminate\Database\Eloquent\Model;

class Plan extends Model
{
    protected $fillable = [
        'name_ar',
        'name_en',
        'period',
        'discount',
        'status',
        'month_count',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->serv_name;
        $data['period'] = $this->period;
        $data['discount'] = $this->discount;
        $data['month_count'] = $this->month_count;
        $data['price_per_kilo'] = $this->serv_price_per_kilo;
        $data['price_after_tax'] = $this->price_after_tax;
        return $data;
    }

    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getServPricePerKiloAttribute()
    {
        $pricePerKilo = KiloPerPrice::latest()->first();
        if (!$pricePerKilo) {
            $pricePerKilo = 0;
        }
        return $pricePerKilo->price;
    }

    public function getDashNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }



}
