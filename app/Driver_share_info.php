<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Driver_share_info extends Model
    {
        protected $fillable = [
            'driver_id',
            'school_id',
            'children_count'
        ];
    }
