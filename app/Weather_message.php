<?php

    namespace App;

    use Carbon\Carbon;
    use Illuminate\Database\Eloquent\Model;

    class Weather_message extends Model
    {
        protected $fillable = [
            'name_ar',
            'name_en',
            'start_at',
            'end_at',
        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['name'] = $this->Serv_name;
//            $data["start_at"] = $this->start_at;
//            $data["end_at"] = $this->end_at;
            return $data;
        }

        public function getServNameAttribute()
        {
            if (\request()->lang == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getDashNameAttribute()
        {
            if (app()->getLocale() == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getDashStartAtAttribute()
        {
            $att = "";
            if ($this->start_at)
                $att = Carbon::parse($this->start_at)->format('H:i A');
            return $att;
        }

        public function getDashEndAtAttribute()
        {
            $att = "";
            if ($this->start_at)
                $att = Carbon::parse($this->end_at)->format('H:i A');
            return $att;
        }

        public function getStartTimeAttribute()
        {
            $attribute = date("H:s");
            if ($this->start_at)
                $attribute = Carbon::parse($this->start_at)->format('H:i');
            return $attribute;
        }

        public function getEndTimeAttribute()
        {
            $attribute = date("H:s");
            if ($this->end_at)
                $attribute = Carbon::parse($this->end_at)->format('H:i');
            return $attribute;
        }

        public function getFormatStartTimeAttribute()
        {
            $attribute = date("H:s");
            if ($this->start_at)
                $attribute = Carbon::parse($this->start_at)->format('H:i');
            return $attribute;
        }

        public function getFormatEndTimeAttribute()
        {
            $attribute = date("H:s");
            if ($this->end_at)
                $attribute = Carbon::parse($this->end_at)->format('H:i');
            return $attribute;
        }
    }
