<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Country_startwith extends Model
{
    protected $fillable = [
        'start_with', 'country_id'
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['start_with'] = $this->start_with;
        return $data;
    }
}
