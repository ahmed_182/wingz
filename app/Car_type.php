<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Car_type extends Model
{
    protected $fillable = [
        'seat_number',
        'name_en',
        'name_ar'
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->Serv_name;
        $data['seat_number'] = $this->seat_number;
        return $data;
    }

    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getDashNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }
}
