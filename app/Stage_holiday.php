<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Stage_holiday extends Model
    {
        protected $fillable = [
            'stage_holiday_period_id',
            'school_time_stage_id',
            'day_long',
            'day_stamp',
            'paid_status'
        ];


        public function school_time_stages()
        {
            return $this->belongsTo(School_time::class, 'school_time_stage_id');
        }

        public function stage_holiday_period()
        {
            return $this->belongsTo(Stage_holiday_period::class, 'stage_holiday_period_id');
        }

        public function getdashStageNameAttribute()
        {
            $att = null;
            if ($this->school_time_stages)
                $att = $this->school_time_stages->dash_stage_name;
            return $att;
        }

        public function getdashSchoolNameAttribute()
        {
            $att = null;
            if ($this->school_time_stages)
                $att = $this->school_time_stages->dash_school_name;
            return $att;
        }

        public function getDashStatusNameAttribute()
        {
            $attribute = __('language.notSelected');

            if ($this->paid_status == null) {
                return __('language.notSelected');
            }
            if ($this->paid_status == 0) {
                return __('language.free');
            }
            if ($this->paid_status == 1) {
                return __('language.paid');
            }
            return $attribute;
        }

    }
