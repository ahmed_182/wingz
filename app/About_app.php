<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class About_app extends Model
    {

        // About_app
        protected $fillable = [
            'name_en',
            'name_ar',
            'kilo_price',
            'safety_details_ar',
            'safety_details_en',
        ];

        public function toArray()
        {
            $data['name'] = $this->Serv_name;
            $data['safety_details'] = $this->Safety_details;
            return $data;
        }

        public function getServNameAttribute()
        {
            if (\request()->lang == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getDashNameAttribute()
        {
            if (app()->getLocale() == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getSafetyDetailsAttribute()
        {
            if (app()->getLocale() == "en")
                return $this->safety_details_en;
            else
                return $this->safety_details_ar;
        }
    }
