<?php namespace App\Traits;

use App\ModulesConst\OrderStatus;
use App\Order;
use App\Single_ride;
use Auth;
use Kreait\Firebase\Exception\FirebaseException;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;

trait fireBaseContainer
{
    public function fireBaseHandler($order)
    {
        $order_id = (string)$order->id;
        $status_id = (string)$order->order_status_id;
        //
        $obj['status_id'] = $status_id;
        $reference = "singleRides/$order_id/";
        try {
            $this->setUpFirebase()
                ->getReference($reference)
                ->set($obj);
        } catch (FirebaseException $e) {

        }

        if ($order->driver_id)
            $this->fireBaseHandlerDriverNode($order, $order->driver_id);
    }


    public function fireBaseHandlerDriverNode($order, $driverId)
    {
        $id = (string)$order->id;
        $status_id = (string)$order->order_status_id;
        //
        $obj['status_id'] = $status_id;
        $obj['id'] = $id;
        $reference = "users/drivers/$driverId/order";

        try {
            $this->setUpFirebase()
                ->getReference($reference)
                ->set($obj);
        } catch (FirebaseException $e) {
            //todo
        }
    }


    public function fireBaseConnectionJson()
    {
        $data['type'] = "service_account";
        $data['project_id'] = "wings-d491f";
        $data['private_key_id'] = "14aed8e4036e16434623c3557f346ac3b366a7a9";
        $data['private_key'] = "-----BEGIN PRIVATE KEY-----\nMIIEvQIBADANBgkqhkiG9w0BAQEFAASCBKcwggSjAgEAAoIBAQCLD5z5440rncpS\nYPBoHwGJ2U/vMUdSSSoOzQ1sPvlRcHQXPtgQqf9A7hAxcdPsAZk2jcDCxnQN+tKy\njmQO55HPTTlafUPqThmaY3VAV/DDuf/phkvUx7KAXaPo35qfdQpNH21tqUXpqvcn\nWcuyYEF01KEZ/ZjxZlPwUX+Bgp24trrHeCxa9ztlZ9bGrJhV8QdGtnL68tJk2dht\nSDl8O5K08Vf+LAaY6mQDMKo411UnU3CnXaECOuZ93NzVKJxK8IxD6QrP0okM0Ybc\nRoZFImtOpRKqznDTe4WYda/LJHv7SEOPWIekK/HSFf9WuGuDYZdMieKlMbZFoIbP\nokpa4RNHAgMBAAECggEAEnTVM/H9yRWtW3yj5mCDHu1u51iw1hzWzlXBxnr0h9i3\nLQ9OtFDTtiWX6Zq+x+/jCrNgAO8/n6FezeBfKv6Uwsua5UXYjzmkD/J2W+laulIu\nmIBrV8d/YKZ3puSSjD+ySSUhgXCk0kwPdzNlo9GUWRy6xbfQ+5fGDpB1LgEMzOM6\n+tytS6dELcTL4E8FyuVMOFeQI88zTE+g3kVzYOPmm6olhQin/lQCSJQQFhoGg0pl\nma8KW34tAf+z+Q+sWvPzo2ryw/s0OUIgXBK6uN2n5a8xgAqxQ85F8hIBSRKGMvv5\nVi9ujXxwEGZ7302+/58dG+syelhEgN+c8eECM4rBeQKBgQDCq2IDaU/jbX+mU1lT\nucrQFag/1bGbcLw90qQ4/ivRsgM1Sqz3fNuON8QeWjSOC4uH+SgFnhA66QoOif0o\n9xdfBVlM0mBNmb9f0sSdRCL/mDUUktaoMViowsHdtbOKiL1ecmOZ4i2Uldt04ncO\n8WApM0ciTqANY0VjSwSeXRNKvwKBgQC230HCD7YS+ZweixHUOmycwCwm2hEgFwJC\n/o+bPPIqYRByfpLFvMN8u3yJm8EgyWoktWQ15pTfrgP40vyh4mpOPa0OMCDN0XyU\nBB0tV0yi5sGjz336NfMm1cXF76MbJY3ziOYNzmJxywOeWeVcFCxAQhfDx8jh7tij\n1Gp1bIgBeQKBgG+4xCuJCRxQ/22JcD6o5ki95S2diqzW6nHgrfc3iBZ0v3qaPOJc\nefUI9DknM/pBjfafe+Sdblsg23C0qOIVzQfvneyE4Fgo4Lgx0j0fmaZuyCfijedB\nSsyv2WtQmJXtHCFULO1n7RjVWHmgbwvYY4PBthVuYlTMoEn4B9KLQEMpAoGBAK9K\nadn+/WRK559PZxOrvRi0DJ5hQTElXQGiyTzgurBswjs9oKryOeCCRuoQ5prq9leh\nmCEimWmfgCcu9wKl6Yh+DjgfYhJnz6aCGwWvPMKZttuytOr2c9Cx8RW+kvYelWbv\nvrVVM4AoPZu4R+ZvX8tojycXhjMro3XG0xUjp2rhAoGAQcWBwiF7iLTm7PGxfXLR\nRNOlVq+RZkZWYjLMq9ZxBJ+CfZ+bYDiARcm5jQBVPtd9VmjLFj2ipDi1rCXrYSfc\n9B+W7g0mbFbDg7aQGGocC329htfp9Ki3l/gsHUQeKxgVaakbWlX1u9J3YyzQMQ4i\nMSZYnasBpjUq2A7jZp/w0kY=\n-----END PRIVATE KEY-----\n";
        $data['client_email'] = "firebase-adminsdk-j62zt@wings-d491f.iam.gserviceaccount.com";
        $data['client_id'] = "112302729314224414430";
        $data['auth_uri'] = "https://accounts.google.com/o/oauth2/auth";
        $data['token_uri'] = "https://oauth2.googleapis.com/token";
        $data['auth_provider_x509_cert_url'] = "https://www.googleapis.com/oauth2/v1/certs";
        $data['client_x509_cert_url'] = "https://www.googleapis.com/robot/v1/metadata/x509/firebase-adminsdk-j62zt%40wings-d491f.iam.gserviceaccount.com";
        $json = json_encode($data);
        return $json;
    }

    public function setUpFirebase()
    {
        $json = $this->fireBaseConnectionJson();
        $serviceAccount = ServiceAccount::fromJson($json);
        $firebase = (new Factory)
            ->withServiceAccount($serviceAccount)
            ->withDatabaseUri('https://wings-d491f.firebaseio.com/')
            ->create();
        $database = $firebase->getDatabase();
        return $database;
    }


    public function fireBaseHandlerOrderNotification(Single_ride $order)
    {
        $client = $order->client;
        $token = "";
        switch ($order->order_status_id) {
            case Single_ride::driverAcceptOrder:
            case Single_ride::driverInHisWayToPickup:
            case Single_ride::driverArrivedToPickup:
            case Single_ride::driverInHisWayToDestination:
            case Single_ride::driverArrivedToDestination:
            case Single_ride::driverCancelOrder:
            case Single_ride::driverCompleteOrder:
            case Single_ride::driverNotFound:
                {
                    $token = $client->fire_base_token;
                    break;
                }
            case Single_ride::clientCancelOrder:
                {
                    if ($order->driver)
                        $token = $order->driver->user->fire_base_token;
                    break;
                }
        }

        $data['body'] = $order->orderStatusrel->name;
        $data['title'] = trans('language.order_status_update');
        $data['order_id'] = $order->id;
        $data['fire_base_token'] = $token;
        $data["notification_type_id"] = '1';
        $this->push_notification($token, $data);

    }



}
