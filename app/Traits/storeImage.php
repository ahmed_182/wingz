<?php
namespace App\Traits;

trait storeImage
{

    protected function storeImage($file)
    {
        $path = $file->store('public/images');
        $url = url('/');
        $url = str_replace('public', '', $url);
        $serverPath = $url . '/storage/app/';
        $path = $serverPath . $path;
        return $path;
    }

    public function base64Image($base64_image)
    {
        $image = $base64_image;  // your base64 encoded
//            $image = str_replace('data:image/png;base64,', '', $image);
        $pos = strrpos($base64_image, 'base64,', 0);
        $image = substr($image, $pos + 7);
        $image = str_replace(' ', '+', $image);
        $imageName = str_random(10) . '.' . 'png';
        $path = storage_path() . '/' . $imageName;
        \File::put($path, base64_decode($image));
        $image = url('/') . "/storage/" . $imageName;
        return $image;
    }


    protected function storeFiles($file)
    {
        $path = $file->store('public/files');
        $url = url('/');
        $url = str_replace('public', '', $url);
        $serverPath = $url . '/storage/app/';
        $path = $serverPath . $path;
        return $path;
    }
}
