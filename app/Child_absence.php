<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Child_absence extends Model
{
    protected $fillable = [
        'child_id',
        'user_id'
    ];
}
