<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Single_ride extends Model
    {

        const  clientAddOrder = "1";
        const  driverAcceptOrder = "2";
        const  driverInHisWayToPickup = "3";
        const  driverArrivedToPickup = "4";
        const  driverInHisWayToDestination = "5";
        const  driverArrivedToDestination = "6";
        const  driverCompleteOrder = "7";
        const  clientAddScheduledOrder = "8";
        const  clientCancelOrder = "9";
        const  driverNotFound = "10";
        const  driverRejectOrder = "11";
        const  driverCancelOrder = "12";
        const  adminCancelOrder = "13";

        protected $casts = [
            'created_at' => 'timestamp',
        ];


        protected $fillable = [
            'client_id',
            'child_id',
            'driver_id',
            'start_at',
            'end_at',
            'tracking',
            'order_status_id',
            'cancellation_reason_id',
            'rate_id',
            'receipt_id',
            'voucher_id',
            'destination_id',
            'pickup_id',
            //  'pickup_lat',
            //  'pickup_lng',
            //  'pickup_name',
            //  'pickup_address',
            //  'distension_lat',
            //  'distension_lng',
            //  'destination_name',
            //  'distension_address',


        ];


        public function toArray()
        {
            $data['id'] = $this->id;
            $data['tracking'] = $this->tracking;
            $data['payment'] = $this->payment;
            $data['orderStatus'] = $this->Order_status;
            $data['pickupAddress'] = $this->pickupAddress;
            $data['destinationAddress'] = $this->destinationAddress;
            $data['rate'] = $this->rate;
            $data['receipt'] = $this->receipt;
            $data['client'] = $this->client;
            $data['driver'] = $this->driver;
            $data['child'] = $this->child->profile();
            $data['created_at'] = $this->created_at;
            return $data;
        }

        protected function payment()
        {
            return $this->belongsTo(Payment_types::class, 'payment_id');
        }

        protected function Order_status()
        {
            return $this->belongsTo(SingleRideStatus::class, 'order_status_id');
        }

        protected function pickupAddress()
        {
            return $this->belongsTo(Favorite_places::class, 'pickup_id');
        }

        protected function destinationAddress()
        {
            return $this->belongsTo(Favorite_places::class, 'destination_id');
        }

        protected function rate()
        {
            return $this->belongsTo(SingleRideRate::class, 'rate_id');
        }

        protected function receipt()
        {
            return $this->belongsTo(Receipt::class, 'receipt_id');
        }

        protected function driver()
        {
            return $this->belongsTo(User::class, 'driver_id');
        }

        protected function client()
        {
            return $this->belongsTo(User::class, 'client_id');
        }

        protected function child()
        {
            return $this->belongsTo(Child::class, 'child_id');
        }

        protected function Cancellation_reasons()
        {
            return $this->belongsTo(Cancellation_reasons::class, 'cancellation_reason_id');
        }


        // Status
        //scopes
        public function scopeActive($query)
        {
            $active = [
                Single_ride::clientAddOrder,
                Single_ride::driverAcceptOrder,
                Single_ride::driverInHisWayToPickup,
                Single_ride::driverArrivedToPickup,
                Single_ride::driverInHisWayToDestination,
                Single_ride::driverArrivedToDestination,
//            Single_ride::clientAddScheduledOrder,
                Single_ride::driverCompleteOrder// todo for ratting check
            ];
            return $query->whereIn('order_status_id', $active);
        }

        public function isActive()
        {
            switch ($this->orderStatusrel->id) {
                case Single_ride::clientAddOrder:
                case Single_ride::driverAcceptOrder:
                case Single_ride::driverInHisWayToPickup:
                case Single_ride::driverArrivedToPickup:
                case Single_ride::driverInHisWayToDestination:
                case Single_ride::driverArrivedToDestination:
                case Single_ride::clientAddScheduledOrder:
                    return true;
            }

        }

        public function scopeInactive($query)
        {
            $inactive = [
                Single_ride::driverCompleteOrder,
                Single_ride::clientCancelOrder,
                Single_ride::driverRejectOrder,
                Single_ride::driverCancelOrder,
                Single_ride::adminCancelOrder
            ];

            return $query->whereIn('order_status_id', $inactive);
        }

        public function scopeDriverNotFound($query)
        {
            $driverNotFound = [
                Single_ride::driverNotFound
            ];

            return $query->whereIn('order_status_id', $driverNotFound);
        }

        public function scopeEnded($query)
        {
            $ended = [
                Single_ride::driverCompleteOrder,
                Single_ride::clientCancelOrder,
                Single_ride::driverCancelOrder,
                Single_ride::adminCancelOrder
            ];

            return $query->whereIn('order_status_id', $ended);
        }

        public function scopeScheduled($query)
        {
            $ended = [
                Single_ride::clientAddScheduledOrder
            ];
            return $query->whereIn('order_status_id', $ended);
        }

        public function scopeAssignable($query)
        {

            $ended = [
                Single_ride::clientAddOrder,
                Single_ride::clientAddScheduledOrder,
                Single_ride::driverNotFound,
                Single_ride::driverRejectOrder,
                Single_ride::driverCancelOrder,
            ];
            return $query->whereIn('order_status_id', $ended);
        }

        public function scopeCancelable($query)
        {
            $ended = [
                Single_ride::clientAddOrder,
                Single_ride::driverAcceptOrder,
                Single_ride::clientAddScheduledOrder,
                Single_ride::driverNotFound,
                Single_ride::driverRejectOrder,
                Single_ride::driverCancelOrder,
                Single_ride::driverInHisWayToPickup,
            ];
            return $query->whereIn('order_status_id', $ended);
        }

        protected function SingleRideStatus()
        {
            return $this->belongsTo(SingleRideStatus::class, 'order_status_id');
        }


        // Dash board

        public function getOrderCreatedAttribute()
        {
            $attribute = " غير محدد";
            if ($this->created_at)
                $attribute = date('D m Y , H:i A', $this->created_at);
            return $attribute;
        }


    }
