<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Commissary extends Model
{
    protected $fillable = [
        'user_id',
        'commissary_reason_id',
        'amount',
        'request_date',
        'confirm_date'
    ];

    protected $casts = [
        'confirm_date' => 'timestamp',
        'request_date' => 'timestamp',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
//          $data['user'] = $this->user;
        $data['commissary_reason'] = $this->serv_commissary_reason;
        $data['amount'] = $this->amount;
        $data['request_date'] = $this->request_date * 1000;
        $data['confirm_date'] = $this->confirm_date * 1000;
        return $data;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function commissary_reason()
    {
        return $this->belongsTo(Commissary_reasons::class, 'commissary_reason_id');
    }


    public function getServCommissaryReasonAttribute()
    {
        $att = null;
        if ($this->commissary_reason)
            $att = $this->commissary_reason->serv_name;
        return $att;
    }


    public function getServConfirmDateAttribute()
    {
        return $this->confirm_date;
    }


    // dashboard
    public function getDashCommissaryReasonAttribute()
    {
        $att = null;
        if ($this->commissary_reason)
            $att = $this->commissary_reason->dash_name;
        return $att;
    }

    public function getDashConfirmDateAttribute()
    {
        $att = null;
        if ($this->confirm_date)
            $att = date(" M Y d  ", $this->confirm_date );
        return $att;
    }

    public function getDashRequestDateAttribute()
    {
        $att = null;
        if ($this->request_date)
            $att = date(" M Y d ", $this->request_date);
        return $att;
    }


}
