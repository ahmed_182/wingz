<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class School_level extends Model
    {
        protected $fillable = [
            'level_id',
            'school_id'
        ];


        public function toArray()
        {
            $data['id'] = $this->level_id;
            $data['name'] = $this->serv_level_name;
            $data['stages'] = $this->stagesList();
            return $data;
        }

        public function school()
        {
            return $this->belongsTo(School::class, 'school_id');
        }

        public function level()
        {
            return $this->belongsTo(Level::class, 'level_id');
        }

        public function stagesList()
        {
            $items = School_time::where('school_level_id',$this->id)->get();
            return $items;
        }

        public function getServLevelNameAttribute()
        {
            $attribute = "";
            if ($this->level)
                $attribute = $this->level->dash_name;
            return $attribute;
        }

        // Dashboard
        public function getDashLevelNameAttribute()
        {
            $attribute = "";
            if ($this->level)
                $attribute = $this->level->dash_name;
            return $attribute;
        }

        public function getDashSchoolNameAttribute()
        {
            $attribute = "";
            if ($this->level)
                $attribute = $this->school->dash_name;
            return $attribute;
        }


    }
