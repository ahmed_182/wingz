<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Salary_package extends Model
    {
        protected $fillable = [
            'name_ar',
            'name_en',
            'description_ar',
            'description_en',
            'image',
            'value'
        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['name'] = $this->Serv_name;
            $data['description'] = $this->Serv_description;
            $data['image'] = $this->serv_image;
            $data['value'] = $this->value;
            return $data;
        }

        public function getServImageAttribute()
        {
            if ($this->image)
                return $attribute = $this->image;
            else
                return asset('assets/admin/images/logo.png');
        }


        public function getDashImageAttribute()
        {
            if ($this->image)
                return $attribute = $this->image;
            else
                return asset('assets/admin/images/logo.png');
        }

        public function getServNameAttribute()
        {
            if (\request()->lang == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getServDescriptionAttribute()
        {
            if (\request()->lang == "en")
                return $this->description_en;
            else
                return $this->description_ar;
        }

        public function getDashNameAttribute()
        {
            if (app()->getLocale() == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getDashDescription_arAttribute()
        {
            if (app()->getLocale() == "en")
                return $this->description_en;
            else
                return $this->description_ar;
        }


    }
