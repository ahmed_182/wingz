<?php

    namespace App;

    use Carbon\Carbon;
    use Illuminate\Database\Eloquent\Model;

    class Plan_details extends Model
    {
        protected $fillable = [
            'plan_id',
            'user_id',
            'child_id',
            'place_id',
            'payment_type_id'
        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['user'] = $this->user;
            $data['plan'] = $this->plan;
            $data['child'] = $this->child;
            $data['place'] = $this->place;
            $data['payment_type'] = $this->payment_type;
            return $data;
        }

        public function object()
        {
            $data['id'] = $this->id;
            $data['plan'] = $this->plan;
            $data["join_at"] = $this->time();
            return $data;
        }


        public function time()
        {
            $time = Carbon::parse($this->created_at)->format('d M Y');
            return $time;
        }

        public function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }

        public function plan()
        {
            return $this->belongsTo(Plan::class, 'plan_id');
        }

        public function child()
        {
            return $this->belongsTo(Child::class, 'child_id');
        }

        public function place()
        {
            return $this->belongsTo(Favorite_places::class, 'place_id');
        }

        public function payment_type()
        {
            return $this->belongsTo(Payment_types::class, 'payment_type_id');
        }


    }
