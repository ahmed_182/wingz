<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Driver extends Model
    {
        protected $fillable = [
            'user_id',
            'current_balance',
            'children_count',
            'car_photo',
            'cat_type_id',
            'cat_name_id',
            'manufacturing_year_id',
            'plate_number',
            'driving_license',
            'driver_type'
        ];

        public function car_type()
        {
            return $this->belongsTo(Car_type::class, 'cat_type_id');
        }

        public function car_name()
        {
            return $this->belongsTo(Car_name::class, 'cat_name_id');
        }
    }
