<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Stage extends Model
{
    protected $fillable = [
        'level_id',
        'name_ar',
        'name_en',
    ];

    public function toArray()
    {
        $data["id"] = $this->id;
        $data["name"] = $this->serv_name;
        return $data;
    }

    public function level()
    {
        return $this->belongsTo(Level::class, 'level_id');
    }

    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getDashNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getDashLevelNameAttribute()
    {
        if ($this->level)
            return $this->level->dash_name;
        else
            return trans('language.notSelected');
    }


}
