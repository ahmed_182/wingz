<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Driver_driving_car_licence extends Model
    {
        protected $fillable = ["driver_id", "data"];

        public function toArray()
        {
            $data["id"] = $this->id;
            $data["data"] = $this->data;
            return $data;
        }
    }
