<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Receipt extends Model
{
    protected $casts=[
        'starting' => 'string',
        'moving' => 'string',
        'duration' => 'string',
        'cost'  => 'string',
        'discount'  => 'discount',
        'balance'  => 'balance',
        'debit'  => 'debit',
    ];

    protected $fillable = [
        'id',
        'starting',
        'moving',
        'duration',
        'cost',
        'discount',
        'balance',
        'debit',
    ];

}
