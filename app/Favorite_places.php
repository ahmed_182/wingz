<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Favorite_places extends Model
{
    protected $fillable = [
        'user_id',
        'name',
        'lat',
        'lng',
        'address',
        'comments',
        'apartment_number',
        'building_number',
    ];

    public function toArray()
    {
//            $data["user"] = $this->user;
        $data["id"] = $this->id;
        $data["user_id"] = $this->user_id;
        $data["name"] = $this->name;
        $data["lat"] = $this->lat;
        $data["lng"] = $this->lng;
        $data["address"] = $this->address;
        $data["comments"] = $this->comments;
        $data["apartment_number"] = $this->apartment_number;
        $data["building_number"] = $this->building_number;
        return $data;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }
}
