<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class School extends Model
{
    protected $fillable = [
        'image',
        'name_ar',
        'name_en',
        'education_type_id',
        'address',
        'district_id',
        'lat',
        'lng'
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data["image"] = $this->serv_image;
        $data['name'] = $this->Serv_name;
        $data['education_type'] = $this->education_type;
        $data['levels'] = $this->times();
        $data['address'] = $this->address;
        $data['district'] = $this->district;
        $data['lat'] = $this->lat;
        $data['lng'] = $this->lng;
        return $data;
    }

    public function object()
    {
        $data['id'] = $this->id;
        $data["image"] = $this->serv_image;
        $data['name'] = $this->Serv_name;
        $data['address'] = $this->address;
        $data['district'] = $this->district;
        $data['lat'] = $this->lat;
        $data['lng'] = $this->lng;
        return $data;
    }


    public function getServImageAttribute()
    {
        if ($this->image)
            return $attribute = $this->image;
        else
            return asset('assets/admin/images/logo.png');
    }


    public function getDashImageAttribute()
    {
        if ($this->image)
            return $attribute = $this->image;
        else
            return asset('assets/admin/images/logo.png');
    }


    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }


    public function getDashNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }


    public function getDashEducationTypeNameAttribute()
    {
        $attribute = "";
        if ($this->education_type)
            $attribute = $this->education_type->dash_name;
        return $attribute;
    }


    public function getDashDistrictNameAttribute()
    {
        $attribute = "";
        if ($this->district)
            $attribute = $this->district->dash_name;
        return $attribute;
    }


    public function education_type()
    {
        return $this->belongsTo(Education_type::class, 'education_type_id');
    }

    public function district()
    {
        return $this->belongsTo(District::class, 'district_id');
    }

    public function times()
    {
//            return $this->hasMany(School_level::class, 'school_id');
        $arr = null;
        $levels = School_level::where('school_id', $this->id)->get();
        foreach ($levels as $level) {
            $stages = School_time::where('school_level_id', $level->id)->get();
            if (count($stages) > 0) {
                $arr[] = $level;
            }
        }
        return $arr;
    }
}
