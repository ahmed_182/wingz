<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Child_absence_days extends Model
{
    protected $fillable = [
        'child_absences_id',
        'day'
    ];


    public function toArray()
    {
        $data['id'] = $this->id;
        $data['day'] = $this->day;
        $data['created_at'] = date_format($this->created_at, 'd/m/Y H:i A');
        return $data;
    }
}
