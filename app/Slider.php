<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Slider extends Model
    {
        protected $fillable = [
            'image',
            'link',
            'type'
        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['image'] = $this->image;
            $data['link'] = $this->link;
            return $data;
        }

        public function getDashClickCountAttribute()
        {
            if ($this->SliderClicks)
                return $attribute = count($this->SliderClicks);
            else
                return 0;
        }

        public function SliderClicks()
        {
            return $this->hasMany(Slider_clicks::class, 'slider_id');
        }
    }
