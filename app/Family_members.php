<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Family_members extends Model
    {
        protected $fillable = [
            'user_id', 'member_id'
        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['user'] = $this->user;
            $data['member'] = $this->member;
            $data['created_at'] = $this->getTime();
            return $data;
        }

        public function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }

        public function member()
        {
            return $this->belongsTo(User::class, 'member_id');
        }

        public function getTime()
        {
            if ($this->created_at) {
                return $this->created_at->diffforhumans();
            }
            return null;
        }
    }
