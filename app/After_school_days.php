<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class After_school_days extends Model
{
    protected $fillable = [
        'after_school_id',
        'day',
        'time',
    ];

    protected function after_school()
    {
        return $this->belongsTo(Plan::class, 'after_school_id');
    }
}
