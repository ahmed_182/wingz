<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Trip_ride_life_cycle extends Model
{
    protected $fillable = [
        'trip_id',
        'child_id',
        'trip_ride_id',
        'trip_ride_status_id',
        'lat',
        'lng',
    ];


    public function toArray()
    {
        $data['id'] = $this->id;
        $data['trip'] = $this->trip;
        $data['ride'] = $this->ride;
        $data['ride_status'] = $this->ride_status;
        $data['lat'] = $this->lat;
        $data['lng'] = $this->lng;
        $data['created_at'] = $this->getTime();
        return $data;
    }

    public function obj()
    {
        $data['id'] = $this->id;
        $data['ride_status_id'] = $this->trip_ride_status_id;
        $data['ride_status_name'] = $this->ride_status->serv_name;
        $data['lat'] = $this->lat;
        $data['lng'] = $this->lng;
        $data['created_at'] = $this->getTime();
        return $data;
    }

    public function trip()
    {
        return $this->belongsTo(Trip::class, 'trip_id');
    }

    public function child()
    {
        return $this->belongsTo(Child::class, 'child_id');
    }

    public function ride()
    {
        return $this->belongsTo(Trip_ride::class, 'trip_ride_id');
    }

    public function ride_status()
    {
        return $this->belongsTo(Trip_ride_status::class, 'trip_ride_status_id');
    }


    public function getTime()
    {
        $time = Carbon::parse($this->created_at)->format('h:i A');
        return $time;
    }

    public function getChildImage()
    {
        if ($this->child) {
            return $this->child->image;
        } else {
            return asset('assets/admin/images/childImage.png');
        }
    }

}
