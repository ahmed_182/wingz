<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_plan extends Model
{
    protected $fillable = [
        'order_id',
        'name',
        'period',
        'month_count',
        'price_per_kilo',
        'working_days_count',
        'distance',
        'join_price',
        'tax_value',
        'price_with_discount',
        'total_price_without_discount',
        'discount_value',
        'total_price',
        'discount_per',
        'is_discount',
        'tax',
        'price_after_tax',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->name;
        $data['period'] = $this->period;
        $data['month_count'] = $this->month_count;
        $data['price_per_kilo'] = $this->price_per_kilo;
        $data['working_days_count'] = $this->working_days_count;
        $data['distance'] = $this->distance;
        $data['join_price'] = $this->join_price;
        $data['tax_value'] = $this->tax_value;
        $data['price_with_discount'] = $this->price_with_discount;
        $data['total_price_without_discount'] = $this->total_price_without_discount;
        $data['discount_value'] = $this->discount_value;
        $data['total_price'] = $this->total_price;
        $data['discount_per'] = $this->discount_per;
        $data['is_discount'] = $this->is_discount;
        $data['tax'] = $this->tax;
        $data['price_after_tax'] = $this->price_after_tax;
        $data['created_at'] = $this->created;
        return $data;
    }

    public function getCreatedAttribute()
    {
        $attribute = null;
        if ($this->created_at)
            $attribute = $this->created_at->format('Y-m-d');
        return $attribute;
    }
}
