<?php

    namespace App\Http\Middleware;

    use Carbon\Carbon;
    use Closure;

    class OnlineUpdateDate
    {
        /**
         * Handle an incoming request.
         *
         * @param \Illuminate\Http\Request $request
         * @param \Closure $next
         * @return mixed
         */
        public function handle($request, Closure $next)
        {
            $user = $request->user();
            $user->online_at = Carbon::now();
            $user->save();
            return $next($request);
        }
    }
