<?php

namespace App\Http\Resources\DriverAuth;

use Illuminate\Http\Resources\Json\JsonResource;

class DriverProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'bio' => $this->serv_bio,
            'mobile' => $this->mobile,
            'image' => $this->serv_image,
            'email' => $this->email,
            'fire_base_token' => $this->fire_base_token,
            'userVerify' => $this->userVerify,
            'lat' => $this->lat,
            'lng' => $this->lng,
            'rate' => $this->serv_rate,
            'points_count' => $this->serv_point_count,
            'wallet_value' => $this->serv_wallet_value,

            'car_type' => $this->driver->car_type,
            'car_name' => $this->driver->car_name,
            // Driver relations
            'country' => $this->country,
            'Licence_images' => $this->Licence_images,
            'criming_records' => $this->criming_records,
            'driver_identities' => $this->driver_identities,
            'car_Licence_images' => $this->car_Licence_images,
            'car_images' => $this->car_images,
        ];
    }
}
