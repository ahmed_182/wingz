<?php

    namespace App\Http\Resources\DriverAuth;

    use Illuminate\Http\Resources\Json\JsonResource;

    class DriverBasicInfoResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         * @return array
         */
        public function toArray($request)
        {
            return [
                'id' => $this->id,
                'name' => $this->name,
                'mobile' => $this->mobile,
                'image' => $this->serv_image,
                'email' => $this->email,
                'fire_base_token' => $this->fire_base_token,
                'userVerify' => $this->userVerify,
                'lat' => $this->lat,
                'lng' => $this->lng,
                'bio' => $this->serv_bio,
                'rate' => $this->serv_rate,
                'points_count' => $this->serv_point_count,
                'wallet_value' => $this->serv_wallet_value,
            ];
        }
    }
