<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class TripRideLifeCycleResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'child' => $this->getChildImage(),
            'status' => $this->ride_status,
            'time' => $this->getTime(),
        ];
    }
}
