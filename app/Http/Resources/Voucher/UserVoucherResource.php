<?php


    namespace App\Http\Resources\Voucher;


    use Illuminate\Http\Resources\Json\JsonResource;

    class UserVoucherResource extends JsonResource
    {
        /**
         * Transform the resource into an array.
         *
         * @param \Illuminate\Http\Request $request
         * @return array
         */
        public function toArray($request)
        {
            return [
                'id' => $this->id,
//                'user' => $this->user,
                'voucher' => $this->voucher,
                'plans' => $this->plans,
                'number_of_use' => $this->number_of_use,


            ];
        }
    }
