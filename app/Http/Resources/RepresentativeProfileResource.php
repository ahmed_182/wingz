<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class RepresentativeProfileResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param \Illuminate\Http\Request $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'id' => $this->id,
            'name' => $this->name,
            'mobile' => $this->mobile,
            'image' => $this->serv_image,
            'email' => $this->email,
            'fire_base_token' => $this->fire_base_token,
            'userVerify' => $this->userVerify,
            'country' => $this->country,
        ];
    }
}
