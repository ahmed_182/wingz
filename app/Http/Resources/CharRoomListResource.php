<?php

namespace App\Http\Resources;

use Illuminate\Http\Resources\Json\JsonResource;

class CharRoomListResource extends JsonResource
{
    /**
     * Transform the resource into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return [
            'room_id' => $this->serv_chat_room_id,
            'product_id' => $this->product_id,
            'product' => $this->product,
            'is_owner' => $this->servIsOwner,
            'profile' => $this->profile,
        ];
    }
}
