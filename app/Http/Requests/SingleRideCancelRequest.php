<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SingleRideCancelRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'single_ride_id' => ['required','exists:single_rides,id','string', 'max:255'],
            'cancellation_reason_id' => ['string', 'max:255'],
        ];
    }
}
