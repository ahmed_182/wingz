<?php

    namespace App\Http\Requests;

    use Illuminate\Foundation\Http\FormRequest;

    class SingleRideStoreRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'child_id' => ['required', 'string', 'max:255'],

                'pickup_name' => ['required', 'string', 'max:255'],
                'pickup_lat' => ['required', 'string', 'max:255'],
                'pickup_lng' => ['required', 'string', 'max:255'],
                'pickup_address' => ['string', 'max:255'],

                'destination_lat' => ['required', 'string', 'max:255'],
                'destination_lng' => ['required', 'string', 'max:255'],
                'destination_name' => ['required', 'string', 'max:255'],
                'destination_address' => ['required', 'string', 'max:255'],
                
                'scheduled' => ['required', 'string', 'max:255'],
                'start_at' => ['string', 'string', 'max:255'],
                'voucher_id' => ['max:255'],
            ];
        }
    }
