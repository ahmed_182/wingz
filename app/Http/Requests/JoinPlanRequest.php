<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class JoinPlanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'plan_id' => 'required|exists:plans,id',
            'child_id' => 'required|exists:children,id',
            'place_id' => 'required|exists:favorite_places,id',
            'payment_type_id' => 'required||exists:payment_types,id'
        ];
    }
}
