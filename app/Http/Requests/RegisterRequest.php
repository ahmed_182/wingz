<?php

    namespace App\Http\Requests;

    use Illuminate\Foundation\Http\FormRequest;

    class RegisterRequest extends FormRequest
    {
        /**
         * Determine if the user is authorized to make this request.
         *
         * @return bool
         */
        public function authorize()
        {
            return true;
        }

        /**
         * Get the validation rules that apply to the request.
         *
         * @return array
         */
        public function rules()
        {
            return [
                'name' => 'required',
                'mobile' => 'required|unique:users,mobile',
                'password' => 'required',
                'country_id' => 'required|exists:countries,id',
                'lat' => 'required',
                'lng' => 'required',
                'cities' => 'required',
                'address' => 'required',
                'image' => 'required',
                // Driver Table Info
                'cat_type_id' => 'required|exists:car_types,id',
                'cat_name_id' => 'required|exists:car_names,id',
                'manufacturing_year_id' => 'required|exists:manufacturing_years,id',
                'plate_number' => 'required',
                'driving_license' => 'required',
                'driver_type' => 'required|in:1,2',
                // Driver Driving Licence
                'Licence_images' => 'required',
                // Driver Driving Licence
                'criming_records' => 'required',
                // Driver Identity
                'driver_identities' => 'required',
                // Driver Car Licences
                'car_Licence_images' => 'required',
                // Driver Car Images
                'driver_car_images' => 'required',

                // share Driver Info
                'school_id' => 'required_if:driver_type,==,2',
                'children_count' => 'required',
            ];
        }
    }
