<?php

namespace App\Http\Controllers\Api\Weather;

use App\Traits\apiResponse;
use App\Weather_message;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Bioudi\LaravelMetaWeatherApi\Weather;

class IndexController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        // get now hour
        $now_hour = date('H:i');
        $get_message = Weather_message::where('start_at', '<=', $now_hour)->where('end_at', '>=', $now_hour)->first();
        if (!$get_message) {
            $get_last_message = Weather_message::orderBy('id', 'desc')->first();
            $data['message'] = $get_last_message->Serv_name;
        } else {
            $data['message'] = $get_message->Serv_name;
        }

        $weather = new Weather();
        $data['weather'] = $weather->getByCityName('cairo');

        return $this->apiResponse($request, trans('language.message'), $data, true);
    }
}
