<?php

    namespace App\Http\Controllers\Api\Education;

    use App\Education_type;
    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class indexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $types = Education_type::paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.education_types'), $types, true);
        }
    }
