<?php

    namespace App\Http\Controllers\Api\SliderDriver;

    use App\ModulesConst\SliderType;
    use App\Slider;
    use App\Slider_driver;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $items = Slider::where('type', SliderType::driver)->get();
            return $this->apiResponse($request, trans('language.message'), $items, true);
        }
    }
