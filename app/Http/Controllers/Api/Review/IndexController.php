<?php

    namespace App\Http\Controllers\Api\Review;

    use App\Http\Requests\AddReviewRequest;
    use App\Traits\apiResponse;
    use App\User_review;
    use Auth;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(AddReviewRequest $request)
        {
            $data = $request->all();
            $data['reviewer_id'] = Auth::user()->id;
            $review = User_review::create($data);
            return $this->apiResponse($request, trans('language.reviews'), $review, true);
        }
    }
