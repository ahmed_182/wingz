<?php

namespace App\Http\Controllers\Api\Validation;

use App\Child;
use App\Traits\apiResponse;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {

        $request->validate([
            'country_id' => 'required',
            'mobile' => 'required',
        ]);

        $mobile = $request->mobile;
        // check in users about this mobile :-
        $check_mobile_in_users = User::where('country_id', $request->country_id)->where('mobile', $mobile)->first();
        if ($check_mobile_in_users) {
            return $this->apiResponse($request, trans('language.Existmobile'), null, false, 400);
        }
        // check in children mobile :-
        $check_mobile_in_children = Child::where('mobile', $mobile)->first();
        if ($check_mobile_in_children) {
            return $this->apiResponse($request, trans('language.Existmobile'), null, false, 400);
        }

        return $this->apiResponse($request, trans('language.not_Existemobile'), null, true);

    }

}
