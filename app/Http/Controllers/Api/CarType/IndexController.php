<?php

    namespace App\Http\Controllers\Api\CarType;

    use App\Car_type;
    use App\Cay_type;
    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $car_types = Car_type::paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.car_types'), $car_types, true);
        }
    }
