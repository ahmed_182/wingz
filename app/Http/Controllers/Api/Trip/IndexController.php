<?php

namespace App\Http\Controllers\Api\Trip;

use App\About_app;
use App\Child;
use App\Http\Resources\TripRideLifeCycleResource;
use App\ModulesConst\destinationWay;
use App\ModulesConst\Paginate;
use App\Traits\apiResponse;
use App\Trip;
use App\ModulesConst\NotificationTyps;
use App\User;
use App\Trip_ride;
use App\Trip_children;
use App\Trip_ride_life_cycle;
use App\Trip_ride_status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use apiResponse;

    public function details(Request $request)
    {
        $request->validate([
            'trip_id' => 'required'
        ]);
        $item = Trip::find($request->trip_id);

        return $this->apiResponse($request, trans('language.message'), $item, true);
    }

    public function rides(Request $request)
    {
        $request->validate([
            'trip_id' => 'required'
        ]);
        $rides = Trip_ride::where('trip_id', $request->trip_id)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $rides, true);
    }


    public function ride_ststus(Request $request)
    {
        $request->validate([
            'trip_id' => 'required'
        ]);

        // get latest ride of this trip :-
        $ride = Trip_ride::where('trip_id', $request->trip_id)->latest()->first();
        if ($ride) {
            $statues = Trip_ride_life_cycle::where('trip_ride_id', $ride->id)->paginate(Paginate::value)->getCollection();
        } else {
            $statues = [];
        }
        return $this->apiResponse($request, trans('language.message'), $statues, true);
    }

    public function lifeCycle(Request $request)
    {
        $request->validate([
            'trip_id' => 'required'
        ]);
        // get latest ride of this trip :-
        $ride = Trip_ride::where('trip_id', $request->trip_id)->where('ride_work', Trip_ride::ride_work_start)->latest()->first();

        if ($ride) {
            $statues = Trip_ride_life_cycle::where('trip_ride_id', $ride->id)->latest()->paginate(Paginate::value)->getCollection();
            $statues = TripRideLifeCycleResource::collection($statues);
        } else {
            $statues = [];
        }
        return $this->apiResponse($request, trans('language.message'), $statues, true);
    }

    public function finishRide(Request $request)
    {
        // Start New Ride
        $request->validate([
            'trip_id' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);

        $trip = Trip::find($request->trip_id);
        // get latest ride of this trip :-
        $ride = Trip_ride::where('trip_id', $trip->id)->latest()->first();
        $ride->update(['ride_work' => Trip_ride::ride_work_finish]);
        // check if last trip direction to ( school ) finish in school , if to ( home ) finish Home .
        $ride_last_cycle = Trip_ride_life_cycle::where('trip_ride_id', $ride->id)->latest()->first();
        $status_obj = Trip_ride_status::find($ride_last_cycle->trip_ride_status_id);
        if ($status_obj->destination == destinationWay::school) {
            $ride_life_cycle_data['trip_ride_status_id'] = Trip_ride_status::driverArrivedToSchool; // 1 -> Finish Ride ....
        } else {
            $ride_life_cycle_data['trip_ride_status_id'] = Trip_ride_status::driverArrivedToHome; // 1 -> Finish Ride ....
        }
        $ride_life_cycle_data['trip_id'] = $ride->trip_id;
        $ride_life_cycle_data['trip_ride_id'] = $ride->id;
        $ride_life_cycle_data['lat'] = $request->lat;
        $ride_life_cycle_data['lng'] = $request->lng;
        Trip_ride_life_cycle::create($ride_life_cycle_data);
        // trip update status :-
        $trip->status = $ride_life_cycle_data['trip_ride_status_id'];
        $trip->save();

        /* asaid send notifications to all childs */
        $this->sendNotficationToInformParentThatTripWillEnd($request, $trip);

        return $this->apiResponse($request, trans('language.message'), null, true);
    }

    public function sendNotficationToInformParentThatTripWillEnd($request, $trip)
    {
        // get children in trip :-
        $children_ids = Trip_children::where('trip_id', $trip->id)->pluck('child_id');

        $x = 0;

        foreach ($children_ids as $child_id) {
            $x++;

            $child_info = Child::where('id', $child_id)->get();
            // Send notification to every Child Parent
            $body = trans('language.driver_end_your_child_trip') . " ( $trip->id ) " . trans('language.driver_end_your_child_trip_num');
            $users = User::where('id', $child_info[0]->user_id)->pluck("id")->unique();
            $this->notificationHandler(trans("language.appName"), $body, $users, NotificationTyps::inform_driver_end_trip, $child_info[0]->id);
        }
    }


    public function sendNotificationToAllParents($request, $trip)
    {
        // get children in trip :-
        $children_ids = Trip_children::where('trip_id', $trip->id)->pluck('child_id');

        $x = 0;

        foreach ($children_ids as $child_id) {
            $x++;

            $child_info = Child::where('id', $child_id)->get();
            // Send notification to every Child Parent
            $body = trans('language.driver_start_your_child_trip') . " ( $trip->id ) " . trans('language.driver_start_your_child_trip_num');
            $users = User::where('id', $child_info[0]->user_id)->pluck("id")->unique();
            $this->notificationHandler(trans("language.appName"), $body, $users, NotificationTyps::inform_driver_start_trip, $child_info[0]->id);
        }
    }
}
