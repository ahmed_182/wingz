<?php

    namespace App\Http\Controllers\Api\Stage;

    use App\ModulesConst\Paginate;
    use App\Stage;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $request->validate([
                'level_id' => 'required|exists:levels,id'
            ]);

            $items = Stage::where('level_id', $request->level_id)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $items, true);
        }
    }
