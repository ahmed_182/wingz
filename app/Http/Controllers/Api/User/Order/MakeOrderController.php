<?php

namespace App\Http\Controllers\Api\User\Order;

use App\About_app;
use App\After_school;
use App\After_school_days;
use App\Child;
use App\Favorite_places;
use App\KiloPerPrice;
use App\ModulesConst\TripStatus;
use App\Order;
use App\Order_car_type;
use App\Order_plan;
use App\Plan;
use App\School;
use App\School_time;
use App\Setting;
use App\Traits\apiResponse;
use App\Trip;
use App\Trip_children;
use App\Trip_order_receipt;
use App\Voucher;
use Carbon\Carbon;
use DateInterval;
use DatePeriod;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DateTime;


class MakeOrderController extends Controller
{
    use apiResponse;

    public function afterSchoolDaysHandeler($after_school_obj, $days_arr, $time)
    {

        for ($i = 0; $i < count($days_arr); $i++) {
            $data_info['time'] = $time[$i];
            $data_info['after_school_id'] = $after_school_obj->id;
            $data_info['day'] = $days_arr[$i];
            After_school_days::create($data_info);
        }

    }

    public function index(Request $request)
    {
        $user = $request->user();
        $user_children_ids = Child::where('user_id', $user->id)->pluck('id')->toArray();
        $request->validate([
            'order_car_type_id' => 'required',
            'favourite_place_id' => 'required|exists:favorite_places,id',
            'school_id' => 'required|exists:schools,id',
            'child_id' => "required",
            'plan_id' => "required",
//            'total_price' => "required",
            'promo_code_id' => "required",
            'school_time_id' => 'required',
            'after_school' => '',
            'start_date' => '',
            'end_date' => '',
            'monday' => '',
            'tuesday' => '',
            'wednesday' => '',
            'thursday' => '',
            'friday' => '',
            'saturday' => '',
            'sunday' => '',
            // calc plans price ..
            'day' => 'required',
            'stage_id' => 'required',
        ]);

        // check if this child in user children
        if (!in_array($request->child_id, $user_children_ids)) {
            return $this->apiResponse($request, trans('language.notBelongstoyouChild'), null, false, 400);
        }

        // After School
        if ($request->after_school and $request->start_date) {
            $start = new DateTime($request->start_date);
            $end = new DateTime($request->end_date);
            $interval = DateInterval::createFromDateString('1 day');
            $period = new DatePeriod($start, $interval, $end);
            $day_list = [];
            $time_list = [];
            foreach ($period as $dt) {
                if ($request->saturday) {
                    if ($dt->format('N') == 6) {
                        $day_list[] = $dt->format('y-m-d');
                        $time_list[] = $request->saturday;
                    }
                }
                if ($request->sunday) {
                    if ($dt->format('N') == 7) {
                        $day_list[] = $dt->format('y-m-d');
                        $time_list[] = $request->sunday;
                    }
                }
                if ($request->monday) {
                    if ($dt->format('N') == 1) {
                        $day_list[] = $dt->format('y-m-d');
                        $time_list[] = $request->monday;
                    }
                }
                if ($request->tuesday) {
                    if ($dt->format('N') == 2) {
                        $day_list[] = $dt->format('y-m-d');
                        $time_list[] = $request->tuesday;
                    }
                }
                if ($request->wednesday) {
                    if ($dt->format('N') == 3) {
                        $day_list[] = $dt->format('y-m-d');
                        $time_list[] = $request->wednesday;
                    }
                }
                if ($request->thursday) {
                    if ($dt->format('N') == 4) {
                        $day_list[] = $dt->format('y-m-d');
                        $time_list[] = $request->thursday;
                    }
                }
                if ($request->friday) {
                    if ($dt->format('N') == 5) {
                        $day_list[] = $dt->format('y-m-d');
                        $time_list[] = $request->friday;
                    }
                }
            }
            $after_school_data['child_id'] = $request->child_id;
            $after_school_data['start_date'] = $request->start_date;
            $after_school_data['end_date'] = $request->end_date;
            $after_school_obj = After_school::create($after_school_data);
            $this->afterSchoolDaysHandeler($after_school_obj, $day_list, $time_list);
        }

//            $kilo_price = $this->kiloPriceCalc();
        $school = School::find($request->school_id);
        $calc_distance = $this->calcDistance($request);
        $distance = ceil($calc_distance); // ceil ->  round to the largest number?
        // get price of kilo
        if ($request->after_school) {
            $pricePerKiloOrderCarType = Order_car_type::find($request->order_car_type_id)->price_per_kilo_after_school;
        } else {
            $pricePerKiloOrderCarType = Order_car_type::find($request->order_car_type_id)->price_per_kilo;
        }
        $data['user_id'] = $user->id;
        $data['distance'] = $distance * 2;
        $data['child_id'] = $request->child_id;
        $data['school_id'] = $school->id;
        $data['kilo_price'] = $pricePerKiloOrderCarType;
        $data['favourite_place_id'] = $request->favourite_place_id;
        $data['status'] = TripStatus::pending;
        $data['plan_id'] = $request->plan_id;
        $data['order_car_type_id'] = $request->order_car_type_id;
// clack Plan price ....
        $today = $request->day / 1000;
        $day = (int)date("d", $today);
        $month = (int)date("m", $today);
        $items = Plan::where('id', $request->plan_id)->get();
        $plans_data = [];
        $stage = School_time::find($request->stage_id);
        $tax_value = $this->getTaxValue();
        foreach ($items as $plan) {
            // get month count from plan
            $month_count = $plan->month_count;
            // make function to get count working days in plan month
            $all_days = $this->getWorkingDaysCount($day, $month, $month_count, $stage);
            $plan_data['id'] = $plan->id;
            // make function to get count working days in plan month
            $all_days = $this->getWorkingDaysCount($day, $month, $month_count, $stage);
            $plan_data['id'] = $plan->id;
            $plan_data['name'] = $plan->serv_name;
            $plan_data['period'] = (int)$plan->period;
            $plan_data['month_count'] = (int)$plan->month_count;
            $plan_data['price_per_kilo'] = $pricePerKiloOrderCarType;
            $plan_data['working_days_count'] = $all_days;
            $plan_data['distance'] = $distance * 2;
            $total_price = ceil($distance * 2 * $all_days * $pricePerKiloOrderCarType); // calc total Price , ceil ->  round to the largest number?
            $plan_data['join_price'] = $total_price;
            $plan_data['tax_value'] = substr($total_price * ($tax_value / 100), 0, 6);
            $plan_data['price_with_discount'] = $total_price - ($total_price * ($plan->discount / 100));
            $plan_data['discount_value'] = $total_price * ($plan->discount / 100);
            $plan_data['total_price'] = $total_price + $plan_data['tax_value'] - $plan_data['discount_value'];
            $plan_data['total_price_without_discount'] = $total_price + $plan_data['tax_value'];
            $plan_data['discount_per'] = (int)$plan->discount;
            $plan_data['is_discount'] = (bool)$plan->discount;
            $plan_data['tax'] = $tax_value;
            $plan_data['price_after_tax'] = ($total_price + ($total_price * ($tax_value / 100))) - $plan_data['discount_value'];
            $data['price_after_tax'] = ($total_price + ($total_price * ($tax_value / 100))) - $plan_data['discount_value'];
            $plans_data[] = $plan_data;
            $price_with_discount = $plan_data['price_with_discount'];
        }
// End clack Plan price .... ( $price_with_discount ) is the final price of plan with it's discount

        if ($request->promo_code_id == 0) {
            $data['total_price'] = $price_with_discount;
        } else {
            $promo = Voucher::find($request->promo_code_id);
            $data['total_price'] = $price_with_discount * ($promo->discount / 100);
        }
        $data['promo_code_id'] = $request->promo_code_id;
        $data['working_days_count'] = $plan_data['working_days_count'];
        $order = Order::create($data);
// Store Order Plan Details
        $order_plan_data = $plan_data;
        $order_plan_data['order_id'] = $order->id;
        $order_plan = Order_plan::create($order_plan_data);
// create Order Recipt :-
        $order_recipt = $plan_data;
        $order_recipt['kilo_price'] = $pricePerKiloOrderCarType;
        $order_recipt['status'] = Trip_order_receipt::pending;
        $order_recipt['user_id'] = $user->id;
        $order_recipt['due_date'] = $this->due_date_value($order);
        $trip_recipt = Trip_order_receipt::create($order_recipt);
// update order ( trip_order_receipt_id ) ..
        $order->update(['trip_order_receipt_id' => $trip_recipt->id]);
        return $this->apiResponse($request, trans('language.add_order_user'), $order, true);
    }

    public
    function due_date_value($order)
    {
        // get days count from setting :-
        $days = Setting::first();
        if (!$days) {
            $days_count = 0;
        } else {
            $days_count = $days->due_date;
        }
        $new_date = $order->created_at->addDays($days_count);
        return $new_date;
    }

    public
    function kiloPriceCalc()
    {
        $bout = KiloPerPrice::first();
        if ($bout) {
            $kilo_price = $bout->price;
        } else {
            $kilo_price = 0;
        }
        return $kilo_price;
    }


    public
    function point2point_distance($lat1, $lon1, $lat2, $lon2, $unit = 'K')
    {
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }
    }


//        public function afterSchoolDaysHandeler($after_school_obj, $start_long, $end_long, $days_arr)
//        {
//            foreach ($days_arr as $day) {
//                $this_day_long = strtotime($day);
//                $week = 0; // only for the first start of this day .
//                for ($i = $start_long; $i <= $end_long;) {
//                    $info['after_school_id'] = $after_school_obj->id;
//                    $info['day'] = $this_day_long + $week;
//                    if ($end_long >= $info['day']) {
//                        After_school_days::create($info);
//                    }
//                    // counter
//                    $week = 7 * 24 * 60 * 60;
//                    $i = $i + $week;
//                    $this_day_long = $this_day_long + $week;
//                }
//            }
//        }

    public
    function calcDistance($request)
    {
        $favourite_place = Favorite_places::find($request->favourite_place_id);
        $school = School::find($request->school_id);
        $lat1 = $favourite_place->lat;
        $lon1 = $favourite_place->lng;
        $lat2 = $school->lat;
        $lon2 = $school->lng;
        $unit = 'K';
        // run calc function ..
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }

    }

    public
    function getWorkingDaysCount($day, $month, $month_count, $stage)
    {
        $currentMonth = $this->currentMonth($day, $month, $month_count, $stage);
        $nextMonths = $this->nextMonths($day, $month, $month_count, $stage);
        $all_days = $currentMonth + $nextMonths;
        return $all_days;
    }

    public
    function currentMonth($day, $month, $month_count, $stage)
    {


        $days_count = 30 - $day;
        return $days_count;
    }

    public
    function nextMonths($day, $month, $month_count)
    {
        $nowmonth = $month_count - 1;

        $days_count = $nowmonth * 30;
        return $days_count;

//            for ($j = 1; $j <= $month_count; $j++) {
//                if ($month + $j > 12) {
//                    $arr[] = $month + $j - 12;
//                } else {
//                    $arr[] = $month + $j;
//                }
//            }
//
//            // Get Days Count ..
//            for ($i = $month + 1; $i <= $nowmonth; $i++) {
//                if ($i == $nowmonth) {
//                    // all days in month
//                    $days_count = Day::where('month_id', $i)->where('is_vacation', 0)->where('day_number', '<=', $day)->count();
//                } else {
//                    // days which low than now day
//                    $days_count = Day::where('month_id', $i)->where('is_vacation', 0)->count();
//                }
//
//                $arr_days = $arr_days + $days_count;
//            }
//
//            return $arr_days;
    }

}
