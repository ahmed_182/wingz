<?php

namespace App\Http\Controllers\Api\User\Order\Receipt;

use App\ModulesConst\Paginate;
use App\Traits\apiResponse;
use App\Trip_order_receipt;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $user = $request->user();
        $receipts = Trip_order_receipt::where('user_id', $user->id)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $receipts, true);
    }

    public function pending(Request $request)
    {
        $user = $request->user();
        $receipts = Trip_order_receipt::where('user_id', $user->id)->where('status', Trip_order_receipt::pending)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $receipts, true);
    }

    public function payment_request(Request $request)
    {
        $user = $request->user();
        $receipts = Trip_order_receipt::where('user_id', $user->id)->where('status', Trip_order_receipt::payment_request)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $receipts, true);
    }

    public function inWay(Request $request)
    {
        $user = $request->user();
        $receipts = Trip_order_receipt::where('user_id', $user->id)->where('status', Trip_order_receipt::in_way_to_client)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $receipts, true);
    }

    public function paid_done(Request $request)
    {
        $user = $request->user();
        $receipts = Trip_order_receipt::where('user_id', $user->id)->where('status', Trip_order_receipt::paid_done)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $receipts, true);
    }

    public function paid_failed(Request $request)
    {
        $user = $request->user();
        $receipts = Trip_order_receipt::where('user_id', $user->id)->where('status', Trip_order_receipt::paid_failed)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $receipts, true);
    }
}
