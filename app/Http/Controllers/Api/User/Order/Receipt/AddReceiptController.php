<?php

namespace App\Http\Controllers\Api\User\Order\Receipt;

use App\Child;
use App\Commissary;
use App\ModulesConst\NotificationTyps;
use App\Traits\apiResponse;
use App\Trip_order_receipt;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddReceiptController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $request->validate([
            'trip_order_receipt_id' => 'required',
            'request_date' => 'required',
            'lat' => 'required',
            'lng' => 'required',
            'address' => '',
        ]);
        $receipt = Trip_order_receipt::find($request->trip_order_receipt_id);
        $receipt->update(['status' => Trip_order_receipt::payment_request]);
        // create new Commissary linked with this receipt cost :-
        $data['user_id'] = $request->user()->id;
        $data['commissary_reason_id'] = 1;
        $data['amount'] = $receipt->price_after_tax;
        $data['request_date'] = $receipt->request_date;
        $item = Commissary::create($data);
        // send notification to Admin that user add new ( payment request ) .
        $this->thisNotificationHandler($receipt);
        return $this->apiResponse($request, trans('language.message'), $item, true);
    }

    public function thisNotificationHandler($request)
    {
        // Send Notification to Admin :-
        $body = trans('language.client_add_receipt_request') . " ( $request->trip_order_receipt_id ) ";
        $this->adminNotificationHandler(trans("language.appName"), $body);

    }
}
