<?php

namespace App\Http\Controllers\Api\User\Order;

use App\ModulesConst\Paginate;
use App\Order;
use App\Traits\apiResponse;
use App\Trip;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetOrderController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $user = $request->user();
        $items = Order::where('user_id', $user->id)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $items, true);
    }

    public function details(Request $request)
    {
        $request->validate([
            'order_id' => 'required'
        ]);
        $item = Order::find($request->order_id);
        return $this->apiResponse($request, trans('language.message'), $item, true);
    }
}
