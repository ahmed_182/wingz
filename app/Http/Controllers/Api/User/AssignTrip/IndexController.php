<?php

namespace App\Http\Controllers\Api\User\AssignTrip;

use App\ModulesConst\AssignedStatus;
use App\ModulesConst\NotificationTyps;
use App\ModulesConst\Paginate;
use App\Traits\apiResponse;
use App\Trip;
use App\Trip_children;
use App\User;
use App\User_assigin_trips;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use apiResponse;

    public function all_trips(Request $request)
    {
        $trips = User_assigin_trips::where('user_id', $request->user()->id)->where('status_id', AssignedStatus::pending)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $trips, true);
    }

    public function index(Request $request)
    {
        $request->validate([
            'child_id' => 'required'
        ]);
        $trips = User_assigin_trips::where('child_id', $request->child_id)->where('status_id', AssignedStatus::pending)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $trips, true);
    }

    public function accept(Request $request)
    {
        $request->validate([
            'assign_trip_id' => 'required'
        ]);
        $trip = User_assigin_trips::find($request->assign_trip_id);
        $trip->update(['status_id' => AssignedStatus::accepted]);

        // update other trips -> Expired
        $other_trips = User_assigin_trips::where('id', '!=', $trip->id)->where('child_id', $trip->child_id)->where('user_id', $trip->user_id)
            ->update(['status_id' => AssignedStatus::expired]);

        // confirm adding Child in Trip ..
        $child_trips = Trip_children::where('child_id', $trip->child_id)->where('trip_id', $trip->id)
            ->update(['status_id' => Trip_children::approved]);

        // Send Notification to Driver To inform him with new Child Data :-
        $driver = $trip->trip->driver;
        $this->thisNotificationHandler($request, $driver->id, $trip);
        return $this->apiResponse($request, trans('language.trip_accepted'), $trip, true);
    }

    public function thisNotificationHandler($request, $user_id, $trip)
    {
        // Send Notification to :-
        $body = trans('language.assigin_driver_inform') . " ( $trip->trip_id ) ";
        $users = User::where('id', $user_id)->pluck("id")->unique();
        $this->notificationHandler(trans("language.appName"), $body, $users, NotificationTyps::inform_assigin_trips_accept, $user_id);
    }

    public function reject(Request $request)
    {
        $request->validate([
            'assign_trip_id' => 'required'
        ]);
        $trip = User_assigin_trips::find($request->assign_trip_id);
        $trip->update(['status_id' => AssignedStatus::rejected]);
        return $this->apiResponse($request, trans('language.trip_rejected'), $trip, true);
    }

    public function meeting(Request $request)
    {
        $request->validate([
            'assign_trip_id' => 'required'
        ]);
        $trip = User_assigin_trips::find($request->assign_trip_id);
        $trip->update(['status_id' => AssignedStatus::meeting]);
        return $this->apiResponse($request, trans('language.trip_meeting'), $trip, true);
    }
}
