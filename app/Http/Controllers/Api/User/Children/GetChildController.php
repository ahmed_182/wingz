<?php

    namespace App\Http\Controllers\Api\User\Children;

    use App\Child;
    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class GetChildController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {

            $user = $request->user();
            $children = Child::where('is_deleted', 0)->where('user_id', $user->id)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $children, true);
        }
    }
