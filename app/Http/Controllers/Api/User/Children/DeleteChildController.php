<?php

    namespace App\Http\Controllers\Api\User\Children;

    use App\Child;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class DeleteChildController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $request->validate([
                'child_id' => 'required'
            ]);
            $child = Child::findorfail($request->child_id);
            //check if this child belongs to me
            if ($request->user()->id != $child->user_id) {
                // Error Not belongs To me
                return $this->apiResponse($request, trans('language.notBelongstoyouChild'), null, false, 400);
            }

            $child->is_deleted = 1;
            $child->save();
            return $this->apiResponse($request, trans('language.delete_child'), null, true);
        }
    }
