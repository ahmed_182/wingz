<?php

namespace App\Http\Controllers\Api\User\Children;

use App\After_school;
use App\Child;
use App\Traits\apiResponse;
use App\Traits\storeImage;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AddChildController extends Controller
{
    use apiResponse, storeImage;

    public function index(Request $request)
    {
        $request->validate([
            'image' => '',
            'name' => 'required',
            'birthday' => 'required',
            'school_time_id' => 'required',
            'mobile' => '',
        ]);

        if ($request->mobile) { // if this child has mobile number check from it , if not its okay skip validations ..
            $child_mobile = Child::where('mobile', $request->mobile)->first();
            if ($child_mobile) {
                return $this->apiResponse($request, trans('language.Existmobile'), null, false, 400);
            } // check from mobile ( Exist ) .

            $child_mobile_match_with_parent_mobile = User::where('mobile', $request->mobile)->first();
            if ($child_mobile_match_with_parent_mobile) {
                return $this->apiResponse($request, trans('language.Existmobilewithparent'), null, false, 400);
            }
        }

        if ($request->school_time_id == 0 or $request->school_time_id == null) {
            return $this->apiResponse($request, trans('language.invalid_stage_id'), null, false, 400);
        }
        $data["user_id"] = $request->user()->id;
        $data["name"] = $request->name;
        if ($request->image) {
            $data["image"] = $this->storeImage($request->image);
        }
        $data['birthday'] = $request->birthday;
        $data['mobile'] = $request->mobile;
        $data['school_time_id'] = $request->school_time_id;
        $child = Child::create($data);
        return $this->apiResponse($request, trans('language.user_add_child'), $child, true);
    }

}
