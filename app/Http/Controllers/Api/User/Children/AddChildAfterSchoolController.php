<?php

    namespace App\Http\Controllers\Api\User\Children;

    use App\After_school;
    use App\After_school_days;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class AddChildAfterSchoolController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $request->validate([
                'child_id' => 'required',
                'start_date' => 'required',
                'end_date' => 'required',
                'days' => 'required',
                'time' => 'required',
            ]);

            // After School
            $goodUrl = str_replace('[', '', $request->days);
            $goodUrl = str_replace(']', '', $goodUrl);
            $days_arr = explode(',', $goodUrl);

            // convert start at to long
            $start_long = strtotime($request->start_date);
            $end_long = strtotime($request->end_date);
            $after_school_data['child_id'] = $request->child_id;
            $after_school_data['start_date'] = $request->start_date;
            $after_school_data['end_date'] = $request->end_date;
            $after_school_data['time'] = $request->time;
            $after_school_obj = After_school::create($after_school_data);
            $this->afterSchoolDaysHandeler($after_school_obj, $start_long, $end_long, $days_arr);
            return $this->apiResponse($request, trans('language.storeAfterSchoolDays'), null, true);
        }

        public function afterSchoolDaysHandeler($after_school_obj, $start_long, $end_long, $days_arr)
        {
            foreach ($days_arr as $day) {
                $this_day_long = strtotime($day);
                $week = 0; // only for the first start of this day .
                for ($i = $start_long; $i <= $end_long;) {
                    $info['after_school_id'] = $after_school_obj->id;
                    $info['day'] = $this_day_long + $week;
                    if ($end_long >= $info['day']) {
                        After_school_days::create($info);
                    }
                    // counter
                    $week = 7 * 24 * 60 * 60;
                    $i = $i + $week;
                    $this_day_long = $this_day_long + $week;
                }
            }
        }
    }
