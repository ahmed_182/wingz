<?php

    namespace App\Http\Controllers\Api\User\Children\Absence;

    use App\Child;
    use App\Child_absence;
    use App\Child_absence_days;
    use App\Traits\apiResponse;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $data = $request->validate([
                'child_id' => 'required',
                'day' => 'required',
            ]);

            $data['user_id'] = $request->user()->id;
            $child = Child::find($request->child_id);
            // check if this child has old row ,,,
            $child_absence = Child_absence::where('child_id', $request->child_id)->first();
            if (!$child_absence) {
                $child_absence = Child_absence::create($data);
            }
            $data['child_absences_id'] = $child_absence->id;
            Child_absence_days::create($data);
            // send notification to this child parent that his child ...........
            $user_ids = User::where('id', $child->user_id)->pluck('id');
            $body = trans('language.driver_add_child_absence');
            $this->notificationHandler(trans("language.appName"), $body, $user_ids);
            return $this->apiResponse($request, trans('language.message'), null, true);
        }

        public function parent_add(Request $request)
        {
            $data = $request->validate([
                'child_id' => 'required',
                'days' => 'required', // array
            ]);

            $data['user_id'] = $request->user()->id;
            $child = Child::find($request->child_id);
            // check if this child has old row ,,,
            $child_absence = Child_absence::where('child_id', $request->child_id)->first();
            if (!$child_absence) {
                $child_absence = Child_absence::create($data);
            }
            $data['child_absences_id'] = $child_absence->id;
            foreach ($request->days as $day) {
                $data['day'] = $day;
                Child_absence_days::create($data);
            }

            // send notification to driver  ...........
            if ($child->driver_profile()) {
                $user_ids = User::where('id', $child->driver_profile()->id)->pluck('id');
                $body = trans('language.parent_add_child_absence') . " ( " . $child->name . " ) ";
                $this->notificationHandler(trans("language.appName"), $body, $user_ids);
            }
            return $this->apiResponse($request, trans('language.message'), null, true);

        }
    }
