<?php

    namespace App\Http\Controllers\Api\User\Children;

    use App\Child;
    use App\Traits\apiResponse;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class ChildProfileController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $request->validate([
                'child_id' => 'required'
            ]);

            $child = Child::find($request->child_id);
            return $this->apiResponse($request, trans('language.message'), $child, true);
        }
    }
