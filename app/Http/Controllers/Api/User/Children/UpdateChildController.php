<?php

    namespace App\Http\Controllers\Api\User\Children;

    use App\Child;
    use App\Traits\apiResponse;
    use App\Traits\storeImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class UpdateChildController extends Controller
    {
        use apiResponse, storeImage;

        public function index(Request $request)
        {
            $request->validate([
                'child_id' => 'exists:children,id',
                'image' => '',
                'name' => '',
                'birthday' => '',
                'school_time_id' => 'exists:school_times,id',
            ]);
            $data = $request->all();
            if ($request->image) {
                $data["image"] = $this->storeImage($request->image);
            }
            $child = Child::findorfail($request->child_id);
            //check if this child belongs to me
            if ($request->user()->id != $child->user_id) {
                // Error Not belongs To me
                return $this->apiResponse($request, trans('language.notBelongstoyouChildUpdate'), null, false, 400);
            }
            $child->update($data);
            return $this->apiResponse($request, trans('language.update_child_profile'), null, true);

        }
    }
