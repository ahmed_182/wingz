<?php

namespace App\Http\Controllers\Api\User\Children;

use App\Child_absence;
use App\Child_absence_days;
use App\ModulesConst\Paginate;
use App\Traits\apiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AbsenceController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $request->validate([
            'child_id' => 'required'
        ]);
        $child_absence = Child_absence::where('child_id', $request->child_id)->first();
        if (!$child_absence) {
            return $this->apiResponse($request, trans('لا توجد قائمه غيابات للطفل '), null, false, 400);
        }
        $items = Child_absence_days::where('child_absences_id', $child_absence->id)->orderBy('id', 'desc')->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $items, true);
    }

    public function delete(Request $request)
    {
        $request->validate([
            'child_absence_day_id' => 'required'
        ]);
        $item = Child_absence_days::find($request->child_absence_day_id);
        if ($item) {
            $item->delete();
        }
        return $this->apiResponse($request, trans('language.message'), null, true);
    }
}
