<?php

    namespace App\Http\Controllers\Api\User\Commissary;

    use App\Commissary;
    use App\Commissary_reasons;
    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $items = Commissary::where('user_id', $request->user()->id)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $items, true);
        }

        public function store(Request $request)
        {
            $data = $request->validate([
                'amount' => 'required',
                'request_date' => 'required',
                'commissary_reason_id' => 'required|exists:commissary_reasons,id',
            ]);
            $data['user_id'] = $request->user()->id;
            $item = Commissary::create($data);
            return $this->apiResponse($request, trans('language.add_commissary'), $item, true);
        }

        public function reasons(Request $request)
        {
            $items = Commissary_reasons::paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $items, true);
        }
    }
