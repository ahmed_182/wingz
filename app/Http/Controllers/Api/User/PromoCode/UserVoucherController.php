<?php

    namespace App\Http\Controllers\Api\User\PromoCode;

    use App\Http\Resources\Voucher\UserVoucherResource;
    use App\Traits\apiResponse;
    use App\UserVoucher;
    use App\Voucher;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class UserVoucherController extends Controller
    {
        use apiResponse;

        public function requestAddPromoCode(Request $request)
        {
            $request->validate([
                'code' => 'required',
            ]);
            $voucher = Voucher::where('code', $request->code)->first();
            if (!$voucher)
                return $this->apiResponse($request, trans('language.voucher_not_exist'),
                    null,
                    false,
                    400);

            if ($voucher->uses >= $voucher->max_uses)
                return $this->apiResponse($request, trans('language.voucher_max_uses'),
                    null,
                    false,
                    400);

            $userVoucher = UserVoucher::where("user_id", $request->user()->id)->where("voucher_id", $voucher->id)->first();

            if (!$userVoucher)
                return $this->apiResponse($request, trans('language.voucher_user_voucher_not_exist'),
                    null,
                    false,
                    400);

            if ($userVoucher->number_of_use >= $voucher->max_uses_user)
                return $this->apiResponse($request, trans('language.voucher_max_uses_user'),
                    null,
                    false,
                    400);


            return $this->apiResponse($request, trans('language.message'),

                $userVoucher,
                true);


        }
    }
