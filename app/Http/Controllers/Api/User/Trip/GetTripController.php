<?php

    namespace App\Http\Controllers\Api\User\Trip;

    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use App\Trip;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class GetTripController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $user = $request->user();
            $trips = Trip::where('user_id', $user->id)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $trips, true);
        }
    }
