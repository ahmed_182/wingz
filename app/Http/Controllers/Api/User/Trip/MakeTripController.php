<?php

    namespace App\Http\Controllers\Api\User\Trip;

    use App\About_app;
    use App\Child;
    use App\ModulesConst\TripStatus;
    use App\School;
    use App\Traits\apiResponse;
    use App\Trip;
    use App\Trip_children;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class MakeTripController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $user = $request->user();
            $user_children_ids = Child::where('user_id', $user->id)->pluck('id')->toArray();
            $request->validate([
                'school_id' => 'required|exists:schools,id',
                'children_id' => "required|json|distinct"
            ]);


            $kilo_price = $this->kiloPriceCalc();
            $school = School::find($request->school_id);
            $distance = (int)$this->point2point_distance($user->lat, $user->lng, $school->lat, $school->lng, $unit = 'K');

            $data['user_id'] = $user->id;
            $data['distance'] = $distance;
            $data['kilo_price'] = $kilo_price;
            $data['status'] = TripStatus::pending;
            $trip = Trip::create($data);
            $this->addTripChildrenHandeler($request, $trip);
            return $this->apiResponse($request, trans('language.message'), $trip, true);
        }

        public function kiloPriceCalc()
        {
            $bout = About_app::first();
            if ($bout) {
                $kilo_price = $bout->kilo_price;
            } else {
                $kilo_price = 0;
            }
            return $kilo_price;
        }


        function point2point_distance($lat1, $lon1, $lat2, $lon2, $unit = 'K')
        {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper($unit);

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }

        public function addTripChildrenHandeler($request, $trip)
        {
            $chilrenArray = json_decode($request->children_id);
            $info["trip_id"] = $trip->id;
            if ($chilrenArray) {
                foreach ($chilrenArray as $child) {
                    $info["child_id"] = $child;
                    Trip_children::create($info);
                }
            }

        }

    }
