<?php

namespace App\Http\Controllers\Api\User\Trip;

use App\Child;
use App\ModulesConst\Paginate;
use App\Traits\apiResponse;
use App\Trip;
use App\Trip_children;
use App\Trip_ride;
use App\Trip_ride_life_cycle;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class childrenTripController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {

        // get this user children ids
        $children_ids = Child::where('user_id', $request->user()->id)->pluck('id');
        // get all trips which children in ..
        $trips_ids = Trip_children::whereIn('child_id', $children_ids)->pluck('trip_id')->unique();

        // get rides of this trips .. ( because we wany only to return ride ( no end or must start ) .
        $status_array = [3, 4];
        $rides_life_trips_id = Trip_ride_life_cycle::whereIn('trip_id', $trips_ids)
            ->whereNotIn('trip_ride_status_id', $status_array)
            ->pluck('trip_id')->unique();
        $trips = Trip::whereIn('id', $rides_life_trips_id)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $trips, true);
    }
}
