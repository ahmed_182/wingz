<?php

    namespace App\Http\Controllers\Api\User\FamilyMember;

    use App\Family_members;
    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class GetFamilyMemberController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $user = $request->user();
            $family_members_id = Family_members::where('user_id', $user->id)->pluck('member_id')->unique();
            $family_members_id[] = "$user->id";
            $users = User::whereIn('id', $family_members_id)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $users, true);
        }
    }
