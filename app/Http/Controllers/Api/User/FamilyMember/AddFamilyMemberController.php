<?php

    namespace App\Http\Controllers\Api\User\FamilyMember;

    use App\ModulesConst\UserTyps;
    use App\ModulesConst\UserVerify;
    use App\Traits\apiResponse;
    use App\Traits\storeImage;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Family_members;

    class AddFamilyMemberController extends Controller
    {
        use  apiResponse, storeImage;

        public function index(Request $request)
        {
            $data = $request->validate([
                'user_relation_id' => 'required',
                'user_relation_name' => '',
                'name' => 'required',
                'email' => '',
                'image' => '',
                'country_id' => 'required',
                'mobile' => 'required',
                'fire_base_token' => '',
            ]);
            $user = $request->user();
            if (!$user)
                abort(404);

//            // check if this email existed ..
//            $EmailCheck = User::where('email', $request->email)->get();
//            if ($EmailCheck->count() > 0) {
//                return $this->apiResponse($request, trans('language.Existemail'), null, false, 400);
//            }

            $EmailCheck = User::where('mobile', $request->mobile)->get();
            if ($EmailCheck->count() > 0) {
                return $this->apiResponse($request, trans('language.Existmobile'), null, false, 400);
            }

            if ($request->image) {
                $data['image'] = $this->storeImage($request->image);
            }

            if ($request->user_relation_name) {
                $data["user_relation_name"] = $request->user_relation_name;
            }

            $data['api_token'] = rand(99999999, 999999999) . time();
            $data["user_type_id"] = UserTyps::member;
            $data["fire_base_token"] = $request->fire_base_token;
            $data["country_id"] = $request->country_id;
            $data["mobile"] = $request->mobile;
            $data["userVerify"] = UserVerify::no;
            $new_member = User::create($data);

            $data['user_id'] = $request->user()->id;
            $data['member_id'] = $new_member->id;
            $family_member = Family_members::create($data);
            // todo send sms ( to invite member to the app
            return $this->apiResponse($request, trans('language.add_family_member'), null, true);
        }
    }
