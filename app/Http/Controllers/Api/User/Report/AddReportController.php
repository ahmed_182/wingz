<?php

    namespace App\Http\Controllers\Api\User\Report;

    use App\Driver_report;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class AddReportController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $request->validate([
                'driver_id' => 'required|exists:users,id',
                'rate' => 'required',
                'report' => 'required',
            ]);
            $data = $request->all();
            $data['user_id'] = $request->user()->id;
            $report = Driver_report::create($data);
            return $this->apiResponse($request, trans('language.add_report_msg'), $report, true);
        }
    }
