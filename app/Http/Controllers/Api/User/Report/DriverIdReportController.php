<?php

    namespace App\Http\Controllers\Api\User\Report;

    use App\Driver_report;
    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class DriverIdReportController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $request->validate([
                'driver_id' => 'required|exists:users,id',
            ]);
            $reports = Driver_report::where("driver_id", $request->driver_id)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $reports, true);

        }
    }
