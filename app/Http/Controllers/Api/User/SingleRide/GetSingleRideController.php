<?php

    namespace App\Http\Controllers\Api\User\SingleRide;

    use App\Http\Controllers\SingleRideFireBaseContainerController;
    use App\ModulesConst\Paginate;
    use App\Single_ride;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class GetSingleRideController extends SingleRideFireBaseContainerController
    {
        use apiResponse;

        public function index(Request $request)
        {
            $single_rides = Single_ride::where('client_id', $request->user()->id)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $single_rides, true);
        }

        public function childRides(Request $request)
        {
            $request->validate([
                'child_id' => 'required'
            ]);
            $single_rides = Single_ride::where('child_id', $request->child_id)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $single_rides, true);
        }
    }
