<?php

    namespace App\Http\Controllers\Api\User\SingleRide;

    use App\Http\Controllers\SingleRideFireBaseContainerController;
    use App\ModulesConst\UserTyps;
    use App\Single_ride;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class ExistSingleRideController extends SingleRideFireBaseContainerController
    {
        use apiResponse;

        public function index(Request $request)
        {
            $data = $this->haveProcessingOrder($request);
            if (!$data)
                return $this->apiResponse($request, trans('language.message'), null, true);

            $order = $this->haveProcessingOrder($request);

            return $this->apiResponse($request, trans('language.message'), $order, true);
        }

        public function haveProcessingOrder($request)
        {

            if ($request->user()->user_type_id == UserTyps::driver) {
                $order = Single_ride::Where('driver_id', $request->user()->id)->active()->orderBy('id', 'desc')->first();
                if ($order && $order->order_status_id == Single_ride::driverCompleteOrder)
                    $order = $order->rate->driver_rate == null ? $order : null;

            } else {
                $order = Single_ride::where('client_id', $request->user()->id)->active()->orderBy('id', 'desc')->first();
                if ($order && $order->order_status_id == Single_ride::driverCompleteOrder)
                    $order = $order->rate->client_rate == null ? $order : null;
            }

            return $order;

        }
    }
