<?php

    namespace App\Http\Controllers\Api\User\SingleRide;

    use App\Http\Controllers\SingleRideFireBaseContainerController;
    use App\Http\Requests\SingleRideCancelRequest;
    use App\Single_ride;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class CancelSingleRideController extends SingleRideFireBaseContainerController
    {
        use apiResponse;

        public function index(SingleRideCancelRequest $request)
        {
            $order = Single_ride::where('id', $request->single_ride_id)->where('client_id', $request->user()->id);
            if (!$order->first())
                return $this->apiResponse($request, trans('language.order_not_allow'), null, false);

            if (!$this->cancelable($request))
                return $this->apiResponse($request, trans('language.order_not_cancelable'), null, false);

            $this->cancelByClient($request);

            return $this->apiResponse($request, trans('language.user_cancel_singleride'), null, true);
        }

        public function cancelable($request)
        {
            $order = Single_ride::where('id', $request->single_ride_id);
            return !$order->first()->driver || $order->cancelable()->first();
        }

        public function cancelByClient($request)
        {
            $order = Single_ride::where('id', $request->single_ride_id)->first();

            if ($request->cancellation_reason_id != null)
                $order->cancellation_reason_id = $request->cancellation_reason_id;
            $order->order_status_id = Single_ride::clientCancelOrder;
            $order->update();
            if ($order->driver_id) {
                $this->fireBaseHandler($order);
                $this->fireBaseHandlerRemoveOrderDriverNode($order->driver->id);
            } else
                $this->fireBaseHandler($order);

            $this->fireBaseHandlerOrderNotification($order);

            return $order;
        }

    }
