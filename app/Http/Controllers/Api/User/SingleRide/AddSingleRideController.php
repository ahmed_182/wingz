<?php

    namespace App\Http\Controllers\Api\User\SingleRide;

    use App\Child;
    use App\Favorite_places;
    use App\Http\Controllers\SingleRideFireBaseContainerController;
    use App\Http\Requests\OrderStoreRequest;
    use App\Http\Requests\SingleRideStoreRequest;
    use App\Receipt;
    use App\Single_ride;
    use App\SingleRideRate;
    use App\Traits\apiResponse;
    use App\User;
    use Auth;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Kreait\Firebase\Exception\FirebaseException;
    use LaravelFCM\Facades\FCM;
    use LaravelFCM\Message\OptionsBuilder;
    use LaravelFCM\Message\PayloadDataBuilder;
    use LaravelFCM\Message\PayloadNotificationBuilder;

    class AddSingleRideController extends SingleRideFireBaseContainerController
    {
        use apiResponse;

        public function index(SingleRideStoreRequest $request)
        {
            $data = $this->requestHandler($request);

            $data = $this->createAddresses($data);
            $data = $this->createReceipt($data);
            $data = $this->createRate($data);
            if ($request->voucher_id)
                $data['voucher_id'] = $request->voucher_id;
            $order = Single_ride::create($data);
            $this->fireBaseHandler($order, null);
//            $this->sendOrderToDriversHandler($order);
            return $this->apiResponse($request, trans('language.user_add_single_ride'), $order, true);
        }

        public function requestHandler($request)
        {
            $data = $request->data;
            $data['client_id'] = Auth::user()->id;
            $data['child_id'] = $request->child_id;
            $data['payment_id'] = $request->payment_type_id;
            if ($request->scheduled == 1)
                $data['order_status_id'] = Single_ride::clientAddScheduledOrder;
            else
                $data['order_status_id'] = Single_ride::clientAddOrder;
            return $data;
        }

        public function createAddresses($request)
        {
            $child = Child::find($request['child_id']);
            $user = User::find($child->user_id);

            // Pickup Address
            $data['user_id'] = $user->id;
            $data['name'] = $request['pickup_name'];
            $data['address'] = $request['pickup_address'];
            $data['lat'] = $request['pickup_lat'];
            $data["lng"] = $request['pickup_lng'];
            $address = $this->createAddress($data);
            $request['pickup_id'] = $address->id;

            // Destination Address
            $data['user_id'] = $user->id;
            $data['name'] = $request['destination_name'];
            $data['address'] = $request['destination_address'];
            $data['lat'] = $request['destination_lat'];
            $data["lng"] = $request['destination_lng'];
            $address = $this->createAddress($data);
            $request['destination_id'] = $address->id;
            return $request;

        }

        public function createAddress($data)
        {
            return Favorite_places::create($data);
        }

        private function createReceipt($data)
        {
            $receipt = Receipt::create();
            $data['receipt_id'] = $receipt->id;
            return $data;
        }

        private function createRate($data)
        {
            $rate = SingleRideRate::create();
            $data['rate_id'] = $rate->id;
            return $data;
        }

//        public function fireBaseHandler($order)
//        {
//            $order_id = (string)$order->id;
//            $status_id = (string)$order->order_status_id;
//            //
//            $obj['status_id'] = $status_id;
//            $reference = "singleRides/$order_id/";
//            try {
//                $this->setUpFirebase()
//                    ->getReference($reference)
//                    ->set($obj);
//            } catch (FirebaseException $e) {
//
//            }
//
//            if ($order->driver_id)
//                $this->fireBaseHandlerDriverNode($order, $order->driver_id);
//
//        }



//        public function sendOrderToDriversHandler(Single_ride $order)
//        {
////            $url = "https://us-central1-lemoallover.cloudfunctions.net/sendOrderToDrivers?";
//            $url = "000";
//            $parameters = "
//        latFrom={$order->pickupAddress->lat}
//        &lngFrom={$order->pickupAddress->lng}
//        &latCenter={$order->pickupAddress->lat}
//        &lngCenter={$order->pickupAddress->lng}
//        &typeId={$order->car_category_id}
//        &radius=1000
//        &timeout=20000
//        &orderId=$order->id";
//            $url = $url . $parameters;
//            $client = new Client();
//            $response = $client->get($url);
//            //dd($order->pickupAddress->lat);
//            return $response;
//        }


    }
