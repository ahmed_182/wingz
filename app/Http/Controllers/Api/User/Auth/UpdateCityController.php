<?php

    namespace App\Http\Controllers\Api\User\Auth;

    use App\City;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class UpdateCityController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $request->validate([
                "city_id" => "required"
            ]);

            $user = $request->user();
            if (!$user)
                abort(404);
            $city= City::find($request->city_id);
            if(!$city){
                return $this->apiResponse($request,trans('language.no_city_exist') ,null,false , 400);
            }
            $user->update(["city_id" => $request->city_id , "country_id" => $city->country_id]);
            return $this->apiResponse($request,trans('language.message') ,$user,true);

        }
    }
