<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\City;
use App\Country;
use App\Http\Resources\UserAuth\UserRegisterResource;
use App\ModulesConst\UserOnlineStatus;
use App\ModulesConst\UserTyps;
use App\Traits\apiResponse;
use App\Traits\storeImage;
use App\User;
use App\UserVoucher;
use App\Voucher;
use Auth;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;

class RegistrationController extends Controller
{
    use apiResponse, storeImage;

    public function index(Request $request)
    {
        $request->validate([
            'name' => 'required',
            'user_name' => 'required|unique:users',
            'mobile' => 'required|unique:users',
            'password' => 'required',
            'image' => '',
            'country_id' => 'required',
        ]);
        $data = $request->all();
        // Check start of mobile must be without zero :-
        $mob = $request->mobile;
        $MobstartWith = substr($mob, 0, 1);
        if ($MobstartWith == 0) {
            $str = ltrim($mob, '0');
            $data["mobile"] = $str;
        }

        $country = Country::find($request->country_id);
        if (!$country) {
            return $this->apiResponse($request, trans('language.no_country_exist'), null, false, 400);
        }

        // check if this mobile existed ..
        $mobileCheck = User::where('mobile', $data["mobile"])->get();
        if ($mobileCheck->count() > 0) {
            return $this->apiResponse($request, trans('language.Existmobile'), null, false, 400);
        }

        // check if this user_name existed ..
        $user_nameCheck = User::where('user_name', $request->user_name)->get();
        if ($user_nameCheck->count() > 0) {
            return $this->apiResponse($request, trans('language.Existuser_name'), null, false, 400);
        }

        $data['api_token'] = rand(99999999, 999999999) . time();
        $data["password"] = Hash::make($request->password);
        $data["user_type_id"] = UserTyps::user;
        $data["fire_base_token"] = $request->fire_base_token;
        $data["online_at"] = Carbon::now();
        if ($request->image) {
            $data['image'] = $this->storeImage($request->image);
        }
        $user = User::create($data);
        Auth::login($user);
        $item = auth()->user();
        $token = Auth::user()->createToken('myMob');
        $accessToken = $token->accessToken;

        // promo code Handler
        $this->promoCodeHandeler($request, $user);
        return $this->sendResponse($request, trans('language.login'), $item, true, $accessToken);
    }

    public function promoCodeHandeler($request, $user)
    {
        // get the last promo code ( is_new_user ) .
        $promoCode = Voucher::where('is_new_user', 1)->latest()->first();
        if ($promoCode) {
            $info['user_id'] = $user->id;
            $info['voucher_id'] = $promoCode->id;
            $info['number_of_use'] = 1;
            UserVoucher::create();
        }
    }
}
