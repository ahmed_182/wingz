<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\ModulesConst\UserTyps;
use App\ModulesConst\UserVerify;
use App\Traits\apiResponse;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ActiveProfileController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $request->validate([
            'mobile' => 'required',
//            'country_id' => 'required',
        ]);

//        $user_mobile = User::where('mobile', $request['mobile'])->where('country_id', $request->country_id)->first();
        $user_mobile = User::where('mobile', $request['mobile'])->first();
        if (!$user_mobile) {
            return $this->apiResponse($request, trans('language.not_Existemobile'), null, false, 400);
        }

        $user_mobile->userVerify = UserVerify::yes;
        $user_mobile->save();
        return $this->apiResponse($request, trans('language.user_active_profile'), $user_mobile, true);

    }
}
