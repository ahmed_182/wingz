<?php

    namespace App\Http\Controllers\Api\User\Auth;

    use App\Child;
    use App\Http\Resources\UserAuth\UserRegisterResource;
    use App\Traits\apiResponse;
    use App\Traits\storeImage;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class UpdateProfileController extends Controller
    {
        use apiResponse, storeImage;

        public function index(Request $request)
        {
            $data = $request->validate([
                'user_relation_id' => '',
                'user_relation_name' => '',
                'name' => '',
                'family_name' => '',
                'email' => '',
                'image' => '',
                'mobile' => '',
            ]);
            $user = $request->user();
            if (!$user)
                abort(404);

            // check if this email existed ..
            $EmailCheck = User::where('email', $request->email)->where('id', '!=', $user->id)->where('email', '!=', null)->get();
            if ($EmailCheck->count() > 0) {
                return $this->apiResponse($request, trans('language.Existemail'), null, false, 400);
            } else {
                // send activation link to this user new Email :-
            }

            $EmailCheck = User::where('mobile', $request->mobile)->where('id', '!=', $user->id)->where('mobile', '!=', null)->get();
            if ($EmailCheck->count() > 0) {
                return $this->apiResponse($request, trans('language.Existmobile'), null, false, 400);
            }

            if ($request->image) {
                $data['image'] = $this->storeImage($request->image);
            }

            if ($request->user_relation_name) {
                $data["user_relation_name"] = $request->user_relation_name;
            }
            $user->update($data);
            return $this->apiResponse($request, trans('language.message'), $user, true);
        }

        public function updateMobile(Request $request)
        {
            $data = $request->validate([
                'mobile' => 'required',
            ]);
            $user = $request->user();
            if (!$user)
                abort(404);


            $mobile = $request->mobile;
            // check in users about this mobile :-
            $check_mobile_in_users = User::where('mobile', $mobile)->first();
            if ($check_mobile_in_users) {
                return $this->apiResponse($request, trans('language.Existmobile'), null, false, 400);
            }
            // check in children mobile :-
            $check_mobile_in_children = Child::where('mobile', $mobile)->first();
            if ($check_mobile_in_children) {
                return $this->apiResponse($request, trans('language.Existmobile'), null, false, 400);
            }

            $user->update(['mobile' => $request->mobile]);
            return $this->apiResponse($request, trans('language.message'), $user, true);
        }

        public function updateChildMobile(Request $request)
        {
            $data = $request->validate([
                'mobile' => 'required',
                'child_id' => 'required',
                'country_id' => 'required',
            ]);
            $user = Child::find($request->child_id);
            if (!$user)
                abort(404);
            $mobile = $request->mobile;
            // check in users about this mobile :-
            $check_mobile_in_users = User::where('mobile', $mobile)->first();
            if ($check_mobile_in_users) {
                return $this->apiResponse($request, trans('language.Existmobile'), null, false, 400);
            }
            // check in children mobile :-
            $check_mobile_in_children = Child::where('mobile', $mobile)->first();
            if ($check_mobile_in_children) {
                return $this->apiResponse($request, trans('language.Existmobile'), null, false, 400);
            }

            $user->update(['mobile' => $request->mobile]);
            return $this->apiResponse($request, trans('language.message'), $user, true);
        }

    }
