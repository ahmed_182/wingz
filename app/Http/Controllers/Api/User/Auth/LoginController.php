<?php

namespace App\Http\Controllers\Api\User\Auth;

use App\Country;
use App\Http\Resources\User\UserResource;
use App\Http\Resources\UserAuth\UserLoginResource;
use App\ModulesConst\NotificationTyps;
use App\ModulesConst\UserOnlineStatus;
use App\ModulesConst\UserTyps;
use App\ModulesConst\UserVerify;
use App\Traits\apiResponse;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Hash;


class LoginController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $data = $this->validation($request);
        $country = Country::find($request->country_id);
        if (!$country) {
            return $this->apiResponse($request, trans('language.no_country_exist'), null, false, 400);
        }

        // Check start of mobile must be without zero :-
        $mob = $data['mobile'];
        $MobstartWith = substr($data['mobile'], 0, 1);
        if ($MobstartWith == 0) {
            $str = ltrim($mob, '0');
            $data["mobile"] = $str;
        }

        $user_mobile = User::where("mobile", $data["mobile"])->where("country_id", $request->country_id)->first();
        if (!$user_mobile) {
            //Create New Account with This Mobile Number ?
            $data['api_token'] = rand(99999999, 999999999) . time();
            $data["user_type_id"] = UserTyps::user;
            $data["fire_base_token"] = $request->fire_base_token;
            $data["mobile"] = $data["mobile"];
            $data["userVerify"] = UserVerify::no;
            $user = User::create($data);
            Auth::login($user);
            $item = auth()->user();
            $token = Auth::user()->createToken('myMob');
            $accessToken = $token->accessToken;
            // Send Welcome Notification :-
            $this->sendWelcomeNotification($request, $user);
            return $this->sendResponse($request, trans('language.auth_register_success'), $item, true, $accessToken);
        }

        $newData["fire_base_token"] = $request->fire_base_token;
        $this->updateData($user_mobile, $newData);
        Auth::login($user_mobile);
        return $this->response($request);
    }

    public function sendWelcomeNotification($request, $user)
    {
        // Send Notification to :-
        $body = trans('language.client_welcome_msg');
        $users = User::where('id', $user->id)->pluck("id")->unique();
        $this->notificationHandler(trans("language.appName"), $body, $users, NotificationTyps::normal, $user->id);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public
    function validation(Request $request)
    {
        $data = $request->validate([
            'country_id' => ['required'],
            'mobile' => ['required'],
        ]);
        return $data;
    }

    /**
     * @param $newData
     */
    public
    function updateData($user_mobile, $newData)
    {
        $user_mobile->update($newData);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public
    function response(Request $request)
    {
        $item = auth()->user();
        $token = Auth::user()->createToken('myMob');
        $accessToken = $token->accessToken;
        return $this->sendResponse($request, trans('language.auth_login_success'), $item, true, $accessToken);
    }


}
