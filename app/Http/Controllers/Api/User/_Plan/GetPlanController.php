<?php

    namespace App\Http\Controllers\Api\User\_Plan;

    use App\ModulesConst\Paginate;
    use App\Plan;
    use App\Plan_details;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class GetPlanController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $plans_list = Plan_details::where("user_id", $request->user()->id)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $plans_list, true);
        }
    }
