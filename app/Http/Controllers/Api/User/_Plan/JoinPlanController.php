<?php

    namespace App\Http\Controllers\Api\User\_Plan;

    use App\Child;
    use App\Http\Requests\JoinPlanRequest;
    use App\Plan_details;
    use App\Traits\apiResponse;
    use Auth;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class JoinPlanController extends Controller
    {
        use apiResponse;

        public function index(JoinPlanRequest $request)
        {
            //check if this is your child
            $child = Child::find($request->child_id);
            if ($child->user_id != Auth::user()->id) {
                return $this->apiResponse($request, trans('language.notBelongstoyouChild'), null, false, 400);
            }

            $data = $request->all();
            $data['user_id'] = $request->user()->id;
            $plan_details = Plan_details::create($data);
            return $this->apiResponse($request, trans('language.message'), $plan_details, true);
        }


    }
