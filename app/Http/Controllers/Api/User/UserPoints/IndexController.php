<?php

    namespace App\Http\Controllers\Api\User\UserPoints;

    use App\ModulesConst\Paginate;
    use App\Point_history;
    use App\Point_terms;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $items = Point_history::where('user_id', $request->user()->id)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $items, true);
        }

        public function terms(Request $request)
        {
            $term = Point_terms::first();
            return $this->apiResponse($request, trans('language.message'), $term, true);
        }
    }
