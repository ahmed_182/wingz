<?php

    namespace App\Http\Controllers\Api\User\UserWallet;

    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use App\User_wallet;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $items = User_wallet::where('user_id', $request->user()->id)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $items, true);
        }
    }
