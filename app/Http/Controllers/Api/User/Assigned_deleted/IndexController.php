<?php

    namespace App\Http\Controllers\Api\User\Assigned_deleted;

    use App\Assigned_driver;
    use App\ModulesConst\AssignedStatus;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $request->validate([
                'assigned_id' => 'required|exists:assigned_drivers,id'
            ]);

            $item = Assigned_driver::find($request->assigned_id);
            $item->status = AssignedStatus::rejected;
            $item->save();
            return $this->apiResponse($request, trans('language.message'), $item, true);
        }

        public function meet(Request $request)
        {
            $request->validate([
                'assigned_id' => 'required|exists:assigned_drivers,id'
            ]);

            $item = Assigned_driver::find($request->assigned_id);
            $item->status = AssignedStatus::meeting;
            $item->save();
            return $this->apiResponse($request, trans('language.message'), $item, true);
        }

        public function accept(Request $request)
        {
            $request->validate([
                'assigned_id' => 'required|exists:assigned_drivers,id'
            ]);

            $item = Assigned_driver::find($request->assigned_id);
            $item->status = AssignedStatus::accepted;
            $item->save();
            return $this->apiResponse($request, trans('language.message'), $item, true);
        }
    }
