<?php

    namespace App\Http\Controllers\Api\Tax;

    use App\ModulesConst\Paginate;
    use App\Tax;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $taxes = Tax::paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.taxes'), $taxes, true);
        }
    }
