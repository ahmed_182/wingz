<?php

    namespace App\Http\Controllers\Api\CancellationReason;

    use App\Cancellation_reasons;
    use App\ModulesConst\UserTyps;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class ReasonsController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $items = Cancellation_reasons::where('user_type_id', 1)->get();
            return $this->apiResponse($request, trans('language.user_cancellation_reasons'), $items, true);
        }

        public function driver(Request $request)
        {
            $items = Cancellation_reasons::where('user_type_id', 2)->get();
            return $this->apiResponse($request, trans('language.driver_cancellation_reasons'), $items, true);
        }
    }
