<?php

namespace App\Http\Controllers\Api\Distract;

use App\District;
use App\Traits\apiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use apiResponse;


    public function index(Request $request)
    {
        $request->validate([
            'city_id' => 'required',
        ]);

        $items = District::where('city_id', $request->city_id)->get();
        return $this->apiResponse($request, trans('language.districts'), $items, true);

    }
}
