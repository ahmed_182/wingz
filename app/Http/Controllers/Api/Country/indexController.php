<?php

    namespace App\Http\Controllers\Api\Country;

    use App\City;
    use App\Country;
    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class indexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $items = Country::orderBy("id", "desc")->get();
            return $this->apiResponse($request, trans('language.countries'), $items, true);
        }

        public function myCountryCities(Request $request)
        {

            $user = $request->user();
            if (!$user)
                abort(404);
            $country_id = $user->country_id;
            $cities = City::where("country_id", $country_id)->orderBy("id", "desc")->get();
            return $this->apiResponse($request, trans('language.my_country_cities'), $cities, true);
        }
    }
