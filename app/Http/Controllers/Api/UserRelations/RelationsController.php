<?php

    namespace App\Http\Controllers\Api\UserRelations;

    use App\Family_members;
    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use App\User;
    use App\User_relation;
    use Auth;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class RelationsController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {

            $relations = User_relation::paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $relations, true);

        }

//        public function index(Request $request)
//        {
//            if (Auth::guard("api")->user()) {
//                $user = Auth::guard("api")->user();
//                $member_ids = Family_members::where('user_id', $user->id)->pluck('member_id');
//                $user_relation_ids = User::whereIn('id', $member_ids)->pluck('user_relation_id');
//                $relations = User_relation::whereNotIn('id', $user_relation_ids)->paginate(Paginate::value)->getCollection();
//                return $this->apiResponse($request, trans('language.message'), $relations, true);
//            } else {
//                $relations = User_relation::paginate(Paginate::value)->getCollection();
//                return $this->apiResponse($request, trans('language.message'), $relations, true);
//            }
//
//        }


    }
