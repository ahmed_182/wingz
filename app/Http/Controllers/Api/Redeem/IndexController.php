<?php

    namespace App\Http\Controllers\Api\Redeem;

    use App\ModulesConst\Paginate;
    use App\Redeem;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $items = Redeem::paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.redeems'), $items, true);
        }
    }
