<?php

namespace App\Http\Controllers\Api\PricePerKilo;

use App\KiloPerPrice;
use App\Traits\apiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $pricePerKilo = KiloPerPrice::latest()->first()->price;
        if (!$pricePerKilo) {
            $pricePerKilo = 0;
        }

        $data['price_per_kilo'] = $pricePerKilo;
        return $this->apiResponse($request, trans('language.message'), $data, true);
    }
}
