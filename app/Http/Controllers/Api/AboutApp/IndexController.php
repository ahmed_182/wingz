<?php

    namespace App\Http\Controllers\Api\AboutApp;

    use App\About_app;
    use App\Terms;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $termeApp = About_app::first();
            return $this->apiResponse($request, trans('language.about_app'), $termeApp, true);

        }
    }
