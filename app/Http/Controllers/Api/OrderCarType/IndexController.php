<?php

    namespace App\Http\Controllers\Api\OrderCarType;


    use App\ModulesConst\Paginate;
    use App\Order_car_type;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $levels = Order_car_type::paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.car_types'), $levels, true);
        }
    }
