<?php

    namespace App\Http\Controllers\Api\ContactUs;

    use App\Contact;
    use App\Contact_us;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $data = $request->validate([
                'country_id' => 'required',
                'message' => 'required',
            ]);
            Contact::create($data);
            return $this->apiResponse($request, trans('language.contact_us_successful'), null, true);

        }
    }
