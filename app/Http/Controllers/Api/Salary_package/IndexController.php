<?php

    namespace App\Http\Controllers\Api\Salary_package;

    use App\ModulesConst\Paginate;
    use App\Salary_package;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $items = Salary_package::paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.salary_package'), $items, true);
        }
    }
