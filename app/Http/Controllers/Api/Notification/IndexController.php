<?php

    namespace App\Http\Controllers\Api\Notification;

    use App\ModulesConst\Paginate;
    use App\Notification;
    use App\Traits\apiResponse;
    use App\User_notification;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $userid = $request->user()->id;
            if (!$userid)
                abort(404);
            $data = User_notification::where('user_id', $userid)->where('viewed', 0)->orderBy('id', 'desc')->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $data, true);
        }

        public function destroy(Request $request)
        {
            $userid = $request->user()->id;
            if (!$userid)
                abort(404);
            $notification_ids = User_notification::where('user_id', $userid)->pluck("notification_id");
            Notification::destroy($notification_ids);
            User_notification::where('user_id', $userid)->delete();
            return $this->apiResponse($request, trans('language.message'), null, true);
        }


        public function seen(Request $request)
        {

            $userid = $request->user()->id;
            if (!$userid)
                abort(404);
            User_notification::where('user_id', $userid)->update(['viewed' => 1]);
            return $this->apiResponse($request, trans('language.message'), null, true);
        }

        public function count(Request $request)
        {
            $userid = $request->user()->id;
            if (!$userid)
                abort(404);
            $count = User_notification::where('user_id', $userid)->where('viewed', 0)->count();
            $data['count'] = $count;
            return $this->apiResponse($request, trans('language.message'), $data, true);
        }
    }
