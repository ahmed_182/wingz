<?php

namespace App\Http\Controllers\Api\FavouritePlace;

use App\Favorite_places;
use App\ModulesConst\Paginate;
use App\Traits\apiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $user = $request->user();
        $places = Favorite_places::where('user_id', $user->id)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.favourite_places'), $places, true);
    }

    public function store(Request $request)
    {
        $data = $request->validate([
            "name" => "required",
            "lat" => "required",
            "lng" => "required",
            "address" => "required",
            "comments" => "",
            "apartment_number" => "",
            "building_number" => "",
        ]);

        $user = $request->user();
        $data["user_id"] = $user->id;
        $place = Favorite_places::create($data);
        return $this->apiResponse($request, trans('language.favourite_places_add'), $place, true);

    }

    public function remove(Request $request)
    {

        $data = $request->validate([
            "favourite_place_id" => "required",
        ]);

        $place = Favorite_places::find($request->favourite_place_id);
        $place->delete();
        return $this->apiResponse($request, trans('language.done'), null, true);
    }
}
