<?php

    namespace App\Http\Controllers\Api\Slider;

    use App\Slider_clicks;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class SliderClickController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $request->validate([
                'slider_id' => 'required|exists:sliders,id'
            ]);

            $data['slider_id'] = $request->slider_id;
            $data['user_id'] = $request->user()->id;
            Slider_clicks::create($data);
            return $this->apiResponse($request, trans('language.message'), null, true);
        }
    }
