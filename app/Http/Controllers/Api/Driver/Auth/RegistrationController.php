<?php

    namespace App\Http\Controllers\Api\Driver\Auth;

    use App\City;
    use App\Country;
    use App\Driver;
    use App\Driver_car_images;
    use App\Driver_cities;
    use App\Driver_criming_recorde;
    use App\Driver_districts;
    use App\Driver_driving_car_licence;
    use App\Driver_driving_licence;
    use App\Driver_identity;
    use App\Driver_share_info;
    use App\Driving_license_block;
    use App\Http\Requests\RegisterRequest;
    use App\Http\Resources\DriverAuth\DriverProfileResource;
    use App\Http\Resources\UserAuth\UserRegisterResource;
    use App\ModulesConst\DriverTyps;
    use App\ModulesConst\UserOnlineStatus;
    use App\ModulesConst\UserTyps;
    use App\Traits\apiResponse;
    use App\Traits\storeImage;
    use App\User;
    use Auth;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Illuminate\Support\Facades\Hash;

    class RegistrationController extends Controller
    {
        use apiResponse, storeImage;

        public function index(RegisterRequest $request)
        {

            // Check if this Driving_license is blocked  or not ?!
            $checkIfBlocked = Driving_license_block::where('driving_license', $request->driving_license)->first();
            if ($checkIfBlocked) {
                return $this->apiResponse($request, trans('language.driving_license_blocked'), null, false, 400);
            }
            $data = $request->all();
            // Check start of mobile must be without zero :-
            $mob = $request->mobile;
            $MobstartWith = substr($mob, 0, 1);
            if ($MobstartWith == 0) {
                $str = ltrim($mob, '0');
                $data["mobile"] = $str;
            }


            // Register Function
            $data['api_token'] = rand(99999999, 999999999) . time();
            $data["password"] = Hash::make($request->password);
            $data["user_type_id"] = UserTyps::driver;
            $data["fire_base_token"] = $request->fire_base_token;
            if ($request->image) {
                $data['image'] = $this->storeImage($request->image);
            }
            $user = User::create($data);
            // Driver Table
            $this->DriverTableHandeler($request, $user);
            // Driver Cities
            $this->DriverCitiesHandeler($request, $user);
            // Driver Licence
            $this->DrivingLicenceHandeler($request, $user);
            // Driver Car Licence
            $this->DrivingCarLicenceHandeler($request, $user);
            // Driver Car Images
            $this->DrivingCarImagesHandeler($request, $user);
            // Driver Records
            $this->DrivingCrimingRecordsHandeler($request, $user);
            // Driver identity
            $this->Driver_identityHandeler($request, $user);
            // Driver Share Info
            if ($request->driver_type == DriverTyps::share) {
                $this->DriverShareInfoHandeler($request, $user);
            }


            return $this->response($request, $user);
        }

        public function response($request, $user)
        {
            Auth::login($user);
            $item = auth()->user();
            $token = Auth::user()->createToken('myMob');
            $accessToken = $token->accessToken;
            $item = new DriverProfileResource($item);
            return $this->sendResponse($request, trans('language.auth_login_success'), $item, true, $accessToken);
        }


        public function DriverTableHandeler($request, $user)
        {
            $driverInfo["user_id"] = $user->id;
            $driverInfo["cat_type_id"] = $request->cat_type_id;
            $driverInfo["manufacturing_year_id"] = $request->manufacturing_year_id;
            $driverInfo["plate_number"] = $request->plate_number;
            $driverInfo["driver_type"] = $request->driver_type;
            $driverInfo["cat_name_id"] = $request->cat_name_id;
            $driverInfo["children_count"] = $request->children_count;
            $driverInfo["driving_license"] = $request->driving_license;
            $driver = Driver::create($driverInfo);
        }

        public function DriverCitiesHandeler($request, $user)
        {
            $CityArray = json_decode($request->cities);
            $info["driver_id"] = $user->id;
            if ($CityArray) {
                foreach ($CityArray as $city) {
                    $info["district_id"] = $city;
                    Driver_districts::create($info);
                }
            }
        }

        public function DrivingLicenceHandeler($request, $user)
        {
            $info["driver_id"] = $user->id;
            if ($request->Licence_images) {
                foreach ($request->Licence_images as $Licence_images) {
                    $info["data"] = $this->storeFiles($Licence_images);
                    Driver_driving_licence::create($info);
                }
            }
        }

        public function DrivingCarLicenceHandeler($request, $user)
        {
            $info["driver_id"] = $user->id;
            if ($request->car_Licence_images) {
                foreach ($request->car_Licence_images as $car_Licence_images) {
                    $info["data"] = $this->storeFiles($car_Licence_images);
                    Driver_driving_car_licence::create($info);
                }
            }
        }

        public function DrivingCarImagesHandeler($request, $user)
        {
            $info["driver_id"] = $user->id;
            if ($request->driver_car_images) {
                foreach ($request->driver_car_images as $car_images) {
                    $info["image"] = $this->storeFiles($car_images);
                    Driver_car_images::create($info);
                }
            }
        }

        public function DrivingCrimingRecordsHandeler($request, $user)
        {
            $info["driver_id"] = $user->id;
            if ($request->criming_records) {
                foreach ($request->criming_records as $criming_record) {
                    $info["data"] = $this->storeFiles($criming_record);
                    Driver_criming_recorde::create($info);
                }
            }
        }

        public function Driver_identityHandeler($request, $user)
        {
            $info["driver_id"] = $user->id;
            if ($request->driver_identities) {
                foreach ($request->driver_identities as $driver_identities) {
                    $info["data"] = $this->storeFiles($driver_identities);
                    Driver_identity::create($info);
                }
            }
        }

        public function DriverShareInfoHandeler(Request $request, $user)
        {
            $info["driver_id"] = $user->id;
            $info["school_id"] = $request->school_id;
            $info["children_count"] = $request->children_count;
            Driver_share_info::create($info);
        }
    }
