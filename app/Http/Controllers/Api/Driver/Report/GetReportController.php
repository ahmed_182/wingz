<?php

namespace App\Http\Controllers\Api\Driver\Report;

use App\Driver_report;
use App\ModulesConst\Paginate;
use App\Traits\apiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class GetReportController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $reports = Driver_report::where("driver_id", $request->user()->id)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.driver_reports'), $reports, true);

    }

    public function reports(Request $request)
    {
        $request->validate([
            'driver_id' => 'required'
        ]);
        $reports = Driver_report::where("driver_id", $request->driver_id)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.driver_reports'), $reports, true);

    }
}
