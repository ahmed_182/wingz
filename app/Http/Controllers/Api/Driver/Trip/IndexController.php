<?php

    namespace App\Http\Controllers\Api\Driver\Trip;

    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use App\Trip;
    use App\Trip_ride;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $items = Trip::where('driver_id', $request->user()->id)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $items, true);
        }

//        public function details(Request $request)
//        {
//            $request->validate([
//                'trip_id' => 'required'
//            ]);
//            $item = Trip::find($request->trip_id);
//            return $this->apiResponse($request, trans('language.message'), $item, true);
//        }
//
//        public function rides(Request $request)
//        {
//            $request->validate([
//                'trip_id' => 'required'
//            ]);
//            $trip  = Trip::find($request->trip_id);
//            $rides = Trip_ride::where('trip_id',$trip->id)->paginate(Paginate::value)->getCollection();
//            return $this->apiResponse($request, trans('language.message'), $rides, true);
//        }
    }
