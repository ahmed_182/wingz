<?php

namespace App\Http\Controllers\Api\Driver\Trip;

use App\Child;
use App\ModulesConst\destinationWay;
use App\ModulesConst\NotificationTyps;
use App\ModulesConst\TripStatus;
use App\Ride_children_trip;
use App\Traits\apiResponse;
use App\Trip;
use App\Trip_children;
use App\Trip_ride;
use App\Trip_ride_life_cycle;
use App\Trip_ride_status;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class StartTripRideController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        // Start New Ride
        $request->validate([
            'trip_id' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);

        $destination = null;
        // check if any trip of this driver in process ,,,
//        $stsatus = $this->getRideStatus($request);
//        if ($stsatus == true) {
//            return $this->apiResponse($request, trans('عفوا , لا يمكنك بدايه الرحله الا بعد الانتهاء من الرحله القائمه معك .'), null, false, 400);
//        }
        $trip = Trip::find($request->trip_id);
        // get rides in this trip , to check destination .. get last ride if exist ( if school start -> home , if home -> start to school ).
        $ride = Trip_ride::where('trip_id', $trip->id)->latest()->first();

        if ($ride) {
            $ride_last_cycle = Trip_ride_life_cycle::where('trip_ride_id', $ride->id)->latest()->first();
            if ($ride_last_cycle) {
                $status_obj = Trip_ride_status::find($ride_last_cycle->trip_ride_status_id);
                $destination = $status_obj->destination;
            }
        }

        $sort_children_ids = $this->createRideForThisTrip($request, $trip, $destination);
        $children_list = Child::whereIn('id', $sort_children_ids)->get();

        return $this->apiResponse($request, trans('language.message'), $children_list, true);


    }

    public function getRideStatus($request)
    {
        $driver_trips = Trip::where('driver_id', $request->user()->id)->get();
        foreach ($driver_trips as $trip) {
            $ff = $trip->getRideStatusID();
            if ($ff != null and ($ff != Trip_ride_status::driverArrivedToSchool or $ff != Trip_ride_status::driverArrivedToHome)) {
                return true;
            }
        }
        return false;
    }


    public function get_child_status(Request $request)
    {
        // Start New Ride
        $request->validate([
            'trip_id' => 'required',
            'child_id' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);

        $trip = Trip::find($request->trip_id);
        // get rides in this trip , to check destination .. get last ride if exist ( if school start -> home , if home -> start to school ).
        $ride = Trip_ride::where('trip_id', $trip->id)->latest()->first();

        /* get details of child */
        $child = Child::find($request->child_id);

        /* get child ride  */
        $ride_children_trip = Ride_children_trip::where('ride_id', $ride->id)->where('child_id', $request->child_id)->get();
        //$ride_children_trip = Ride_children_trip::where('ride_id', $trip_ride->id)->get();

        return $this->apiResponse($request, trans('language.message'), $ride_children_trip, true);
    }

    public function get_all_child_status(Request $request)
    {
        // Start New Ride
        $request->validate([
            'trip_id' => 'required',
        ]);
        $trip = Trip::find($request->trip_id);
        // get rides in this trip , to check destination .. get last ride if exist ( if school start -> home , if home -> start to school ).
        $ride = Trip_ride::where('trip_id', $trip->id)->latest()->first();

        $ride_children_trip = [];
        if ($ride) {
            /* get child ride  */
            //$ride_children_trip = Ride_children_trip::where('ride_id', $request->ride_id)->where('child_id', $request->child_id)->get();
            // get destination
            $ride_last_cycle = Trip_ride_life_cycle::where('trip_ride_id', $ride->id)->latest()->first();
            $trip_ride_status = Trip_ride_status::find($ride_last_cycle->trip_ride_status_id);
            $destination = $trip_ride_status->destination;
            if ($destination == 1) {
                // sort children by farest from school .....
                $ride_children_trip = Ride_children_trip::where('ride_id', $ride->id)->orderBy('distance_in_kilo', 'desc')->get();
            } else {
                // sort children by nearest from school .....
                $ride_children_trip = Ride_children_trip::where('ride_id', $ride->id)->orderBy('distance_in_kilo', 'asc')->get();
            }
        }


        $data['children'] = $ride_children_trip;
        $data['general_status'] = $this->general_status($ride);
        $data['destination'] = $this->destination($ride);

        return $this->apiResponse($request, trans('language.message'), $data, true);
    }

    public function destination($ride)
    {
        $child_finish_status_count = Trip_ride_life_cycle::where('trip_ride_id', $ride->id)->latest()->first();
        $life_status = Trip_ride_status::find($child_finish_status_count->trip_ride_status_id);
        $destination = $life_status->destination;
        return $destination;
    }


    public function general_status($ride)
    {
        // trid from ride
        // get destantion ..
        // get status by dist and eueal zero , and > cureent status .
        $ride_last_cycle = Trip_ride_life_cycle::where('trip_ride_id', $ride->id)->latest()->first();
        $trip_ride_status = Trip_ride_status::find($ride_last_cycle->trip_ride_status_id); // current
        // get status list
        $list = Trip_ride_status::where('destination', $trip_ride_status->destination)->where('child_count', 0)->where('id', '>', $trip_ride_status->id)->get();
        return $list;
    }


    public function createRideForThisTrip($request, $trip, $destination = null)
    {

        $trip_ride_data['trip_id'] = $trip->id;
        $trip_ride_data['ride_work'] = Trip_ride::ride_work_start;
        $ride = Trip_ride::create($trip_ride_data);
        $this->createRideLifeCycleModel($request, $ride, $destination);
        $sort_children_ids = $this->createRideChildrenTripSortByDistance($request, $ride, $trip);
        $this->sendNotficationToInformParentThatTripWillStart($request, $trip);
        return $sort_children_ids;
    }

    public function createRideLifeCycleModel($request, $ride, $destination = null)
    {
        if ($destination) {
            if ($destination == destinationWay::home) {
                $ride_life_cycle_data['trip_ride_status_id'] = Trip_ride_status::driverLaunchedRide; // 1 -> start Ride .. to school ..
            } else {
                $ride_life_cycle_data['trip_ride_status_id'] = Trip_ride_status::driverGoToArriveChildrenFromSchoolToHome; // 1 -> start Ride .. to Home ..
            }
        } else {
            $ride_life_cycle_data['trip_ride_status_id'] = Trip_ride_status::driverLaunchedRide; // 1 -> start Ride .. to school ..
        }
        $ride_life_cycle_data['trip_id'] = $ride->trip_id;
        $ride_life_cycle_data['trip_ride_id'] = $ride->id;
        $ride_life_cycle_data['lat'] = $request->lat;
        $ride_life_cycle_data['lng'] = $request->lng;
        Trip_ride_life_cycle::create($ride_life_cycle_data);

        // trip update status :-
        $trip = Trip::find($ride->trip_id);
        $trip->status = $ride_life_cycle_data['trip_ride_status_id'];
        $trip->save();
    }

    public function sendNotficationToInformParentThatTripWillStart($request, $trip)
    {
        // get children in trip :-
        $children_ids = Trip_children::where('trip_id', $trip->id)->pluck('child_id');
        foreach ($children_ids as $child_id) {
            $child = Child::where('id', $child_id)->first();
            // Send notification to every Child Parent
            $body = trans('language.driver_start_your_child_trip') . " ( $trip->id ) " . trans('language.driver_start_your_child_trip_num');
            $users = User::where('id', $child->user_id)->pluck("id")->unique();
            $this->notificationHandler(trans("language.appName"), $body, $users, NotificationTyps::inform_driver_start_trip, $child->id);
        }
    }

    public function createRideChildrenTripSortByDistance($request, $ride, $trip)
    {
        // get trip children :-
        $children_ids = Trip_children::where('trip_id', $trip->id)->pluck('child_id')->toArray();
        foreach ($children_ids as $child_id) {
            // Get Child data
            $child_obj = Child::find($child_id);
            $distance_between_school_and_home = $this->getDistance($child_obj->serv_school_object['lat'], $child_obj->serv_school_object['lng'], $child_obj->child_home()->lat, $child_obj->child_home()->lng);
            $data['ride_id'] = $ride->id;
            $data['child_id'] = $child_id;
            $data['sort_by_distance'] = 0;
            $data['distance_in_kilo'] = $distance_between_school_and_home;
//            $data['status_id'] = Trip_ride_status::driverLaunchedRide;
            $data['status_id'] = 0;
            Ride_children_trip::create($data);
        }

        $children_ids = Ride_children_trip::where('ride_id', $ride->id)->orderBy("distance_in_kilo", "asc")->pluck('child_id')->toArray();

        return $children_ids;
    }

    public function getDistance($lat1, $lon1, $lat2, $lon2)
    {
        if (($lat1 == $lat2) && ($lon1 == $lon2)) {
            return 0;
        } else {
            $theta = $lon1 - $lon2;
            $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
            $dist = acos($dist);
            $dist = rad2deg($dist);
            $miles = $dist * 60 * 1.1515;
            $unit = strtoupper("M");

            if ($unit == "K") {
                return ($miles * 1.609344);
            } else if ($unit == "N") {
                return ($miles * 0.8684);
            } else {
                return $miles;
            }
        }
    }

}
