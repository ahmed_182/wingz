<?php

namespace App\Http\Controllers\Api\Driver\Trip;

use App\Child;
use App\ModulesConst\NotificationTyps;
use App\Ride_children_trip;
use App\Traits\apiResponse;
use App\Trip;
use App\Trip_children;
use App\Trip_ride;
use App\Trip_ride_life_cycle;
use App\Trip_ride_status;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ChangeTripRideController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        // Start New Ride
        $request->validate([
            'trip_id' => 'required',
            'child_id' => 'required',
            'trip_ride_status_id' => 'required',
            'lat' => 'required',
            'lng' => 'required',
        ]);
        // latest ride in this trip :-
        $trip = Trip::find($request->trip_id);
        $ride = Trip_ride::where('trip_id', $trip->id)->latest()->first();
        if (!$ride) {
            return $this->apiResponse($request, trans('Sorry no rides in this trip , start ride before update its status'), null, false, 400);
        }


        if ($request->child_id != -1) {
            // Update status for child
            $ride_children_trip = Ride_children_trip::where('ride_id', $ride->id)->where('child_id', $request->child_id)->first();
            $data['status_id'] = $request->trip_ride_status_id;
            $ride_children_trip->update($data);
        }
        $this->UpdateStatusRideLifeCycleModel($request, $ride);

        if ($request->child_id != -1) {
            // send for this child parent
            $this->sendNotificationHere($request, $ride);
        } else {
            // Send for all parents
            $this->sendNotificationHereToAllParent($request, $ride);
        }

        // trip update status :-
        $trip->status = $request->trip_ride_status_id;
        $trip->save();
        return $this->apiResponse($request, trans('language.message'), null, true);
    }

    public function sendNotificationHere($request, $ride)
    {
        // get children in trip :-
        $trip = Trip::find($ride->trip_id);
        $children_ids = Trip_children::where('trip_id', $trip->id)->pluck('child_id');
        $child_info = Child::where('id', $request->child_id)->get();

        $status = Trip_ride_status::find($request->trip_ride_status_id);
        if ($request->lang == "en")
            $body = 'The driver of trip Number' . " ( $trip->id ) " . 'Update trip status ' . " ( $status->Serv_name ) ";
        else
            $body = ' قام السائق الخاص بالرحله رقم  ' . " ( $trip->id ) " . 'بتغير حاله الرحله الي ' . " ( $status->Serv_name ) ";

        $users = User::where('id', $child_info[0]->user_id)->pluck("id")->unique();
        $this->notificationHandler(trans("language.appName"), $body, $users, NotificationTyps::inform_driver_start_trip, $request->child_id);
    }

    public function sendNotificationHereToAllParent($request, $ride)
    {
        // get children in trip :-
        $trip = Trip::find($ride->trip_id);
        $children_ids = Trip_children::where('trip_id', $trip->id)->pluck('child_id');
        $status = Trip_ride_status::find($request->trip_ride_status_id);
        $users = Child::whereIn('id', $children_ids)->pluck('user_id');

        if ($request->lang == "en")
            $body = 'The driver of trip Number' . " ( $trip->id ) " . 'Update trip status ' . " ( $status->Serv_name ) ";
        else
            $body = ' قام السائق الخاص بالرحله رقم  ' . " ( $trip->id ) " . 'بتغير حاله الرحله الي ' . " ( $status->Serv_name ) ";

        $this->notificationHandler(trans("language.appName"), $body, $users, NotificationTyps::inform_driver_start_trip, $request->child_id);
    }

    public function UpdateStatusRideLifeCycleModel($request, $ride)
    {
        // get children in this ride sorted by distance :-
        $children = Ride_children_trip::where('ride_id', $ride->id)->orderBy('sort_by_distance', 'ASC')->pluck('child_id')->toArray();
        $status_refrence_object = Trip_ride_status::find($request->trip_ride_status_id);
        if ($status_refrence_object and $status_refrence_object->child_count > 0) {
            $ride_life_cycle_data['child_id'] = $children[$status_refrence_object->child_count - 1];
        }
        $ride_life_cycle_data['trip_id'] = $ride->trip_id;
        $ride_life_cycle_data['trip_ride_id'] = $ride->id;
        $ride_life_cycle_data['trip_ride_status_id'] = $request->trip_ride_status_id;
        $ride_life_cycle_data['lat'] = $request->lat;
        $ride_life_cycle_data['lng'] = $request->lng;
        Trip_ride_life_cycle::create($ride_life_cycle_data);
    }
}
