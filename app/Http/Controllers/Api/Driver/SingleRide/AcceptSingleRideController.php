<?php

    namespace App\Http\Controllers\Api\Driver\SingleRide;

    use App\Driver;
    use App\Http\Controllers\SingleRideFireBaseContainerController;
    use App\Http\Requests\OrderFindRequest;
    use App\Http\Requests\SingleRideFindRequest;
    use App\Http\Resources\Order\OrderResource;
    use App\Order;
    use App\Single_ride;
    use App\Traits\apiResponse;
    use App\Traits\fireBaseContainer;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Kreait\Firebase\Exception\FirebaseException;

    class AcceptSingleRideController extends SingleRideFireBaseContainerController
    {
        use apiResponse;

        public function index(SingleRideFindRequest $request)
        {

            if (!$this->assignable($request))
                return $this->apiResponse($request, trans('language.order_not_assignable'),
                    null,
                    false, 400);

            $driver = $request->user();
            $order = Single_ride::where('id', $request->single_ride_id)->first();
            $order->order_status_id = Single_ride::driverAcceptOrder;
            $order->driver_id = $driver->id;
            $order->update();
            $this->removeAllTasksHandler($order, $request);
            $this->fireBaseHandler($order);

            if ($driver->fire_base_token)
                $this->fireBaseHandlerOrderNotification($order);

            return $this->apiResponse($request, trans('language.driver_accept_singleRide'), $order, true);
        }

        private function removeAllTasksHandler(Single_ride $order, SingleRideFindRequest $request)
        {
            //todo  remove search for driver
        }

        public function assignable(SingleRideFindRequest $request)
        {
            $order = Single_ride::where('id', $request->single_ride_id);
            return $order->assignable()->first() ? true : false;
        }

    }
