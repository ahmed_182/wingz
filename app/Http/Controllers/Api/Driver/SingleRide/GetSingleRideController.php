<?php

    namespace App\Http\Controllers\Api\Driver\SingleRide;

    use App\Http\Controllers\SingleRideFireBaseContainerController;
    use App\ModulesConst\Paginate;
    use App\Single_ride;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class GetSingleRideController extends SingleRideFireBaseContainerController
    {
        use apiResponse;

        public function index(Request $request)
        {
            $single_rides = Single_ride::where('driver_id', $request->user()->id)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.driver_single_ride_list'), $single_rides, true);
        }
    }
