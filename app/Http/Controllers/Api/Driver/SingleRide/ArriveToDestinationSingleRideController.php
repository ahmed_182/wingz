<?php

    namespace App\Http\Controllers\Api\Driver\SingleRide;

    use App\Driver;
    use App\Http\Controllers\SingleRideFireBaseContainerController;
    use App\Http\Requests\SingleRideArrivedToDestinationRequest;
    use App\Http\Requests\SingleRideFindRequest;
    use App\KiloPerPrice;
    use App\Single_ride;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class ArriveToDestinationSingleRideController extends SingleRideFireBaseContainerController
    {
        use apiResponse;

        public function index(SingleRideFindRequest $request)
        {
            $driver = Driver::where('user_id', $request->user()->id)->first();
            if (!$driver)
                return $this->apiResponse($request, trans('language.driver_not_exist'), null, false);

            $order = Single_ride::where('id', $request->single_ride_id)->where('driver_id', $request->user()->id)->first();
            if (!$order)
                return $this->apiResponse($request, trans('language.order_not_allow'), null, false);
            $obj = $this->arriveToDestination($request);
            $order = Single_ride::where('id', $request->single_ride_id);

            return $this->apiResponse($request, trans('language.driver_arrive_toDestination'),
                $order->first(),
                true);

        }

        public function arriveToDestination(Request $request)
        {

            $request->validate([
                'single_ride_id' => ['required', 'exists:single_rides,id', 'string', 'max:255'],
                'distance_m' => ['required', 'string', 'max:255'],
                'duration_s' => ['required', 'string', 'max:255'],
                'lat_lng' => ['required', 'string'],
            ]);

            $order = Single_ride::where('id', $request->single_ride_id)->first();

            $distance_k = $request->distance_m / 1000;
            $duration_m = $request->duration_s / 60;


            // get kilo price
            $pricePerKilo = 0;
            $kilo_price = KiloPerPrice::latest()->first();
            if ($kilo_price)
                $pricePerKilo = $kilo_price->price;

//            $order->receipt->starting = $order->category->base_fare;
//            $order->receipt->moving = $distance_k * $order->category->cost_per_k;
//            $order->receipt->duration = $duration_m * $order->category->cost_per_m;

//            $cost = $order->receipt->starting + $order->receipt->moving + $order->receipt->duration;
            $cost = $pricePerKilo;

            $order->receipt->cost = $pricePerKilo;
//            if ($cost < $order->category->minimum_fare)
//                $cost = $order->category->minimum_fare;

//            $order = $this->handelOrderVoucher($order, $cost);
//            $order->receipt->cost = $cost - $order->receipt->discount;
//            $order = $this->handelUserBalance($order, $cost);
//            $order->receipt->debit = $cost - $order->receipt->discount - $order->receipt->balance;
//            $order->receipt->driver_profit = $order->driver->car->carCategory->profit_percentage / 100 * $cost;

            $order->order_status_id = Single_ride::driverArrivedToDestination;
            $order->tracking = $request->lat_lng;
            $order->save();
            $order->receipt->save();
            $this->fireBaseHandler($order);
            $this->fireBaseHandlerOrderNotification($order);
            return $order->receipt;
        }
    }
