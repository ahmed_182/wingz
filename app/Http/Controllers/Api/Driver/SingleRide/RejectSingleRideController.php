<?php

    namespace App\Http\Controllers\Api\Driver\SingleRide;

    use App\Http\Controllers\SingleRideFireBaseContainerController;
    use App\Http\Requests\OrderFindRequest;
    use App\Http\Requests\SingleRideFindRequest;
    use App\Order;
    use App\Single_ride;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class RejectSingleRideController extends SingleRideFireBaseContainerController
    {
        use apiResponse;

        public function index(SingleRideFindRequest $request)
        {

            if (!$this->rejectable($request))
                return $this->apiResponse($request, trans('language.order_not_rejectable'), null, false, 400);
            $order = Single_ride::where('id', $request->single_ride_id)->first();
            $order->order_status_id = Single_ride::driverRejectOrder;
            $order->update();
            return $this->apiResponse($request, trans('language.single_ride_rejected'), null, true);
        }

        public function rejectable(SingleRideFindRequest $request)
        {
            return $this->assignable($request);
        }

        public function assignable(SingleRideFindRequest $request)
        {
            $order = Single_ride::where('id', $request->single_ride_id);
            return $order->assignable()->first() ? true : false;
        }


    }
