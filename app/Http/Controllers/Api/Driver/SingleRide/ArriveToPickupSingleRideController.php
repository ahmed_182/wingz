<?php

    namespace App\Http\Controllers\Api\Driver\SingleRide;

    use App\Driver;
    use App\Http\Controllers\SingleRideFireBaseContainerController;
    use App\Http\Requests\SingleRideFindRequest;
    use App\Single_ride;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class ArriveToPickupSingleRideController extends SingleRideFireBaseContainerController
    {
        use apiResponse;

        public function index(SingleRideFindRequest $request)
        {
            $driver = Driver::where('user_id', $request->user()->id)->first();
            if (!$driver)
                return $this->apiResponse($request, trans('language.driver_not_exist'), null, false);

            $order = Single_ride::where('id', $request->single_ride_id)->where('driver_id', $request->user()->id)->first();
            if (!$order)
                return $this->apiResponse($request, trans('language.order_not_allow'), null, false);

            $order = $this->arriveToPickup($request);
            return $this->apiResponse($request, trans('language.message'), $order, true);

        }

        public function arriveToPickup($request)
        {
            $order = Single_ride::where('id', $request->single_ride_id)->first();
            $order->order_status_id = Single_ride::driverArrivedToPickup;
            $order->update();
            $this->fireBaseHandler($order);
            $this->fireBaseHandlerOrderNotification($order);
            return $order;
        }

    }
