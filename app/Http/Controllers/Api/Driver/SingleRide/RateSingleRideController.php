<?php

namespace App\Http\Controllers\Api\Driver\SingleRide;

use App\Http\Controllers\SingleRideFireBaseContainerController;
use App\Single_ride;
use App\Traits\apiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class RateSingleRideController extends SingleRideFireBaseContainerController
{
    use apiResponse;

    public function index(Request $request)
    {
        $data = $request->validate([
            'single_ride_id' => ['required', 'exists:single_rides,id', 'string', 'max:255'],
            'driver_rate' => ['required', 'max:255'],
            'driver_feedBack' => ['max:255'],
        ]);

        $single_ride = Single_ride::where('id', $request->single_ride_id)->where('driver_id', $request->user()->id)->first();
        if (!$single_ride)
            return $this->apiResponse($request, trans('language.order_not_allow'), null, false);
        $data['driver_id'] = $request->user()->id;
        $single_ride->rate->update($data);

        return $this->apiResponse($request, trans('language.single_ride_rated'), $single_ride, true);
    }
}
