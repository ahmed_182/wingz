<?php

namespace App\Http\Controllers\Api\Driver\SingleRide;

use App\Http\Controllers\SingleRideFireBaseContainerController;
use App\Http\Requests\SingleRideFindRequest;
use App\Single_ride;
use App\Traits\apiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DetailsSingleRideController extends SingleRideFireBaseContainerController
{
    use apiResponse;

    public function index(SingleRideFindRequest $request)
    {
        $single_ride = Single_ride::find($request->single_ride_id);
        return $this->apiResponse($request, trans('language.single_ride_details'), $single_ride, true);
    }
}
