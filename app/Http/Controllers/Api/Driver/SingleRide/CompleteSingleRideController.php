<?php

    namespace App\Http\Controllers\Api\Driver\SingleRide;

    use App\Driver;
    use App\Http\Controllers\SingleRideFireBaseContainerController;
    use App\Http\Requests\SingleRideCompleteRequest;
    use App\Http\Requests\SingleRideFindRequest;
    use App\ModulesConst\CommissaryStatus;
    use App\Single_ride;
    use App\SingleRideOperation;
    use App\Traits\apiResponse;
    use App\User;
    use App\User_wallet;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class CompleteSingleRideController extends SingleRideFireBaseContainerController
    {
        use apiResponse;

        public function index(SingleRideFindRequest $request)
        {
            $driver = Driver::where('user_id', $request->user()->id)->first();
            if (!$driver)
                return $this->apiResponse($request, trans('language.driver_not_exist'), null, false);

            $order = Single_ride::where('id', $request->single_ride_id)->where('driver_id', $request->user()->id)->first();
            if (!$order)
                return $this->apiResponse($request, trans('language.order_not_allow'), null, false);

            $order = $this->complete($request);
            return $this->apiResponse($request, trans('language.driver_completed_ride'), $order, true);
        }

        public function complete($request)
        {
            $request->validate([
                'single_ride_id' => ['required', 'exists:single_rides,id', 'string', 'max:255'],
                'paid' => ['required', 'string', 'max:255'],
            ]);

            $order = Single_ride::findOrFail($request->single_ride_id);
            $data['order_id'] = $request->single_ride_id;
            $data['user_id'] = $order->client_id;
            $data['description'] = 'Single Ride  cost';
            $data['paid'] = $request->paid;
            $data['cost'] = $order->receipt->cost;
//            $data['debit'] = $order->receipt->debit;

            $client = User::findOrFail($order->client_id);
            $lastBalance = $client->serv_wallet_value;

            if (!$lastBalance)
                $lastBalance = 0;

            $data['balance'] = $request->paid - $order->receipt->cost + $lastBalance;
            $operation = SingleRideOperation::create($data);

            // sub from user balance
            $sub_walletp['user_id'] = $client->id;
            $sub_walletp['value'] = $request->paid - $order->receipt->cost;
            $sub_walletp['reason'] = 'Single Ride  cost';
            if ($request->paid > $order->receipt->cost)
                $sub_walletp['status'] = CommissaryStatus::add; // add Money
            else
                $sub_walletp['status'] = CommissaryStatus::withdrawal; // withdrawal
            User_wallet::create($sub_walletp);


            // Driver Side
            $driver = $order->driver;
            $data['user_id'] = $driver->id;
            $data['description'] = 'Single Ride profit';
//            $data['debit'] = $order->receipt->driver_profit;

            $driverProfile = Driver::where('user_id', $driver->id)->first(); // Driver Model
            $lastBalance = $driverProfile->current_balance;

            if (!$lastBalance)
                $lastBalance = 0;

//            $data['balance'] = $order->receipt->driver_profit + $lastBalance + $order->receipt->cost - $request->paid;
            $data['balance'] = $lastBalance + $order->receipt->cost - $request->paid;

            $operation = SingleRideOperation::create($data);
            // sub from user balance
            $sub_walletp['user_id'] = $driver->id;
            $sub_walletp['value'] = $order->receipt->cost;
            $sub_walletp['reason'] = 'Single Ride  cost';
            $sub_walletp['status'] = CommissaryStatus::add; // withdrawal
            User_wallet::create($sub_walletp);


            $order->order_status_id = Single_ride::driverCompleteOrder;
            $order->save();
            $this->fireBaseHandler($order);

            return $order;
        }

    }
