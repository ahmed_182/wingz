<?php

namespace App\Http\Controllers\Api\CarName;

use App\Car_name;
use App\ModulesConst\Paginate;
use App\Traits\apiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class Indexcontroller extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $car_types = Car_name::paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.car_names'), $car_types, true);
    }
}
