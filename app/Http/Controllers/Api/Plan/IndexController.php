<?php

namespace App\Http\Controllers\Api\Plan;

use App\Day;
use App\Favorite_places;
use App\Order_car_type;
use App\Plan;
use App\School;
use App\School_time;
use App\Stage_holiday;
use App\Stage_holiday_period;
use App\Traits\apiResponse;
use Carbon\Carbon;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $request->validate([
            'day' => 'required',
//                'distance' => 'required',
            'order_car_type_id' => 'required',
            'stage_id' => 'required',
            'favourite_place_id' => 'required',
            'school_id' => 'required',
            'after_school' => 'required', // 1 -> true , 0 -> false
        ]);

        $stage = School_time::find($request->stage_id);
        $calc_distance = $this->calcDistance($request);
        $distance = ceil($calc_distance); // ceil ->  round to the largest number?
        $today = $request->day / 1000;
        $day = (int)date("d", $today);
        $month = (int)date("m", $today);
        $items = Plan::all();
        $plans_data = [];
        $tax_value = $this->getTaxValue();
        // get price of kilo
        if ($request->after_school == true) {
            $pricePerKiloOrderCarType = Order_car_type::find($request->order_car_type_id)->price_per_kilo_after_school;
        } else {
            $pricePerKiloOrderCarType = Order_car_type::find($request->order_car_type_id)->price_per_kilo;
        }
        foreach ($items as $plan) {

            // get month count from plan
            $month_count = $plan->month_count;

            // make function to get count working days in plan month
            $all_days = $this->getWorkingDaysCount($day, $month, $month_count, $stage);
            $data['id'] = $plan->id;
            $data['name'] = $plan->serv_name;
            $data['period'] = (int)$plan->period;
            $data['month_count'] = (int)$plan->month_count;
            $data['price_per_kilo'] = $pricePerKiloOrderCarType;
            $data['working_days_count'] = $all_days;
            $data['distance'] = $distance * 2;
            $total_price = ceil($distance * 2 * $all_days * $pricePerKiloOrderCarType); // calc total Price , ceil ->  round to the largest number?
            $data['join_price'] = $total_price;
            $data['tax_value'] = substr($total_price * ($tax_value / 100), 0, 6);
            $data['price_with_discount'] = $total_price - ($total_price * ($plan->discount / 100));
            $data['discount_value'] = $total_price * ($plan->discount / 100);
            $data['total_price'] = $total_price + $data['tax_value'] - $data['discount_value'];
            $data['total_price_without_discount'] = $total_price + $data['tax_value'] ;
            $data['discount_per'] = (int)$plan->discount;
            $data['is_discount'] = (bool)$plan->discount;
            $data['tax'] = $tax_value;
//            dd($total_price ,($total_price * ($tax_value / 100)) , $data['discount_value'] );
            $data['price_after_tax'] = ($total_price + ($total_price * ($tax_value / 100))) - $data['discount_value'];
            $plans_data[] = $data;
        }
        return $this->apiResponse($request, trans('language.message'), $plans_data, true);
    }

    public function getWorkingDaysCount($day, $month, $month_count, $stage)
    {
        $currentMonth = $this->currentMonth($day, $month, $month_count, $stage);
        $nextMonths = $this->nextMonths($day, $month, $month_count, $stage);
        $all_days = $currentMonth + $nextMonths;
        return $all_days;
    }

    public function currentMonth($day, $month, $month_count, $stage)
    {

        $daysinmonth = Carbon::now()->daysInMonth;
        $daysinmonth = $daysinmonth - $day;
        return $daysinmonth;
    }

    public function nextMonths($day, $month, $month_count)
    {
        $nowmonth = $month_count;

        $days_count = $nowmonth * 30;
        return $days_count;

//            for ($j = 1; $j <= $month_count; $j++) {
//                if ($month + $j > 12) {
//                    $arr[] = $month + $j - 12;
//                } else {
//                    $arr[] = $month + $j;
//                }
//            }
//
//            // Get Days Count ..
//            for ($i = $month + 1; $i <= $nowmonth; $i++) {
//                if ($i == $nowmonth) {
//                    // all days in month
//                    $days_count = Day::where('month_id', $i)->where('is_vacation', 0)->where('day_number', '<=', $day)->count();
//                } else {
//                    // days which low than now day
//                    $days_count = Day::where('month_id', $i)->where('is_vacation', 0)->count();
//                }
//
//                $arr_days = $arr_days + $days_count;
//            }
//
//            return $arr_days;
    }

    public function calcDistance($request)
    {
        $favourite_place = Favorite_places::find($request->favourite_place_id);
        $school = School::find($request->school_id);
        $lat1 = $favourite_place->lat;
        $lon1 = $favourite_place->lng;
        $lat2 = $school->lat;
        $lon2 = $school->lng;
        $unit = 'K';
        // run calc function ..
        $theta = $lon1 - $lon2;
        $dist = sin(deg2rad($lat1)) * sin(deg2rad($lat2)) + cos(deg2rad($lat1)) * cos(deg2rad($lat2)) * cos(deg2rad($theta));
        $dist = acos($dist);
        $dist = rad2deg($dist);
        $miles = $dist * 60 * 1.1515;
        $unit = strtoupper($unit);

        if ($unit == "K") {
            return ($miles * 1.609344);
        } else if ($unit == "N") {
            return ($miles * 0.8684);
        } else {
            return $miles;
        }

    }


}

