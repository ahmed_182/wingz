<?php

namespace App\Http\Controllers\Api\Representative\Auth;

use App\Http\Resources\RepresentativeProfileResource;
use App\Http\Resources\UserAuth\GuestProfileResource;
use App\Http\Resources\UserAuth\ProfileResource;
use App\Http\Resources\UserAuth\UserLoginResource;
use App\Product;
use App\Traits\apiResponse;
use App\User;
use App\User_reviews;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ProfileController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $user = $request->user();
        if (!$user)
            abort(404);
        $item = new RepresentativeProfileResource($user);
        return $this->apiResponse($request, trans('language.driver_profile'), $item, true);
    }

    public function user_profile_guest(Request $request)
    {
        $request->validate([
            "user_id" => "required"
        ]);
        $user = User::find($request->user_id);
        $item = new RepresentativeProfileResource($user);
        return $this->apiResponse($request, trans('language.guest_profile'), $item, true);
    }

}
