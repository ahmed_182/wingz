<?php

namespace App\Http\Controllers\Api\Representative\Auth;

use App\Http\Resources\DriverAuth\DriverProfileResource;
use App\Http\Resources\RepresentativeProfileResource;
use App\ModulesConst\UserTyps;
use App\Traits\apiResponse;
use App\User;
use Auth;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $data = $this->validation($request);

        // Check start of mobile must be without zero :-
        $mob = $data['mobile'];
        $MobstartWith = substr($data['mobile'], 0, 1);
        if ($MobstartWith == 0) {
            $str = ltrim($mob, '0');
            $data["mobile"] = $str;
        }

        $user_mobile = User::where("mobile", $data["mobile"])->where("user_type_id", UserTyps::representative)->where("country_id", $request->country_id)->first();
        if (!$user_mobile) {
            return $this->apiResponse($request, trans('language.representative_auth_failed'), null, false, 400);
        }
        $newData = [];
        if ($request->fire_base_token) {
            $newData["fire_base_token"] = $request->fire_base_token;
        }
        $this->updateData($user_mobile, $newData);
        Auth::login($user_mobile);
        return $this->response($request);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function validation(Request $request)
    {
        $data = $request->validate([
            'country_id' => ['required', 'exists:countries,id'],
            'mobile' => ['required'],
            'fire_base_token' => [''],
        ]);
        return $data;
    }

    /**
     * @param $newData
     */
    public function updateData($user_mobile, $newData)
    {
        $user_mobile->update($newData);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function response(Request $request)
    {
        $item = auth()->user();
        $token = Auth::user()->createToken('myMob');
        $accessToken = $token->accessToken;
        $item = new RepresentativeProfileResource($item);
        return $this->sendResponse($request, trans('language.auth_login_success'), $item, true, $accessToken);
    }
}
