<?php

namespace App\Http\Controllers\Api\Representative\Order\Receipt;

use App\ModulesConst\Paginate;
use App\Traits\apiResponse;
use App\Trip_order_receipt;
use App\Trip_order_receipt_lifeCycle;
use App\Trip_order_receipt_status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $user = $request->user();
        $receipts = Trip_order_receipt::where('representative_id', $user->id)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $receipts, true);
    }

    public function statusList(Request $request)
    {
        $request->validate([
            'trip_order_receipt_id' => 'required'
        ]);

        $receipt = Trip_order_receipt::find($request->trip_order_receipt_id);
        $receipt_status_ids = Trip_order_receipt_lifeCycle::where('trip_order_receipt_id', $receipt->id)->pluck('trip_order_receipt_status_id');
        $status_list = Trip_order_receipt_status::whereNotIn('id', $receipt_status_ids)->get();
        return $this->apiResponse($request, trans('language.message'), $status_list, true);
    }

    public function statusChange(Request $request)
    {
        $data = $request->validate([
            'trip_order_receipt_id' => 'required',
            'trip_order_receipt_status_id' => 'required',
        ]);
        $receipt = Trip_order_receipt::find($request->trip_order_receipt_id);
        Trip_order_receipt_lifeCycle::create($data);
        // update inline status
        $receipt->update(['status' => $request->trip_order_receipt_status_id]);
        return $this->apiResponse($request, trans('language.message'), $receipt, true);
    }

}
