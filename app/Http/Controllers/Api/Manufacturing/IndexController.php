<?php

    namespace App\Http\Controllers\Api\Manufacturing;

    use App\Car_type;
    use App\Manufacturing_year;
    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $car_types = Manufacturing_year::paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.manufacturing_years'), $car_types, true);
        }
    }
