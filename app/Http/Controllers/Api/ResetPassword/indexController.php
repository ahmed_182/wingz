<?php

    namespace App\Http\Controllers\Api\ResetPassword;

    use App\Mail\resetPassworMail;
    use App\ModulesConst\UserVerify;
    use App\Traits\apiResponse;
    use App\User;
    use App\User_lastpasswords;
    use Auth;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use Mail;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $request->validate([
                'mobile' => 'required',
            ]);

            // check if this mobile existed ..
            $mobileCheck = User::where('mobile', $request->mobile)->get();
            if (!$mobileCheck->count() > 0) {
                return $this->apiResponse($request, trans('language.not_Existemobile'), null, false , 400);
            }
            $user = User::where('mobile', $request->mobile)->first();
            // Create user new Pass Code
            $data['passCode'] = rand(1111, 9999);
            $data['user_id'] = $user->id;
            $data['expired_at'] = \Carbon\Carbon::today()->addDays(7);
            User_lastpasswords::create($data);
            $user->passCode = $data['passCode'];
            $user->password = null;
            $user->save();
            //todo send sms message for mobile with PassCode .
            $code['code'] = "only for Test code Is :" . $data['passCode']; // only for Test
            return $this->apiResponse($request, trans('language.forget_pass_success'), $code, true);
        }

        public function confirm_code(Request $request)
        {

            $request->validate([
                'mobile' => 'required',
                'code' => 'required',
            ]);

            // check if this mobile existed ..
            $mobileCheck = User::where('mobile', $request->mobile)->get();
            if (!$mobileCheck->count() > 0) {
                return $this->apiResponse($request, trans('language.not_Existemobile'), null, false , 400);
            }

            $user = User::where('mobile', $request->mobile)->first();

            // check expired of this passCode
            $userPassCode = $user->lastpasswords->last()->passCode;
            if ($userPassCode == $request->code) {
                $userPassCodedate = User_lastpasswords::where('passCode', $request->code)->where('expired_at', '>', Carbon::now())->count();
                if ($userPassCodedate >= 1) {
                    $data['is_code_expired '] = false ;
                } else {
                    $data['is_code_expired '] = true;
                    return $this->apiResponse($request, trans('language.code_expired_resend_code'), null, false , 400);
                }
            } else {
                return $this->apiResponse($request, trans('language.invalid_code'), null, false , 400);
            }
            $user->userVerify = UserVerify::yes;
            $user->password = null;
            $user->save();
            Auth::login($user);
            $token = Auth::user()->createToken('myMob');
            $accessToken = $token->accessToken;
            $info['access_token'] = $accessToken;
            return $this->apiResponse($request, trans('language.code_confirmed'), $info, true);

        }

        public function resend_password_code(Request $request)
        {
            $request->validate([
                'mobile' => 'required',
            ]);

            // check if this mobile existed ..
            $mobileCheck = User::where('mobile', $request->mobile)->get();
            if (!$mobileCheck->count() > 0) {
                return $this->apiResponse($request, trans('language.not_Existemobile'), null, false , 400);
            }

            $user = User::where('mobile', $request->mobile)->first();

            $data['passCode'] = rand(1111, 9999);
            $data['user_id'] = $user->id;
            $data['expired_at'] = \Carbon\Carbon::today()->addDays(7);
            User_lastpasswords::create($data);
            $user->passCode = $data['passCode'];
            $user->password = null;
            $user->save();
            //todo send sms message for mobile with PassCode .
            $code['code'] = "only for Test code Is :" . $data['passCode']; // only for Test
            return $this->apiResponse($request, trans('language.sendEmail'), $code, true);
        }

        public function reset_new_password(Request $request)
        {
            $request->validate([
                'password' => 'required',
            ]);
            $user = $request->user();
            if (!$user)
                abort(404);
            $user->password = \Hash::make($request->password);
            $user->passCode = null;
            $user->save();
            return $this->apiResponse($request, trans('language.reset_new_password'), null, true);
        }

    }
