<?php

    namespace App\Http\Controllers\Api\Level;

    use App\Level;
    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class indexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $levels = Level::paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.levels'), $levels, true);
        }
    }
