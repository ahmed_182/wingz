<?php

namespace App\Http\Controllers\Api\openScreen;

use App\Country;
use App\ModulesConst\Paginate;
use App\Open_screen;
use App\Traits\apiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use apiResponse;

    public function openScreen(Request $request)
    {

        if ($request->page) {
            $items = Open_screen::paginate(Paginate::value)->getCollection();
        } else {
            $items = Open_screen::get();
        }
        return $this->apiResponse($request, trans('language.message'), $items, true);
    }

}
