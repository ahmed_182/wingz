<?php

namespace App\Http\Controllers\Api\School;

use App\ModulesConst\Paginate;
use App\School;
use App\School_level;
use App\Traits\apiResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class indexController extends Controller
{
    use apiResponse;

    public function index(Request $request)
    {
        $schools = School::get();
        $school_ids = [];
        foreach ($schools as $school) {
            $school_levels = School_level::where('school_id', $school->id)->first();
            if ($school_levels) {
                $school_ids[] = $school_levels->school_id;
            }
        }
        $schools = School::whereIn('id', $school_ids)->paginate(Paginate::value)->getCollection();
        return $this->apiResponse($request, trans('language.message'), $schools, true);
    }
}
