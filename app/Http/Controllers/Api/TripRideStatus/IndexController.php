<?php

    namespace App\Http\Controllers\Api\TripRideStatus;

    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use App\Trip;
    use App\Trip_children;
    use App\Trip_ride;
    use App\Trip_ride_life_cycle;
    use App\Trip_ride_status;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;
    use App\Ride_children_trip;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $request->validate([
                'trip_id' => 'required'
            ]);
            $trip = Trip::findorfail($request->trip_id);
            // get latest ride of this trip :-
            $latest_ride = Trip_ride::where('trip_id', $trip->id)->latest()->first();
            if (!$latest_ride) {
                return $this->apiResponse($request, trans('language.noRideYet'), null, false, 400);
            }
            // search in life cycle if driver arrive to school or not ..
            $check_if_driver_arrive_toSchool = Trip_ride_life_cycle::where('trip_ride_id', $latest_ride->id)->where('trip_ride_status_id', Trip_ride_status::driverArrivedToSchool)->first();
            if ($check_if_driver_arrive_toSchool) {
                // driver finish first part of ride ( GO TO School ) and in his way to Home .
                $destination = 2; // 2- Home
            } else {
                // driver start his trip .
                $destination = 1; // 1- school
            }
            $trip_children_count = Trip_children::where('trip_id', $trip->id)->count();
            $trip_children_statues = Trip_ride_status::where('destination', $destination)->where('child_count', '!=', 0)->Where('child_count', '!=', null)->where('child_count', '<=', $trip_children_count)->pluck('id')->toArray();
            $trip_main_statues = Trip_ride_status::where('destination', $destination)->where('child_count', 0)->orWhere('child_count', null)->pluck('id')->toArray();
            $ids = array_merge($trip_children_statues, $trip_main_statues);
            // dont return first status ( it start when driver start ride ) ..
            // get life cycle of this ride to unique status list ..
            $selected_status_ids = Trip_ride_life_cycle::where('trip_ride_id', $latest_ride->id)->pluck('trip_ride_status_id');
            $items = Trip_ride_status::where('destination', $destination)->whereIn('id', $ids)->where('id', '!=', 1)->whereNotIn('id', $selected_status_ids)->paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.trips_ride_status'), $items, true);
        }

        public function status_for_child(Request $request)
        {
            $request->validate([
                'child_index' => 'required',
                'trip_id' => 'required'
            ]);

            $trip = Trip::find($request->trip_id);
            // get rides in this trip , to check destination .. get last ride if exist ( if school start -> home , if home -> start to school ).
            $ride = Trip_ride::where('trip_id', $trip->id)->latest()->first();

            /* get child id from his index  */
            $ride_children_trip = Ride_children_trip::where('ride_id', $ride->id)->get();


            $child_id = $ride_children_trip[$request->child_index]->child_id;

            /* get last status id  */
            $trip_ride_life_cycles = Trip_ride_life_cycle::where('trip_ride_id', $ride->id)->where('child_id', $child_id)->orderBy('id', 'DESC')->get();

            $initial_status_id = 0;
            if (count($trip_ride_life_cycles) > 0) {
                $initial_status_id = $trip_ride_life_cycles[0]->trip_ride_status_id;
            }

            // get $destination
            $ride_last_cycle = Trip_ride_life_cycle::where('trip_ride_id', $ride->id)->latest()->first();
            $trip_ride_status = Trip_ride_status::find($ride_last_cycle->trip_ride_status_id);
            /* select ride status for child index */
            $trip_children_statues = Trip_ride_status::where('destination', $trip_ride_status->destination)->where('child_count', $request->child_index + 1)->where('id', '>', $initial_status_id)->get();

            return $this->apiResponse($request, trans('language.trips_ride_status'), $trip_children_statues, true);
        }
    }
