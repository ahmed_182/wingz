<?php

    namespace App\Http\Controllers\Api\Partner;

    use App\Brand;
    use App\ModulesConst\Paginate;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $items = Brand::paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.brands'), $items, true);
        }
    }
