<?php

    namespace App\Http\Controllers\Api\Payment;

    use App\ModulesConst\Paginate;
    use App\Payment_types;
    use App\Traits\apiResponse;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use apiResponse;

        public function index(Request $request)
        {
            $payment_types = Payment_types::paginate(Paginate::value)->getCollection();
            return $this->apiResponse($request, trans('language.message'), $payment_types, true);
        }
    }
