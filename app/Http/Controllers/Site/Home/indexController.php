<?php

    namespace App\Http\Controllers\Site\Home;

    use App\Slider;
    use App\Slider_clicks;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class indexController extends Controller
    {
        public function index()
        {
            return view("welcome");
        }

        public function openLink($user_id, $slider_id)
        {
            $data['user_id'] = $user_id;
            $data['slider_id'] = $slider_id;
            Slider_clicks::create($data);
            // redirect to slider link .
            $slirder = Slider::find($slider_id);
            return redirect("$slirder->link");
        }
    }
