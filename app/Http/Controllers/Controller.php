<?php

namespace App\Http\Controllers;

use App\ModulesConst\NotificationTyps;
use App\ModulesConst\TaxSystemType;
use App\Notification;
use App\Tax;
use App\User;
use App\User_notification;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Kreait\Firebase\Factory;
use Kreait\Firebase\ServiceAccount;
use LaravelFCM\Facades\FCM;
use LaravelFCM\Message\OptionsBuilder;
use LaravelFCM\Message\PayloadDataBuilder;
use LaravelFCM\Message\PayloadNotificationBuilder;

class Controller extends BaseController
{

    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;


    public function notificationHandler($title, $body, $users_ids, $notiType = null, $notiTypeId = null)
    {
        if ($notiType == null) {
            $notiType = NotificationTyps::normal;
        }
        $data["title"] = $title;
        $data["body"] = $body;
        $data["type"] = $notiType;
        $data["item_id"] = $notiTypeId;
        $notification = Notification::create($data);
        foreach ($users_ids as $user_id) {
            $user = User::find($user_id);
            $data["user_id"] = $user_id;
            $data["notification_id"] = $notification->id;
            $data["user_id"] = $user->id;
            User_notification::create($data);
            if ($user->fire_base_token) {

                $this->push_notification([$user->fire_base_token], $data);
            }
        }
    }

    public function adminNotificationHandler($title, $body)
    {
        $data["title"] = $title;
        $data["body"] = $body;
        $notification = Notification::create($data);
        $data["user_id"] = 0;
        $data["notification_id"] = $notification->id;
        User_notification::create($data);
    }




    //send to client
    public function fireBaseNotificationsHandler($tokens, $data = [])
    {
        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $notificationBuilder = new PayloadNotificationBuilder($data['title']);
        $notificationBuilder->setBody($data['body'])
            ->setSound('default');
        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($data);
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
        $downstreamResponse->tokensToDelete();
        $downstreamResponse->tokensToModify();
        $downstreamResponse->tokensToRetry();
        $downstreamResponse->tokensWithError();
    }


    function push_notification($tokens, $data = [])
    {

        $optionBuilder = new OptionsBuilder();
        $optionBuilder->setTimeToLive(60 * 20);
        $notificationBuilder = new PayloadNotificationBuilder($data['title']);
        $notificationBuilder->setBody($data['body'])
            ->setSound('default');
        $notificationBuilder->setclickAction($data['type']);
        $notificationBuilder->setChannelId($data['item_id']);

        $dataBuilder = new PayloadDataBuilder();
        $dataBuilder->addData($data);
        $option = $optionBuilder->build();
        $notification = $notificationBuilder->build();
        $data = $dataBuilder->build();
        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);

        $downstreamResponse->numberSuccess();
        $downstreamResponse->numberFailure();
        $downstreamResponse->numberModification();
        $downstreamResponse->tokensToDelete();
        $downstreamResponse->tokensToModify();
        $downstreamResponse->tokensToRetry();
        $downstreamResponse->tokensWithError();

//        $optionBuilder = new OptionsBuilder();
//        $optionBuilder->setTimeToLive(60 * 20);
//        $notificationBuilder = new PayloadNotificationBuilder($data['title']);
//        $notificationBuilder->setBody($data['title'])
//            ->setSound('default');
//        $dataBuilder = new PayloadDataBuilder();
//        $dataBuilder->addData($data);
//        $option = $optionBuilder->build();
//        $notification = $notificationBuilder->build();
//        $data = $dataBuilder->build();
//        $downstreamResponse = FCM::sendTo($tokens, $option, $notification, $data);
//        $downstreamResponse->numberSuccess();
//        $downstreamResponse->numberFailure();
//        $downstreamResponse->numberModification();
//        $downstreamResponse->tokensToDelete();
//        $downstreamResponse->tokensToModify();
//        $downstreamResponse->tokensToRetry();
//        $downstreamResponse->tokensWithError();

    }

// Tax Global functions :-
    public
    function getTaxValue()
    {
        $tax = Tax::where('tax_type_id', 1)->first();
        if ($tax) {
            return $tax->value;
        } else {
            return 0;
        }
    }

    public
    function getRepresentativeTaxValue()
    {
        $tax = Tax::where('tax_type_id', 2)->first();
        if ($tax) {
            return $tax->value;
        } else {
            return 0;
        }
    }

}
