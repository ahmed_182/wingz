<?php

namespace App\Http\Controllers\Admin\Ajax;

use App\City;
use App\District;
use App\Level;
use App\School_level;
use App\School_time;
use App\Stage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function getLevels(Request $request)
    {
        $lvls_ids = School_level::where("school_id", $request->school_id)->pluck('level_id');
        $levels = Level::whereIn('id', $lvls_ids)->get();
        $data['levels'] = $levels;
        if (count($levels) > 0) {
            $data["status"] = true;
        } else {
            $data["status"] = false;
        }
        return response()->json($data);
    }

    public function getSchoolTimes(Request $request)
    {
        $school_level = School_level::where('level_id', $request->level_id)->where('school_id', $request->school_id)->first();
        if ($school_level) {
            $school_times = School_time::where('school_level_id', $school_level->id)->pluck('stage_id')->unique();
            $stages = Stage::whereIn('id', $school_times)->get();
            $data['stages'] = $stages;
            $data["status"] = true;
        } else {
            $data["status"] = false;
        }
        return response()->json($data);
    }

    public function getCities(Request $request)
    {
        $cities = City::where("country_id", $request->country_id)->orderBy("id", "desc")->get();
        $data['cities'] = $cities;
        if (count($cities) > 0) {
            $data["status"] = true;
        } else {
            $data["status"] = false;
        }
        return response()->json($data);
    }

    public function getDistracts(Request $request)
    {
        $distracts = District::where("city_id", $request->city_id)->orderBy("id", "desc")->get();
        $data['distracts'] = $distracts;
        if (count($distracts) > 0) {
            $data["status"] = true;
        } else {
            $data["status"] = false;
        }
        return response()->json($data);
    }

    public function getDistractDetails(Request $request)
    {
        $distract = District::find($request->district_id);
        $data['distract'] = $distract;
        if ($distract) {
            $data["status"] = true;
        } else {
            $data["status"] = false;
        }
        return response()->json($data);
    }

}
