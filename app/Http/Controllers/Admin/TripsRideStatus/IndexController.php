<?php

namespace App\Http\Controllers\Admin\TripsRideStatus;

use App\Trip_ride_status;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{/**
 * Display a listing of the resource.
 *
 * @return \Illuminate\Http\Response
 */
    public function index()
    {
        $items = Trip_ride_status::orderBy('id', 'desc')->paginate(10);
        return view('admin.trips_ride_status.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.trips_ride_status.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
        ]);

        Trip_ride_status::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/trips_ride_status'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Trip_ride_status::findOrFail($id);
        return view('admin.trips_ride_status.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Trip_ride_status::findOrFail($id);
        $data = $request->validate([
            'name_ar' => '',
            'name_en' => '',
        ]);


        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/trips_ride_status'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Trip_ride_status::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/trips_ride_status'));
    }
}
