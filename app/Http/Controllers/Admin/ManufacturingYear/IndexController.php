<?php

namespace App\Http\Controllers\Admin\ManufacturingYear;

use App\Manufacturing_year;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Manufacturing_year::orderBy('id', 'desc')->paginate(10);
        //dd($items);
        return view('admin.manufacturing_years.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.manufacturing_years.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
        ]);

        $options = Manufacturing_year::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/manufacturing_years'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Manufacturing_year::findOrFail($id);
        return view('admin.manufacturing_years.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Manufacturing_year::findOrFail($id);
        $data = $request->validate([
            'name_ar' => '',
            'name_en' => '',
        ]);


        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/manufacturing_years'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Manufacturing_year::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/manufacturing_years'));
    }
}
