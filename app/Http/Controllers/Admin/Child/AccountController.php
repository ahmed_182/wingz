<?php

namespace App\Http\Controllers\Admin\Child;

use App\Child;
use App\ModulesConst\blockStatus;
use App\ModulesConst\UserVerify;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AccountController extends Controller
{
    public function index($id)
    {
        $child = Child::find($id);
        $child->is_deleted = 0;
        $child->save();
        session()->flash('success', trans('language.done'));
        return back();
    }

    public function delete_account($id)
    {
        $child = Child::find($id);
        $child->is_deleted = 1;
        $child->save();
        session()->flash('success', trans('language.done'));
        return back();
    }
}
