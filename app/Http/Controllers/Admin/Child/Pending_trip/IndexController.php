<?php

namespace App\Http\Controllers\Admin\Child\Pending_trip;

use App\Child;
use App\ModulesConst\AssignedStatus;
use App\ModulesConst\NotificationTyps;
use App\ModulesConst\TripStatus;
use App\Trip;
use App\Trip_children;
use App\User;
use App\User_assigin_trips;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $trips = Trip::pluck('id');
        $tripCount = count($trips);
        $trip_children = [];
        for ($i = 0; $i < $tripCount; $i++) {
            $trip_children = Trip_children::where('trip_id', $trips[$i])->pluck('child_id')->toArray();
        }
        // get children who not in Trip ..
        $items = Child::whereNotIn('id', $trip_children);
        $items = $this->filter($request, $items);
        return view('admin.trips.children.pending', compact('items'));
    }

    public function filter($request, $items)
    {
        if ($request->name)
            $items = $items->where("name", 'LIKE', '%' . $request->name . '%');
        if ($request->phone) {
            $items = $items->where("mobile", 'LIKE', '%' . $request->phone . '%');
        }
        if ($request->user_id and $request->user_id != 0) {
            $items = $items->where("user_id", $request->user_id);
        }
        return $items->orderBy("id", "desc")->paginate(10);
    }

    public function assign_drivers($child_id)
    {
        $child = Child::find($child_id);
        $child_school = $child->serv_school_object;
        $school_trips_driver_ids = Trip::where('school_id', $child_school['id'])->orderBy('id', 'desc')->pluck('driver_id');
        $items = User::whereIn('id', $school_trips_driver_ids)->get();
        return view('admin.trips.children.available_drivers', compact('items', 'child'));
    }


    public function child_assign_drivers(Request $request)
    {
        $request->validate([
            'child_id' => 'required',
            'trip_id' => 'required',
        ]);
        $child = Child::find($request->child_id);
        $trips_ids = $request->trip_id;
        // send this trip to child ( parent - user )
        foreach ($trips_ids as $trip_id) {
            // check if this trip not assign to this user before ?
            $check_assign = User_assigin_trips::where('child_id', $child->id)->where('trip_id', $trip_id)->first();
            if (!$check_assign) {
                User_assigin_trips::create([
                    'user_id' => $child->user_id,
                    'child_id' => $child->id,
                    'trip_id' => $trip_id,
                    'status_id' => AssignedStatus::pending,
                ]);
            }
            // add child in every trip with pending status :-
            $child_dt['child_id'] = $child->id;
            $child_dt['trip_id'] = $trip_id;
            $child_dt['status_id'] = 1;
            Trip_children::create($child_dt);
        }

        $this->thisNotificationHandler($request, $child);
        session()->flash('success', trans('language.show_parent_driver_noti'));
        return redirect('/admin/pending_children');
    }

    public function thisNotificationHandler($request, $child)
    {
        // Send Notification to
        $body = trans('language.show_parent_driver_noti_api') . "( $child->name )";
        $users = User::where('id', $child->user_id)->pluck("id")->unique();
        $this->notificationHandler(trans("language.appName"), $body, $users, NotificationTyps::assigin_trips, $child->id);
    }

    public function pending_child_map_location($child_id)
    {
        $child = Child::find($child_id);
        $other_children = Child::whereNotIn("id", [$child_id])->get();
        $green_lats = [];
        $green_lngs = [];
        $orange_lats = [];
        $orange_lngs = [];
        foreach ($other_children as $child_pin) {
            // set children which in trip in array
            if ($child_pin->is_in_trip()) { // if this child has trip => put it in green arr :-
                $green_lats[] = $child_pin->user->lat;
                $green_lngs[] = $child_pin->user->lng;
            } else {
                $orange_lats[] = $child_pin->user->lat;
                $orange_lngs[] = $child_pin->user->lng;
            }

        }
        return view('admin.trips.children.pending_child_map_location', compact('child', 'green_lats', "green_lngs", "orange_lats", 'orange_lngs'));
    }

}
