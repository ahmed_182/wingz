<?php

    namespace App\Http\Controllers\Admin\Plan;

    use App\Plan;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class ActionController extends Controller
    {
        public function DeactivatePlan($id)
        {

            $plan = Plan::find($id);

            // check if this someone using this plan now ?!

            $plan->status = 0;
            $plan->save();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/plans'));
        }

        public function activatePlan($id)
        {
            $plan = Plan::find($id);
            $plan->status = 1;
            $plan->save();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/plans'));
        }

    }
