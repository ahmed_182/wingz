<?php

    namespace App\Http\Controllers\Admin\Plan;

    use App\Plan;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $active_items = Plan::where('status', 1)->orderBy('id', 'desc')->paginate(10);
            $Notactive_items = Plan::where('status', 0)->orderBy('id', 'desc')->paginate(10);
            return view('admin.plans.index', compact('active_items', 'Notactive_items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {

            return view('admin.plans.create');

        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {

            $data = $request->validate([

                'name_ar' => 'required',
                'name_en' => 'required',
                'period' => 'required',
                'month_count' => 'required',
                'discount' => 'required',
            ]);

            $options = Plan::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/plans'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Plan::findOrFail($id);
            return view('admin.plans.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Plan::findOrFail($id);
            $data = $request->validate([
                'name_ar' => '',
                'name_en' => '',
                'period' => '',
                'month_count' => '',
                'discount' => '',
            ]);


            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/plans'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Plan::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/plans'));
        }
    }
