<?php

namespace App\Http\Controllers\Admin\Term;

use App\Terms;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       // $items = Terms::orderBy('id', 'desc')->paginate(10);
        $item = Terms::first();
       // dd($item);
        if (!$item) {
            $terms["name_ar"] ="الشروط" ;
            $terms["name_en"] = "terms";
            $item = Terms::create($terms);
        }
        //dd($items);
        return view('admin.terms.index', compact('item'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.terms.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name_ar' => 'required',
            'name_en' => 'required',
        ]);
        $term = Terms::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/terms'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Terms::findOrFail($id);
        return view('admin.terms.edit',compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Terms::findOrFail($id);
        $data = $request->validate([
            'name_ar' => '',
            'name_en' => '',
        ]);

        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/terms'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Terms::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/terms'));
    }
}
