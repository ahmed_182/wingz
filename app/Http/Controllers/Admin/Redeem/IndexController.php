<?php

    namespace App\Http\Controllers\Admin\Redeem;

    use App\Redeem;
    use App\Traits\storeImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use storeImage;

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $items = Redeem::orderBy('id', 'desc')->paginate(10);
            return view('admin.redeems.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.redeems.create');

        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {

            $data = $request->validate([

                'image' => 'required',
                'name_ar' => 'required',
                'name_en' => 'required',
            ]);

            if ($request->image) {
                $data['image'] = $this->storeImage($request->image);
            }
            $options = Redeem::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/redeems'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Redeem::findOrFail($id);
            return view('admin.redeems.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Redeem::findOrFail($id);
            $data = $request->validate([
                'image' => '',
                'name_ar' => '',
                'name_en' => '',
            ]);

            if ($request->image) {
                $data['image'] = $this->storeImage($request->image);
            }
            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/redeems'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Redeem::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/redeems'));
        }
    }
