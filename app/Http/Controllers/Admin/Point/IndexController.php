<?php

    namespace App\Http\Controllers\Admin\Point;

    use App\Point;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $items = Point::orderBy('id', 'desc')->paginate(10);
            return view('admin.points.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.points.create');

        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {

            $data = $request->validate([

                'value' => 'required',
                'name_ar' => 'required',
                'name_en' => 'required',
            ]);

            $options = Point::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/points'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Point::findOrFail($id);
            return view('admin.points.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Point::findOrFail($id);
            $data = $request->validate([
                'value' => '',
                'name_ar' => '',
                'name_en' => '',
            ]);

            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/points'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Point::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/points'));
        }
    }
