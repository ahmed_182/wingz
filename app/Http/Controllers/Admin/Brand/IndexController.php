<?php

    namespace App\Http\Controllers\Admin\Brand;

    use App\Brand;
    use App\Repositories\Brands\BrandsRepositoryInterface;
    use App\Repositories\Brands\PostsRepositoryInterface;
    use App\Traits\storeImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use storeImage;

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $items = Brand::orderBy('id', 'desc')->paginate(10);
            return view('admin.brands.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()

        {

            return view('admin.brands.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $data = $request->validate([

                'name_ar' => 'required',
                'name_en' => 'required',
                'image' => 'mimes:jpeg,jpg,png,gif|required|max:2048',
            ]);
            if ($request['image']) {
                $data['image'] = $this->storeImage($request['image']);
            }
            $item = Brand::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/brands'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Brand::findOrFail($id);
            return view('admin.brands.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Brand::findOrFail($id);
            $data = $request->validate([
                'name_ar' => '',
                'name_en' => '',
                'image' => '',
            ]);


            if ($request->image) {
                $data['image'] = $this->storeImage($data['image']);
            }
            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/brands'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Brand::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/brands'));
        }
    }
