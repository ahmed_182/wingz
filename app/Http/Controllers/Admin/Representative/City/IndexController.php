<?php

namespace App\Http\Controllers\Admin\Representative\City;

use App\District;
use App\Driver_districts;
use App\Representative_districts;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($representative_id)
    {
        $items = Representative_districts::where("representative_id", $representative_id)->orderBy('id', 'desc')->paginate(10);
        return view('admin.representative.districts.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $driver = User::find($id);
        $driver_cities_id = Representative_districts::where("representative_id", $id)->pluck('district_id');
        $cities = District::whereNotIn("id",$driver_cities_id)->get();
        return view('admin.representative.districts.create', compact("driver",'cities'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , $id)
    {
        $data = $request->validate([
            'district_id' => 'required',
        ]);
        $data['representative_id'] = $id;
        Representative_districts::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url("/admin/representative/$id/districts"));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($representative_id , $id)
    {
        $item = Representative_districts::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url("/admin/representative/$representative_id/districts"));
    }
}
