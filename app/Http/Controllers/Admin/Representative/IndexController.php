<?php

namespace App\Http\Controllers\Admin\Representative;

use App\ModulesConst\UserTyps;
use App\Traits\storeImage;
use App\User;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use storeImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = User::where('user_type_id', UserTyps::representative);
        $items = $this->filter($request, $items);
        return view('admin.representative.index', compact('items'));

    }

    public function filter($request, $items)
    {
        if ($request->name)
            $items = $items->where("name", 'LIKE', '%' . $request->name . '%');
        if ($request->mobile) {
            $items = $items->where("mobile", 'LIKE', '%' . $request->mobile . '%');
        }
        $items = $items->orderBy("id", "desc")->paginate(10);
        return $items;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.representative.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'mobile' => 'required|unique:users',
            'name' => 'required',
            'country_id' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:2048',
        ]);

        $data['user_type_id'] = UserTyps::representative;
        if ($request->image) {
            $data['image'] = $this->storeImage($request->image);
        }
        $user = User::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/representative'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = User::findOrFail($id);
        return view('admin.representative.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = User::findOrFail($id);
        $data = $request->validate([
            'mobile' => '',
            'name' => '',
            'country_id' => '',
            'image' => '',
        ]);

        if ($request->image) {
            $data['image'] = $this->storeImage($data['image']);
        }
        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/representative'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = User::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/representative'));
    }
}
