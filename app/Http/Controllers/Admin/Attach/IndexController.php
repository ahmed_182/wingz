<?php

namespace App\Http\Controllers\Admin\Attach;

use App\Child;
use App\Driver;
use App\ModulesConst\AssignedStatus;
use App\ModulesConst\NotificationTyps;
use App\ModulesConst\UserTyps;
use App\ModulesConst\UserVerify;
use App\Order;
use App\School;
use App\User_assigin_trips;
use App\Trip;
use App\Trip_children;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index(Request $request, $id, $school_id)
    {
        // 1- get school object
        $school = School::find($school_id);
        // 2- get all trips which go to this school ?
        $school_trips = Trip::where('school_id', $school->id);
        $drivers_ids = $school_trips->pluck('driver_id')->unique();
        $size = sizeof($drivers_ids);
        $avliable_trips = [];

        //3- get all trips for this Driver
        for ($i = 0; $i < $size; $i++) {

            $driver_trips = Trip::where('driver_id', $drivers_ids[$i])->where('school_id', $school->id)->first();
            if ($driver_trips) {
                $childreen = Trip_children::where('trip_id', $driver_trips->id)->get();
                $seat_nums = count($childreen);

                // 4- check if number like the driver seat numbers ?
                $driver_obj = Driver::where('user_id', $drivers_ids[$i])->first();
                if ($driver_obj) {
                    //check
                    if ($driver_obj->children_count > $seat_nums) {
                        $avliable_trips[] = $driver_trips->id;
                    }
                }
            }

        }
        $items = Trip::whereIn('id', $avliable_trips)->orderBy('id', 'desc')->paginate(10);
        $order = Order::find($id);

        if (count($items) > 0) {
            return view('admin.attach.index', compact('items', 'order'));
        } else {
            $drivers = User::where('user_type_id', UserTyps::driver)->where('userVerify', UserVerify::yes)->get();
            return view('admin.attach.newTrip', compact('drivers', 'order'));
        }


    }

    public function store($order_id, $trip_id)
    {
        $order = Order::find($order_id);
        $child = Child::find($order->child_id);
        $trips_ids = [$trip_id];
        // send this trip to child ( parent - user )
        foreach ($trips_ids as $trip_id) {
            // check if this trip not assign to this user before ?
            $check_assign = User_assigin_trips::where('child_id', $child->id)->where('trip_id', $trip_id)->first();
            if (!$check_assign) {
                User_assigin_trips::create([
                    'user_id' => $child->user_id,
                    'child_id' => $child->id,
                    'trip_id' => $trip_id,
                    'status_id' => AssignedStatus::pending,
                ]);
            }
            // add child in every trip with pending status :-
            $child_dt['child_id'] = $child->id;
            $child_dt['trip_id'] = $trip_id;
            $child_dt['status_id'] = 1;
            Trip_children::create($child_dt);
        }

        $this->thisNotificationHandler($child);
        session()->flash('success', trans('language.show_parent_driver_noti'));
        return redirect('/admin/orders');
    }

    public function thisNotificationHandler( $child)
    {
        // Send Notification to
        $body = trans('language.show_parent_driver_noti_api') . "( $child->name )";
        $users = User::where('id', $child->user_id)->pluck("id")->unique();
        $this->notificationHandler(trans("language.appName"), $body, $users, NotificationTyps::assigin_trips, $child->id);
    }
}
