<?php

namespace App\Http\Controllers\Admin\TripChildren;

use App\Child;
use App\ModulesConst\AssignedStatus;
use App\ModulesConst\NotificationTyps;
use App\Notification;
use App\Trip;
use App\Trip_children;
use App\User;
use App\User_assigin_trips;
use App\User_notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $childs_id = Trip_children::where('trip_id', $id)->pluck('child_id');
        $items = Child::whereIn('id', $childs_id)->orderBy('id', 'desc')->paginate(10);
        $trip = Trip::find($id);
        return view('admin.trips.children.index', compact('items', 'trip'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($trid_id)
    {
        $trip = Trip::find($trid_id);
        $trip_childreen_ids = Trip_children::where('trip_id', $trid_id)->pluck('child_id');
        $children = Child::whereNotIn('id', $trip_childreen_ids)->get();
        return view('admin.trips.children.create', compact('children', 'trip'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $trip_id)
    {
        $data = $request->validate([
            'child_id' => 'required'
        ]);
        $data['trip_id'] = $trip_id;
        $trip = Trip::find($trip_id);
        $child_trip = Trip_children::create($data);
        $child = Child::find($request->child_id);
        // Send Notification for child Father to show driver account of trip ...
        $User_assigin_trips['user_id'] = $child->user_id;
        $User_assigin_trips['trip_id'] = $trip->id;
        $User_assigin_trips['child_id'] = $child->id;
        $User_assigin_trips['status_id'] = AssignedStatus::pending;
        User_assigin_trips::create($User_assigin_trips);
        $this->thisNotificationHandler($request, $child);
        session()->flash('success', trans('language.show_parent_driver_noti'));
        return redirect(url("/admin/trips/$trip_id/children"));
    }

    public function thisNotificationHandler($request, $child)
    {
        // Send Notification to
        $body = trans('language.show_parent_driver_noti_api') . "( $child->name )";;
        $users = User::where('id', $child->user_id)->pluck("id")->unique();
        $this->notificationHandler(trans("language.appName"), $body, $users, NotificationTyps::assigin_trips, $child->id);
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($trid_id, $child_id)
    {
        $child_trip = Trip_children::where('trip_id', $trid_id)->where('child_id', $child_id)->first();
        if (!$child_trip) {
            session()->flash('error', trans('language.noTripFound'));
            return back();
        }
        $child_trip->delete();
        session()->flash('success', trans('language.done'));
        return back();
    }
}
