<?php

    namespace App\Http\Controllers\Admin\Voucher_type;

    use App\VoucherType;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $items = VoucherType::orderBy('id', 'desc')->paginate(10);
            //dd($items);
            return view('admin.voucher-type.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.voucher-type.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $data = $request->validate([
                'name_ar' => 'required',
                'name_en' => 'required',
            ]);
            VoucherType::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/voucher-type'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = VoucherType::findOrFail($id);
            return view('admin.voucher-type.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {

            $item = VoucherType::findOrFail($id);
            $data = $request->validate([
                'name_ar' => '',
                'name_en' => '',

            ]);
            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/voucher-type'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = VoucherType::findOrFail($id);
            $item->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/voucher-type'));
        }
    }
