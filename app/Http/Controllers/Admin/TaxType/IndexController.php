<?php

namespace App\Http\Controllers\Admin\TaxType;

use App\Tax_type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Tax_type::orderBy('id', 'desc')->paginate(10);
        return view('admin.tax_type.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.tax_type.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
        ]);

        $options = Tax_type::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/tax_type'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Tax_type::findOrFail($id);
        return view('admin.tax_type.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Tax_type::findOrFail($id);
        $data = $request->validate([
            'name_ar' => '',
            'name_en' => '',
        ]);


        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/tax_type'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Tax_type::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/tax_type'));
    }
}
