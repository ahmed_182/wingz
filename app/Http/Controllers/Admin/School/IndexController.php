<?php

    namespace App\Http\Controllers\Admin\School;

    use App\School;
    use App\Traits\storeImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use storeImage;
        public function address($id)
        {
            $item = School::findOrFail($id);
            return view('admin.schools.address.show', compact('item'));
        }

        public function index(Request $request)
        {
            $items = School::orderBy('id', 'desc');
            $items = $this->filter($request, $items);
            return view('admin.schools.index', compact('items'));
        }

        public function filter($request, $items)
        {
            if ($request->name)
                $items = $items->where("name_en", 'LIKE', '%' . $request->name . '%')->orWhere("name_ar", 'LIKE', '%' . $request->name . '%');
            if ($request->district_id) {
                $items = $items->where("district_id", $request->district_id);
            }
            $items = $items->orderBy("id", "desc")->paginate(10);
            return $items;
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.schools.create');

        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {

            $data = $request->validate([
                'image' => 'required',
                'name_ar' => 'required',
                'name_en' => 'required',
                'education_type_id' => 'required',
                'address' => 'required',
                'district_id' => 'required',
                'location' => 'required',

            ]);


            if ($request->location) {
                $location = explode(",", $request->location);
                $data["lat"] = $location[0];
                $data["lng"] = $location[1];
            }

            if($request->image){
                $data['image'] = $this->storeImage($request->image);
            }

            School::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/schools'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = School::findOrFail($id);
            return view('admin.schools.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = School::findOrFail($id);
            $data = $request->validate([
                'image' => '',
                'name_ar' => '',
                'name_en' => '',
                'education_type_id' => '',
                'address' => '',
                'district_id' => '',
                'location' => '',

            ]);

            if ($request->location) {
                $location = explode(",", $request->location);
                $data["lat"] = $location[0];
                $data["lng"] = $location[1];
            }

            if($request->image){
                $data['image'] = $this->storeImage($request->image);
            }
            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/schools'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = School::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/schools'));
        }
    }
