<?php

    namespace App\Http\Controllers\Admin\School\Level;

    use App\Level;
    use App\School;
    use App\School_level;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index($school_id)
        {
            $items = School_level::where("school_id", $school_id)->orderBy('id', 'desc')->paginate(10);
            return view('admin.schools.levels.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create($id)
        {
            $school_levels_ids = School_level::where("school_id", $id)->pluck('level_id');

            $school = School::find($id);
            $levels = Level::whereNotIn('id',$school_levels_ids)->get();
            return view('admin.schools.levels.create', compact("school",'levels'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request, $id)
        {
            $data = $request->validate([
                'level_id' => 'required',
            ]);
            $data['school_id'] = $id;
            School_level::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url("/admin/schools/$id/levels"));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
