<?php

namespace App\Http\Controllers\Admin\EducationType;

use App\Education_type;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Education_type::orderBy('id', 'desc')->paginate(10);
        //dd($items);
        return view('admin.education_types.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.education_types.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        $data = $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
        ]);

        $options = Education_type::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/education_types'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Education_type::findOrFail($id);
        return view('admin.education_types.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Education_type::findOrFail($id);
        $data = $request->validate([
            'name_ar' => '',
            'name_en' => '',
        ]);


        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/education_types'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Education_type::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/education_types'));
    }
}
