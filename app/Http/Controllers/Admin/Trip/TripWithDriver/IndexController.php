<?php

    namespace App\Http\Controllers\Admin\Trip\TripWithDriver;

    use App\Child;
    use App\ModulesConst\AssignedStatus;
    use App\ModulesConst\NotificationTyps;
    use App\ModulesConst\TripStatus;
    use App\Order;
    use App\Trip;
    use App\Trip_children;
    use App\User;
    use App\User_assigin_trips;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function index(Request $request)
        {
            $order = Order::findorfail($request->order_id);
            $drivers = $request->driver_id;
            if (!$drivers) {
                session()->flash('danger', trans('language.no_drivers_exist'));
                return back();
            }
            foreach ($drivers as $driver) {
                $data['driver_id'] = $driver;
                $data['school_id'] = $order->school_id;
                $data['status'] = TripStatus::pending;
                $trip = Trip::create($data);
                // create trip child ..
                $child_trip['trip_id'] = $trip->id;
                $child_trip['child_id'] = $order->child_id;
                $child_trip['status_id'] = Trip_children::pending;
                Trip_children::create($child_trip);
                // todo  Send Notification for child Father to show driver account of trip ...
                $User_assigin_trips['user_id'] = $order->user_id;
                $User_assigin_trips['trip_id'] = $trip->id;
                $User_assigin_trips['child_id'] = $order->child_id;
                $User_assigin_trips['status_id'] = AssignedStatus::pending;
                User_assigin_trips::create($User_assigin_trips);
            }
            $child = Child::find($order->child_id);
            $this->thisNotificationHandler($request, $child);
            session()->flash('success', trans('language.show_parent_driver_noti'));
            return redirect(url("/admin/trips"));
        }

        public function thisNotificationHandler($request, $child)
        {
            // Send Notification to
            $body = trans('language.show_parent_driver_noti_api') . "( $child->name )";
            $users = User::where('id', $child->user_id)->pluck("id")->unique();

            $this->notificationHandler(trans("language.appName"), $body, $users, NotificationTyps::assigin_trips, $child->id);

        }
    }
