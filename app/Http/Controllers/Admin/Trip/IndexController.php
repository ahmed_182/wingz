<?php

    namespace App\Http\Controllers\Admin\Trip;

    use App\Child;
    use App\ModulesConst\UserTyps;
    use App\School;
    use App\Traits\apiResponse;
    use App\Trip;
    use App\Trip_children;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {

        public function TripChildren($id)
        {
            $childs_id = Trip_children::where('trip_id', $id)->pluck('child_id');
            $items = Child::whereIn('id', $childs_id)->orderBy('id', 'desc')->paginate(10);
            return view('admin.trips.children.index', compact('items'));
        }


        public function index()
        {
            $items = Trip::orderBy('id', 'desc')->paginate(10);
            return view('admin.trips.index', compact('items'));
        }

        public function create()
        {
            $schools = School::get();
            $drivers = User::Verified()->where('user_type_id', UserTyps::driver)->get();
            return view('admin.trips.create', compact('schools', 'drivers'));
        }

        public function store(Request $request)
        {
            $data = $request->validate([
                'school_id' => 'required',
                'driver_id' => 'required',
            ]);

            $checkIfThisDriverHaveCurrentTripWithThisSchool =
                Trip::where('school_id', $request->school_id)->where('driver_id', $request->driver_id)->first();
            if ($checkIfThisDriverHaveCurrentTripWithThisSchool) {
                session()->flash('error', trans('language.driverAlreadyHasTripWithThisSchool'));
                return back();
            }
            Trip::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/trips'));
        }

        public function destroy($id)
        {
            $item = Trip::findOrFail($id);
            $item->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/trips'));
        }
    }
