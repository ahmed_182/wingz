<?php

namespace App\Http\Controllers\Admin\Trip\meetingRequest;

use App\ModulesConst\AssignedStatus;
use App\User_assigin_trips;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index(Request $request)
    {
        $items = User_assigin_trips::where('status_id', AssignedStatus::meeting)->orderBy('id', 'desc')->paginate(10);
        return view('admin.meeting_requests.index', compact('items'));
    }
}
