<?php

    namespace App\Http\Controllers\Admin\Review;

    use App\User_review;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function index($id)
        {
            $items = User_review::where('user_id', $id)->orderBy('id', 'desc')->paginate(10);
            return view('admin.reviews.index', compact('items'));
        }
    }
