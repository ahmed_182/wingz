<?php

    namespace App\Http\Controllers\Admin\User;

    use App\Brand;
    use App\Family_members;
    use App\Traits\storeImage;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class userFamilyMembersController extends Controller
    {
        use storeImage;

        public function index($id)
        {
            $user = User::find($id);
            return view('admin.users.family.create', compact("user"));
        }

        public function store(Request $request, $user_id)
        {
            $data = $request->validate([
                'user_relation_id' => 'required',
                'image' => 'required',
                'name' => 'required',
                'email' => 'required',
                'mobile' => 'required',
            ]);
            if ($request['image']) {
                $data['image'] = $this->storeImage($request['image']);
            }

            $member = User::create($data);
            $info['user_id'] = $user_id;
            $info['member_id'] = $member->id;
            Family_members::create($info);
            session()->flash('success', trans('language.done'));
            return redirect(url("/admin/familyMembers/$user_id"));
        }
    }
