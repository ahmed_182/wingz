<?php

namespace App\Http\Controllers\Admin\User;

use App\Assigned_driver;
use App\Child;
use App\Client;
use App\Family_members;
use App\Favorite_places;
use App\ModulesConst\is_block;
use App\ModulesConst\UserTyps;
use App\ModulesConst\UserVerify;
use App\Order;
use App\Point_history;
use App\Traits\storeImage;
use App\Trip;
use App\Trip_children;
use App\User;
use App\User_notification;
use App\UserVoucher;
use Hash;
use DB;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use storeImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = User::where('user_type_id', UserTyps::user);
        $items = $this->filter($request, $items);
        return view('admin.users.index', compact('items'));

    }

    public function filter($request, $items)
    {

        if ($request->no_members) {
            $members_users_id = Family_members::pluck("user_id")->toArray();
            $items = $items->whereIn("id", $members_users_id);
        }
        if ($request->is_not_approved == 1) {
            $items = $items->where("userVerify", UserVerify::no);
        }

        if ($request->name)
            $items = $items->where("name", 'LIKE', '%' . $request->name . '%');
        if ($request->email) {
            $items = $items->where("email", 'LIKE', '%' . $request->email . '%');
        }
        if ($request->phone) {
            $items = $items->where("mobile", 'LIKE', '%' . $request->phone . '%');
        }


        if ($request->userVerify and $request->userVerify != 3) {
            if ($request->userVerify == 1) {
                $items = $items->where("userVerify", 1);
            } else {
                $items = $items->where("userVerify", 0);
            }

        }


        if ($request->is_block and $request->userVerify != 3) {
            if ($request->is_block == 1) {
                $items = $items->where("is_block", 1);
            } else {
                $items = $items->where("is_block", 0);
            }

        }

        $items = $items->orderBy("id", "desc")->paginate(10);
        return $items;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.users.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'email' => 'required|email|unique:users',
            'name' => 'required',
            'password' => 'required|min:6|confirmed',
            'password_confirmation' => 'required|min:6',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:2048',
        ]);
        $data['password'] = Hash::make($data['password']);
        $data['user_type_id'] = UserTyps::user;
        if ($request->image) {
            $data['image'] = $this->storeImage($request->image);
        }
        $user = User::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/users'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = User::findOrFail($id);
        return view('admin.users.edit', compact('item'));
    }

    public function block($id)
    {
        $item = User::findOrFail($id);
        return view('admin.users.block', compact('item'));
    }

    public function deblock($id)
    {
        $user = User::find($id);
        $user->is_block = is_block::no;
        $user->block_reason = null;
        $user->save();
        // Send Notifiction to user to inform him that his account has been activated ..
        $user_ids = User::where('id', $user->id)->pluck('id');
        $body = trans('language.admin_block_you_account');
        $this->notificationHandler(trans("language.appName"), $body, $user_ids);
        session()->flash('success', trans('language.done'));
        return back();
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = User::findOrFail($id);
        $data = $request->validate([
            'email' => 'email',
            'name' => 'string',
            'block_reason' => 'string',
            'image' => 'mimes:jpeg,jpg,png,gif|max:2048',
        ]);

        if ($request->password) {
            $data['password'] = Hash::make($data['password']);
        }
        if ($request->image) {
            $data['image'] = $this->storeImage($data['image']);
        }
        if ($request->block_reason) {
            $item->is_block = is_block::yes;

        }
        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/users'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = User::findOrFail($id)->delete();
        Child::where('user_id', $id)->delete();
        Family_members::where('user_id', $id)->delete();
        Favorite_places::where('user_id', $id)->delete();
        Order::where('user_id', $id)->delete();
        User_notification::where('user_id', $id)->delete();
        Assigned_driver::where('user_id', $id)->delete();
        Point_history::where('user_id', $id)->delete();
        UserVoucher::where('user_id', $id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/users'));
    }
}
