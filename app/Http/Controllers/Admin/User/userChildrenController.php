<?php

    namespace App\Http\Controllers\Admin\User;

    use App\Child;
    use App\School_level;
    use App\School_time;
    use App\Stage;
    use App\Traits\storeFile;
    use App\Traits\storeImage;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class userChildrenController extends Controller
    {
        use storeImage;

        public function index($id)
        {
            $items = Child::where('user_id', $id)->orderBy('id', 'desc')->paginate(10);
            return view('admin.users.children.index', compact('items'));
        }

        public function create($id)
        {
            $user = User::find($id);
            return view('admin.users.children.create', compact("user"));

        }

        public function store(Request $request, $id)
        {
            $data = $request->validate([
                'name' => 'required',
                'mobile' => 'required',
                'birthday' => 'required',
                'image' => 'required',
                'school_id' => 'required',
                'level_id' => 'required',
                'stage_id' => 'required',
            ]);
            $data['user_id'] = $id;
            if ($request->image) {
                $data['image'] = $this->storeImage($request->image);
            }
            $school_level = School_level::where('level_id', $request->level_id)->where('school_id', $request->school_id)->first();
            if (!$school_level) {
                session()->flash('success', trans('language.no_school_level'));
                return redirect(url("/admin/users/$id/children"));
            }
            $school_time = School_time::where('stage_id', $request->stage_id)->where('school_level_id', $school_level->id)->first();
            if (!$school_time) {
                session()->flash('success', trans('language.no_school_time'));
                return redirect(url("/admin/users/$id/children"));
            }
            $data['school_time_id'] = $school_time->id;
            Child::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url("/admin/users/$id/children"));
        }


        public function destroy($user_id, $id)
        {
            $item = Child::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url("/admin/users/$user_id/children"));
        }
    }
