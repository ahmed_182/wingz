<?php

    namespace App\Http\Controllers\Admin\User;

    use App\Single_ride;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class userSingleRideController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index($user_id)
        {
            $pending_rides = Single_ride::where('client_id', $user_id)->where('order_status_id', Single_ride::clientAddOrder)->orderBy('id', 'desc')->paginate(10);
            $accepted_rides = Single_ride::where('client_id', $user_id)->where('order_status_id', Single_ride::driverAcceptOrder)->orderBy('id', 'desc')->paginate(1);
            $rejected_rides = Single_ride::where('client_id', $user_id)->where('order_status_id', Single_ride::driverRejectOrder)->orderBy('id', 'desc')->paginate(10);
            $user_cancelled_rides = Single_ride::where('client_id', $user_id)->where('order_status_id', Single_ride::clientCancelOrder)->orderBy('id', 'desc')->paginate(10);
            $driver_cancelled_rides = Single_ride::where('client_id', $user_id)->where('order_status_id', Single_ride::driverCancelOrder)->orderBy('id', 'desc')->paginate(10);
            $admin_cancelled_rides = Single_ride::where('client_id', $user_id)->where('order_status_id', Single_ride::adminCancelOrder)->orderBy('id', 'desc')->paginate(10);
            $compeleted_rides = Single_ride::where('client_id', $user_id)->where('order_status_id', Single_ride::driverCompleteOrder)->orderBy('id', 'desc')->paginate(10);

            return view('admin.users.singleRides.index', compact('pending_rides', 'accepted_rides', 'rejected_rides', 'user_cancelled_rides', 'driver_cancelled_rides', 'admin_cancelled_rides', 'compeleted_rides'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            //
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            //
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
