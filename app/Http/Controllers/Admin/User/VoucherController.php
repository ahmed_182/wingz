<?php

namespace App\Http\Controllers\Admin\User;

use App\UserVoucher;
use App\Voucher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VoucherController extends Controller
{
    public function index(Request $request , $id){

        $ids = UserVoucher::where('user_id',$id)->pluck('voucher_id')->unique();
        $items = Voucher::whereIn('id', $ids)->orderBy('id', 'desc')->paginate(10);
        return view('admin.users.vouchers.index', compact('items'));
    }
}
