<?php

namespace App\Http\Controllers\Admin\User\Family;

use App\Child;
use App\Family_members;
use App\ModulesConst\AssignedStatus;
use App\ModulesConst\Paginate;
use App\User;
use App\User_assigin_trips;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index($id)
    {
        $user = User::find($id);
        $family_members_ids = Family_members::where('user_id', $id)->pluck('member_id');
        $family_members = User::whereIn('id', $family_members_ids)->get();
        $children = Child::where('user_id', $id)->orderBy('id', 'desc')->get();
        return view('admin.users.family.index', compact('family_members', 'children', 'user'));
    }

    public function users_not_answer_drivers_request()
    {
        $items = User_assigin_trips::where('status_id', AssignedStatus::pending)->orderBy('id', 'desc')->paginate(30);
        return view('admin.users.not_answer_drivers', compact('items'));
    }
}
