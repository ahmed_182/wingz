<?php

namespace App\Http\Controllers\Admin\Voucher;


use App\Voucher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Voucher::orderBy('id', 'desc')->paginate(10);
        //dd($items);
        return view('admin.voucher.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.voucher.create');

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'type_id' => 'required',
            'code' => 'required',
            'description' => 'required',
            'discount' => 'required',
            'maximum_discount' => 'required',

            'max_uses' => 'required',
            'max_uses_user' => 'required',
            'starts_at' => 'required',
            'expires_at' => 'required',
        ]);
        $data["uses"] = 0 ;
        $voucher = Voucher::create($data);
        session()->flash('success', trans('language.done'));

        return redirect(url('/admin/voucher'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Voucher::findOrFail($id);
        return view('admin.voucher.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Voucher::findOrFail($id);
        return view('admin.voucher.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Voucher::findOrFail($id);
        $data = $request->validate([
            'type_id' => '',
            'code' => '',
            'description' => '',
            'discount' => '',
            'maximum_discount' => '',
            'uses' => '',
            'max_uses' => '',
            'max_uses_user' => '',
            'starts_at' => '',
            'expires_at' => '',

        ]);

        $item->update($data);
        session()->flash('success', trans('language.done'));

        return redirect(url('/admin/voucher'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Voucher::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));

        return redirect(url('/admin/voucher'));
    }
}
