<?php

    namespace App\Http\Controllers\Admin\PaymentType;

    use App\Payment_types;
    use App\Traits\storeImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use storeImage;

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $items = Payment_types::orderBy('id', 'desc')->paginate(10);
            return view('admin.payment_types.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()

        {

            return view('admin.payment_types.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $data = $request->validate([

                'name_ar' => 'required',
                'name_en' => 'required',
                'image' => 'mimes:jpeg,jpg,png,gif|required|max:2048',
            ]);
            if ($request['image']) {
                $data['image'] = $this->storeImage($request['image']);
            }
            $item = Payment_types::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/payment_types'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Payment_types::findOrFail($id);
            return view('admin.payment_types.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Payment_types::findOrFail($id);
            $data = $request->validate([
                'name_ar' => '',
                'name_en' => '',
                'image' => '',
            ]);


            if ($request->image) {
                $data['image'] = $this->storeImage($data['image']);
            }
            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/payment_types'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Payment_types::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/payment_types'));
        }
    }
