<?php

    namespace App\Http\Controllers\Admin\CarName;

    use App\Car_name;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $items = Car_name::orderBy('id', 'desc')->paginate(10);
            return view('admin.car_names.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.car_names.create');

        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {

            $data = $request->validate([

                'name_ar' => 'required',
                'name_en' => 'required',
            ]);

            $options = Car_name::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/car_names'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Car_name::findOrFail($id);
            return view('admin.car_names.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Car_name::findOrFail($id);
            $data = $request->validate([
                'name_ar' => '',
                'name_en' => '',
            ]);


            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/car_names'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Car_name::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/car_names'));
        }
    }
