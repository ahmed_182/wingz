<?php

    namespace App\Http\Controllers\Admin\Stage;

    use App\Level;
    use App\Stage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $items = Stage::orderBy('id', 'desc')->paginate(10);
            return view('admin.stages.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.stages.create');

        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {

            $data = $request->validate([
                'level_id' => 'required',
                'name_ar' => 'required',
                'name_en' => 'required',
            ]);

            $options = Stage::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/stages'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Stage::findOrFail($id);
            return view('admin.stages.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Stage::findOrFail($id);
            $data = $request->validate([
                'level_id' => '',
                'name_ar' => '',
                'name_en' => '',
            ]);


            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/stages'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Stage::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/stages'));
        }
    }
