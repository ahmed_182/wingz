<?php

    namespace App\Http\Controllers\Admin\Slider;

    use App\ModulesConst\SliderType;
    use App\Slider;
    use App\Traits\storeImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use storeImage;

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $items_user = Slider::where('type', SliderType::user)->orderBy('id', 'desc')->get();
            $items_driver = Slider::where('type', SliderType::driver)->orderBy('id', 'desc')->get();
            return view('admin.sliders.index', compact('items_driver', 'items_user'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()

        {

            return view('admin.sliders.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $data = $request->validate([

                'type' => 'required',
                'link' => 'required',
                'image' => 'mimes:jpeg,jpg,png,gif|required|max:2048',
            ]);
            if ($request['image']) {
                $data['image'] = $this->storeImage($request['image']);
            }
            $item = Slider::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/sliders'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Slider::findOrFail($id);
            return view('admin.sliders.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Slider::findOrFail($id);
            $data = $request->validate([
                'type' => '',
                'link' => '',
                'image' => '',
            ]);


            if ($request->image) {
                $data['image'] = $this->storeImage($data['image']);
            }
            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/sliders'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Slider::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/sliders'));
        }
    }
