<?php

namespace App\Http\Controllers\Admin\Slider;

use App\Slider_user;
use App\Traits\storeImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class UserSliderController extends Controller
{
    use storeImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Slider_user::orderBy('id', 'desc')->paginate(10);
        return view('admin.user_slider.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {

        return view('admin.user_slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([

            'link' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:2048',
        ]);
        if ($request['image']) {
            $data['image'] = $this->storeImage($request['image']);
        }
        $item = Slider_user::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/user_slider'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Slider_user::findOrFail($id);
        return view('admin.user_slider.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Slider_user::findOrFail($id);
        $data = $request->validate([
            'link' => '',
            'image' => '',
        ]);


        if ($request->image) {
            $data['image'] = $this->storeImage($data['image']);
        }
        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/user_slider'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Slider_user::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/user_slider'));
    }
}
