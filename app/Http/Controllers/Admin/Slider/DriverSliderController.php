<?php

namespace App\Http\Controllers\Admin\Slider;

use App\Slider_driver;
use App\Traits\storeImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class DriverSliderController extends Controller
{
    use storeImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Slider_driver::orderBy('id', 'desc')->paginate(10);
        return view('admin.driver_slider.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {

        return view('admin.driver_slider.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([

            'link' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:2048',
        ]);
        if ($request['image']) {
            $data['image'] = $this->storeImage($request['image']);
        }
        $item = Slider_driver::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/driver_slider'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Slider_driver::findOrFail($id);
        return view('admin.driver_slider.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Slider_driver::findOrFail($id);
        $data = $request->validate([
            'link' => '',
            'image' => '',
        ]);


        if ($request->image) {
            $data['image'] = $this->storeImage($data['image']);
        }
        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/driver_slider'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Slider_driver::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/driver_slider'));
    }
}
