<?php

    namespace App\Http\Controllers\Admin\VoucherPlans;

    use App\Plan;
    use App\Voucher;
    use App\Voucher_plan;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index($id)
        {
            $items = Voucher_plan::where('voucher_id', $id)->orderBy('id', 'desc')->paginate(10);
            $voucher_id = $id;
            $voucher_ids = Voucher_plan::pluck('plan_id');
            $plans = Plan::whereNotIn('id', $voucher_ids)->count();
            return view('admin.voucher.plans.index', compact('items', 'voucher_id', 'plans'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create($voucher_id)
        {
            $voucher = Voucher::findorfail($voucher_id);
            $voucher_ids = Voucher_plan::where("voucher_id", $voucher->id)->pluck('plan_id');
            $plans = Plan::whereNotIn('id', $voucher_ids)->get();
            return view('admin.voucher.plans.create', compact('voucher', 'plans'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request, $voucher_id)
        {
            $data = $request->validate([
                'plan_id' => ['required'],
            ]);
            foreach ($request->plan_id as $cat) {
                $data["plan_id"] = $cat;
                $data["voucher_id"] = $voucher_id;
                Voucher_plan::create($data);
            }
            session()->flash('success', trans('language.done'));
            return redirect("admin/voucher/$voucher_id/plans");
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($voucher_id,$id)
        {
            $item = Voucher_plan::findOrFail($id);
            $item->delete();
            session()->flash('success', trans('language.done'));
            return redirect("admin/voucher/$voucher_id/plans");
        }
    }
