<?php

namespace App\Http\Controllers\Admin\Country\StartWith;

use App\City;
use App\Country;
use App\Country_startwith;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($country_id)
    {
        $items = Country_startwith::where("country_id", $country_id)->orderBy('id', 'desc')->paginate(10);
        return view('admin.countries.start_with.index', compact('items', 'country_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($country_id)
    {
        $country = Country::find($country_id);
        return view('admin.countries.start_with.create', compact('country'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($id, Request $request)
    {
        $data = $request->validate([
            'start_with' => 'required',
        ]);
        $data['country_id'] = $id;
        Country_startwith::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url("/admin/countries/$id/start_with"));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($country_id, $id)
    {
        $item = Country_startwith::find($id);
        return view('admin.countries.start_with.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $country_id, $id)
    {

        $item = Country_startwith::findorfail($id);
        $data = $request->validate([
            'start_with' => '',
        ]);
        $item->update(['start_with' => $request->start_with]);
        session()->flash('success', trans('language.done'));
        return redirect(url("/admin/countries/$country_id/start_with"));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($country_id, $id)
    {
        $item = Country_startwith::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
            return redirect(url("/admin/countries/$country_id/start_with"));
    }
}
