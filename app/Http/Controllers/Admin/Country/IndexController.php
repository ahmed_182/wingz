<?php

    namespace App\Http\Controllers\Admin\Country;

    use App\City;
    use App\Country;
    use App\Traits\storeImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use storeImage;


        public function index()
        {
            $items = Country::orderBy('id', 'desc')->paginate(10);
            return view('admin.countries.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.countries.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $data = $request->validate([
                'name_ar' => 'required',
                'name_en' => 'required',
                'currency_ar' => 'required',
                'currency_en' => 'required',
                'code' => 'required',
                'start_with' => 'required',
                'number_count' => 'required',
                'image' => 'mimes:jpeg,jpg,png,gif|required|max:2048',
            ]);
            if ($data['image']) {
                $data['image'] = $this->storeImage($data['image']);
            }

            Country::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/countries'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Country::findOrFail($id);
            return view('admin.countries.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {

            $item = Country::findOrFail($id);
            $data = $request->validate([
                'name_ar' => '',
                'name_en' => '',
                'currency_ar' => '',
                'currency_en' => '',
                'code' => '',
                'start_with' => '',
                'number_count' => '',
                'image' => '',
            ]);
            if ($request->image) {
                $data['image'] = $this->storeImage($data['image']);
            }
            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/countries'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            Country::findOrFail($id)->delete();
            City::where('country_id', $id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/countries'));
        }
    }
