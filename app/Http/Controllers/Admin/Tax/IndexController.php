<?php

    namespace App\Http\Controllers\Admin\Tax;

    use App\District;
    use App\Tax;
    use App\Terms;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $items = Tax::orderBy('id', 'desc')->paginate(10);
            return view('admin.taxes.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.taxes.create');

        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $data = $request->validate([
                'name_ar' => 'required',
                'name_en' => 'required',
                'value' => 'required',
                'is_enable' => 'required',
                'tax_type_id' => 'required',
                'type' => 'required',
            ]);
            Tax::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/taxes'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Tax::findOrFail($id);
            return view('admin.taxes.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Tax::findOrFail($id);
            $data = $request->validate([
                'name_ar' => '',
                'name_en' => '',
                'value' => '',
                'is_enable' => '',
                'tax_type_id' => '',
                'type' => '',
            ]);


            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/taxes'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Tax::findOrFail($id);
            $item->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/taxes'));
        }
    }
