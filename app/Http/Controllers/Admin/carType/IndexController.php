<?php

    namespace App\Http\Controllers\Admin\carType;

    use App\Car_type;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $items = Car_type::orderBy('id', 'desc')->paginate(10);
            //dd($items);
            return view('admin.car_types.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.car_types.create');

        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {

            $data = $request->validate([

                'name_ar' => 'required',
                'name_en' => 'required',
                'seat_number' => 'required',
            ]);

            $options = Car_type::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/car_types'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Car_type::findOrFail($id);
            return view('admin.car_types.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Car_type::findOrFail($id);
            $data = $request->validate([
                'name_ar' => '',
                'name_en' => '',
                'seat_number' => '',
            ]);


            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/car_types'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Car_type::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/car_types'));
        }
    }
