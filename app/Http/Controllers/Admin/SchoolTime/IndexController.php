<?php

    namespace App\Http\Controllers\Admin\SchoolTime;

    use App\School;
    use App\School_level;
    use App\School_time;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {

        public function index($school_level_id)
        {
            $items = School_time::where("school_level_id", $school_level_id)->orderBy('id', 'desc')->paginate(10);
            return view('admin.schools.school_times.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create($id)
        {
            $school_level = School_level::find($id);
            return view('admin.schools.school_times.create', compact("school_level"));

        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request, $id)
        {

            $data = $request->validate([
                'stage_id' => 'required',
                'start_at' => 'required',
                'end_at' => 'required',
                'year_start_at' => 'required',
                'year_end_at' => 'required',
            ]);
            $data['school_level_id'] = $id;
            School_time::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url("/admin/schoolLevels/$id/school_times"));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($schoo_id, $id)
        {

            $item = School_time::findOrFail($id);
            return view('admin.schools.school_times.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $Sc_id, $id)
        {
            $item = School_time::findOrFail($id);
            $data = $request->validate([
                'stage_id' => '',
                'start_at' => '',
                'end_at' => '',
                'year_start_at' => '',
                'year_end_at' => '',
            ]);

            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url("/admin/schoolLevels/$Sc_id/school_times"));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($Sc_id, $id)
        {
            $item = School_time::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url("/admin/schoolLevels/$Sc_id/school_times"));
        }
    }
