<?php

namespace App\Http\Controllers\Admin\District;

use App\District;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */


    public function index()
    {
        $items = District::orderBy('id', 'desc')->paginate(10);
        return view('admin.districts.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.districts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name_ar' => 'required',
            'name_en' => 'required',
            'city_id' => 'required',
            'location' => 'required',
        ]);
        if ($request->location) {
            $location = explode(",", $request->location);
            $data["lat"] = $location[0];
            $data["lng"] = $location[1];
        }
        District::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/districts'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = District::findOrFail($id);
        return view('admin.districts.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {

        $item = District::findOrFail($id);
        $data = $request->validate([
            'name_ar' => '',
            'name_en' => '',
            'city_id' => '',
            'location' => '',
        ]);
        if ($request->location) {
            $location = explode(",", $request->location);
            $data["lat"] = $location[0];
            $data["lng"] = $location[1];
        }
        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/districts'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = District::findOrFail($id);
        $item->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/districts'));
    }
}
