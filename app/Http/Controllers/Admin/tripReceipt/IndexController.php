<?php

namespace App\Http\Controllers\Admin\tripReceipt;

use App\Level;
use App\ModulesConst\UserTyps;
use App\Trip_order_receipt;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Trip_order_receipt::orderBy('id', 'desc')->paginate(10);
        return view('admin.tripReceipt.index', compact('items'));
    }

    public function applyRepresentative($receipt_id)
    {
        $items = User::where('user_type_id', UserTyps::representative)->get();
        $receipt = Trip_order_receipt::find($receipt_id);
        return view('admin.tripReceipt.apply', compact('items', 'receipt'));
    }

    public function saveApplyRepresentative(Request $request)
    {
        $request->validate([
            'receipt_id' => 'required',
            'representative_id' => 'required',
        ]);
        $receipt = Trip_order_receipt::find($request->receipt_id);
        $receipt->update(['representative_id' => $request->representative_id]);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/tripReceipt'));
    }

}
