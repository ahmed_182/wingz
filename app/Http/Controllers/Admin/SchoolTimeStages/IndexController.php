<?php

    namespace App\Http\Controllers\Admin\SchoolTimeStages;

    use App\School_time;
    use App\Stage_holiday;
    use App\Stage_holiday_period;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index($school_time_satge_id)
        {
            $items = Stage_holiday_period::where("school_time_stage_id", $school_time_satge_id)->orderBy('id', 'desc')->paginate(10);
            return view('admin.schools.stage_holiday.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create($id)
        {
            $school_time = School_time::find($id);
            return view('admin.schools.stage_holiday.create', compact("school_time"));

        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request, $id)
        {

            $data = $request->validate([
                'start_at' => 'required',
                'end_at' => 'required',
                'paid_status' => 'required',
            ]);

            $start_at_long = strtotime($request->start_at) * 1000;
            $end_at_long = strtotime($request->end_at) * 1000;

            // Store times in stage holiday perios
            $peroid['school_time_stage_id'] = $id;
            $peroid['start_at'] = $request->start_at;
            $peroid['end_at'] = $request->end_at;
            $peroid['paid_status'] = $request->paid_status;
            $peroid_data = Stage_holiday_period::create($peroid);

            // Add the Final Day ( end at )
            $days_long[] = $end_at_long;
            // Add Other Days ..
            for ($i = $end_at_long; $i > $start_at_long; $i) {

                // sub one day every loop
                $one_day = 24 * 60 * 60 * 1000;
                $new_time = $i - $one_day;
                $days_long[] = $new_time;

                $i = $i - $one_day;
            }

            // Store This Days in ( Stage Holiday Table )
            foreach ($days_long as $day) {
                $info['stage_holiday_period_id'] = $peroid_data->id;
                $info['school_time_stage_id'] = $id;
                $info['day_long'] = $day;
                $info['day_stamp'] = date("d-m-Y", $day / 1000);
                $info['paid_status'] = $request->paid_status;
                Stage_holiday::create($info);
            }
            session()->flash('success', trans('language.done'));
            return redirect(url("/admin/school_times_stages/$id/holidays"));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id , $holiday_id)
        {
            Stage_holiday_period::find($holiday_id)->delete();
            Stage_holiday::where('stage_holiday_period_id',$holiday_id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url("/admin/school_times_stages/$id/holidays"));
        }
    }
