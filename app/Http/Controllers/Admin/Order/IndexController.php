<?php

namespace App\Http\Controllers\Admin\Order;

use App\Child;
use App\ModulesConst\NotificationTyps;
use App\ModulesConst\OrderStatus;
use App\ModulesConst\Paginate;
use App\Order;
use App\Trip;
use App\User;
use App\User_assigin_trips;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items_pending = Order::orderBy('id', 'desc')->where('status', OrderStatus::pending);
        $items_pending = $this->filter($request, $items_pending);

        $items_rejected = Order::orderBy('id', 'desc')->where('status', OrderStatus::rejected);
        $items_rejected = $this->filter($request, $items_rejected);


//            $items_accepted = Order::orderBy('id', 'desc')->where('status', OrderStatus::accepted)->paginate(10);

        return view('admin.orders.index', compact('items_pending', 'items_rejected'));
    }


    public function filter($request, $items)
    {
        if ($request->status)
            $items = $items->where("status", $request->status);

        if ($request->payment_status)
            $items = $items->where("payment_status", $request->payment_status);

        $items = $items->orderBy("id", "desc")->paginate(10);
        return $items;
    }


    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $item = Order::findOrFail($id);
        return view('admin.orders.show', compact('item'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = Order::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/orders'));
    }

    public function reject($id)
    {
        $item = Order::find($id);
        return view('admin.orders.rejectPage', compact('item'));
    }

    public function doReject(Request $request, $id)
    {
        $request->validate([
            'rejected_reason' => 'required'
        ]);

        $item = Order::find($id);
        $item->rejected_reason = $request->rejected_reason;
        $item->status = OrderStatus::rejected;
        $item->save();

        // send notification to order user ( to explain whh his order rejected .. )
        $this->thisNotificationHandler($request, $item->user_id, $item);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/orders'));
    }

    public function thisNotificationHandler($request, $user_id, $item)
    {
        // Send Notification to
        $child = Child::find($item->child_id);
        $body = trans('language.about_child_name') . " ( $child->name ) , " . $request->rejected_reason;
        $users = User::where('id', $user_id)->pluck("id")->unique();
        $this->notificationHandler(trans("language.appName"), $body, $users, NotificationTyps::reject_request, $user_id);

    }

    // responded
    public function responded($id)
    {
        $order = Order::find($id);
        $user = User::find($order->user_id);
        $child = Child::find($order->child_id);
        $items = User_assigin_trips::where('child_id', $child->id)->paginate(Paginate::value);
        return view('admin.drivers.orders.responded', compact('items'));
    }
}
