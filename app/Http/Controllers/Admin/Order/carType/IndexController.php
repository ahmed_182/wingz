<?php

    namespace App\Http\Controllers\Admin\Order\carType;

    use App\ModulesConst\Paginate;
    use App\Order_car_type;
    use App\Traits\storeImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        use storeImage;

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $items = Order_car_type::orderBy('id', 'desc')->paginate(Paginate::value);
            return view('admin.orders.car_types.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()
        {
            return view('admin.orders.car_types.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $data = $request->validate([
                'image' => 'required',
                'name_ar' => 'required',
                'name_en' => 'required',
                'description_ar' => 'required',
                'description_en' => 'required',
                'price_per_kilo' => 'required',
                'price_per_kilo_after_school' => 'required',
            ]);

            if ($request['image']) {
                $data['image'] = $this->storeImage($request['image']);
            }

            $options = Order_car_type::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/order_car_types'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Order_car_type::findOrFail($id);
            return view('admin.orders.car_types.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Order_car_type::findOrFail($id);
            $data = $request->validate([
                'image' => '',
                'name_ar' => '',
                'name_en' => '',
                'description_ar' => '',
                'description_en' => '',
                'price_per_kilo' => '',
                'price_per_kilo_after_school' => '',
            ]);
            if ($request['image']) {
                $data['image'] = $this->storeImage($request['image']);
            }

            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/order_car_types'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Order_car_type::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/order_car_types'));
        }
    }
