<?php

    namespace App\Http\Controllers\Admin\SalaryPackage;

    use App\Salary_package;
    use App\Traits\storeImage;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        // salary_packages
        use storeImage;

        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index()
        {
            $items = Salary_package::orderBy('id', 'desc')->paginate(10);
            return view('admin.salary_packages.index', compact('items'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create()

        {

            return view('admin.salary_packages.create');
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request)
        {
            $data = $request->validate([

                'name_ar' => 'required',
                'name_en' => 'required',
                'description_ar' => 'required',
                'description_en' => 'required',
                'value' => 'required',
                'image' => 'mimes:jpeg,jpg,png,gif|required|max:2048',
            ]);
            if ($request['image']) {
                $data['image'] = $this->storeImage($request['image']);
            }
            $item = Salary_package::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/salary_packages'));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            $item = Salary_package::findOrFail($id);
            return view('admin.salary_packages.edit', compact('item'));
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            $item = Salary_package::findOrFail($id);
            $data = $request->validate([
                'name_ar' => '',
                'name_en' => '',
                'description_ar' => '',
                'description_en' => '',
                'value' => '',
                'image' => '',
            ]);


            if ($request->image) {
                $data['image'] = $this->storeImage($data['image']);
            }
            $item->update($data);
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/salary_packages'));
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            $item = Salary_package::findOrFail($id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url('/admin/salary_packages'));
        }
    }
