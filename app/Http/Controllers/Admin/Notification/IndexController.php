<?php

namespace App\Http\Controllers\Admin\Notification;


use App\City;
use App\Country;
use App\District;
use App\Driver;
use App\Driver_districts;
use App\ModulesConst\UserOnlineStatus;
use App\ModulesConst\UserPaidTyps;
use App\ModulesConst\UserTyps;
use App\Notification;
use App\User;
use App\User_notification;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    public function index()
    {
        $clients = User::where('user_type_id', UserTyps::user)->orderBy('id', 'desc')->paginate(10);
        $drivers = User::where('user_type_id', UserTyps::driver)->orderBy('id', 'desc')->get();
        $countries = Country::orderBy('id', 'desc')->get();
        $cities = City::orderBy('id', 'desc')->get();

        return view('admin.notifications.index', compact('clients', 'drivers', 'countries', 'cities'));
    }

    public function store(Request $request)
    {

        $request->validate([
            'title' => 'required|string',
            'body' => 'required|string',
        ]);
        $this->clientsTypeHandler($request);
        session()->flash('success', trans('language.SendNotifMessage'));
        return back();
    }

    public function clientsTypeHandler($request)
    {

        //users
        if ($request->users) {
            $ids = User::whereIn('id', $request->users)->pluck('id');
            $this->clientsHandler($ids, $request);
        }

        // drivers
        if ($request->drivers) {
            $ids = User::whereIn('id', $request->drivers)->pluck('id');
            $this->clientsHandler($ids, $request);
        }
        // countries
        if ($request->countries) {
            $ids = User::whereIn('country_id', $request->countries)->pluck('id');
            $this->clientsHandler($ids, $request);
        }
        // cities
        if ($request->cities) {
            $districts = District::whereIn('city_id', $request->cities)->pluck('id');
            $drivers = Driver_districts::whereIn('district_id', $districts)->pluck('driver_id');
            $ids = User::whereIn('id', $drivers)->pluck('id');
            $this->clientsHandler($ids, $request);
        }


    }

    public function clientsHandler($users, $request)
    {

        $users_ids = $users;
        $tokens = User::whereIn("id", $users_ids)
            ->where('fire_base_token', "!=", null)
            ->pluck('fire_base_token')->toArray();

        $data["title"] = $request->title;
        $data["body"] = $request->body;
        $data["type"] = "admin_notification";
        $data["item_id"] = "1";

        if (count($tokens) > 0) {
            $this->push_notification($tokens, $data);
        }
        $this->mySqlHandler($data, $users_ids);
    }


    public function mySqlHandler($data, $users_ids)
    {

        $notification = Notification::create($data);
        foreach ($users_ids as $user_id) {
            $data["user_id"] = $user_id;
            $data["notification_id"] = $notification->id;
            User_notification::create($data);
        }
    }
}
