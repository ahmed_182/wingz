<?php

    namespace App\Http\Controllers\Admin\Commissary;

    use App\Commissary;
    use App\ModulesConst\CommissaryStatus;
    use App\ModulesConst\PaymentStatus;
    use App\Point;
    use App\Point_history;
    use App\User;
    use App\User_wallet;
    use Auth;
    use Carbon\Carbon;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        public function ConfirmCommissary($id)
        {
            $item = Commissary::find($id);
            $item->confirm_date = Carbon::parse(now());
            $item->save();

            // user object
            $user = User::find($item->user_id);
            $wallet_info['user_id'] = $user->id;
            $wallet_info['value'] = $item->amount;
            $wallet_info['reason'] = $item->serv_commissary_reason;
            $wallet_info['status'] = CommissaryStatus::add;
            User_wallet::create($wallet_info);

            // user History Points
            $history_points['user_id'] = $user->id;
            $history_points['commissary_id'] = $item->id;

            //convert money to points
            $point_price = Point::latest()->first();
            if ($point_price) {
                $point_price_value = $point_price->price;
                $total_points = ($item->amount / $point_price_value);
            } else {
                $total_points = 0;
            }

            $history_points['points'] = $total_points;
            $history_points['points_money_value'] = $item->amount;
            $history_points['points_value_id'] = $point_price->id;
            $history_points['reason'] = $item->serv_commissary_reason;
            Point_history::create($history_points);

            session()->flash('success', trans('language.done'));
            return back();
        }

        public function index($id)
        {
            $user_id = $id;
            $items = Commissary::where('user_id', $id)->orderBy('id', 'desc')->paginate(10);
            return view('admin.commissaries.index', compact('items', 'user_id'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create(Request $request, $user_id)
        {
            $user = User::find($user_id);
            return view('admin.commissaries.crate', compact('user'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request, $user_id)
        {
            $data = $request->validate([
                'amount' => 'required',
                'request_date' => 'required',
                'commissary_reason_id' => 'required',
            ]);
            $data['user_id'] = $user_id;
            $commissary = Commissary::create($data);
            // confirm and store in wallet
            $this->ConfirmCommissary($commissary->id);
            session()->flash('success', trans('language.done'));
            return redirect("admin/users/$user_id/commissaries");
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($id)
        {
            //
        }
    }
