<?php

namespace App\Http\Controllers\Admin\openScreen;

use App\Country;
use App\Open_screen;
use App\Traits\storeImage;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{

    use storeImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = Open_screen::orderBy('id', 'desc')->paginate(10);
        return view('admin.openScreens.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.openScreens.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'image' => 'required',
            'name_ar' => 'required',
            'name_en' => 'required',
            'body_ar' => 'required',
            'body_en' => 'required',
        ]);
        if ($request->image) {
            $data['image'] = $this->storeImage($request->image);
        }
        Open_screen::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/openScreens'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = Open_screen::findOrFail($id);
        return view('admin.openScreens.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = Open_screen::findOrFail($id);
        $data = $request->validate([
            'image' => '',
            'name_ar' => '',
            'name_en' => '',
            'body_ar' => '',
            'body_en' => '',
        ]);
        if ($request->image) {
            $data['image'] = $this->storeImage($request->image);
        }
        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/openScreens'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        Open_screen::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/openScreens'));
    }
}
