<?php

    namespace App\Http\Controllers\Admin\Driver\SingleRide;

    use App\Single_ride;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class DriverSingleRideController extends Controller
    {
        public function index($drier_id)
        {

            $accepted_rides = Single_ride::where('driver_id', $drier_id)->where('order_status_id', Single_ride::driverAcceptOrder)->orderBy('id', 'desc')->paginate(10);
            $rejected_rides = Single_ride::where('driver_id', $drier_id)->where('order_status_id', Single_ride::driverRejectOrder)->orderBy('id', 'desc')->paginate(10);
            $user_cancelled_rides = Single_ride::where('driver_id', $drier_id)->where('order_status_id', Single_ride::clientCancelOrder)->orderBy('id', 'desc')->paginate(10);
            $driver_cancelled_rides = Single_ride::where('driver_id', $drier_id)->where('order_status_id', Single_ride::driverCancelOrder)->orderBy('id', 'desc')->paginate(10);
            $admin_cancelled_rides = Single_ride::where('driver_id', $drier_id)->where('order_status_id', Single_ride::adminCancelOrder)->orderBy('id', 'desc')->paginate(10);
            $compeleted_rides = Single_ride::where('driver_id', $drier_id)->where('order_status_id', Single_ride::driverCompleteOrder)->orderBy('id', 'desc')->paginate(10);

            return view('admin.drivers.singleRides.index', compact('accepted_rides', 'rejected_rides', 'user_cancelled_rides', 'driver_cancelled_rides', 'admin_cancelled_rides', 'compeleted_rides'));
        }
    }
