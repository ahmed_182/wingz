<?php

namespace App\Http\Controllers\Admin\Driver\DrivingCarImages;

use App\Driver_car_images;
use App\Traits\storeFile;
use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use storeFile;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($driver_id)
    {
        $items = Driver_car_images::where("driver_id", $driver_id)->orderBy('id', 'desc')->paginate(10);
        return view('admin.drivers.driving_car_images.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id)
    {
        $driver = User::find($id);
        return view('admin.drivers.driving_car_images.create', compact("driver"));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        $data = $request->validate([
            'image' => 'required',
        ]);
        $data['driver_id'] = $id;
        if ($request->image) {
            $data["image"] = $this->storeFile($request->image);
        }
        Driver_car_images::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url("/admin/drivers/$id/driving_car_images"));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($driver_id, $id)
    {
        $item = Driver_car_images::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url("/admin/drivers/$driver_id/driving_car_images"));
    }
}
