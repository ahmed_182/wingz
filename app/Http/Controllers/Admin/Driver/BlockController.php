<?php

    namespace App\Http\Controllers\Admin\Driver;

    use App\Driver;
    use App\Driving_license_block;
    use App\ModulesConst\blockStatus;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class BlockController extends Controller
    {
        public function index($id)
        {
            $user = User::find($id);
            $driver = Driver::where('user_id', $id)->first();
            if (!$driver) {
                session()->flash('alert', trans('language.no_data'));
                return back();
            }
            $driving_license = $driver->driving_license;
            $data['driving_license'] = $driving_license;
            Driving_license_block::create($data);
            $user->blockStatus = blockStatus::yes;
            $user->save();
            session()->flash('success', trans('language.done'));
            return back();
        }
    }
