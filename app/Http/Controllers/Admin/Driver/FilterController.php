<?php

    namespace App\Http\Controllers\Admin\Driver;

    use App\Driver;
    use App\ModulesConst\blockStatus;
    use App\ModulesConst\UserTyps;
    use App\ModulesConst\UserVerify;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class FilterController extends Controller
    {

        public function filter($request, $items)
        {
            if ($request->name)
                $items = $items->where("name", 'LIKE', '%' . $request->name . '%');
            if ($request->email) {
                $items = $items->where("email", 'LIKE', '%' . $request->email . '%');
            }
            if ($request->phone) {
                $items = $items->where("mobile", 'LIKE', '%' . $request->phone . '%');
            }

            $items = $items->orderBy("id", "desc")->paginate(10);
            return $items;
        }

        public function active_drivers(Request $request)
        {
            // get drivers  Active
            $items = User::where('user_type_id', UserTyps::driver)
                ->where('userVerify', UserVerify::yes)
                ->where('blockStatus', blockStatus::no);
            $items = $this->filter($request, $items);

            // get tapType
            if ($request->tapType) {
                $tapType = $request->tapType;
            } else {
                $tapType = 'navTap-1';
            }

            return view('admin.drivers.index', compact('items', 'tapType'));

        }

        public function waiting_drivers(Request $request)
        {
            // get drivers Not Active
            $items = User::where('user_type_id', UserTyps::driver)
                ->where('userVerify', UserVerify::no)
                ->orderBy("id", "desc")->paginate(10);
            return view('admin.drivers.waiting_drivers', compact('items'));
        }

        public function block_drivers(Request $request)
        {
            $items = User::where('user_type_id', UserTyps::driver)
                ->where('blockStatus', blockStatus::yes)
                ->orderBy("id", "desc")->paginate(10);
            return view('admin.drivers.block_drivers', compact('items'));
        }



    }
