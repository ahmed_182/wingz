<?php

    namespace App\Http\Controllers\Admin\Driver;

    use App\Driver;
    use App\Driving_license_block;
    use App\ModulesConst\blockStatus;
    use App\ModulesConst\UserVerify;
    use App\User;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class ActiveAccountController extends Controller
    {
        //index
        public function index($id)
        {
            $user = User::find($id);
            $user->userVerify = UserVerify::yes;
            $user->blockStatus = blockStatus::no;
            $user->save();
            // Send Notifiction to user to inform him that his account has been activated ..
            $user_ids = User::where('id', $user->id)->pluck('id');
            $body = trans('language.admin_active_you_account');
            $this->notificationHandler(trans("language.appName"), $body, $user_ids);
            session()->flash('success', trans('language.admin_active_you_account'));
            return back();
        }

        public function drActive_account($id)
        {
            $user = User::find($id);
            $user->userVerify = UserVerify::no;
            $user->save();
            // Send Notifiction to user to inform him that his account has been baned ..
            $user_ids = User::where('id', $user->id)->pluck('id');
            $body = trans('language.admin_deactive_you_account');
            $this->notificationHandler(trans("language.appName"), $body, $user_ids);
            session()->flash('success', trans('language.admin_deactive_you_account'));
            return back();
        }
    }
