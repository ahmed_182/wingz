<?php

namespace App\Http\Controllers\Admin\Driver;

use App\Driver;
use App\Driver_car_images;
use App\ModulesConst\UserTyps;
use App\ModulesConst\UserVerify;
use App\Traits\storeImage;
use App\User;
use Hash;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use storeImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $items = User::where('user_type_id', UserTyps::driver)->where('userVerify',UserVerify::yes);
        $items = $this->filter($request, $items);

        // get tapType
        if ($request->tapType) {
            $tapType = $request->tapType;
        } else {
            $tapType = 'navTap-1';
        }

        return view('admin.drivers.index', compact('items', 'tapType'));
    }


    public function filter($request, $items)
    {
        if ($request->name)
            $items = $items->where("name", 'LIKE', '%' . $request->name . '%');
        if ($request->email) {
            $items = $items->where("email", 'LIKE', '%' . $request->email . '%');
        }
        if ($request->phone) {
            $items = $items->where("mobile", 'LIKE', '%' . $request->phone . '%');
        }

        if ($request->district_id) {
            $items = $items->where("district_id", $request->district_id);
        }
        $user_ids = $items->pluck('id');
        if ($request->driver_type) {
            $drivers = Driver::whereIn('user_id', $user_ids)->where('driver_type', $request->driver_type)->pluck('user_id');
            return User::whereIn('id', $drivers)->orderBy("id", "desc")->paginate(10);
        } else {
            $drivers = Driver::whereIn('user_id', $user_ids)->pluck('user_id');
            return User::whereIn('id', $drivers)->orderBy("id", "desc")->paginate(10);
        }

//        $items = $items->orderBy("id", "desc")->paginate(10);
        return $items;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.drivers.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([
            'name' => 'required',
            'password' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:2048',
            'car_photo' => 'mimes:jpeg,jpg,png,gif|required|max:2048',
            'mobile' => 'required',
            'cat_name_id' => 'required',
            'cat_type_id' => 'required',
            'manufacturing_year_id' => 'required',
            'plate_number' => 'required',
            'driving_license' => 'required',
            'location' => 'required',
        ]);
        $data['password'] = Hash::make($data['password']);
        $data['user_type_id'] = UserTyps::driver;
        if ($request->image) {
            $data['image'] = $this->storeImage($request->image);
        }
        if ($request->car_photo) {
            $data['car_photo'] = $this->storeImage($request->car_photo);
        }

        if ($request->location) {
            $location = explode(",", $request->location);
            $data["lat"] = $location[0];
            $data["lng"] = $location[1];
        }
        $user = User::create($data);
        $data['user_id'] = $user->id;
        $driver = Driver::create($data);

        if ($request->car_photo) {
            $data['image'] = $this->storeImage($request->car_photo);
            $data['driver_id'] = $user->id;
            Driver_car_images::create($data);
        }

        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/waiting_drivers'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = User::findOrFail($id);
        return view('admin.drivers.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = User::findOrFail($id);
        $data = $request->validate([
            'name' => '',
            'password' => '',
            'image' => 'mimes:jpeg,jpg,png,gif|max:2048',
            'car_photo' => 'mimes:jpeg,jpg,png,gif|max:2048',
            'mobile' => '',
            'cat_name_id' => '',
            'cat_type_id' => '',
            'manufacturing_year_id' => '',
            'plate_number' => '',
            'driving_license' => '',
            'location' => '',
        ]);

        if ($request->password) {
            $data['password'] = Hash::make($data['password']);
        }
        if ($request->image) {
            $data['image'] = $this->storeImage($request->image);
        }
        if ($request->car_photo) {
            $data['car_photo'] = $this->storeImage($request->car_photo);
        }

        if ($request->location) {
            $location = explode(",", $request->location);
            $data["lat"] = $location[0];
            $data["lng"] = $location[1];
        }
        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/drivers'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = User::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/drivers'));
    }
}
