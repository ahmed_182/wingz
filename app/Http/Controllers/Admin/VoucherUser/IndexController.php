<?php

namespace App\Http\Controllers\Admin\VoucherUser;

use App\ModulesConst\UserTyps;
use App\User;
use App\UserVoucher;
use App\Voucher;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($id)
    {
        $items = UserVoucher::where('voucher_id', $id)->orderBy('id', 'desc')->paginate(10);
        $voucher_id = $id;
        // $voucher_ids = UserVoucher::pluck('car_category_id');
        //$categories=CarCategory::whereNotIn('id',$voucher_ids)->count();
        return view('admin.voucher.user.index', compact('items', 'voucher_id'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($voucher_id)
    {
        $voucher = Voucher::findorfail($voucher_id);

        $voucher_ids = UserVoucher::where("voucher_id", $voucher->id)->pluck('user_id');
        $users = User::where('user_type_id', UserTyps::user)->whereNotIn('id', $voucher_ids)->get();


        return view('admin.voucher.user.create', compact('voucher', 'users'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store($voucher_id, Request $request)
    {
        $data = $request->validate([
            'user_id' => ['required'],
        ]);
        
        foreach ($request->user_id as $user) {
            $data_store["user_id"] = $user;
            $data_store["voucher_id"] = $voucher_id;
        
            UserVoucher::create($data_store);
        } 
        session()->flash('success', trans('language.done'));
        return redirect("admin/voucher/$voucher_id/user");
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($voucher_id, $id)
    {
        $item = UserVoucher::findOrFail($id);
        $item->delete();
        session()->flash('success', trans('language.done'));
        return redirect("admin/voucher/$voucher_id/user");
    }
}
