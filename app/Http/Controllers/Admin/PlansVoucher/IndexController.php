<?php

    namespace App\Http\Controllers\Admin\PlansVoucher;

    use App\Plan;
    use App\Voucher;
    use App\Voucher_plan;
    use Illuminate\Http\Request;
    use App\Http\Controllers\Controller;

    class IndexController extends Controller
    {
        /**
         * Display a listing of the resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function index($id)
        {
            $items_ids = Voucher_plan::where('plan_id', $id)->pluck('voucher_id');
            $items = Voucher::whereIn('id', $items_ids)->orderBy('id', 'desc')->paginate(10);
            $plan = Plan::find($id);
            return view('admin.plans.voucher.index', compact('items', 'plan'));
        }

        /**
         * Show the form for creating a new resource.
         *
         * @return \Illuminate\Http\Response
         */
        public function create($id)
        {
            $plan = Plan::find($id);

            $plan_vou = Voucher_plan::where('plan_id',$id)->pluck('voucher_id')->unique();
            $vouchers = Voucher::whereNotIn('id',$plan_vou)->get();
            return view('admin.plans.voucher.create', compact("plan",'vouchers'));
        }

        /**
         * Store a newly created resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @return \Illuminate\Http\Response
         */
        public function store(Request $request, $plan_id)
        {
            $data = $request->validate([
                'voucher_id' => 'required',
            ]);
            $data['plan_id'] = $plan_id;
            Voucher_plan::create($data);
            session()->flash('success', trans('language.done'));
            return redirect(url("/admin/plans/$plan_id/vouchers"));
        }

        /**
         * Display the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function show($id)
        {
            //
        }

        /**
         * Show the form for editing the specified resource.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function edit($id)
        {
            //
        }

        /**
         * Update the specified resource in storage.
         *
         * @param \Illuminate\Http\Request $request
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function update(Request $request, $id)
        {
            //
        }

        /**
         * Remove the specified resource from storage.
         *
         * @param int $id
         * @return \Illuminate\Http\Response
         */
        public function destroy($plan_id, $id)
        {
            $item = Voucher_plan::where('plan_id', $plan_id)->where('voucher_id', $id)->delete();
            session()->flash('success', trans('language.done'));
            return redirect(url("/admin/plans/$plan_id/vouchers"));
        }
    }
