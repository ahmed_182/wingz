<?php

namespace App\Http\Controllers\Admin\Relation;

use App\Traits\storeImage;
use App\User_relation;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class IndexController extends Controller
{
    use storeImage;

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $items = User_relation::orderBy('id', 'desc')->paginate(10);
        return view('admin.relations.index', compact('items'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()

    {

        return view('admin.relations.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data = $request->validate([

            'name_ar' => 'required',
            'name_en' => 'required',
            'image' => 'mimes:jpeg,jpg,png,gif|required|max:2048',
        ]);
        if ($request['image']) {
            $data['image'] = $this->storeImage($request['image']);
        }
        $item = User_relation::create($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/relations'));
    }

    /**
     * Display the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $item = User_relation::findOrFail($id);
        return view('admin.relations.edit', compact('item'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param \Illuminate\Http\Request $request
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $item = User_relation::findOrFail($id);
        $data = $request->validate([
            'name_ar' => '',
            'name_en' => '',
            'image' => '',
        ]);


        if ($request->image) {
            $data['image'] = $this->storeImage($data['image']);
        }
        $item->update($data);
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/relations'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $item = User_relation::findOrFail($id)->delete();
        session()->flash('success', trans('language.done'));
        return redirect(url('/admin/relations'));
    }
}
