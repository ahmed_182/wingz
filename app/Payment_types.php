<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Payment_types extends Model
{
    protected $fillable = [
        'name_en',
        'name_ar',
        'image',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->Serv_name;
        $data['image'] = $this->image;
        return $data;
    }

    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getDashNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }
}
