<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Open_screen extends Model
{
    protected $fillable = [
        'image',
        'name_ar',
        'name_en',
        'body_ar',
        'body_en',
    ];

    public function toArray()
    {
        $data['image'] = $this->image;
        $data['title'] = $this->serv_name;
        $data['body'] = $this->serv_body;
        return $data;
    }


    public function getServImageAttribute()
    {
        if ($this->image)
            return $attribute = $this->image;
        else
            return asset('assets/admin/images/logo.png');
    }


    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getDashNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getServBodyAttribute()
    {
        if (\request()->lang == "en")
            return $this->body_en;
        else
            return $this->body_ar;
    }

    public function getDashBodyAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->body_en;
        else
            return $this->body_ar;
    }

}
