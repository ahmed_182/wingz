<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Order_car_type extends Model
    {
        protected $fillable = [
            'image',
            'name_ar',
            'name_en',
            'description_ar',
            'description_en',
            'price_per_kilo',
            'price_per_kilo_after_school',
        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['image'] = $this->image;
            $data['name'] = $this->Serv_name;
            $data['details'] = $this->Serv_description;
            $data['price_per_kilo'] = $this->price_per_kilo;
            $data['price_per_kilo_after_school'] = $this->price_per_kilo_after_school;
            return $data;
        }


        public function getServNameAttribute()
        {
            if (\request()->lang == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getServDescriptionAttribute()
        {
            if (\request()->lang == "en")
                return $this->description_en;
            else
                return $this->description_ar;
        }


        // dashboard
        public function getDashNameAttribute()
        {
            if (app()->getLocale() == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getDashDescriptionAttribute()
        {
            if (app()->getLocale() == "en")
                return $this->description_en;
            else
                return $this->description_ar;
        }
    }
