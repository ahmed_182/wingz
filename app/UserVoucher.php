<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserVoucher extends Model
{
    protected $fillable = [
        'user_id',
        'voucher_id', //this way can be enable for multi product instead of using ProductVoucherId which will use for one product
        'number_of_use',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['voucher'] = $this->voucher;
        $data['plans'] = $this->plans_data();
        $data['number_of_use'] = $this->number_of_use;
        return $data;
    }

    protected function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function getUserAttribute()
    {
        return $this->user()->first();
    }

    protected function voucher()
    {
        return $this->belongsTo(voucher::class, 'voucher_id');
    }

    protected function plans_data()
    {
        $data_ids = Voucher_plan::where('voucher_id', $this->voucher_id)->pluck('plan_id');
        $plans = Plan::whereIn('id', $data_ids)->get();
        return $plans;
    }

    public function getVoucherAttribute()
    {
        return $this->voucher()->first();
    }


    public function getDashUserAttribute()
    {
        $attribute = " غير محدد";
        if ($this->user)
            $attribute = $this->user->dash_name;
        return $attribute;
    }

    public function getDashEmailAttribute()
    {
        $attribute = " غير محدد";
        if ($this->user)
            $attribute = $this->user->dash_email;
        return $attribute;
    }

    public function getDashMobileAttribute()
    {
        $attribute = " غير محدد";
        if ($this->user)
            $attribute = $this->user->dash_mobile;
        return $attribute;
    }
}
