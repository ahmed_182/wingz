<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class City extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name_en',
        'name_ar',
        'country_id',
        'image',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->serv_name;
        return $data;
    }

    public function getProductCountAttribute()
    {
        $items = Product::where("city_id", $this->id)->count();
        return $items;
    }
    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }
//dashboard

    public function getDashNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getDashCountryNameAttribute()
    {
        $att = null;
        if ($this->country)
            $att = $this->country->dash_name;
        return $att;
    }

    public function country()
    {
        return $this->belongsTo(Country::class);
    }
}
