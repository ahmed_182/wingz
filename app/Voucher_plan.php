<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Voucher_plan extends Model
{
    protected $fillable = [
        'plan_id',
        'voucher_id'
    ];


    public function toArray()
    {
        $data['plan'] = $this->plan;
        return $data;
    }

    protected function plan()
    {
        return $this->belongsTo(Plan::class, 'plan_id');
    }


    //dashboard
    public function getDashNameAttribute()
    {
        $attribute = " غير محدد";
        if ($this->plan)
            $attribute = $this->plan->dash_name;
        return $attribute;
    }


}
