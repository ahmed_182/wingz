<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Voucher extends Model
    {

        protected $fillable = [
            'code',
            'description',
            'discount',
            'maximum_discount',
            'uses',
            'max_uses',
            'max_uses_user',
            'type_id',
            'is_enable',
            'is_fixed',
            'starts_at',
            'expires_at',
            'is_new_user',

        ];

        protected $casts = [
            'is_enable' => 'boolean',
            'is_fixed' => 'boolean',
            'created_at' => 'timestamp',
            'expires_at' => 'timestamp',
            'starts_at' => 'timestamp',
        ];

        protected $appends = [
            'type'
        ];

        protected function type()
        {
            return $this->belongsTo(VoucherType::class, 'type_id');
        }

        public function getTypeAttribute()
        {
            return $this->type()->first();
        }


        //dashboard

        public function getDashNameAttribute()
        {
            $attribute = " غير محدد";
            if ($this->description)
                $attribute = $this->description;
            return $attribute;
        }


        public function getDashTypeAttribute()
        {
            $attribute = " غير محدد";
            if ($this->type)
                $attribute = $this->type->dash_name;
            return $attribute;
        }

        public function getDashStartAttribute()
        {
            $attribute = "غير محدد";
            if ($this->starts_at)
                $attribute = date("Y-m-d", $this->starts_at);
            return $attribute;
        }

        public function getDashExpireAttribute()
        {
            $attribute = "غير محدد";
            if ($this->expires_at)
                $attribute = date("Y-m-d", $this->expires_at);
            return $attribute;
        }

        //is_new_user
        public function getDashNewUserStatusNameAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->is_new_user == null) {
                return __('language.not_enable');
            }
            if ($this->is_new_user == 0) {
                return __('language.not_enable');
            }
            if ($this->is_new_user == 1) {
                return __('language.enable');
            }
            return $attribute;
        }

    }
