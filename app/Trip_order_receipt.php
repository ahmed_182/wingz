<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Trip_order_receipt extends Model
{
    const pending = 1;
    const payment_request = 2; // client want to pay cost
    const in_way_to_client = 3;
    const arrived_to_client = 4;
    const paid_done = 5;
    const paid_failed = 6;


    protected $fillable = [
        'note',
        'kilo_price',
        'join_price',
        'tax_value',
        'price_with_discount',
        'discount_value',
        'total_price_without_discount',
        'discount_per',
        'is_discount',
        'tax',
        'price_after_tax',
        'paid_time',
        'status',// link with ( Trip_order_receipt_status )
        'user_id',
        'representative_id',
        'due_date',
        'lat',
        'lng',
        'address',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['kilo_price'] = $this->kilo_price;
        $data['join_price'] = $this->join_price;
        $data['tax_value'] = $this->tax_value;
        $data['price_with_discount'] = $this->price_with_discount;
        $data['discount_value'] = $this->discount_value;
        $data['total_price_without_discount'] = $this->total_price_without_discount;
        $data['discount_per'] = $this->discount_per;
        $data['is_discount'] = $this->is_discount;
        $data['tax'] = $this->tax;
        $data['price_after_tax'] = $this->price_after_tax;
        $data['paid_time'] = $this->paid_time;
        $data['status'] = $this->receipt_status();
        $data['user'] = $this->user;
        $data['representative'] = $this->representative;
        $data['note'] = $this->note;
        $data['due_date'] = Carbon::createFromFormat('Y-m-d H:i:s', $this->due_date)->format('Y-m-d');
        return $data;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function representative()
    {
        return $this->belongsTo(User::class, 'representative_id');
    }

    public function receipt_status()
    {
        return Trip_order_receipt_status::find($this->status);

    }


}
