<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Driver_districts extends Model
    {
        protected $fillable = ["driver_id", "district_id"];

        public function toArray()
        {
            $data["id"] = $this->id;
            $data["district_id"] = $this->district;
            return $data;
        }

        public function district()
        {
            return $this->belongsTo(District::class, 'district_id');
        }

        public function getDashDistrictNameAttribute()
        {
            $att = "";
            if ($this->district)
                $att = $this->district->dash_name;
            return $att;
        }
    }
