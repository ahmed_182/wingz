<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Trip_ride extends Model
{
    public const ride_work_start = 1;
    public const ride_work_finish = 0;
    protected $fillable = [
        'trip_id',
        'ride_work', // 0 -> Finish  , 1-> working
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['trip'] = $this->trip;
        $data['trip_status'] = $this->trip_status;
        $data['created_at'] = $this->getTime();
        return $data;
    }

    public function trip()
    {
        return $this->belongsTo(Trip::class, 'trip_id');
    }

    public function getTripStatusAttribute()
    {
        $trip_status = Trip_ride_life_cycle::where('trip_ride_id', $this->id)->latest()->first();
        if ($trip_status) {
            return $trip_status->obj();
        }
        return null;
    }

    public function getTime()
    {
        $time = Carbon::parse($this->created_at)->format('d M Y H:iA');
        return $time;
    }

}
