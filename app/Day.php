<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Day extends Model
    {
        protected $fillable = [
            'month_id',
            'day_number',
            'name_ar',
            'name_en',
            'is_vacation',
            'vacation_ar',
            'vacation_en',
        ];
    }
