<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Cancellation_reasons extends Model
    {
        public $timestamps = false;
        protected $fillable = [
            'name_ar',
            'name_en',
            'user_type_id',

        ];

        public function getServNameAttribute()
        {
            if (\request()->lang == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getDashNameAttribute()
        {
            if (app()->getLocale() == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }


        public function toArray()
        {
            $data['id'] = $this->id;
            $data['name'] = $this->serv_name;
            return $data;
        }
    }
