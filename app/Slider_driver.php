<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Slider_driver extends Model
    {
        protected $fillable = [
            'image',
            'link'
        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['image'] = $this->image;
            $data['link'] = $this->link;
            return $data;
        }
    }
