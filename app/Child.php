<?php

namespace App;

use App\Http\Resources\ChildPlanResource;
use App\Http\Resources\DriverAuth\DriverBasicInfoResource;
use App\Http\Resources\DriverAuth\DriverProfileResource;
use App\ModulesConst\TripStatus;
use Auth;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Child extends Model
{

    protected $fillable = [
        'user_id',
        'name',
        'mobile',
        'birthday',
        'image',
        'school_time_id',
        'is_deleted',
        'status_id',

    ];

    public function toArray()
    {
        $data["id"] = $this->id;
        $data["user"] = $this->user;
        $data["name"] = $this->name;
        $data["mobile"] = $this->mobile;
        $data["country"] = $this->user->serv_country;
        $data["birthday"] = $this->birthday;
        $data["image"] = $this->serv_image;
        $data["plan"] = $this->order_plan_details(); // get plan obj from order ...
        $data["Subscription"] = $this->order_Subscription_details(); // get Subscription ( plan ) obj from order ...
        $data["is_in_trip"] = $this->is_in_trip();
        $data["trip_children"] = $this->trip_children();
        $data["driver_profile"] = $this->driver_profile();
        $data["school"] = $this->serv_school_object;
        $data["level"] = $this->serv_level_object;
        $data["stage"] = $this->school_time;
        $data["is_my_child"] = $this->is_my_child;
        $data["child_home"] = $this->child_home();
        $data["plan_details"] = $this->serv_joni_plan_text;
        $data["plan_cost"] = $this->serv_joni_plan_cost;

        return $data;
    }

    public function profile()
    {
        $data["id"] = $this->id;
        $data["user_id"] = $this->user_id;
        $data["user"] = $this->user;
        $data["name"] = $this->name;
        $data["mobile"] = $this->mobile;
        $data["country"] = $this->user->serv_country;
        $data["birthday"] = $this->birthday;
        $data["image"] = $this->serv_image;
        $data["school"] = $this->serv_school_object;
        $data["level"] = $this->serv_level_object;
        $data["stage"] = $this->school_time;
        $data["is_my_child"] = $this->is_my_child;
        $data["plan_details"] = $this->serv_joni_plan_text;
        $data["plan_cost"] = $this->serv_joni_plan_cost;
        $data["child_home"] = $this->child_home();
        $data["plan"] = $this->order_plan_details(); // get plan obj from order ...
        $data["Subscription"] = $this->order_Subscription_details(); // get Subscription ( plan ) obj from order ...
        return $data;
    }



    public function getIsMyChildAttribute()
    {
        if (Auth::guard("api")->user()) {
            if ($this->user_id == Auth::guard("api")->user()->id) {
                return true;
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    public function getServSchoolObjectAttribute()
    {
        if ($this->school_time) {
            if ($this->school_time->schoolLevel) {
                if ($this->school_time->schoolLevel->school) {
                    return $this->school_time->schoolLevel->school->object();
                }
                return null;
            }
            return null;
        }
        return null;
    }

    public function getServLevelObjectAttribute()
    {
        if ($this->school_time) {
            if ($this->school_time->schoolLevel) {
                return $this->school_time->schoolLevel->level;
            }
            return null;
        }
        return null;
    }


    public function getServImageAttribute()
    {
        if ($this->image)
            return $attribute = $this->image;
        else
            return asset('assets/admin/images/childImage.png');
    }


    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function school_time()
    {
        return $this->belongsTo(School_time::class, 'school_time_id');
    }

    public function order_plan_details()
    {

        $order_details = Order::where("child_id", $this->id)->latest()->first();
        if ($order_details) {
            return $order_details->plan;
        }
        return null;
    }

    public function order_Subscription_details()
    {

        $order_details = Order::where("child_id", $this->id)->latest()->first();
        if ($order_details) {
            return $order_details->Subscription();
        }
        return null;
    }

    public function child_home()
    {

        $order_details = Order::where("child_id", $this->id)->latest()->first();
        if ($order_details) {
            return $order_details->favourite_place;
        }
        return null;
    }


    public function is_in_trip()
    {
        // get allCurrent Trip ,
        $trips = Trip::pluck('id');
        $tripCount = count($trips);
        for ($i = 0; $i < $tripCount; $i++) {
            $trip_children = Trip_children::where('status_id', Trip_children::approved)->where('trip_id', $trips[$i])->pluck('child_id')->toArray();
            if (in_array($this->id, $trip_children)) {
                return true;
            }
        }
        return false;
    }

    public function trip_children()
    {
        // get allCurrent Trip ,
        $children_details = [];
        $trips = Trip::where('status', TripStatus::onway)->orwhere('status', TripStatus::finished)->pluck('id');
        $tripCount = count($trips);
        for ($i = 0; $i < $tripCount; $i++) {
            $trip_children = Trip_children::where('status_id', Trip_children::approved)->where('trip_id', $trips[$i])->pluck('child_id')->toArray();
            if (in_array($this->id, $trip_children)) {
                for ($j = 0; $j < count($trip_children); $j++) {
                    $child_det = Child::where('id', $trip_children[$j])->where('id', '!=', $this->id)->first();
                    if ($child_det) {
                        $children_details[] = $child_det->profile();
                    }
                }
            }
        }
        return $children_details;
    }

    public function driver_profile()
    {
        // get all Current Trip ,
        $trips = Trip::pluck('id');
        $tripCount = count($trips);
        for ($i = 0; $i < $tripCount; $i++) {
            $trip_children = Trip_children::where('status_id', Trip_children::approved)->where('trip_id', $trips[$i])->pluck('child_id')->toArray();
            if (in_array($this->id, $trip_children)) {
                $trip_details = Trip::where('id', $trips[$i])->first();
                if ($trip_details) {
                    $driver = User::find($trip_details->driver_id);
                    $driver_profile = new DriverProfileResource($driver);
                    return $driver_profile;
                }
            }
        }
        return null;
    }

    public function getDashNameAttribute()
    {
        return $this->name;
    }

    public function getDashStatusNameAttribute()
    {
        if ($this->is_deleted == 1) {
            return trans('language.deleted_status');
        } else {
            return trans('language.not_deleted_status');
        }
    }

    public function getServJoniPlanTextAttribute()
    {

        $order_details = Order::where("child_id", $this->id)->latest()->first();
        if ($order_details) {
            $detail = $order_details->Subscription();
            if (!$detail)
                return null;
            $txt1 = trans('language.join_since_msg');
            $txt2 = Carbon::parse($detail->created_at)->format('d M Y');
            $txt3 = trans('language.with_msg');
            $txt4 = $order_details->plan['serv_name'];
            $msg = $txt1 . $txt2 . $txt3 . $txt4;
            return $msg;
        }

        return null;

    }

    public function getServJoniPlanCostAttribute()
    {

        $order_details = Order::where("child_id", $this->id)->latest()->first();
        if ($order_details) {
            $detail = $order_details->Subscription();
            if (!$detail)
                return null;
            $txt1 = trans('language.plan_cost_msg');
            $txt2 = $detail->total_price / $detail->month_count;
            $txt3 = trans('language.per_msg');
            $txt4 = trans('language.month_msg');
            $msg = $txt1 . $txt2 . $txt3 . $txt4;
            return $msg;
        }

        return null;

    }

    public function getDashBirthdayAttribute()
    {
        $attribute = trans('language.notSelected');
        if ($this->birthday) {
            $attribute = date("Y-m-d", $this->birthday / 1000 );
        }
        return $attribute;
    }


}
