<?php

namespace App;

use App\ModulesConst\destinationWay;
use Illuminate\Database\Eloquent\Model;

class Trip_ride_status extends Model
{

    protected $casts = [
        'created_at' => 'timestamp',
    ];

    // PS : 1 -> Start Ride , you have to make it in database .
    const  driverLaunchedRide = "1";
    const  driverReachedFirstChild = "2";
    const  driverArrivedToSchool = "17";
    const  driverArrivedToHome = "40"; // Finish ride
    const  driverGoToArriveChildrenFromSchoolToHome = "18";

    protected $fillable = [
        'name_en',
        'name_ar',
        'destination',
        'child_count', // -1 = start and end ,  0 = General  , number -> related by child count
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->Serv_name;
        $data['destination'] = $this->Serv_destination_name;
        $data['created_at'] = $this->created_at * 1000;
        return $data;
    }

    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getDashNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getServDestinationNameAttribute()
    {

        $attribute = __('language.notSelected');
        if ($this->destination == null) {
            return 'school';
        }
        if ($this->destination == destinationWay::school) {
            return 'school';
        }
        if ($this->destination == destinationWay::home) {
            return 'home';
        }
        return $attribute;
    }

    public function getDashDestinationNameAttribute()
    {

        $attribute = __('language.notSelected');
        if ($this->destination == null) {
            return 'school';
        }
        if ($this->destination == destinationWay::school) {
            return 'school';
        }
        if ($this->destination == destinationWay::home) {
            return 'home';
        }
        return $attribute;
    }
}
