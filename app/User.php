<?php

namespace App;

use App\ModulesConst\blockStatus;
use App\ModulesConst\CommissaryStatus;
use App\ModulesConst\is_block;
use App\ModulesConst\ProductStatus;
use App\ModulesConst\UserOnlineStatus;
use App\ModulesConst\UserPaidTyps;
use App\ModulesConst\UserRelation;
use App\ModulesConst\UserTyps;
use App\ModulesConst\UserVerify;
use Auth;
use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Laravel\Passport\HasApiTokens;

class User extends Authenticatable
{


    use Notifiable, HasApiTokens;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */


    protected $casts = [
        'email_verified_at' => 'datetime',
        'userVerify' => 'boolean',
    ];

    protected $hidden = [
        'password', 'remember_token',
    ];

    protected $fillable = [
        'blockStatus',
        'block_reason',
        'fire_base_token',
        'api_token',
        'name',
        'family_name',
        'email',
        'mobile',
        'address',
        'image',
        'email_verified_at',
        'password',
        'passCode',
        'user_type_id',
        'user_relation_id',
        'user_relation_name',
        'userVerify',
        'social_id',
        'lat',
        'lng',
        'country_id',
        'blockStatus',
        'bio',
    ];

    public function toArray()
    {
        $data["id"] = $this->id;
        $data["user_type_id"] = $this->user_type_id;
        $data["user_relation_id"] = $this->user_relation_id;
        $data["user_relation_name"] = $this->serv_user_relation_name;
        $data["api_token"] = $this->api_token;
        $data["fire_base_token"] = $this->fire_base_token;
        $data["userVerify"] = $this->userVerify;
        $data["name"] = $this->serv_name;
        $data["family_name"] = $this->serv_family_name;
        $data["email"] = $this->email;
        $data["mobile"] = $this->mobile;
        $data["image"] = $this->serv_image;
        $data['lat'] = $this->lat;
        $data['lng'] = $this->lng;
        $data['bio'] = $this->bio;
        $data["country"] = $this->serv_country;
        $data["created_time"] = trans('language.account_activated') . ' ' . $this->time;
        $data["points_count"] = $this->serv_point_count;
        $data["wallet_value"] = $this->serv_wallet_value;
        $data["main_account"] = $this->user_family_members();
        return $data;
    }

    public function profile()
    {
        $data["id"] = $this->id;
        $data["user_type_id"] = $this->user_type_id;
        $data["user_relation_id"] = $this->user_relation_id;
        $data["user_relation_name"] = $this->serv_user_relation_name;
        $data["api_token"] = $this->api_token;
        $data["fire_base_token"] = $this->fire_base_token;
        $data["userVerify"] = $this->userVerify;
        $data["name"] = $this->serv_name;
        $data["family_name"] = $this->serv_family_name;
        $data["email"] = $this->email;
        $data["mobile"] = $this->mobile;
        $data["image"] = $this->serv_image;
        $data['lat'] = $this->lat;
        $data['lng'] = $this->lng;
        $data['bio'] = $this->bio;
        $data["country"] = $this->serv_country;
        $data["created_time"] = trans('language.account_activated') . ' ' . $this->time;
        $data["points_count"] = $this->serv_point_count;
        $data["wallet_value"] = $this->serv_wallet_value;
        $data["main_account"] = $this->user_family_members();
        return $data;
    }

    public function scopeVerified($query)
    {
        return $query->where('userVerify', UserVerify::yes)->where("blockStatus", blockStatus::no);
    }


    public function getServWalletValueAttribute()
    {
        $user_wallet_add = User_wallet::where('user_id', $this->id)->where('status', CommissaryStatus::add)->sum('value');
        $user_wallet_withdrawal = User_wallet::where('user_id', $this->id)->where('status', CommissaryStatus::withdrawal)->sum('value');
        return $user_wallet_add - $user_wallet_withdrawal;
    }

    public function getServPointCountAttribute()
    {
        $user_points = Point_history::where('user_id', $this->id)->sum('points');
        return $user_points;
    }

    public function getServRateAttribute()
    {

        $count = Driver_report::where("driver_id", $this->id)->count();
        $sum = Driver_report::where("driver_id", $this->id)->sum('rate');
        if ($count == 0) {
            return 5;
        }
        return (int)($sum / $count);
    }


    protected function getServUserRelationNameAttribute()
    {
        return $this->user_relation_name;
    }


    public function getTimeAttribute()
    {
        $time = $this->created_at->diffforhumans();
        return $time;
    }


    public function scopeUserType($query)
    {
        return $query->where('user_type_id', '==', UserTyps::user);
    }

    public function scopeAdminType($query)
    {
        return $query->where('user_type_id', '==', UserTyps::admin);
    }

    public function getServImageAttribute()
    {
        if ($this->image)
            return $attribute = $this->image;
        else
            return asset('assets/admin/images/logo.png');
    }


    public function getDashImageAttribute()
    {
        if ($this->image)
            return $attribute = $this->image;
        else
            return asset('assets/admin/images/dash_logo.png');
    }


    public function getServNameAttribute()
    {
        $attribute = null;
        if ($this->name)
            $attribute = $this->name;
        return $attribute;
    }

    public function getServBioAttribute()
    {
        $attribute = null;
        if ($this->bio)
            $attribute = $this->bio;
        return $attribute;
    }

    public function getServEmailAttribute()
    {
        $attribute = null;
        if ($this->email)
            $attribute = $this->email;
        return $attribute;
    }

    public function getServAddressAttribute()
    {
        $attribute = null;
        if ($this->address)
            $attribute = $this->address;
        return $attribute;
    }

    public function getCreatedAttribute()
    {
        $attribute = null;
        if ($this->created_at)
            $attribute = $this->created_at->format('Y-m-d');
        return $attribute;
    }


    public function getServCountryAttribute()
    {
        $attribute = null;
        if ($this->country)
            $attribute = $this->country;
        return $attribute;
    }


    public function lastpasswords()
    {
        return $this->hasMany('App\User_lastpasswords', 'user_id');
    }


    public function country()
    {
        return $this->belongsTo(Country::class, 'country_id');
    }


    public function accessTokens()
    {
        return $this->hasMany('App\OauthAccessToken');
    }

    public function user_relation()
    {
        return $this->belongsTo(User_relation::class, 'user_relation_id');
    }

    public function user_family_members()
    {
        $data = Family_members::where('user_id', $this->id)->get();
        if (count($data))
            return true;
        else
            return false;
    }

    public function getServFamilyNameAttribute()
    {
        $check_if_this_user_is_member = Family_members::where('member_id', $this->id)->first();
        if ($check_if_this_user_is_member)
            return $check_if_this_user_is_member->user->family_name;
        else
            return $this->family_name;
    }

    //dahboard

    public function getDashNameAttribute()
    {

        if ($this->user_type_id == UserTyps::user) {
            $att = $this->name . ' ' . $this->family_name;
        } else {
            $att = $this->name;
        }

        if ($att == " " or $att == null) {
            return trans('language.notSelected');
        }
        return $att;
    }

    public function getDashEmailAttribute()
    {
        $attribute = trans('language.notSelected');
        if ($this->email)
            $attribute = $this->email;
        return $attribute;
    }

    public function getDashMobileAttribute()
    {
        $attribute = trans('language.notSelected');
        if ($this->mobile)
            $attribute = $this->mobile;
        return $attribute;
    }


    public function getDashCountryNameAttribute()
    {
        $att = trans('language.notSelected');
        if ($this->country)
            $att = $this->country->dash_name;
        return $att;
    }

    public function getDashCodeAttribute()
    {
        $att = trans('language.notSelected');
        if ($this->country)
            $att = $this->country->code;
        return $att;
    }

    public function getDashCodeArAttribute()
    {
        $att = trans('language.notSelected');
        if ($this->country)
            $att = $this->country->code_ar;
        return $att;
    }


    public function getDashRelationNameAttribute()
    {
        $att = trans('language.notSelected');
        if ($this->user_relation)
            $att = $this->user_relation->dash_name;
        return $att;
    }

    public function getDashTypeNameAttribute()
    {
        if ($this->user_type_id == UserTyps::user) {
            return trans("language.user");
        } else {
            return trans("language.driver");
        }
    }

    public function getDashDistrictNameAttribute()
    {
        $attribute = "";
        if ($this->district)
            $attribute = $this->district->dash_name;
        return $attribute;
    }

    public function getDashStatusNameAttribute()
    {
        if ($this->userVerify == UserVerify::yes) {
            return trans("language.verfied");
        } else {
            return trans("language.not_verfied");
        }
    }

    public function getDashFamilyMemberStatusNameAttribute()
    {
        if ($this->userVerify == UserVerify::no) {
            return trans("language.pending");
        } else {
            return trans("language.accepted");
        }
    }

    public function getDashStatusBlockAttribute()
    {
        if ($this->is_block == is_block::yes) {
            return trans("language.blocked");
        } else {
            return trans("language.not_blocked");
        }
    }


    // Driver Relations

    public function districts()
    {
        return $this->hasMany(Driver_districts::class, "driver_id");
    }

    public function Licence_images()
    {
        return $this->hasMany(Driver_driving_licence::class, "driver_id");
    }

    public function criming_records()
    {
        return $this->hasMany(Driver_criming_recorde::class, "driver_id");
    }

    public function driver_identities()
    {
        return $this->hasMany(Driver_identity::class, "driver_id");
    }

    public function car_Licence_images()
    {
        return $this->hasMany(Driver_driving_car_licence::class, "driver_id");
    }

    public function car_images()
    {
        return $this->hasMany(Driver_car_images::class, "driver_id");
    }

    public function driver()
    {
        return $this->hasOne(Driver::class, "user_id");
    }



}
