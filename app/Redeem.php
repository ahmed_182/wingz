<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Redeem extends Model
    {
        public $timestamps = false;

        protected $fillable = [
            'image',
            'name_ar',
            'name_en',
        ];


        public function toArray()
        {
            $data['id'] = $this->id;
            $data['name'] = $this->serv_name;
            $data['image'] = $this->image;
            return $data;
        }

        public function getDashNameAttribute()
        {
            if (\request()->lang == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getServNameAttribute()
        {
            if (\request()->lang == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }


    }
