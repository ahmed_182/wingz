<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Tax extends Model
    {
        protected $fillable = [
            'tax_type_id', 'value', 'is_enable', 'type', 'name_en', 'name_ar'
        ];


        public function toArray()
        {
            $data['id'] = $this->id;
            $data['name'] = $this->serv_name;
            $data['value'] = $this->value;
            $data['is_enable'] = $this->dash_status_name;
            $data['type'] = $this->dash_kind_name;
            $data['Tax_type'] = $this->Tax_type;
            return $data;
        }



        public function getServNameAttribute()
        {
            if (\request()->lang == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getDashNameAttribute()
        {
            if (app()->getLocale() == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }



        public function getDashStatusNameAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->is_enable == null) {
                return __('language.notSelected');
            }
            if ($this->is_enable == 0) {
                return __('language.not_enable');
            }
            if ($this->is_enable == 1) {
                return __('language.enable');
            }
            return $attribute;
        }

        public function getDashKindNameAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->type == null) {
                return __('language.notSelected');
            }
            if ($this->type == 1) {
                return __('language.system');
            }
            if ($this->type == 2) {
                return __('language.client');
            }
            return $attribute;
        }

        public function Tax_type()
        {
            return $this->belongsTo(Tax_type::class, 'tax_type_id');
        }

        public function getDashTypeNameAttribute()
        {
            $attribute = "";
            if ($this->Tax_type)
                $attribute = $this->Tax_type->dash_name;
            return $attribute;
        }

    }
