<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class SingleRideRate extends Model
    {
        protected $fillable = [
            'single_ride_id',
            'driver_rate',
            'driver_feedBack',
            'client_rate',
            'client_feedBack',
            'client_id',
            'driver_id'

        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['driver'] = $this->driver;
            $data['driver_rate'] = $this->driver_rate;
            $data['driver_feedBack'] = $this->driver_feedBack;
            $data['client'] = $this->client;
            $data['client_rate'] = $this->client_rate;
            $data['client_feedBack'] = $this->client_feedBack;
            return $data;
        }

        protected function client()
        {
            return $this->belongsTo(User::class, 'client_id');
        }

        protected function driver()
        {
            return $this->belongsTo(User::class, 'driver_id');
        }


        // Dash Board

        public function getDashNameAttribute()
        {
            $attribute = "غير محدد";
            if ($this->child)
                $attribute = $this->child->dash_name;
            return $attribute;
        }

        public function getDashDriverAttribute()
        {
            $attribute = "غير محدد";
            if ($this->driver)
                $attribute = $this->driver->dash_name;
            return $attribute;
        }
    }
