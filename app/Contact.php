<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Contact extends Model
    {
        protected $fillable = [
            'country_id',
            'message'
        ];

        public function getDashCreatedAttribute()
        {
            $attribute = "";
            if ($this->created_at)
                $attribute = $this->created_at->format('Y-m-d');
            return $attribute;
        }

    }
