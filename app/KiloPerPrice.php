<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class KiloPerPrice extends Model
{
    protected $fillable = ['price'];
}
