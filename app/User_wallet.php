<?php

    namespace App;

    use App\ModulesConst\TripStatus;
    use Carbon\Carbon;
    use Illuminate\Database\Eloquent\Model;

    class User_wallet extends Model
    {
        protected $fillable = [
            'user_id',
            'value',
            'reason',
            'status'
        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['value'] = $this->value;
            $data['reason'] = $this->reason;
            $data['status'] = $this->serv_status_name;
            $data['created_at'] = $this->getTime();
            return $data;
        }

        protected function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }

        public function getServStatusNameAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->status == null) {
                return __('language.notSelected');
            }
            if ($this->status == 1) {
                return __('language.add_money');
            }
            if ($this->status == 2) {
                return __('language.withdrawal');
            }
            return $attribute;
        }

        public function getTime()
        {
            $time = Carbon::parse($this->created_at)->format('d M Y H:iA');
            return $time;
        }

    }
