<?php

namespace App;

use App\ModulesConst\AssignedStatus;
use Illuminate\Database\Eloquent\Model;

class User_assigin_trips extends Model
{
    protected $fillable = [
        'user_id',
        'child_id',
        'trip_id',
        'status_id',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
//            $data['user'] = $this->user;
//            $data['child'] = $this->child;
        $data['trip'] = $this->trip;
        $data['status'] = $this->status_name;
        $data['status_id'] = $this->status_id;
        return $data;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function child()
    {
        return $this->belongsTo(Child::class, 'child_id');
    }

    public function trip()
    {
        return $this->belongsTo(Trip::class, 'trip_id');
    }


    public function getDashDriverNameAttribute()
    {
        if ($this->trip) {
            if ($this->trip->driver) {
                return $this->trip->driver->dash_name;
            }
        }
        return trans('language.notSelected');
    }

    public function getStatusNameAttribute()
    {
        //status_id
        $attribute = __('language.notSelected');
        if ($this->status_id == null) {
            return __('language.pending');
        }
        if ($this->status_id == AssignedStatus::pending) {
            return __('language.pending');
        }
        if ($this->status_id == AssignedStatus::accepted) {
            return __('language.accepted');
        }

        if ($this->status_id == AssignedStatus::expired) {
            return __('language.expired');
        }

        if ($this->status_id == AssignedStatus::rejected) {
            return __('language.rejected');
        }
        if ($this->status_id == AssignedStatus::meeting) {
            return __('language.meeting');
        }
        return $attribute;
    }
}
