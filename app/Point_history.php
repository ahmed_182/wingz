<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Point_history extends Model
    {

        protected $fillable = [
            'user_id',
            'commissary_id',
            'points',
            'points_money_value',
            'points_value_id',
            'reason',
        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['commissary_reason'] = $this->commissary;
            $data['points'] = $this->points;
            $data['points_money_value'] = $this->points_money_value;
            $data['reason'] = $this->reason;
            return $data;
        }

        public function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }

        public function commissary()
        {
            return $this->belongsTo(Commissary::class, 'commissary_id');
        }

        public function getCreatedAttribute()
        {
            $attribute = null;
            if ($this->created_at)
                $attribute = $this->created_at->format('Y-m-d');
            return $attribute;
        }


    }
