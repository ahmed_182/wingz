<?php

    namespace App;

    use App\ModulesConst\AssignedStatus;
    use App\ModulesConst\PlanTyps;
    use Illuminate\Database\Eloquent\Model;

    class Assigned_driver extends Model
    {
        protected $fillable = [
            'user_id',
            'driver_id',
            'status'
        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['user'] = $this->user;
            $data['driver'] = $this->driver;
            $data['status_id'] = $this->status;
            $data['status'] = $this->serv_status_name;
            return $data;
        }

        public function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }

        public function driver()
        {
            return $this->belongsTo(User::class, 'driver_id');
        }

        public function getServStatusNameAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->status == AssignedStatus::pending) {
                return "Pending";
            }
            if ($this->status == AssignedStatus::meeting) {
                return "Pending";
            }
            if ($this->status == AssignedStatus::accepted) {
                return "Accepted";
            }

            if ($this->status == AssignedStatus::rejected) {
                return "Rejected";
            }
            return $attribute;
        }

    }
