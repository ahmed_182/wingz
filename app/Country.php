<?php

namespace App;

use Auth;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{
    public $timestamps = false;
    protected $fillable = [
        'image',
        'name_ar',
        'name_en',
        'currency_ar',
        'currency_en',
        'code',
        'code_ar',
        'number_count',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['image'] = $this->serv_image;
        $data['name'] = $this->serv_name;
        $data['code'] = $this->code;
        $data['start_with'] = $this->start_with; // array of objects
        $data['number_count'] = $this->number_count;
        $data['currency_name'] = $this->serv_currency_name;
        return $data;
    }

    public function getServIsMyCountryAttribute()
    {

        if (Auth::guard("api")->user()) {
            $user = User::where('id', Auth::guard("api")->user()->id)->first();
            $check = User::where('id', $user->id)->where('country_id', $this->id)->first();
            if ($check) {
                return true;
            } else {
                return false;
            }
        } else {
            return false;
        }
    }

    public function getServImageAttribute()
    {
        if ($this->image)
            return $attribute = $this->image;
        else
            return asset('assets/admin/images/logo.png');
    }

    public function getServCurrencyNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->currency_en;
        else
            return $this->currency_ar;
    }

    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getServCurrencyAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->currency_en;
        else
            return $this->currency_ar;
    }

//dashboard
    public function getDashImageAttribute()
    {
        $attribute = asset('assets/admin/images/logo.png');
        if ($this->image)
            $attribute = $this->image;
        return $attribute;
    }

    public function getDashNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getDashCurrencyAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->currency_en;
        else
            return $this->currency_ar;
    }

    public function cities()
    {
        return $this->hasMany(City::class, 'country_id');
    }

    public function start_with()
    {
        return $this->hasMany(Country_startwith::class, 'country_id');
    }
}
