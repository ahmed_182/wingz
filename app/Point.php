<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Point extends Model
    {
        protected $fillable = [
            'price'
        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['price'] = $this->name;
            return $data;
        }

    }
