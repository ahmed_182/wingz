<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Trip_children extends Model
    {

        public const pending = 1;
        public const approved = 2;


        protected $fillable = [
            'child_id',
            'trip_id',
            'status_id',
        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['child'] = $this->child->profile();
            $data['status_id'] = $this->status_id;
            return $data;
        }


        public function child()
        {
            return $this->belongsTo(Child::class, 'child_id');
        }

        public function trip()
        {
            return $this->belongsTo(Trip::class, 'trip_id');
        }
    }
