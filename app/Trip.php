<?php

namespace App;

use App\Http\Resources\DriverAuth\DriverProfileResource;
use App\ModulesConst\TripStatus;
use Illuminate\Database\Eloquent\Model;

class Trip extends Model
{
    protected $fillable = [
        'school_id',
        'driver_id',
        'status'
    ];


    public function toArray()
    {
        $data['id'] = $this->id;
//            $data['driver'] = $this->driver;
        $data['driver'] = $this->driver_id ? DriverProfileResource::make($this->driver) : null;
        $data['school'] = $this->school;
//            $data['status_id'] = $this->status;
//            $data['status'] = $this->serv_status_name;
        $data['children'] = $this->children;
        $data['ride_status'] = $this->serv_ride_status_name;
        $data['ride_status_name'] = $this->ride_status_name();
        $data['ride_time'] = $this->serv_ride_time;
        $data['is_exist_ride'] = $this->is_exist_ride();
        $data['safety_details'] = About_app::first()->safety_details;

        return $data;
    }

    public function school()
    {
        return $this->belongsTo(School::class, 'school_id');
    }


    public function driver()
    {
        return $this->belongsTo(User::class, 'driver_id');
    }

    public function children()
    {
        return $this->hasMany(Trip_children::class, 'trip_id');
    }


    public function getServStatusAttribute()
    {
        $latest_ride = Trip_ride_life_cycle::where('trip_id', $this->id)->latest()->first();
        if ($latest_ride) {
            $status = $latest_ride->ride_status;
            return $status;
        }
        return __('language.schooltripName') . ' ( ' . $this->school->dash_name . ' ) ';
    }

    public function getServRideStatusNameAttribute()
    {
        $latest_ride = Trip_ride_life_cycle::where('trip_id', $this->id)->latest()->first();
        if ($latest_ride) {
            $status = $latest_ride->ride_status;
            return $status;
        }
        return null;
    }

    public function getDashDriverNameAttribute()
    {
        $attribute = __('language.notSelected');
        if ($this->driver) {
            $attribute = $this->driver->name;
        }
        return $attribute;
    }

    public function getDashSchoolNameAttribute()
    {
        $attribute = __('language.notSelected');
        if ($this->school) {
            $attribute = $this->school->dash_name;
        }
        return $attribute;
    }

    public function getDashChildrenCountAttribute()
    {
        $attribute = __('language.notSelected');
        if ($this->children) {
            $attribute = count($this->children);
        }
        return $attribute;
    }

    public function getDashStatusNameAttribute()
    {
        $latest_ride = Trip_ride_life_cycle::where('trip_id', $this->id)->latest()->first();
        if ($latest_ride) {
            $status = $latest_ride->ride_status;
            return $status->dash_name;
        }
        return __('language.schooltripName') . ' ( ' . $this->school->dash_name . ' ) ';
    }

    public function getDashRideTimeAttribute()
    {
        $latest_ride = Trip_ride_life_cycle::where('trip_id', $this->id)->latest()->first();
        if ($latest_ride) {
            $time = $latest_ride->getTime();
            return $time;
        }
        return __('language.notSelected');
    }

    public function getServRideTimeAttribute()
    {
        $latest_ride = Trip_ride_life_cycle::where('trip_id', $this->id)->latest()->first();
        if ($latest_ride) {
            $time = strtotime($latest_ride->created_at) * 1000;
            return $time;
        }
        return null;
    }


    public function ride_status_name()
    {
        $latest_ride = Trip_ride_life_cycle::where('trip_id', $this->id)->latest()->first();
        if ($latest_ride) {
            $status = $latest_ride->ride_status;
            if (!$status)
                return __('language.schooltripName') . ' ( ' . $this->school->dash_name . ' ) ';

            return $status->serv_name;
        }
        return __('language.schooltripName') . ' ( ' . $this->school->dash_name . ' ) ';
    }

    public function is_exist_ride()
    {
        $latest_ride = Trip_ride_life_cycle::where('trip_id', $this->id)->latest()->first();
        if ($latest_ride) {
            return true;
        }
        return false;
    }


    public function getRideStatusID()
    {
        $latest_ride = Trip_ride_life_cycle::where('trip_id', $this->id)->latest()->first();
        if ($latest_ride) {
            $status = $latest_ride;
            return $status->trip_ride_status_id;
        }
        return null;
    }


}
