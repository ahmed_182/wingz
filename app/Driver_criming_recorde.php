<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Driver_criming_recorde extends Model
    {
        protected $fillable = ["driver_id", "data"];

        public function toArray()
        {
            $data["id"] = $this->id;
            $data["data"] = $this->data;
            return $data;
        }
    }
