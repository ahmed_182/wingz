<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class District extends Model
{
    public $timestamps = false;

    protected $fillable = [
        'name_en',
        'name_ar',
        'city_id',
        'lat',
        'lng',
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['name'] = $this->serv_name;
        $data['lat'] = $this->lat;
        $data['lng'] = $this->lng;
        return $data;
    }

    public function getServNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getNameAttribute()
    {
        if (\request()->lang == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

//dashboard

    public function getDashNameAttribute()
    {
        if (app()->getLocale() == "en")
            return $this->name_en;
        else
            return $this->name_ar;
    }

    public function getDashCityNameAttribute()
    {
        $att = null;
        if ($this->city)
            $att = $this->city->dash_name;
        return $att;
    }


    public function city()
    {
        return $this->belongsTo(City::class);
    }
}
