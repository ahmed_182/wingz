<?php

    namespace App;

    use App\ModulesConst\NotificationTyps;
    use Carbon\Carbon;
    use Illuminate\Database\Eloquent\Model;

    class Notification extends Model
    {
        protected $fillable = [
            'title',
            'body',
            'type',
            'item_id',
        ];

        public function getTime()
        {
            $time = Carbon::parse($this->created_at)->format('H:i A d M Y');
            return $time;
        }

        public function getServTypeNameAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->type == null) {
                return "Normal";
            }
            if ($this->type == NotificationTyps::normal) {
                return "Normal";
            }
            if ($this->type == NotificationTyps::delivering) {
                return "Delivering";
            }
            if ($this->type == NotificationTyps::assigin_trips) {
                return "Assigin Trips";
            }
            return $attribute;
        }


        // Relations
        public function rel()
        {
            return $this->belongsTo(Product::class, 'item_id');
        }

        public function chatRoom()
        {
            return $this->belongsTo(ChatRoom::class, 'item_id');
        }


    }
