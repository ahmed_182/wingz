<?php

    namespace App;

    use App\ModulesConst\OrderStatus;
    use App\ModulesConst\PaymentStatus;
    use Illuminate\Database\Eloquent\Model;

    class Order extends Model
    {
        protected $fillable = [
            'trip_order_receipt_id',
            'user_id',
            'driver_id',
            'distance',
            'kilo_price',
            'status',
            'payment_status',
            'child_id',
            'favourite_place_id',
            'school_id',
            'plan_id',
            'total_price',
            'promo_code_id',
            'order_car_type_id',
            'rejected_reason',
            'working_days_count',
        ];


        public function toArray()
        {
            $data['id'] = $this->id;
            $data['user'] = $this->user;
            $data['driver'] = $this->getDriver();
            $data['distance'] = $this->distance;
            $data['kilo_price'] = $this->kilo_price;
            $data['status_id'] = $this->status;
            $data['status'] = $this->serv_type_name;
            $data['child'] = $this->child;
            $data['school'] = $this->school;
            // $data['plan'] = $this->plan;
            $data['subscription'] = $this->Subscription(); // order Plan
            $data['favourite_place'] = $this->favourite_place;
            $data['order_car_type'] = $this->order_car_type;
            $data['promo_code'] = $this->promo_code;
            $data['rejected_reason'] = $this->rejected_reason;
            $data['working_days_count'] = $this->working_days_count;
            $data['trip_order_receipt'] = $this->trip_order_receipt;
            return $data;
        }

        public function trip_order_receipt()
        {
            return $this->belongsTo(Trip_order_receipt::class, 'trip_order_receipt_id');
        }

        public function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }

        public function order_car_type()
        {
            return $this->belongsTo(Order_car_type::class, 'order_car_type_id');
        }

        public function promo_code()
        {
            return $this->belongsTo(Voucher::class, 'promo_code_id');
        }


        public function driver()
        {
            return $this->belongsTo(User::class, 'driver_id');
        }

        public function child()
        {
            return $this->belongsTo(Child::class, 'child_id');
        }

        public function school()
        {
            return $this->belongsTo(School::class, 'school_id');
        }

        public function favourite_place()
        {
            return $this->belongsTo(Favorite_places::class, 'favourite_place_id');
        }

        public function plan()
        {
            return $this->belongsTo(Plan::class, 'plan_id');
        }

        public function Subscription()
        {
            $plan = Order_plan::where('order_id', $this->id)->latest()->first();
            if ($plan) {
                return $plan;
            } else {
                return null;
            }
        }

        public function getServTypeNameAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->status == null) {
                return __('language.order_pending');
            }
            if ($this->status == OrderStatus::pending) {
                return __('language.order_pending');
            }
            if ($this->status == OrderStatus::accepted) {
                return __('language.accepted');
            }

            if ($this->status == OrderStatus::onway) {
                return __('language.onway');
            }

            if ($this->status == OrderStatus::rejected) {
                return __('language.rejected');
            }
            return $attribute;
        }

        public function getDashPaymentStatusNameAttribute()
        {
            $attribute = __('language.notSelected');

            if ($this->payment_status == null) {
                return __('language.notSelected');
            }
            if ($this->payment_status == PaymentStatus::not_active) {
                return __('language.not_actived');
            }
            if ($this->payment_status == PaymentStatus::active) {
                return __('language.active');
            }
            return $attribute;
        }

        public function getDashUserNameAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->user) {
                $attribute = $this->user->name;
            }
            return $attribute;
        }

        public function getDashSchoolNameAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->school) {
                $attribute = $this->school->dash_name;
            }
            return $attribute;
        }


        public function getDashChildNameAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->child) {
                $attribute = $this->child->name;
            }
            return $attribute;
        }

        public function getDashPlaceNameAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->favourite_place) {
                $attribute = $this->favourite_place->name;
            }
            return $attribute;
        }

        public function getDashPlanNameAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->plan) {
                $attribute = $this->plan->dash_name;
            }
            return $attribute;
        }

        public function getDashPlanDaysAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->plan) {
                $attribute = $this->plan->period;
            }
            return $attribute;
        }

        public function getDriver()
        {
            if ($this->child) {
                return $this->child->driver_profile();
            } else {
                return null;
            }
        }
    }

