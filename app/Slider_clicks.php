<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class Slider_clicks extends Model
    {
        protected $fillable = [
            'user_id',
            'slider_id',
        ];

        public function user()
        {
            return $this->belongsTo(User::class, 'user_id');
        }

        public function slider()
        {
            return $this->belongsTo(Slider::class, 'slider_id');
        }

        public function getDashUserNameAttribute()
        {
            $attribute = __('language.notSelected');
            if ($this->user) {
                $attribute = $this->user->name;
            }
            return $attribute;
        }

        public function getDashCreatedAttribute()
        {
            $attribute = null;
            if ($this->created_at)
                $attribute = $this->created_at->diffForHumans();
            return $attribute;
        }



    }
