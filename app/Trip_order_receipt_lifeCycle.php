<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Trip_order_receipt_lifeCycle extends Model
{
    protected $fillable = [
        'trip_order_receipt_status_id', 'trip_order_receipt_id'
    ];
}
