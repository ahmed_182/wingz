<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Driver_report extends Model
{

    protected $casts = [
        'created_at' => 'timestamp',
    ];

    protected $fillable = [
        'user_id',
        'driver_id',
        'rate',
        'report'
    ];

    public function toArray()
    {
        $data['id'] = $this->id;
        $data['user'] = $this->user;
        $data['driver'] = $this->driver;
        $data['rate'] = $this->rate;
        $data['report'] = $this->report;
        $data['created_at'] = $this->created_at * 1000 ;
        return $data;
    }

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id');
    }

    public function driver()
    {
        return $this->belongsTo(User::class, 'driver_id');
    }
}
