<?php

    namespace App;

    use Illuminate\Database\Eloquent\Model;

    class VoucherType extends Model
    {
        //
        const promoCode = 1;
        const gift = 2;
        const cashBack = 3;

        protected $fillable = [

            'name_ar',
            'name_en',

        ];

        public function toArray()
        {
            $data['id'] = $this->id;
            $data['name'] = $this->Serv_name;
            return $data;
        }

        public function getServNameAttribute()
        {
            if (\request()->lang == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }

        public function getDashNameAttribute()
        {
            if (app()->getLocale() == "en")
                return $this->name_en;
            else
                return $this->name_ar;
        }


        public function getVoucherTypeAttribute()
        {
            $attribute = " غير محدد";
            if ($this->name)
                $attribute = $this->name;
            return $attribute;
        }
    }



