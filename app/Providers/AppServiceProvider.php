<?php

    namespace App\Providers;

    use App\Repositories\Brands\BrandsCacheRepository;
    use App\Repositories\Brands\BrandsRepository;
    use App\Repositories\Brands\BrandsRepositoryInterface;
    use App\Repositories\Brands\PostsCacheRepository;
    use App\Repositories\Brands\PostsRepositoryInterface;
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Support\ServiceProvider;

    class AppServiceProvider extends ServiceProvider
    {
        /**
         * Register any application services.
         *
         * @return void
         */
        public function register()
        {
            if ($this->app->environment() !== 'production') {
                $this->app->register(\Barryvdh\LaravelIdeHelper\IdeHelperServiceProvider::class);
            }
        }

        /**
         * Bootstrap any application services.
         *
         * @return void
         */
        public function boot()
        {
            Schema::defaultStringLength(191);
        }
    }
