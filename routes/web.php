<?php


Route::get('/admin', 'Admin\Auth\LoginController@index');
Route::post('/adminlogin', 'Admin\Auth\LoginController@adminlogin');
Route::post('/adminlogout', 'Admin\Auth\LoginController@adminlogout');


Route::get('/clear', function() {
    Artisan::call('cache:clear');
    session()->flash('success',trans('Cache has bees cleared successfully.'));
    return redirect('/');
});

// Site Routetes
    Route::get('/', 'Site\Home\indexController@index');

    // Ajax Controller
    Route::post('/getLevels', 'Admin\Ajax\IndexController@getLevels');
    Route::post('/getSchoolTimes', 'Admin\Ajax\IndexController@getSchoolTimes');
    Route::post('/getCities', 'Admin\Ajax\IndexController@getCities');
    Route::post('/getDistracts', 'Admin\Ajax\IndexController@getDistracts');
    Route::post('/getDistractDetails', 'Admin\Ajax\IndexController@getDistractDetails');

    // Slider count click
    Route::get('/openLink/{user_id}/{slider_id}', 'Site\Home\indexController@openLink');

Auth::routes();
