<?php

Route::group(['prefix' => 'driver'], function () {
    Route::post('/profile', 'Auth\ProfileController@index');


    // get my reports
    Route::post('/driver-reports', 'Report\GetReportController@index');
    Route::post('/driver-id-reports', 'Report\GetReportController@reports');

    // Driver Trip Side
    Route::post('/trips', 'Trip\IndexController@index');
    Route::post('/start-trip-ride', 'Trip\StartTripRideController@index');
    Route::post('/ride-child-status', 'Trip\StartTripRideController@get_child_status');
    Route::post('/all-child-status', 'Trip\StartTripRideController@get_all_child_status');
    Route::post('/update-trip-ride', 'Trip\ChangeTripRideController@index');

    // # Single Ride
    // rate single ride ( Driver Part )
    Route::post('/rate-single-ride', 'SingleRide\RateSingleRideController@index');
    // Driver single rides
    Route::post('get-single-ride', 'SingleRide\GetSingleRideController@index');
    // single ride  details
    Route::post('single-ride-details', 'SingleRide\DetailsSingleRideController@index');
    // accept
    Route::post('accept-single-ride', 'SingleRide\AcceptSingleRideController@index');
    // reject
    Route::post('reject-single-ride', 'SingleRide\RejectSingleRideController@index');
    // cancelByDriver
    Route::post('cancel-single-ride', 'SingleRide\CancelSingleRideController@index');
    // arriveToPickup
    Route::post('arriveToPickup-single-ride', 'SingleRide\ArriveToPickupSingleRideController@index');
    // inHisWayToDestination
    Route::post('inHisWayToDestination-single-ride', 'SingleRide\InHisWayToDestinationSingleRideController@index');
    // arriveToDestination
    Route::post('arriveToDestination-single-ride', 'SingleRide\ArriveToDestinationSingleRideController@index');
    // complete
    Route::post('complete-single-ride', 'SingleRide\CompleteSingleRideController@index');
    // exist Single Ride
    Route::post('exist-single-ride', 'SingleRide\ExistSingleRideController@index');

    Route::fallback(function () {
        return response()->json([
            'message' => 'Page Not Found. If error persists, contact info@website.com'], 404);
    });

});
