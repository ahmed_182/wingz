<?php


Route::post('/update-fireBaseToken', 'User\Auth\FireBaseTokenUpdateController@index');
Route::post('/logout', 'User\Auth\LogoutController@index');

// miss password
Route::post('/reset-new-password', 'ResetPassword\IndexController@reset_new_password');

// Notification
Route::post('/user-notifications', 'Notification\IndexController@index');
Route::post('/user-notifications-count', 'Notification\IndexController@count');
Route::post('/user-seen-notifications', 'Notification\IndexController@seen');

// FavouritePlaces
Route::post('/favourite-places', 'FavouritePlace\IndexController@index');
Route::post('/add-favourite-place', 'FavouritePlace\IndexController@store');
Route::post('/remove-favourite-place', 'FavouritePlace\IndexController@remove');

// Slider ( slider-click )
Route::post('/slider-click', 'Slider\SliderClickController@index');

Route::post('/add-review', 'Review\IndexController@index');

// Trip All Side
Route::post('/trip-rides', 'Trip\IndexController@rides');
Route::post('/trip-details', 'Trip\IndexController@details');
Route::post('/trip-status', 'Trip\IndexController@ride_ststus');
Route::post('/trip-lifeCycle', 'Trip\IndexController@lifeCycle');
Route::post('/trip-finishRide', 'Trip\IndexController@finishRide');

// User Assign Trips
Route::post('/user-assign-trips', 'User\AssignTrip\IndexController@all_trips');
Route::post('/user-child-assign-trips', 'User\AssignTrip\IndexController@index');
Route::post('/user-accept-assign-trips', 'User\AssignTrip\IndexController@accept');
Route::post('/user-reject-assign-trips', 'User\AssignTrip\IndexController@reject');
Route::post('/user-meeting-assign-trips', 'User\AssignTrip\IndexController@meeting');


// Child absence
Route::post('/driver-add-child-absence', 'User\Children\Absence\IndexController@index');
Route::post('/parent-add-child-absence', 'User\Children\Absence\IndexController@parent_add');

Route::fallback(function () {
    return response()->json([
        'message' => 'make sure your request POST or it may be Page Not Found. If error persists, contact info@website.com'], 404);
});

