<?php


Route::group(['prefix' => 'user'], function () {
    Route::post('/login', 'User\Auth\LoginController@index');
});

Route::group(['prefix' => 'driver'], function () {
    Route::post('/login', 'Driver\Auth\LoginController@index');
    Route::post('/register', 'Driver\Auth\RegistrationController@index');
});

Route::group(['prefix' => 'representative'], function () {
    Route::post('/login', 'Representative\Auth\IndexController@index');
});


Route::post('user-relations', 'UserRelations\RelationsController@index');

Route::post('terms', 'Terms\IndexController@index');
Route::post('about-app', 'AboutApp\IndexController@index');
Route::post('/countries', 'Country\indexController@index');
Route::post('/cities', 'City\indexController@index');
Route::post('/districts', 'Distract\indexController@index');
Route::post('/all_cities', 'City\indexController@all_cities');

Route::post('/education_types', 'Education\indexController@index');
Route::post('/levels', 'Level\indexController@index');
//stages
Route::post('stages', 'Stage\IndexController@index');
Route::post('/schools', 'School\indexController@index');

Route::post('/contact-us', 'ContactUs\IndexController@index');
Route::post('/payment-types', 'Payment\IndexController@index');
Route::post('/car-types', 'CarType\IndexController@index');
Route::post('/car-names', 'CarName\IndexController@index');
Route::post('/manufacturing-years', 'Manufacturing\IndexController@index');
//  # Slider
Route::post('/slider-driver', 'SliderDriver\IndexController@index');
Route::post('/slider-user', 'SliderUser\IndexController@index');

// Cancellation Reasons
Route::post('/user-cancellation-reasons', 'CancellationReason\ReasonsController@index');
Route::post('/driver-cancellation-reasons', 'CancellationReason\ReasonsController@driver');

// Plans
Route::post('plans', 'Plan\IndexController@index');


// price Per Kilo
Route::post('price-per-kilo', 'PricePerKilo\IndexController@index');

//weather-messages
Route::post('weather-messages', 'Weather\IndexController@index');

Route::post('/salary-package', 'Salary_package\IndexController@index');

// brands
Route::post('partners', 'Partner\IndexController@index');

// Redeem
Route::post('redeem', 'Redeem\IndexController@index');

// Order Car Type
Route::post('order-car-type', 'OrderCarType\IndexController@index');

//openScreen
Route::post('/openScreen', 'openScreen\IndexController@openScreen');

// Taxes
Route::post('taxes', 'Tax\IndexController@index');


// Mobile Vefiy :-
Route::post('check-exist-mobile', 'Validation\IndexController@index');


// Trips Ride Status :-
Route::post('trip-ride-status', 'TripRideStatus\IndexController@index');

// Trips Ride Status :-
Route::post('status-for-child', 'TripRideStatus\IndexController@status_for_child');

Route::fallback(function () {
    return response()->json([
        'message' => 'Page Not Found. If error persists, contact wingz@info.com'], 404);
});

