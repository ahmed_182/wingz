<?php

Route::group(['prefix' => 'representative'], function () {
    Route::post('/profile', 'Auth\ProfileController@index');

    //   representative Order receipt :-
    Route::post('/representative-order-receipts', 'Order\Receipt\IndexController@index');
    Route::post('/representative-order-receipt-status', 'Order\Receipt\IndexController@statusList');
    Route::post('/representative-order-receipt-status-change', 'Order\Receipt\IndexController@statusChange');

    Route::fallback(function () {
        return response()->json([
            'message' => 'Page Not Found. If error persists, contact info@website.com'], 404);
    });

});
