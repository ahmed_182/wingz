<?php

Route::group(['prefix' => 'user'], function () {

    Route::post('/profile', 'Auth\ProfileController@index');
    Route::post('/update-profile', 'Auth\UpdateProfileController@index');
    Route::post('/update-mobile', 'Auth\UpdateProfileController@updateMobile');
    Route::post('/update-child-mobile', 'Auth\UpdateProfileController@updateChildMobile');
    Route::post('/active-profile', 'Auth\ActiveProfileController@index');

    // Family Members
    Route::post('/add-new-member', 'FamilyMember\AddFamilyMemberController@index');
    Route::post('/get-members', 'FamilyMember\GetFamilyMemberController@index');

    // Children
    Route::post('/add-new-child', 'Children\AddChildController@index');
    Route::post('/add-child-afterSchool', 'Children\AddChildAfterSchoolController@index');
    Route::post('/get-children', 'Children\GetChildController@index');
    Route::post('/child-profile', 'Children\ChildProfileController@index');
    Route::post('/delete-child', 'Children\DeleteChildController@index');
    Route::post('/update-child', 'Children\UpdateChildController@index');

    // user child absence
    Route::post('/user-child-absence', 'Children\AbsenceController@index');
    Route::post('/user-delete-child-absence', 'Children\AbsenceController@delete');


    // plan
    Route::post('/join-plan', 'Plan\JoinPlanController@index');
    Route::post('/plans-list', 'Plan\GetPlanController@index');

    // Make Driver Report
    Route::post('/add-driver-report', 'Report\AddReportController@index');
    Route::post('/driver-id-reports', 'Report\DriverIdReportController@index');

    // Assigned Driver
    Route::post('/assigned-driver', 'Assigned\IndexController@index'); // admin panel will send if in notification ( assigned_id ) .
    Route::post('/meet-assigned-driver', 'Assigned\IndexController@meet');
    Route::post('/accept-assigned-driver', 'Assigned\IndexController@accept');
    Route::post('/reject-assigned-driver', 'Assigned\IndexController@index');

    // Make trip
    Route::post('/add-trip', 'Trip\MakeTripController@index');
    Route::post('/user-trips', 'Trip\GetTripController@index');

    // Send Request Order
    Route::post('/add-order', 'Order\MakeOrderController@index');
    Route::post('/user-orders', 'Order\GetOrderController@index');
    Route::post('/user-order-details', 'Order\GetOrderController@details');

    //   User Order receipt :-
    Route::post('/user-order-receipts', 'Order\Receipt\IndexController@index');
    Route::post('/user-order-receipts-pending', 'Order\Receipt\IndexController@pending');
    Route::post('/user-order-receipts-payment_request', 'Order\Receipt\IndexController@payment_request');
    Route::post('/user-order-receipts-inWay', 'Order\Receipt\IndexController@inWay');
    Route::post('/user-order-receipts-paid_done', 'Order\Receipt\IndexController@paid_done');
    Route::post('/user-order-receipts-paid_failed', 'Order\Receipt\IndexController@paid_failed');
    // :: create ::
    Route::post('/user-add-order-receipts-payment_request', 'Order\Receipt\AddReceiptController@index');

    // children Trip
    Route::post('/children', 'Plan\JoinPlanController@index');

    // Voucher
    Route::post('/request-add-promo-code', 'PromoCode\UserVoucherController@requestAddPromoCode');


    // Commissary
    Route::post('/commissary-reasons', 'Commissary\IndexController@reasons');
    Route::post('/add-commissary', 'Commissary\IndexController@store');
    Route::post('/commissary', 'Commissary\IndexController@index');

    // points
    Route::post('/points', 'UserPoints\IndexController@index');
    Route::post('/points-terms', 'UserPoints\IndexController@terms');

    // Wallet
    Route::post('/wallet-history', 'UserWallet\IndexController@index');

    // Trips User side
    Route::post('/children-trips', 'Trip\childrenTripController@index');

    // # Single Ride
    // add single ride
    Route::post('/add-single-ride', 'SingleRide\AddSingleRideController@index');
    // rate single ride ( User Part )
    Route::post('/rate-single-ride', 'SingleRide\RateSingleRideController@index');
    // single ride  details
    Route::post('single-ride-details', 'SingleRide\DetailsSingleRideController@index');
    // client single rides
    Route::post('get-single-ride', 'SingleRide\GetSingleRideController@index');
    // client ( child )  single rides
    Route::post('get-child-single-ride', 'SingleRide\GetSingleRideController@childRides');
    // cancelByClient
    Route::post('cancel-single-ride', 'SingleRide\CancelSingleRideController@index');
    // exist Single Ride
    Route::post('exist-single-ride', 'SingleRide\ExistSingleRideController@index');


    Route::fallback(function (Request $request) {
        $response['message'] = "Page Not Found.If error persists,contact info@wings.com";
        $response['statusCode'] = 404;
        $statusCode = 404;
        return \Response::json($response, $statusCode);
    });

});

