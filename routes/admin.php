<?php

// admin

Route::get('/dash', 'Dashboard\DashboardController@index');

// receipts :-
Route::resource('/tripReceipt', 'tripReceipt\IndexController');
Route::get('/applyRepresentative/{id}', 'tripReceipt\IndexController@applyRepresentative');
Route::post('/saveApplyRepresentative', 'tripReceipt\IndexController@saveApplyRepresentative');


// meeting_requests
Route::get('/meeting_requests', 'Trip\meetingRequest\IndexController@index');

Route::resource('/openScreens', 'openScreen\IndexController');

// pending children
Route::get('/pending_children', 'Child\Pending_trip\IndexController@index');
Route::get('/assign_drivers/{id}', 'Child\Pending_trip\IndexController@assign_drivers');
Route::post('/child_assign_drivers', 'Child\Pending_trip\IndexController@child_assign_drivers');
Route::get('/pending_child_map_location/{id}', 'Child\Pending_trip\IndexController@pending_child_map_location');

// Plans
Route::resource('/plans', 'Plan\IndexController');
Route::resource('/plans.vouchers', 'PlansVoucher\IndexController');
Route::get('/DeactivatePlan/{id}', 'Plan\ActionController@DeactivatePlan');
Route::get('/activatePlan/{id}', 'Plan\ActionController@activatePlan');

// Review List
Route::get('/getReviews/{id}', 'Review\IndexController@index');

// order
Route::resource('/orders', 'Order\IndexController');
Route::get('/orders/{id}/reject', 'Order\IndexController@reject');
Route::post('/orders/{id}/reject', 'Order\IndexController@doReject');
Route::resource('/order_status', 'Order\OrderStatusController');
Route::get('/order_client_drivers_responded/{id}', 'Order\IndexController@responded');

Route::resource('/users', 'User\IndexController');
Route::get('/userVoucher/{id}', 'User\VoucherController@index');
Route::get('/familyMembers/{id}', 'User\Family\IndexController@index');
Route::get('/users_not_answer_drivers_request', 'User\Family\IndexController@users_not_answer_drivers_request');
Route::resource('/users.children', 'User\userChildrenController');
Route::resource('/users.familyMembers', 'User\userFamilyMembersController');
Route::resource('/users.wallet_details', 'User\userWalletController');
Route::resource('/users.points', 'User\userPointsController');
Route::resource('/users.single_rides', 'User\userSingleRideController');
Route::resource('/users.favourite_places', 'User\userFavouritePlacesController');
Route::get('/placeLocation/{id}', 'User\userFavouritePlacesController@location');
Route::resource('/countries', 'Country\IndexController');
Route::resource('/countries.start_with', 'Country\StartWith\IndexController');
Route::resource('/countries.cities', 'Country\City\IndexController');
Route::resource('/cities', 'City\IndexController');
Route::resource('/districts', 'District\IndexController');
Route::resource('/terms', 'Term\IndexController');

Route::get('/userNotifiy/{id}', 'User\UserNotificationController@index');
Route::post('/userNotifiyStore', 'User\UserNotificationController@userNotifiyStore');
// Relations
Route::resource('/relations', 'Relation\IndexController');

// Slider User , Driver
Route::resource('/sliders', 'Slider\IndexController');
Route::resource('/sliders.click', 'SliderClicks\IndexController');
Route::resource('/user_slider', 'Slider\UserSliderController');
Route::resource('/driver_slider', 'Slider\DriverSliderController');

Route::resource('/order_car_types', 'Order\carType\IndexController');
Route::resource('/car_types', 'carType\IndexController');
Route::resource('/car_names', 'CarName\IndexController');
Route::resource('/education_types', 'EducationType\IndexController');
Route::resource('/levels', 'Level\IndexController');
Route::resource('/stages', 'Stage\IndexController');
Route::resource('/manufacturing_years', 'ManufacturingYear\IndexController');
Route::resource('/payment_types', 'PaymentType\IndexController');
Route::resource('/schools', 'School\IndexController');
Route::resource('/schools.levels', 'School\Level\IndexController');
Route::resource('/schoolLevels.school_times', 'SchoolTime\IndexController'); // stages
Route::resource('/school_times_stages.holidays', 'SchoolTimeStages\IndexController'); // stages holidays
Route::resource('/stage_holidays.days', 'StageHolidayDays\IndexController'); // stages holidays Days


Route::get('/schoolsAddress/{id}', 'School\IndexController@address');


// Driver Side
Route::resource('/drivers', 'Driver\IndexController');
Route::get('/active_drivers', 'Driver\FilterController@active_drivers');
Route::get('/waiting_drivers', 'Driver\FilterController@waiting_drivers');
Route::get('/block_drivers', 'Driver\FilterController@block_drivers');

// Driver Singlr Ride
Route::resource('/drivers.single_rides', 'Driver\SingleRide\DriverSingleRideController');


//Driver
Route::resource('/drivers.orders', 'Driver\Order\IndexController');
Route::resource('/drivers.districts', 'Driver\City\IndexController');
Route::resource('/drivers.criming_recorder', 'Driver\CrimingRecorder\IndexController');
Route::resource('/drivers.driving_licences', 'Driver\DrivingLicences\IndexController');
Route::resource('/drivers.driving_car_licences', 'Driver\DrivingCarLicences\IndexController');
Route::resource('/drivers.driving_car_images', 'Driver\DrivingCarImages\IndexController');
Route::resource('/drivers.driver_identities', 'Driver\DriverIdentities\IndexController');
Route::resource('/drivers.driver_reports', 'Driver\DriverReports\IndexController');
Route::resource('/drivers.driver_share_infos', 'Driver\driverShareInfo\IndexController');
Route::get('/drivers_driving_license_block/{id}', 'Driver\BlockController@index');

// Active , deActive accounts ( user , Driver )
Route::get('/active_account/{id}', 'Driver\ActiveAccountController@index');
Route::get('/drActive_account/{id}', 'Driver\ActiveAccountController@drActive_account');

// delete , return ( Child Account ).
Route::get('/retuen_account/{id}', 'Child\AccountController@index');
Route::get('/delete_account/{id}', 'Child\AccountController@delete_account');


// Trip
Route::resource('/trips', 'Trip\IndexController');
Route::resource('/trips.children', 'TripChildren\IndexController');
Route::resource('/trips.rides', 'TripRide\IndexController');
//    Route::get('/TripChildren/{id}', 'Trip\IndexController@TripChildren');

//trips_ride_status
Route::resource('/trips_ride_status', 'TripsRideStatus\IndexController');


// Attach
Route::get('/attach/{id}/school/{school_id}', 'Attach\IndexController@index');
Route::get('/order/{order_id}/attach/{trip_id}/store', 'Attach\IndexController@store');
Route::post('/addTripsWithDrivers', 'Trip\TripWithDriver\IndexController@index');

//voucher
Route::resource('/voucher-type', 'Voucher_type\IndexController');
Route::resource('/voucher', 'Voucher\IndexController');
Route::resource('/voucher.plans', 'VoucherPlans\IndexController');
Route::resource('/voucher.user', 'VoucherUser\IndexController');


// Month
Route::resource('/months', 'Month\IndexController');
Route::resource('/months.days', 'Day\IndexController');

// Tax
Route::resource('/tax_type', 'TaxType\IndexController');
Route::resource('/taxes', 'Tax\IndexController');


Route::resource('/weather_messages', 'Weather\IndexController');

// Notifications
Route::resource('/notifications', 'Notification\IndexController');

// Representative
Route::resource('/representative', 'Representative\IndexController');
Route::resource('/representative.districts', 'Representative\City\IndexController');

// redeem
Route::resource('/redeems', 'Redeem\IndexController');
Route::resource('/points', 'Point\IndexController');

// brands
Route::resource('/brands', 'Brand\IndexController');
// salary Packages
Route::resource('/salary_packages', 'SalaryPackage\IndexController');

Route::get('/afterSchool/{id}', 'afterSchool\IndexController@index');

// commissary
Route::resource('/commissary_reasons', 'Commissary\ReasonController');
Route::resource('/users.commissaries', 'Commissary\IndexController');
Route::get('/ConfirmCommissary/{id}', 'Commissary\IndexController@ConfirmCommissary');


//contacts
Route::resource('/contacts', 'ContactUs\IndexController');


//setting
Route::resource('/setting', 'Setting\IndexController');//

Route::get('/users/{id}/block', 'User\IndexController@block');

Route::get('/deblock/{id}', 'User\IndexController@deblock');

Auth::routes();
