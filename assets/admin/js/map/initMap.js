function initMap() {

    mapProp = {
        center: new google.maps.LatLng(lat, lng),
        zoom: 15,
        gestureHandling: 'greedy'
    };

    map = new google.maps.Map(document.getElementById("map"), mapProp);
}

