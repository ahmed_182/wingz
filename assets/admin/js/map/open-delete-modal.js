$(document).on('click', '.deleteButton', function () {
    id = $(this).data('id');
    action = $(this).data('action');
    message = $(this).data('message');
    $(".deleteMessage").text(message);
    $(".deleteId").val(id);
    $(".deleteForm").attr('action', action);
});

$(document).on('click', '.showButton', function () {
    id = $(this).data('id');
    message = $(this).data('message');
    $(".showMessage").text(message);
    $(".showId").val(id);
});

