<?php

    use Illuminate\Database\Seeder;

    class PlansSeeder extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $item['type_id'] = 1;
            $item['price_per_month'] = 1200;
            $item['extra_seats'] = 2;
            \App\Plan::create($item); // basic

            $item['type_id'] = 2;
            $item['price_per_month'] = 1400;
            $item['extra_seats'] = 5;
            \App\Plan::create($item); // stander


            $item['type_id'] = 3;
            $item['price_per_month'] = 1700;
            $item['extra_seats'] = 9;
            \App\Plan::create($item); // Pro

        }
    }
