<?php

    use Illuminate\Database\Seeder;

    class ChildStatus extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $item['name_ar'] = "في المنزل";
            $item['name_en'] = "At Home";
            \App\Child_status::create($item);

            $item['name_ar'] = "الطريق الي المدرسه";
            $item['name_en'] = "Way To School";
            \App\Child_status::create($item);

            $item['name_ar'] = "في المدرسه";
            $item['name_en'] = "At School";
            \App\Child_status::create($item);

            $item['name_ar'] = "في الطريق الي المنزل";
            $item['name_en'] = "Way To home";
            \App\Child_status::create($item);

            $item['name_ar'] = "بعد المدرسه";
            $item['name_en'] = "After School";
            \App\Child_status::create($item);

            $item['name_ar'] = "مشكله في السياره";
            $item['name_en'] = "Car Problem";
            \App\Child_status::create($item);
        }
    }
