<?php

    use Illuminate\Database\Seeder;

    class SingleRideStatus extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        /*
    *  const  clientAddOrder ="1";
       const  driverAcceptOrder="2";
       const  driverInHisWayToPickup="3";
       const  driverArrivedToPickup="4";
       const  driverInHisWayToDestination="5";
       const  driverArrivedToDestination="6";
       const  driverCompleteOrder="7";
       const  clientAddScheduledOrder ="8";
       const  clientCancelOrder="9";
       const  driverNotFound="10";
       const  driverRejectOrder="11";
       const  driverCancelOrder="12";
       const  adminCancelOrder="13";
    */

        public function run()
        {
            \App\SingleRideStatus::query()->truncate();
            $this->add('العميل اضافة طلب جديد', 'Client added new request'); // 1
            $this->add('السائق وافق على الطلب', 'Driver accepted the request'); // 2
            $this->add('السائق في طريقه إلى العميل', 'Driver in his way to provider'); // 3
            $this->add('السائق وصل إلى العميل', 'Driver arrived to provider'); // 4
            $this->add('السائق في طريقه إلى الوجه', 'Driver in his way to client'); // 5
            $this->add('السائق وصل إلى الوجه', 'Driver arrived to client'); // 6
            $this->add('السائق اكمل الطلب', 'Driver completed the request'); // 7
            $this->add('العميل اضافة طلب جديد مجدول', 'Client added a new scheduled request'); // 8
            $this->add('العميل ألغى الطلب', 'Client cancelled the request'); // 9
            $this->add('لا يوجد سائقين', 'Drivers are unavailable'); // 10
            $this->add('السائق رفض الطلب', 'Driver rejected the request'); // 11
            $this->add('السائق ألغى الطلب', 'Driver cancelled the request'); // 12
            $this->add('الإدارة ألغت الطلب', 'Admin cancelled the request'); // 13

        }

        public function add($name_ar, $name_en)
        {
            $data['name_ar'] = $name_ar;
            $data['name_en'] = $name_en;
            \App\SingleRideStatus::create($data);
        }
    }
