<?php

    use Illuminate\Database\Seeder;

    class DatabaseSeeder extends Seeder
    {
        /**
         * Seed the application's database.
         *
         * @return void
         */
        public function run()
        {
            $this->call(UsersRelations::class);
            $this->call(PaymentTypes::class);
            $this->call(PlansSeeder::class);
            $this->call(OrderStatus::class);
            $this->call(ChildStatus::class);
            $this->call(SingleRideStatus::class);
            $this->call(TripRideStatus::class);
        }
    }
