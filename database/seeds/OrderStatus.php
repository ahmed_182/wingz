<?php

    use Illuminate\Database\Seeder;

    class OrderStatus extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $item['name_ar'] = "قيد الانتظار";
            $item['name_en'] = "Pending";
            \App\Order_status::create($item);

            $item['name_ar'] = "مقبول";
            $item['name_en'] = "Accepted";
            \App\Order_status::create($item);

            $item['name_ar'] = "في الطريق";
            $item['name_en'] = "Onway";
            \App\Order_status::create($item);

            $item['name_ar'] = "رفوض";
            $item['name_en'] = "Rejected";
            \App\Order_status::create($item);

        }
    }
