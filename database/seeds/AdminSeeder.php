<?php

use App\ModulesConst\UserTyps;
use Illuminate\Database\Seeder;

class AdminSeeder extends Seeder
{
    public function run()
    {

        $item['user_type_id'] = UserTyps::admin;
        $item['api_token'] = "1";
        $item['name'] = "Admin";
        $item['user_name'] = "admin2020";
        $item['mobile'] = "01127755648";
        $item['email'] = "admin@admin.com";
        $item['password'] = bcrypt(123456);
        \App\User::create($item);


    }

}
