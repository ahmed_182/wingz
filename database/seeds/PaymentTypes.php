<?php

    use Illuminate\Database\Seeder;

    class PaymentTypes extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $item['name_ar'] = "فودافون كاش";
            $item['name_en'] = "Vodafon Cash";
            $item['image'] = "https://newtechclub.com/wp-content/uploads/2018/04/vodafone-cash.png";
            \App\Payment_types::create($item);

            $item['name_ar'] = "اورانج مني";
            $item['name_en'] = "Orang Money";
            $item['image'] = "https://www.free2com.net/file/get/18";
            \App\Payment_types::create($item);


            $item['name_ar'] = "البنك";
            $item['name_en'] = "Bank";
            $item['image'] = "https://banner2.cleanpng.com/20180612/ch/kisspng-indian-bank-finance-computer-icons-online-banking-5b2009280b0da1.2791780215288261520453.jpg";
            \App\Payment_types::create($item);

        }
    }
