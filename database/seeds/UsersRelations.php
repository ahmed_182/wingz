<?php

    use Illuminate\Database\Seeder;

    class UsersRelations extends Seeder
    {
        /**
         * Run the database seeds.
         *
         * @return void
         */
        public function run()
        {
            $item['name_ar'] = "أب";
            $item['name_en'] = "Father";
            \App\User_relation::create($item); // Father

            $item['name_ar'] = "أم";
            $item['name_en'] = "Mother";
            \App\User_relation::create($item); // Mother


            $item['name_ar'] = "جد";
            $item['name_en'] = "Grand Father";
            \App\User_relation::create($item); // Grand Father

            $item['name_ar'] = "جده";
            $item['name_en'] = "Grand Mother";
            \App\User_relation::create($item); // Grand Mother

            $item['name_ar'] = "اخري";
            $item['name_en'] = "Other";
            \App\User_relation::create($item); // Other
        }
    }
