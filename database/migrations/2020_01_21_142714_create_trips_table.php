<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateTripsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('trips', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('school_id')->nullable();
                $table->string('driver_id')->nullable();
                $table->string('status')->default(\App\ModulesConst\TripStatus::pending);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('trips');
        }
    }
