<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripOrderReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_order_receipts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('user_id')->nullable();
            $table->string('kilo_price')->nullable();
            $table->string('join_price')->nullable();
            $table->string('tax_value')->nullable();
            $table->string('price_with_discount')->nullable();
            $table->string('discount_value')->nullable();
            $table->string('total_price_without_discount')->nullable();
            $table->string('discount_per')->nullable();
            $table->string('is_discount')->nullable();
            $table->string('tax')->nullable();
            $table->string('price_after_tax')->nullable();
            $table->string('paid_time')->nullable(); // الوقت اللي تم فيه الدفع من قبل العميل
            $table->string('status')->nullable();
            $table->string('representative_id')->nullable(); // المندوب الي هيحصل قيمه الفاتوره من العميل
            $table->longText('note')->nullable();
            $table->longText('due_date')->nullable(); // the final date to get it from representative
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->string('address')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_order_receipts');
    }
}
