<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSingleRideRatesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('single_ride_rates', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('single_ride_id')->nullable();
                $table->string('driver_id')->nullable();
                $table->string('driver_rate')->nullable();
                $table->string('driver_feedBack')->nullable();
                $table->string('client_id')->nullable();
                $table->string('client_rate')->nullable();
                $table->string('client_feedBack')->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('single_ride_rates');
        }
    }
