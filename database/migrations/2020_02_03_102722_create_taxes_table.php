<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateTaxesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('taxes', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name_ar')->nullable();
                $table->string('name_en')->nullable();
                $table->string('tax_type_id')->nullable();
                $table->string('value')->nullable();
                $table->string('is_enable')->default(0);
                $table->string('type')->default(\App\ModulesConst\TaxSystemType::system);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('taxes');
        }
    }
