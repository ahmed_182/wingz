<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOrderPlansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_plans', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('order_id')->nullable();
            $table->string('name')->nullable();
            $table->string('period')->nullable();
            $table->string('month_count')->nullable();
            $table->string('price_per_kilo')->nullable();
            $table->string('working_days_count')->nullable();
            $table->string('distance')->nullable();
            $table->string('join_price')->nullable();
            $table->string('tax_value')->nullable();
            $table->string('price_with_discount')->nullable();
            $table->string('discount_value')->nullable();
            $table->string('total_price')->nullable();
            $table->string('discount_per')->nullable();
            $table->string('is_discount')->nullable();
            $table->string('tax')->nullable();
            $table->string('price_after_tax')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_plans');
    }
}
