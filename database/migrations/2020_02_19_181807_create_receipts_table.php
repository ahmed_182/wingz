<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateReceiptsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('receipts', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('starting')->nullable();
            $table->string('moving')->nullable();
            $table->string('duration')->nullable();
            $table->string('cost')->nullable();
            $table->string('discount')->nullable();
            $table->string('balance')->nullable();
            $table->string('debit')->nullable();
            $table->string('driver_profit')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('receipts');
    }
}
