<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateCancellationReasonsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('cancellation_reasons', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->integer('user_type_id');
                $table->longText('name_ar');
                $table->longText('name_en');
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('cancellation_reasons');
        }
    }
