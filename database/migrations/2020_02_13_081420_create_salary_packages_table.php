<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSalaryPackagesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('salary_packages', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name_ar')->nullable();
                $table->string('name_en')->nullable();
                $table->longText('description_ar')->nullable();
                $table->longText('description_en')->nullable();
                $table->string('image')->nullable();
                $table->string('value')->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('salary_packages');
        }
    }
