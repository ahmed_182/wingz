<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSchoolTimesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('school_times', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('school_level_id')->nullable();
                $table->string('stage_id')->nullable();
                $table->string('start_at')->nullable();
                $table->string('end_at')->nullable();
                $table->string('year_start_at')->nullable();
                $table->string('year_end_at')->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('school_times');
        }
    }
