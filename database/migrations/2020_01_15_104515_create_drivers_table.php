<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateDriversTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('drivers', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('user_id')->nullable();
                $table->string('current_balance')->default(0);
                $table->string('children_count')->nullable();
                $table->string('car_photo')->nullable();
                $table->string('cat_type_id')->nullable();
                $table->string('cat_name_id')->nullable();
                $table->string('manufacturing_year_id')->nullable();
                $table->string('plate_number')->nullable();
                $table->string('driving_license')->nullable();
                $table->string('driver_type')->default(\App\ModulesConst\DriverTyps::captain); // Caption , share
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('drivers');
        }
    }
