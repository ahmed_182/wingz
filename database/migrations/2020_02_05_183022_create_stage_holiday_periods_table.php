<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStageHolidayPeriodsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('stage_holiday_periods', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('school_time_stage_id')->nullable();
                $table->string('start_at')->nullable();
                $table->string('end_at')->nullable();
                $table->string('paid_status')->default(1);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('stage_holiday_periods');
        }
    }
