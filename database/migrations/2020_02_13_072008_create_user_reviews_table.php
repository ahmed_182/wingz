<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateUserReviewsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('user_reviews', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('reviewer_id')->nullable(); // make review
                $table->string('user_id')->nullable();
                $table->string('rate')->nullable();
                $table->string('feedBack')->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('user_reviews');
        }
    }
