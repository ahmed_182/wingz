<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateSingleRidesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('single_rides', function (Blueprint $table) {
                $table->bigIncrements('id');

                // users
                $table->string('child_id')->nullable();
                $table->string('driver_id')->nullable();

                // pickup details
                $table->string('pickup_id')->nullable();
//                $table->string('pickup_lat')->nullable();
//                $table->string('pickup_lng')->nullable();
//                $table->string('pickup_name')->nullable();
//                $table->string('pickup_address')->nullable();

                // distension details
                $table->string('destination_id')->nullable();
//                $table->string('distension_lat')->nullable();
//                $table->string('distension_lng')->nullable();
//                $table->string('destination_name')->nullable();
//                $table->string('distension_address')->nullable();

                // single ride time
                $table->string('start_at')->nullable(); // the request will start at this time
                $table->string('end_at')->nullable();

                // ride information
                $table->longText('tracking')->nullable();
                $table->string('order_status_id')->default(1);
                $table->string('cancellation_reason_id')->nullable();

                //ride rate
                $table->string('rate_id')->nullable();

                // ride cost details
                $table->string('receipt_id')->nullable();

                // ride promo code discount .
                $table->string('voucher_id')->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('single_rides');
        }
    }
