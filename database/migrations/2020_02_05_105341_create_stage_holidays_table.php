<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateStageHolidaysTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('stage_holidays', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('school_time_stage_id')->nullable();
                $table->string('stage_holiday_period_id')->nullable();
                $table->string('day_long')->nullable();
                $table->string('day_stamp')->nullable();
                $table->string('paid_status')->default(1);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('stage_holidays');
        }
    }
