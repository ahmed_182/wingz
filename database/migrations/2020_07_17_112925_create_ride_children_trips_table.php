<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRideChildrenTripsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('ride_children_trips', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('ride_id')->nullable();
            $table->string('child_id')->nullable();
            $table->string('sort_by_distance')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('ride_children_trips');
    }
}
