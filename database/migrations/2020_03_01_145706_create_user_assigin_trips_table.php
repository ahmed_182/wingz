<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateUserAssiginTripsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('user_assigin_trips', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('user_id')->nullable();
                $table->string('child_id')->nullable();
                $table->string('trip_id')->nullable();
                $table->string('status_id')->default(\App\ModulesConst\AssignedStatus::pending);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('user_assigin_trips');
        }
    }
