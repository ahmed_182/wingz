<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripChildrensTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_childrens', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trip_id')->nullable();
            $table->string('child_id')->nullable();
            $table->string('status_id')->default(\App\Trip_children::pending);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_childrens');
    }
}
