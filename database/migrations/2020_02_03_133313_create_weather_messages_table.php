<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateWeatherMessagesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('weather_messages', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('name_ar')->nullable();
                $table->string('name_en')->nullable();
                $table->string('start_at')->nullable();
                $table->string('end_at')->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('weather_messages');
        }
    }
