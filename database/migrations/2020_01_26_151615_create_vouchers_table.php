<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateVouchersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('vouchers', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('code')->nullable();
                $table->text('description')->nullable();
                $table->string('discount')->nullable();
                $table->string('maximum_discount')->nullable();
                // The number of uses currently
                $table->string('uses')->nullable();
                // The max uses this voucher has
                $table->string('max_uses')->nullable();
                // How many times a user can use this voucher.
                $table->string('max_uses_user')->nullable();
                // The type can be: gift, discount.
                $table->string('type_id')->nullable();
                // When is enable or not
                $table->boolean('is_enable')->default(true);
                // Whether or not the voucher is a percentage or a fixed price.
                $table->boolean('is_fixed')->default(true);
                // When the voucher begins
                $table->timestamp('starts_at')->nullable();
                // When the voucher ends
                $table->timestamp('expires_at')->nullable();
                // user Can use it only ( new user )
                $table->boolean('is_new_user')->default(false);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('vouchers');
        }
    }
