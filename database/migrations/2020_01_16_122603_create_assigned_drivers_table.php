<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateAssignedDriversTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('assigned_drivers', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string("user_id")->nullable();
                $table->string("driver_id")->nullable();
                $table->string("status")->default(\App\ModulesConst\AssignedStatus::pending);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('assigned_drivers');
        }
    }
