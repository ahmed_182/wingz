<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateOrderCarTypesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('order_car_types', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string("name_ar")->nullable();
                $table->string("name_en")->nullable();
                $table->string("description_ar")->nullable();
                $table->string("description_en")->nullable();
                $table->string("image")->nullable();
                $table->string("price_per_kilo")->nullable();
                $table->string("price_per_kilo_after_school")->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('order_car_types');
        }
    }
