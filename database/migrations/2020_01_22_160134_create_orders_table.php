<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateOrdersTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('orders', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('trip_order_receipt_id')->nullable();
                $table->string('user_id')->nullable();
                $table->string('favourite_place_id')->nullable();
                $table->string('driver_id')->nullable();
                $table->string('child_id')->nullable();
                $table->string('school_id')->nullable();
                $table->string('distance')->nullable();
                $table->string('kilo_price')->nullable();
                $table->string('status')->default(\App\ModulesConst\OrderStatus::pending);
                $table->string('payment_status')->default(\App\ModulesConst\PaymentStatus::not_active);
                $table->string('plan_id')->nullable();
                $table->string('car_type_id')->nullable();
                $table->string('rejected_reason')->nullable();
                $table->string('working_days_count')->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('orders');
        }
    }
