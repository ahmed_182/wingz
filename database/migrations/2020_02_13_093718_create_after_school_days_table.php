<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateAfterSchoolDaysTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('after_school_days', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('after_school_id')->nullable();
                $table->string('day')->nullable();
                $table->string('time')->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('after_school_days');
        }
    }
