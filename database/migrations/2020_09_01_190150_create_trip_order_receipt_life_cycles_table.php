<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripOrderReceiptLifeCyclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_order_receipt_life_cycles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trip_order_receipt_status_id')->nullable();
            $table->string('trip_order_receipt_id')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_order_receipt_life_cycles');
    }
}
