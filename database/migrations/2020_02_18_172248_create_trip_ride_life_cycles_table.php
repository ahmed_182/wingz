<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTripRideLifeCyclesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('trip_ride_life_cycles', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('trip_id')->nullable();
            $table->string('child_id')->nullable();
            $table->string('trip_ride_id')->nullable();
            $table->string('trip_ride_status_id')->nullable();
            $table->string('lat')->nullable();
            $table->string('lng')->nullable();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('trip_ride_life_cycles');
    }
}
