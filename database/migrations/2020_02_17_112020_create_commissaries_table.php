<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateCommissariesTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('commissaries', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string('user_id')->nullable();
                $table->string('commissary_reason_id')->nullable();
                $table->string('amount')->nullable();
                $table->string('request_date')->nullable();
                $table->string('confirm_date')->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('commissaries');
        }
    }
