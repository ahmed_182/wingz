<?php

    use App\ModulesConst\ChildStatus;
    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateChildrenTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('children', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->string("user_id")->nullable();
                $table->string("name")->nullable();
                $table->string("mobile")->nullable();
                $table->string("birthday")->nullable();
                $table->string("image")->nullable();
                $table->string("school_time_id")->nullable();
                $table->string("is_deleted")->default(0); // 0 no , 1 yes
                $table->string("status_id")->default(ChildStatus::home);
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('children');
        }
    }
