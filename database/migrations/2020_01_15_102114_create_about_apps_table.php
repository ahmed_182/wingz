<?php

    use Illuminate\Support\Facades\Schema;
    use Illuminate\Database\Schema\Blueprint;
    use Illuminate\Database\Migrations\Migration;

    class CreateAboutAppsTable extends Migration
    {
        /**
         * Run the migrations.
         *
         * @return void
         */
        public function up()
        {
            Schema::create('about_apps', function (Blueprint $table) {
                $table->bigIncrements('id');
                $table->longText("name_ar")->nullable();
                $table->longText("name_en")->nullable();
                $table->string("kilo_price")->nullable();
                $table->timestamps();
            });
        }

        /**
         * Reverse the migrations.
         *
         * @return void
         */
        public function down()
        {
            Schema::dropIfExists('about_apps');
        }
    }
