@extends('admin.layout.forms.edit.index')
@section('action' , "car_types/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.car_types'))
@section('form-groups')
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.seat_number'),'name'=>'seat_number', 'placeholder'=>trans('language.seat_number')])

@endsection
@section('submit-button-title' ,trans('language.edit'))
