@extends('admin.layout.index')
@section('content')
    <style>
        a {
            color: #E96F6E;
        }
    </style>

    @php
        $all_client  =  \App\User::where('user_type_id',\App\ModulesConst\UserTyps::user)->get();
        $not_active_clients  =  \App\User::where('user_type_id',\App\ModulesConst\UserTyps::user)->where("userVerify",0)->get();
        $all_drivers =  \App\User::where('user_type_id',\App\ModulesConst\UserTyps::driver)->get();
        $request_meting = \App\User_assigin_trips::where('status_id',   \App\ModulesConst\AssignedStatus::meeting)->get();
        $childreens = \App\Child::get();
       $School_LatArr= \App\School::orderBy('id', 'desc')->pluck('lat');
       $School_LngArr= \App\School::orderBy('id', 'desc')->pluck('lng');

        $Cars_LatArr= \App\User::where('user_type_id',\App\ModulesConst\UserTyps::driver)->pluck('lat');
        $Cars_LngArr= \App\User::where('user_type_id',\App\ModulesConst\UserTyps::driver)->pluck('lng');


        $all_stop_cars =  \App\Trip::whereIn("status",[17 , 40 ])->get();
        $all_run_cars =  \App\Trip::whereNotIn("status",[17 , 40 ])->get();

    @endphp

    <div class="row">
        <div class="col-12 col-xl-12 stretch-card">
            <div class="row flex-grow">
                <div class="col-md-2 grid-margin stretch-card">
                    <div class="card">
                        <a href="{{url("/admin/users")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.clients')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($all_client)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>
                <div class="col-md-2 grid-margin stretch-card">
                    <div class="card">
                        <a href="{{url("/admin/users")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.not_active_users')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($not_active_clients)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>

                </div>

                <div class="col-md-2 grid-margin stretch-card">
                    <div class="card">
                        <a href="{{url("/admin/drivers")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.drivers')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($all_drivers)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>
                <div class="col-md-2 grid-margin stretch-card">
                    <div class="card">

                        <a href="{{url("/admin/payment_types")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.meeting_requests')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($request_meting)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>

                <div class="col-md-2 grid-margin stretch-card">
                    <div class="card">

                        <a href="{{url("/admin/payment_types")}}">
                            <div class="card-body">
                                <div class="d-flex justify-content-between align-items-baseline">
                                    <h6 class="card-title mb-0">{{trans('language.children')}}</h6>
                                    <div class="dropdown mb-2"></div>
                                </div>
                                <div class="row">
                                    <div class="col-6 col-md-12 col-xl-5">
                                        <h3 class="mb-2">{{count($childreens)}}</h3>
                                        <div class="d-flex align-items-baseline">
                                        </div>
                                    </div>
                                    <div class="col-6 col-md-12 col-xl-7">
                                        <div id="" class="mt-md-3 mt-xl-0"></div>
                                    </div>
                                </div>
                            </div>
                        </a>
                    </div>
                </div>


            </div>
        </div>

    </div>








    <div class="col-md-12">
        <div id="googleMap" style="width:100%;height:750px;"></div>
    </div>




@endsection
@section("extra_js")
    <script>
        var School_Latrr = @json($School_LatArr);
        var SchoolLngArr = @json($School_LngArr);


        var lat = 30.008383;
        var lng = 30.008383;

        if (School_Latrr.length > 0) {
            var mapProp = {
                center: new google.maps.LatLng(School_Latrr[0], SchoolLngArr[0]),
                zoom: 9
            };
        } else {
            var mapProp = {
                center: new google.maps.LatLng(lat, lng),
                zoom: 9
            };
        }

        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        var goldenGatePosition = {lat: lat, lng: lng};
        for (i = 0; i < School_Latrr.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(School_Latrr[i], SchoolLngArr[i]),
                map: map,
                animation: google.maps.Animation.DROP,
                icon: "{{asset('assets/admin/images/school.png')}}",
            });
        }
    </script>

    <script>
        var all_stop_cars = @json($all_stop_cars)

        for (s = 0; s < all_stop_cars.length; s++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(all_stop_cars[s]["driver"]["lat"], all_stop_cars[s]["driver"]["lng"]),
                map: map,
                animation: google.maps.Animation.DROP,
                icon: "{{asset('assets/admin/images/car/red_car.png')}}",
            });
        }

    </script>

    <script>

        var all_run_cars = @json($all_run_cars)

        for (l = 0; l < all_run_cars.length; l++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(all_run_cars[l]["driver"]["lat"], all_run_cars[l]["driver"]["lng"]),
                map: map,
                animation: google.maps.Animation.DROP,
                icon: "{{asset('assets/admin/images/car/green_car.png')}}",
            });
        }
    </script>
@endsection
