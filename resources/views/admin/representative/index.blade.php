@extends('admin.layout.table.index')
@section('page-title',trans('language.representatives'))
@section('root' , "representative")
@section('buttons')
<br>
    @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/representative/create"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.representatives')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.mobile')}}</th>
    <th>{{trans('language.country')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->dash_image])</td>
            <td>{{$item->dash_name}}</td>
            <td>{{$item->dash_mobile}}</td>
            <td>{{$item->dash_country_name}}</td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "representative/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/representative/$item->id")])
                <br>
                @includeIf("admin.components.buttons.custom" , ["href" => "representative/$item->id/districts/", 'class' => 'default' , 'title'=> trans('language.districts'), 'icon' => 'fa fa-building','feather' => 'list'])
            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/representative/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control" name="name" placeholder="{{trans('language.name')}}">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" name="mobile" placeholder="{{trans('language.mobile')}}">
            </div>
            <div class="col-md-2">
                <input type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
            </div>
        </div>
    </form>
@stop



