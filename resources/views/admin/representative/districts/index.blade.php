@extends('admin.layout.table.index')
@section('page-title',trans('language.districts'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => "districts/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_district_name}}</td>
            <td>
                 @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/representative/$item->representative_id/districts/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


