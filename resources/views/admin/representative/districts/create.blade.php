@extends('admin.layout.forms.add.index')
@section('action' , "representative/$driver->id/districts")
@section('title' , trans('language.add'))
@section('page-title',trans('language.districts'))
@section('form-groups')
    @includeIf('admin.components.form.add.select', ['label' => trans("language.districts"),'name'=>'district_id', 'items'=> $cities , 'icon' => 'fa fa-list',])

@endsection
@section('submit-button-title' , trans('web.add'))
