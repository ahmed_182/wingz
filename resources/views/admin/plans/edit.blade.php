@extends('admin.layout.forms.edit.index')
@section('action' , "plans/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.plans'))
@section('form-groups')
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.period'),'name'=>'period', 'placeholder'=>trans('language.period')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.month_count'),'name'=>'month_count', 'placeholder'=>trans('language.month_count')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.discount'),'name'=>'discount', 'placeholder'=>trans('language.discount')])

@endsection
@section('submit-button-title' ,trans('language.edit'))
