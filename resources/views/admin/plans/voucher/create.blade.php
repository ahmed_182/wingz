@extends('admin.layout.forms.add.index')
@section('action' , "plans/$plan->id/vouchers")
@section('title' , trans('language.add'))
@section('page-title',trans('language.voucher'))
@section('form-groups')

    @if(count($vouchers))
        @includeIf('admin.components.form.add.select', ['label' => trans("language.voucher"),'name'=>'voucher_id', 'items'=> $vouchers, 'icon' => 'fa fa-list',])
    @else
        <h2> {{trans('language.no_data')}} </h2>
    @endif
@endsection
@if(count($vouchers))
    @section('submit-button-title', trans('web.add'))
@endif

