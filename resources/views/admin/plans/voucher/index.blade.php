@extends('admin.layout.table.index')
@section('page-title',trans('language.voucher'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => "vouchers/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.voucher')}}</th>
    <th>{{trans('language.type')}}</th>
    <th>{{trans('language.starts_at')}}</th>
    <th>{{trans('language.expires_at')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td><a href="{{url("admin/voucher/$item->id")}}">{{ $loop->iteration }}</a></td>
            <td><a href="{{url("admin/voucher/$item->id")}}">{{$item->code}}</a></td>
            <td>{{$item->dash_type}}</td>
            <td>{{$item->dash_start}}</td>
            <td>{{$item->dash_expire}}</td>

            <td>
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/plans/$plan->id/vouchers/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


