@extends('admin.layout.forms.add.index')
@section('action' , "plans")
@section('title' , trans('language.add'))
@section('page-title',trans('language.plans'))
@section('form-groups')
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.days'),'name'=>'period', 'placeholder'=>trans('language.days')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.month_count'),'name'=>'month_count', 'placeholder'=>trans('language.month_count')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.discount'),'name'=>'discount', 'placeholder'=>trans('language.discount')])

@endsection
@section('submit-button-title' ,trans('language.add'))
