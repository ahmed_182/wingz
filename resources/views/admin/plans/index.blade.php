{{--@extends('admin.layout.table.index')--}}
{{--@section('page-title',trans('language.plans'))--}}
{{--@section('buttons')--}}

{{--@stop--}}
{{--@section('thead')--}}
{{--    <th>#</th>--}}

{{--    <th>{{trans('language.name')}}</th>--}}
{{--    <th>{{trans('language.period')}}</th>--}}
{{--    <th>{{trans('language.discount')}}</th>--}}
{{--    <th>{{trans('language.settings')}}</th>--}}
{{--@endsection--}}
{{--@section('tbody')--}}
{{--   @foreach($items as $item)--}}
{{--        <tr>--}}
{{--            <td>{{ $loop->iteration }}</td>--}}
{{--            <td>{{$item->dash_name}}</td>--}}
{{--            <td>{{$item->period}} {{trans('language.day')}}</td>--}}
{{--            <td>{{$item->discount}} % </td>--}}
{{--            <td>--}}
{{--                @includeIf("admin.components.buttons.edit" , ["href" => "plans/$item->id/edit"])--}}

{{--                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/plans/$item->id")])--}}

{{--            </td>--}}
{{--        </tr>--}}
{{--    @endforeach--}}
{{--@endsection--}}


@extends('admin.layout.index')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.plans')}}</h4>

                <ul class="nav nav-pills m-t-30 m-b-30">
                    <li class=" nav-item"><a href="#navpills-1" class="nav-link active" data-toggle="tab"
                                             aria-expanded="false">{{trans('language.active_plans')}} @if(count($active_items) > 0 )
                                ( {{count($active_items)}} ) @endif </a></li>
                    <li class="nav-item"><a href="#navpills-2" class="nav-link" data-toggle="tab"
                                            aria-expanded="false">{{trans('language.not_active_plans')}} @if(count($Notactive_items) > 0 )
                                ( {{count($Notactive_items)}} ) @endif </a></li>

                </ul>
                <br>
                @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/plans/create"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])


                <div class="tab-content br-n pn col-md-12">

                    <div id="navpills-1" class="tab-pane active">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('language.name')}}</th>
                                    <th>{{trans('language.period')}}</th>
                                    <th>{{trans('language.discount')}}</th>
                                    <th>{{trans('language.month_count')}}</th>
                                    <th>{{trans('language.settings')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($active_items as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{$item->dash_name}}</td>
                                        <td>{{$item->period}} {{trans('language.day')}}</td>
                                        <td>{{$item->discount}} %</td>
                                        <td>{{$item->month_count}}  </td>
                                        <td>
                                            @includeIf("admin.components.buttons.edit" , ["href" => "plans/$item->id/edit"])
                                            @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/plans/$item->id")])
                                            @includeIf("admin.components.buttons.custom" , ["href" => "DeactivatePlan/$item->id", 'class' => 'default' , 'title'=> trans('language.DeactivatePlan'), 'icon' => 'fa fa-ban',"feather"=>'x'])
                                            @includeIf("admin.components.buttons.custom" , ["href" => "plans/$item->id/vouchers/", 'class' => 'default' , 'title'=> trans('language.voucher'), 'icon' => 'fa fa-tasks',"feather"=>'gift'])
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="navpills-2" class="tab-pane">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('language.name')}}</th>
                                    <th>{{trans('language.period')}}</th>
                                    <th>{{trans('language.discount')}}</th>
                                    <th>{{trans('language.settings')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($Notactive_items as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{$item->dash_name}}</td>
                                        <td>{{$item->period}} {{trans('language.day')}}</td>
                                        <td>{{$item->discount}} %</td>
                                        <td>
                                            @includeIf("admin.components.buttons.edit" , ["href" => "plans/$item->id/edit"])

                                            @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/plans/$item->id")])

                                            @includeIf("admin.components.buttons.custom" , ["href" => "activatePlan/$item->id", 'class' => 'default' , 'title'=> trans('language.Activeplan'), 'icon' => 'fa fa-check',"feather"=>'check'])

                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>
@endsection

