@extends('admin.layout.index')
@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h1> {{ trans('language.notifications') }}</h1>
            </div>
        </div>
    </div>
    @if ($errors->any())
        <div class="alert alert-danger">
            <ul>
                @foreach ($errors->all() as $error)
                    <li>{{ $error }}</li>
                @endforeach
            </ul>
        </div>
    @endif




    <div class="container-fluid">
        <div class="card-body p-b-0">
            <ul class="nav nav-tabs customtab2" role="tablist">


                <li class="nav-item"><a class="nav-link active " data-toggle="tab" href="#users_rel" role="tab"
                                        aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span>
                        <span class="hidden-xs-down">{{ trans('language.users') }} </span></a></li>

                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#drivers_rel" role="tab"
                                        aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span>
                        <span class="hidden-xs-down">{{ trans('language.drivers') }}</span></a></li>

                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#countries_rel" role="tab"
                                        aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span>
                        <span class="hidden-xs-down">{{ trans('language.countries') }}</span></a></li>

                <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#cities_rel" role="tab"
                                        aria-selected="false"><span class="hidden-sm-up"><i class="ti-email"></i></span>
                        <span class="hidden-xs-down">{{ trans('language.cities') }}</span></a></li>


            </ul>

            <div class="tab-content">


                <div class="tab-pane p-20  show active" id="users_rel" role="tabpanel">
                    <br>
                    <h3>{{ trans('language.users') }}</h3>
                    <br>
                    <div class="col-md-12">


                        {{--  check All Users --}}
                        <div class="checkbox checkbox-success col-md-12">
                            <input id="users_checks" name="users[]" value="" class="chk-col-light-green selectall_users"
                                   type="checkbox">
                            <label class="label-filter" for="users_checks"> {{ trans('language.all') }}</label>
                        </div>


                        <form method="post" action="{{url("admin/notifications")}}"
                              enctype="multipart/form-data">
                            <div class="row">
                                @csrf
                                <input type="hidden" class="form-control" name="client_type"
                                       id="" value="onlineClients"
                                       placeholder="Title">
                                <div class="col-md-4">
                                    <div class="mt-checkbox-list">
                                        @foreach($clients as $client)
                                            @includeIf('admin.components.form.add.checklist', ['customClass'=>'individual_users','id'=>$client->id.'users','name'=>'users[]','value'=>$client->id,'class'=>'default','label'=>$client->name.'( '.$client->mobile.' )'])
                                        @endforeach
                                        <div class="text-center page-full-width">
                                            {{$clients->links()}}
                                        </div>

                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">

                                        <div class="col-md-12 row">
                                            <div style="margin: auto;" class="col-md-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i
                                                        class=" mdi mdi-format-title"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" name="title"
                                                           id="exampleInputEmail1"
                                                           placeholder="{{ trans('language.title') }}">
                                                </div>
                                                <br>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i
                                                        class="mdi mdi-message-text-outline"></i></span>
                                                    </div>
                                                    <textarea placeholder="{{ trans('language.body') }}" name="body"
                                                              class="form-control"
                                                              rows="8"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div style="text-align: center" class="col-md-12">
                                            <button type="submit" class="btn btn-success">{{ trans('language.send') }}
                                                <i
                                                    class="mdi  mdi-send"></i>
                                            </button>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </form>


                    </div>
                </div>
                <div class="tab-pane p-20  show" id="drivers_rel" role="tabpanel">
                    <br>
                    <h3>{{ trans('language.drivers') }}</h3>
                    <br>
                    <div class="col-md-12">

                        {{--  check All drivers --}}
                        <div class="checkbox checkbox-success col-md-12">
                            <input id="drivers_checks" name="technician[]" value="" class="chk-col-light-green selectall_drivers"
                                   type="checkbox">
                            <label class="label-filter" for="drivers_checks"> {{ trans('language.all') }}</label>
                        </div>
                        <form method="post" action="{{url("admin/notifications")}}"
                              enctype="multipart/form-data">
                            <div class="row">
                                @csrf
                                <input type="hidden" class="form-control" name="client_type"
                                       id="" value="paidClients"
                                       placeholder="Title">
                                <div class="col-md-4">
                                    <div class="mt-checkbox-list">
                                        @foreach($drivers as $driver)
                                            @includeIf('admin.components.form.add.checklist', ['customClass'=>'individual_drivers','id'=>$driver->id.'drivers','name'=>'drivers[]','value'=>$driver->id,'class'=>'default','label'=>$driver->name.'( '.$driver->mobile.' )'])

                                        @endforeach

                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">

                                        <div class="col-md-12 row">
                                            <div style="margin: auto;" class="col-md-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i
                                                        class=" mdi mdi-format-title"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" name="title"
                                                           id="exampleInputEmail1"
                                                           placeholder="{{ trans('language.title') }}">
                                                </div>
                                                <br>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i
                                                        class="mdi mdi-message-text-outline"></i></span>
                                                    </div>
                                                    <textarea placeholder="{{ trans('language.body') }}" name="body"
                                                              class="form-control"
                                                              rows="8"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div style="text-align: center" class="col-md-12">
                                            <button type="submit" class="btn btn-success">{{ trans('language.send') }}
                                                <i
                                                    class="mdi  mdi-send"></i>
                                            </button>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </form>


                    </div>
                </div>
                <div class="tab-pane p-20  show" id="countries_rel" role="tabpanel">
                    <br>
                    <h3>{{ trans('language.countries') }}</h3>
                    <br>
                    <div class="col-md-12">

                        {{--  check All Counties --}}
                        <div class="checkbox checkbox-success col-md-12">
                            <input id="countries_checks" name="technician[]" value="" class="chk-col-light-green selectall_countries"
                                   type="checkbox">
                            <label class="label-filter" for="countries_checks"> {{ trans('language.all') }}</label>
                        </div>

                        <form method="post" action="{{url("admin/notifications")}}"
                              enctype="multipart/form-data">
                            <div class="row">
                                @csrf

                                <input type="hidden" class="form-control" name="client_type"
                                       id="" value="unpaidClients"
                                       placeholder="Title">
                                <div class="col-md-4">
                                    <div class="mt-checkbox-list">
                                        @foreach($countries as $country)
                                            @includeIf('admin.components.form.add.checklist', ['customClass'=>'individual_countries','id'=>$country->id.'countries','name'=>'countries[]','value'=>$country->id,'class'=>'default','label'=>$country->dash_name])
                                        @endforeach

                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">

                                        <div class="col-md-12 row">
                                            <div style="margin: auto;" class="col-md-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i
                                                        class=" mdi mdi-format-title"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" name="title"
                                                           id="exampleInputEmail1"
                                                           placeholder="{{ trans('language.title') }}">
                                                </div>
                                                <br>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i
                                                        class="mdi mdi-message-text-outline"></i></span>
                                                    </div>
                                                    <textarea placeholder="{{ trans('language.body') }}" name="body"
                                                              class="form-control"
                                                              rows="8"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div style="text-align: center" class="col-md-12">
                                            <button type="submit" class="btn btn-success">{{ trans('language.send') }}
                                                <i
                                                    class="mdi  mdi-send"></i>
                                            </button>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </form>


                    </div>
                </div>
                <div class="tab-pane p-20  show" id="cities_rel" role="tabpanel">
                    <br>
                    <h3>{{ trans('language.cities') }}</h3>
                    <br>
                    <div class="col-md-12">

                        {{--  check All Cities --}}
                        <div class="checkbox checkbox-success col-md-12">
                            <input id="cities_checks" name="technician[]" value="" class="chk-col-light-green selectall_cities"
                                   type="checkbox">
                            <label class="label-filter" for="cities_checks"> {{ trans('language.all') }}</label>
                        </div>

                        <form method="post" action="{{url("admin/notifications")}}"
                              enctype="multipart/form-data">
                            <div class="row">
                                @csrf

                                <input type="hidden" class="form-control" name="client_type"
                                       id="" value="unpaidClients"
                                       placeholder="Title">
                                <div class="col-md-4">
                                    <div class="mt-checkbox-list">
                                        @foreach($cities as $city)
                                            @includeIf('admin.components.form.add.checklist', ['customClass'=>'individual_cities','id'=>$city->id.'cities','name'=>'cities[]','value'=>$city->id,'class'=>'default','label'=>$city->dash_name])
                                        @endforeach

                                    </div>
                                </div>
                                <div class="col-md-8">
                                    <div class="form-group">

                                        <div class="col-md-12 row">
                                            <div style="margin: auto;" class="col-md-12">
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i
                                                        class=" mdi mdi-format-title"></i></span>
                                                    </div>
                                                    <input type="text" class="form-control" name="title"
                                                           id="exampleInputEmail1"
                                                           placeholder="{{ trans('language.title') }}">
                                                </div>
                                                <br>
                                                <div class="input-group">
                                                    <div class="input-group-prepend">
                                                <span class="input-group-text" id="basic-addon1"><i
                                                        class="mdi mdi-message-text-outline"></i></span>
                                                    </div>
                                                    <textarea placeholder="{{ trans('language.body') }}" name="body"
                                                              class="form-control"
                                                              rows="8"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div style="text-align: center" class="col-md-12">
                                            <button type="submit" class="btn btn-success">{{ trans('language.send') }}
                                                <i
                                                    class="mdi  mdi-send"></i>
                                            </button>
                                        </div>

                                    </div>

                                </div>
                            </div>
                        </form>


                    </div>
                </div>
            </div>
        </div>
    </div>


@endsection

@section('extra_js')
    <script>

        // users
        $(".selectall_users").click(function () {
            $(".individual_users").prop("checked", $(this).prop("checked"));
        });
        // drivers
        $(".selectall_drivers").click(function () {
            $(".individual_drivers").prop("checked", $(this).prop("checked"));
        });
        // countries
        $(".selectall_countries").click(function () {
            $(".individual_countries").prop("checked", $(this).prop("checked"));
        });
        // cities
        $(".selectall_cities").click(function () {
            $(".individual_cities").prop("checked", $(this).prop("checked"));
        });

    </script>
    <script>
        $("#dash").removeClass("active");
        $(".notifications").addClass("open active").css("display", "block");
    </script>
@endsection
