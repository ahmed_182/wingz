@extends('admin.layout.forms.edit.index')
@section('action' , "trips_ride_status/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.order_status'))
@section('form-groups')
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.child_count'),'name'=>'child_count', 'placeholder'=>trans('language.child_count')])

    <div class="form-group">
        <label>{{trans('language.destination')}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-list"></i></span>
            </div>
            <select required class="form-control" name="destination">
                <option @if($item->destination == 1 ) selected @endif value="1">{{trans('language.school')}}</option>
                <option @if($item->destination == 2 ) selected @endif value="2">{{trans('language.home_place')}}</option>
            </select>
        </div>
    </div>
@endsection
@section('submit-button-title' ,trans('language.edit'))
