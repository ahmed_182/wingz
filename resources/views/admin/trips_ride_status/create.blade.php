@extends('admin.layout.forms.add.index')
@section('action' , "trips_ride_status")
@section('title' , trans('language.add'))
@section('page-title',trans('language.trips_ride_status'))
@section('form-groups')
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.child_count'),'name'=>'child_count', 'placeholder'=>trans('language.child_count')])

    <div class="form-group">
        <label>{{trans('language.destination')}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-list"></i></span>
            </div>
            <select required class="form-control" name="destination">
                <option value="1">{{trans('language.school')}}</option>
                <option value="2">{{trans('language.home_place')}}</option>
            </select>
        </div>
    </div>



@endsection
@section('submit-button-title' ,trans('language.add'))
