@extends('admin.layout.table.index')
@section('page-title',trans('language.children'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => "children/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop

@section('thead')
    <th>#</th>
    <th>{{trans('language.number')}}</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.is_vacation')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->day_number}}</td>
            <td>{{$item->dash_name}}</td>
            <td>{{$item->dash_is_vacation}} ( {{$item->dhas_vacation}} ) </td>
            <td>

            </td>
        </tr>
    @endforeach
@endsection


