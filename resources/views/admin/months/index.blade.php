@extends('admin.layout.table.index')
@section('page-title',trans('language.months'))
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.months')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>

    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.days')}}</th>
    <th>{{trans('language.settings')}}</th>

@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_name}}</td>

            <td>
                <a class="btn btn-info"
                   href="{{url("admin/months/$item->id/days/")}}"> {{trans('language.show')}} </a>
            </td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "months/$item->id/edit"])

                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/months/$item->id")])

            </td>
        </tr>
    @endforeach
@endsection


