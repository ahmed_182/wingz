@extends('admin.layout.table.index')
@section('page-title',trans('language.users'))
@section('root' , "users")
@section('buttons')
    <br>
    @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/users/create"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.users')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.email')}}</th>
    <th>{{trans('language.mobile')}}</th>
    {{--
        <th>{{trans('language.country')}}</th>
    --}}
    <th>{{trans('language.type')}}</th>
    {{--    <th>{{trans('language.children')}}</th>--}}
    <th>{{trans('language.family_members')}}</th>
    <th>{{trans('language.verfied_status')}}</th>
    <th>{{trans('language.block_status')}}</th>
    <th>{{trans('language.settings')}}</th>
    <th>{{trans('language.actions')}}</th>

@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->dash_image])</td>
            <td>{{$item->dash_name}}</td>
            <td>{{$item->dash_email}}</td>
            @if (app()->getLocale() == "en")
                <td>{{$item->dash_code}} {{$item->dash_mobile}}</td>
            @else
                <td> {{$item->dash_mobile}} {{$item->dash_code_ar}}</td>

            @endif
            {{--
                        <td>{{$item->dash_code}}</td>
            --}}
            <td><span class="label label-info">{{$item->dash_relation_name}}</span></td>
            {{--            <td>--}}
            {{--                <a class="btn btn-info"--}}
            {{--                   href="{{url("admin/users/$item->id/children/")}}"> {{trans('language.children')}} </a>--}}
            {{--            </td>--}}

            <td>
                @if($item->user_family_members())
                    <a class="btn btn-info"
                       href="{{url("admin/familyMembers/$item->id")}}"> {{trans('language.family_members')}} </a>
                @else
                    <label> {{ trans("language.no_family_members") }} </label>
                    <br>
                    <a class="btn btn-info"
                       href="{{url("admin/familyMembers/$item->id")}}"> {{trans('language.family_members')}} </a>
                @endif

            </td>
            {{--    <td>
                    {{$item->dash_status_name}}
                    <br>
                    <br>
                    @if($item->userVerify == \App\ModulesConst\UserVerify::no)
                        <a class="btn btn-primary"
                           href="{{url("admin/active_account/$item->id")}}"> {{trans('language.active')}} {{trans('language.account')}} </a>
                    @else
                        <a class="btn btn-primary"
                           href="{{url("admin/drActive_account/$item->id")}}"> {{trans('language.deActive')}} {{trans('language.account')}} </a>
                    @endif
                </td>--}}

            <td>

                <strong style="color: #0c5460;font-weight: bold">{{$item->dash_status_name}}</strong>
                <br>
                <br>
                @if($item->userVerify == \App\ModulesConst\UserVerify::no)
                    <a class="btn btn-success "
                       href="{{url("admin/active_account/$item->id")}}"
                       title="{{trans('language.active')}} {{trans('language.account')}}"> <i class=""
                                                                                              data-feather="check"></i>
                    </a>
                @else
                    <a class="btn btn-danger "
                       href="{{url("admin/drActive_account/$item->id")}}"
                       title="{{trans('language.deActive')}} {{trans('language.account')}}"> <i class=""
                                                                                                data-feather="x"></i>
                    </a>
                @endif
            </td>
            <td>

                <strong style="color: #0c5460;font-weight: bold">{{$item->dash_status_block}}</strong>
                <br>
                <br>
                @if($item->is_block == \App\ModulesConst\is_block::no)
                    <a class=""
                       href="{{url("admin/users/$item->id/block")}}"
                       title="{{trans('language.block')}} {{trans('language.account')}}"> <i
                            class="mdi mdi-account-check" data-feather="check"></i> </a>
                @else
                    <a class=" "
                       href="{{url("admin/deblock/$item->id")}}"
                       title="{{trans('language.deblock')}} {{trans('language.account')}}"> <i class=""
                                                                                               data-feather="alert-triangle"></i>
                    </a>
                @endif
            </td>


            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "users/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/users/$item->id")])
            </td>

            <td  style=" width: 200px;display: inline-block;">
                <div class="actions_conainter" style="overflow: auto; width: 100% ; padding-bottom: 15px; ">
                    @includeIf("admin.components.buttons.custom" , ["href" => "userVoucher/$item->id", 'class' => 'default' , 'title'=> trans('language.voucher'), 'icon' => 'fa fa-tasks','feather' => 'box'])
                    @includeIf("admin.components.buttons.custom" , ["href" => "userNotifiy/$item->id", 'class' => 'default' , 'title'=> trans('language.SendNotification'), 'icon' => 'fa fa-bell','feather' => 'bell'])
                    @includeIf("admin.components.buttons.custom" , ["href" => "users/$item->id/favourite_places", 'class' => 'default' , 'title'=> trans('language.favourite_places'), 'icon' => 'fa fa-map-marker','feather' => 'heart'])
                    @includeIf("admin.components.buttons.custom" , ["href" => "users/$item->id/commissaries", 'class' => 'default' , 'title'=> trans('language.commissaries'), 'icon' => 'fa fa-money','feather' => 'award'])
                    @includeIf("admin.components.buttons.custom" , ["href" => "users/$item->id/wallet_details", 'class' => 'default' , 'title'=> trans('language.wallet'), 'icon' => 'fa fa-paypal','feather' => 'dollar-sign'])
                    @includeIf("admin.components.buttons.custom" , ["href" => "users/$item->id/points", 'class' => 'default' , 'title'=> trans('language.points'), 'icon' => 'fa fa-gift','feather' => 'gift'])
                    @includeIf("admin.components.buttons.custom" , ["href" => "users/$item->id/single_rides", 'class' => 'default' , 'title'=> trans('language.single_rides'), 'icon' => 'fa fa-road','feather' => 'list'])
                </div>
            </td>

        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/users/")}}">


        <div class="col-md-1" style="direction: rtl">
            <label id="no_members">{{trans('No Members')}}</label>
            <input type="checkbox" for="no_members"
                   class="today_orders " name="no_members" value="1"
                   @if(request()->no_members  == 1 )  checked @endif >
        </div>

        <div class="col-md-1" style="direction: rtl">
            <label id="no_members">{{trans('Not approved')}}</label>
            <input type="checkbox" for="is_not_approved"
                   class="is_not_approved " name="is_not_approved" value="1"
                   @if(request()->is_not_approved  == 1 )  checked @endif >
        </div>


        <div style="display: flex">
            <div class="col-md-1">
                <select class="form-control" name="userVerify">
                    <option value="3"
                            @if(!request()->userVerify  and request()->userVerify == 3 ) selected @else @endif > {{trans('language.all')}} </option>
                    <option value="1"
                            @if(request()->userVerify  and request()->userVerify == 1 ) selected @else @endif > {{trans('language.active')}} </option>
                    <option value="2"
                            @if(request()->userVerify  and request()->userVerify == 2 ) selected @else @endif >{{trans('language.not_actived')}}</option>
                </select>
            </div>

            <div class="col-md-1">
                <select class="form-control" name="is_block">
                    <option value="3"
                            @if(!request()->is_block  and request()->is_block == 3 ) selected @else @endif > {{trans('language.all')}} </option>
                    <option value="1"
                            @if(request()->is_block  and request()->is_block == 1 ) selected @else @endif > {{trans('language.blocked')}} </option>
                    <option value="2"
                            @if(request()->is_block  and request()->is_block == 2 ) selected @else @endif >{{trans('language.not_block')}}</option>
                </select>
            </div>

            <div class="col-md-2">
                <input type="text" class="form-control" value="{{request()->name}}" name="name"
                       placeholder="{{trans('language.name')}}">
            </div>
            <div class="col-md-2">
                <input type="text" class="form-control" value="{{request()->email}}" name="email"
                       placeholder="{{trans('language.email')}}">
            </div>
            <div class="col-md-2">
                <input type="text" class="form-control" value="{{request()->mobile}}" name="mobile"
                       placeholder="{{trans('language.mobile')}}">
            </div>

            <div class="col-md-2">
                <input type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
            </div>
        </div>
    </form>
@stop
@section("extra_css")
    <style>
        .actions_conainter::-webkit-scrollbar {

        }
    </style>
@endsection


