@extends('admin.layout.table.index')
@section('page-title',trans('language.users_not_answer_drivers_request'))
@section('root' , "users_not_answer_drivers_request")

@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.users_not_answer_drivers_request')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.mobile')}}</th>
    <th>{{trans('language.country')}}</th>

@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>

            <td>{{$item->user->dash_name}}</td>
            <td>{{$item->user->dash_mobile}}</td>
            <td>{{$item->user->dash_country_name}}</td>
            <td>
        </tr>
    @endforeach
@endsection




