@extends('admin.layout.index')
@section('content')

    {{--   <div class="col-12">
           <div class="card">
               <div class="card-body">
                   <h2  style="color: #Fff">{{ trans('language.location') }}</h2>
                   <h1 style="color: #Fff"> ({{$item->pickup_name}}  )</h1>

                   <h4 style="color: #Fff" > # {{$item->pickup_address}}</h4>


               </div>
           </div>
       </div>--}}

    <div id="googleMap" style="width:100%;height:700px;"></div>

    <script>
        var iconUrl = "{{asset('assets/admin/images/marker.png')}}"
    </script>

    <script>

        function myMap() {

            var lat = parseFloat("{{$item->lat}}");
            var lng = parseFloat("{{$item->lng}}");

            var mapProp = {
                center: new google.maps.LatLng(lat, lng),
                zoom: 12,
            };

            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

            var goldenGatePosition = {lat: lat, lng: lng};
            var marker = new google.maps.Marker({
                position: goldenGatePosition,
                map: map,
                title: '{{$item->name}}',
                icon: iconUrl
            });

        }
    </script>




@endsection

