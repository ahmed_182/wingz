@extends('admin.layout.table.index')
@section('page-title',trans('language.favourite_places'))
@section('buttons')
@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.address')}}</th>
    <th>{{trans('language.location')}}</th>

@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>#</td>
            <td>{{$item->name}}</td>
            <td>{{$item->address}}</td>
            <td>
            @includeIf("admin.components.buttons.custom" , ["href" => "placeLocation/$item->id", 'class' => 'default' , 'title'=> trans('language.location'), 'icon' => 'fa fa-map',"feather"=>'map-pin'])
            <td>
        </tr>
    @endforeach
@endsection


