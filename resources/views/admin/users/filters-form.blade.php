@section('filters')
    <form action="{{url()->current()}}" method="get">
        <div class="row">
            <div class="col-md-5">
                @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => 'Search For User','name'=>'filter', 'placeholder'=>'Enter Name , Email , Phone'])
            </div>
            <div style="display: flex;align-items: center;" class="col-md-3">
                <button type="submit" class="btn btn-success waves-effect waves-light m-r-10">filter</button>
            </div>
        </div>
    </form>
@endsection
