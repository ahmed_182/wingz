@extends('admin.layout.forms.edit.index')
@section('action' , "users/$item->id")
@section('root' , "users")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.edit'))
@section('form-groups')
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.reason'),'name'=>'block_reason', 'placeholder'=>trans('language.reason')])
@endsection
@section('submit-button-title' , "save")
