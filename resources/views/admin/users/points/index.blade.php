@extends('admin.layout.table.index')
@section('page-title',trans('language.favourite_places'))
@section('buttons')
@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.points')}}</th>
    <th>{{trans('language.points_money_value')}}</th>
    <th>{{trans('language.pay_reasons')}}</th>
    <th>{{trans('language.created_at')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>#</td>
            <td>{{$item->points}}</td>
            <td>{{$item->points_money_value}}</td>
            <td>{{$item->reason}}</td>
            <td>{{$item->created}}</td>
        </tr>
    @endforeach
@endsection


