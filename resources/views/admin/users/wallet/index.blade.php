@extends('admin.layout.table.index')
@section('page-title',trans('language.favourite_places'))
@section('buttons')
@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.value')}}</th>
    <th>{{trans('language.pay_reasons')}}</th>
    <th>{{trans('language.status')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>#</td>
            <td>{{$item->value}}</td>
            <td>{{$item->reason}}</td>
            <td>{{$item->serv_status_name}}</td>
        </tr>
    @endforeach
@endsection


