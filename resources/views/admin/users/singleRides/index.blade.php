@extends('admin.layout.index')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.requests')}}</h4>

                <ul class="nav nav-pills m-t-30 m-b-30">
                    <li class=" nav-item"><a href="#navpills-1" class="nav-link active" data-toggle="tab"
                                             aria-expanded="false">Pending Rides @if(count($pending_rides) > 0 )
                                ( {{count($pending_rides)}} ) @endif </a>
                    </li>

                    <li class=" nav-item"><a href="#navpills-2" class="nav-link " data-toggle="tab"
                                             aria-expanded="false">accepted Rides @if(count($accepted_rides) > 0 )
                                ( {{count($accepted_rides)}} ) @endif </a>
                    </li>

                    <li class=" nav-item"><a href="#navpills-3" class="nav-link " data-toggle="tab"
                                             aria-expanded="false">rejected Rides @if(count($rejected_rides) > 0 )
                                ( {{count($rejected_rides)}} ) @endif </a>
                    </li>

                    <li class=" nav-item"><a href="#navpills-4" class="nav-link " data-toggle="tab"
                                             aria-expanded="false">user cancelled
                            Rides @if(count($user_cancelled_rides) > 0 )
                                ( {{count($user_cancelled_rides)}} ) @endif </a>
                    </li>
                    <li class=" nav-item"><a href="#navpills-5" class="nav-link " data-toggle="tab"
                                             aria-expanded="false">driver cancelled
                            Rides @if(count($driver_cancelled_rides) > 0 )
                                ( {{count($driver_cancelled_rides)}} ) @endif </a>
                    </li>
{{--                    <li class=" nav-item"><a href="#navpills-6" class="nav-link " data-toggle="tab"--}}
{{--                                             aria-expanded="false">admin cancelled--}}
{{--                            Rides @if(count($admin_cancelled_rides) > 0 )--}}
{{--                                ( {{count($admin_cancelled_rides)}} ) @endif </a>--}}
{{--                    </li>--}}
                    <li class=" nav-item"><a href="#navpills-7" class="nav-link " data-toggle="tab"
                                             aria-expanded="false"> compeleted
                            Rides @if(count($compeleted_rides) > 0 )
                                ( {{count($compeleted_rides)}} ) @endif </a>
                    </li>


                </ul>

                <div class="tab-content br-n pn col-md-12">
                    <div id="navpills-1" class="tab-pane active pending_rides ">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('language.child_name')}}</th>
                                    <th>{{trans('language.pickupAddress')}}</th>
                                    <th>{{trans('language.destinationAddress')}}</th>
                                    {{--                                    <th>{{trans('language.receipt')}}</th>--}}
                                    <th>{{trans('language.created_at')}}</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($pending_rides as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->child['dash_name'] }}</td>
                                        <td>{{ $item->pickupAddress['name'] }}</td>
                                        <td>{{ $item->destinationAddress['name'] }}</td>
                                        {{--                                        <td>{{ $item->receipt['cost'] }}</td>--}}
                                        <td>{{ $item->order_created }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="navpills-2" class="tab-pane accepted_rides ">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('language.child_name')}}</th>
                                    <th>{{trans('language.driver')}}</th>
                                    <th>{{trans('language.pickupAddress')}}</th>
                                    <th>{{trans('language.destinationAddress')}}</th>
                                    <th>{{trans('language.created_at')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($accepted_rides as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->child['dash_name'] }}</td>
                                        <td>{{ $item->driver['name'] }}</td>
                                        <td>{{ $item->pickupAddress['name'] }}</td>
                                        <td>{{ $item->destinationAddress['name'] }}</td>
                                        <td>{{ $item->order_created }}</td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="navpills-3" class="tab-pane rejected_rides ">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('language.child_name')}}</th>
                                    <th>{{trans('language.pickupAddress')}}</th>
                                    <th>{{trans('language.destinationAddress')}}</th>
                                    <th>{{trans('language.created_at')}}</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($rejected_rides as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->child['dash_name'] }}</td>
                                        <td>{{ $item->pickupAddress['name'] }}</td>
                                        <td>{{ $item->destinationAddress['name'] }}</td>
                                        <td>{{ $item->order_created }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="navpills-4" class="tab-pane user_cancelled_rides ">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('language.child_name')}}</th>
                                    <th>{{trans('language.pickupAddress')}}</th>
                                    <th>{{trans('language.destinationAddress')}}</th>
                                    <th>{{trans('language.created_at')}}</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($user_cancelled_rides as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->child['dash_name'] }}</td>
                                        <td>{{ $item->pickupAddress['name'] }}</td>
                                        <td>{{ $item->destinationAddress['name'] }}</td>
                                        <td>{{ $item->order_created }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="navpills-5" class="tab-pane driver_cancelled_rides ">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('language.child_name')}}</th>
                                    <th>{{trans('language.driver')}}</th>
                                    <th>{{trans('language.pickupAddress')}}</th>
                                    <th>{{trans('language.destinationAddress')}}</th>
                                    <th>{{trans('language.cancelation_reasons')}}</th>
                                    <th>{{trans('language.created_at')}}</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($driver_cancelled_rides as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->child['dash_name'] }}</td>
                                        <td>{{ $item->driver['name'] }}</td>
                                        <td>{{ $item->pickupAddress['name'] }}</td>
                                        <td>{{ $item->destinationAddress['name'] }}</td>
                                        <td>{{ $item->Cancellation_reasons['dash_name'] }}</td>
                                        <td>{{ $item->order_created }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="navpills-6" class="tab-pane admin_cancelled_rides ">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('language.child_name')}}</th>
                                    <th>{{trans('language.driver')}}</th>
                                    <th>{{trans('language.pickupAddress')}}</th>
                                    <th>{{trans('language.destinationAddress')}}</th>
                                    <th>{{trans('language.receipt')}}</th>
                                    <th>{{trans('language.created_at')}}</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($admin_cancelled_rides as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->child['dash_name'] }}</td>
                                        <td>{{ $item->driver['name'] }}</td>
                                        <td>{{ $item->pickupAddress['name'] }}</td>
                                        <td>{{ $item->destinationAddress['name'] }}</td>
                                        <td>{{ $item->receipt['cost'] }}</td>
                                        <td>{{ $item->order_created }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="navpills-7" class="tab-pane compeleted_rides ">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('language.child_name')}}</th>
                                    <th>{{trans('language.driver')}}</th>
                                    <th>{{trans('language.pickupAddress')}}</th>
                                    <th>{{trans('language.destinationAddress')}}</th>
                                    <th>{{trans('language.receipt')}}</th>
                                    <th>{{trans('language.created_at')}}</th>

                                </tr>
                                </thead>
                                <tbody>
                                @foreach($compeleted_rides as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $item->child['dash_name'] }}</td>
                                        <td>{{ $item->driver['name'] }}</td>
                                        <td>{{ $item->pickupAddress['name'] }}</td>
                                        <td>{{ $item->destinationAddress['name'] }}</td>
                                        <td>{{ $item->receipt['cost'] }}</td>
                                        <td>{{ $item->order_created }}</td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection



