@extends('admin.layout.table.index')
@section('page-title',trans('language.children'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => "children/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.mobile')}}</th>
    <th>{{trans('language.school')}}</th>
    <th>{{trans('language.plan')}}</th>
    <th>{{trans('language.birthday')}}</th>
    <th>{{trans('language.status')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->serv_image])</td>
            <td>{{$item->name}}</td>
            <td>{{$item->mobile}}</td>
            <td>{{optional($item->school_time->schoolLevel->school)->dash_name ? : trans('language.notSelected')  }}</td>

            <td>{{optional($item->order_plan_details())->dash_name ? : trans('language.notSelected')  }}</td>


            <td>{{$item->birthday}}</td>
            <td>
                {{$item->dash_status_name}}
                <br>
                <br>
                @if($item->is_deleted == 0)
                    <a class="btn btn-primary"
                       href="{{url("admin/delete_account/$item->id")}}"> {{trans('language.remove')}} {{trans('language.account')}} </a>
                @else
                    <a class="btn btn-primary"
                       href="{{url("admin/retuen_account/$item->id")}}"> {{trans('language.retuen')}} {{trans('language.account')}} </a>
                @endif
            </td>
            <td>
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->name)" ,  "action" => url("admin/users/$item->user_id/children/$item->id")])
                @includeIf("admin.components.buttons.custom" , ["href" => "afterSchool/$item->id", 'class' => 'default' , 'title'=> trans('language.after_school'), 'feather' => 'list'])
            </td>
        </tr>
    @endforeach
@endsection


