@extends('admin.layout.forms.add.index')
@section('action' , "users/$user->id/children")
@section('title' , trans('language.add'))
@section('page-title',trans('language.children'))
@section('form-groups')

    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name'),'name'=>'name', 'placeholder'=>trans('language.name')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-phone','label' => trans('language.mobile'),'name'=>'mobile', 'placeholder'=>trans('language.mobile')])
    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.birthday")}}</label>
        <input type="date" class="form-control" name="birthday"
               value="" placeholder="birthday"></div>


    <div class="form-group">
        <label>{{trans("language.schools")}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class='fa fa-list'></i></span>
            </div>
            <select required class="form-control school_id_list school_id" id="school_id"
                    name="school_id">
                <option value="0">{{trans("language.select")}} {{trans('language.schools')}} </option>
                @foreach(\App\School::get() as $school)
                    <option name="school_id" value="{{$school->id}}">{{$school->dash_name}}</option>
                @endforeach
            </select>
        </div>
    </div>


    <div class="form-group">
        <label>{{trans("language.levels")}}</label>
        <div class="input-group level_List">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class='fa fa-list'></i></span>
            </div>
            <select id="level_id" name="level_id"
                    class=" form-control level_id_list level_id">
                <option name="level_id" class="basicOption"
                        value="0">{{trans('language.select')}} {{trans('language.levels')}} </option>
            </select>
        </div>
    </div>

    <div class="form-group">
        <label>{{trans("language.school_times")}}</label>
        <div class="input-group school_time_List">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class='fa fa-list'></i></span>
            </div>
            <select id="school_time_id" name="stage_id"
                    class=" form-control school_time_id_list school_time_id">
                <option name="school_time_id" class="basicOption2"
                        value="0">{{trans('language.select')}} {{trans('language.school_times')}} </option>
            </select>
        </div>
    </div>


@endsection
@section('submit-button-title' ,trans('language.add'))
@section('extra_js')
    <script>

        $(document).ready(function () {
            $('.school_id').on('change', function () {
                var school_id = $('.school_id').val();
                $.post("{{url("/getLevels")}}",
                    {
                        school_id: school_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {

                        if (data.status == true) {
                            $(".level_List").removeClass("hide");
                            $('.level_id_list').html('');
                            if (data.status == true) {
                                $(".level_id_list").prepend('' +
                                    '<option  value="0">اختر من مستوايات  </option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {

                                    $(".level_id_list").prepend('' +
                                        '<option name="level_id"  value="' + item.id + '">' + item.name + '</option>');
                                });
                            });
                        } else {
                            Swal.fire({
                                icon: 'info',
                                text: 'لا توجد مستوايات التعليم  متفرعه من هذه المدرسه  '
                            })
                            $('.level_id_list').html('');
                            $(".level_id_list").prepend('' +
                                '<option name="level_id"  value="0">لا توجد مستوايات التعليم  متفرعه من هذه المدرسه  </option>');
                        }
                    });
            });
            $('.level_id').on('change', function () {
                var level_id = $('.level_id').val();
                var school_id = $('.school_id').val();
                $.post("{{url("/getSchoolTimes")}}",
                    {
                        level_id: level_id,
                        school_id: school_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {
                    console.log(data)
                        if (data.status == true) {
                            console.log(data)
                            $('.school_time_id_list').html('');
                            if (data.status == true) {
                                $(".school_time_id_list").prepend('' +
                                    '<option name="stage_id"  class="basicOption2"  value="0"> اختر من المراحل الدراسيه</option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {
                                    $(".school_time_id_list").prepend('' +
                                        '<option name="stage_id"  value="' + item.id + '">' + item.name + '</option>');
                                });
                            });
                        } else {
                            $('.basicOption2').addClass('hide');
                            Swal.fire({
                                icon: 'info',
                                text: 'لا توجد مراحل دراسيه متفرعه من هذا المستوي التعليمي  '
                            })
                        }
                    });
            });

        });


    </script>

@endsection
