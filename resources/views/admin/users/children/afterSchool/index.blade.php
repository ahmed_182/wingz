@extends('admin.layout.table.index')
@section('page-title',trans('language.children'))
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item"><a
                href="{{url("admin/pending_children")}}">  {{trans('language.pending_children')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.after_school')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.start_at')}}</th>
    <th>{{trans('language.end_at')}}</th>
    <th>{{trans('language.time')}}</th>

@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->start_date}}</td>
            <td>{{$item->end_date}}</td>
            <td>{{$item->time}}</td>
        </tr>
    @endforeach
@endsection


