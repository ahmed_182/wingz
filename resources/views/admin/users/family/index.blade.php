@extends('admin.layout.index')
@section('page-title',trans('language.users'))
@section('root' , "users")
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item"><a
                href="{{url("admin/users")}}">  {{trans('language.users')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.children')}}</li>
    </ol>
@endsection
@section('content')

<div class="container-fluid">
    @if(count($family_members) > 0 )
        <div class="row el-element-overlay">
            <div class="col-md-12">
                <a href="{{url("admin/users/$user->id/familyMembers")}}" class="btn btn-info">
                    <h5 style="color: white ">{{trans('language.add') . ' ' . trans('language.family_members')}}</h5>
                </a>
                <br>
                <br>
            </div>

                @foreach ($family_members as $family_member)
                <div class="col-lg-4 col-xlg-3">
                    <div class="card">
                        <img class="card-img-top" src="{{asset('/assets/admin/')}}/images/background/profile-bg.jpg" alt="Card image cap">
                        <div class="card-body little-profile text-center">
                            <div class="pro-img"><img src="{{$family_member->dash_image}}" alt="user"/></div>
                            <h3 class="m-b-0">{{$family_member->dash_name}}</h3>
                            <p>{{$family_member->mobile}}</p>
                            <button href="javascript:void(0)"
                                    class="m-t-10 waves-effect waves-dark btn btn-info btn-md btn-rounded">
                                {{$family_member->dash_relation_name}}
                                ({{$family_member->dash_family_member_status_name}})
                            </button>
                        </div>
                    </div>
                </div>
                @endforeach

            @endif
        </div>

        <div class="col-lg-12 col-md-12">
            <div class="card">
                <div class="card-body">
                    {{--            <h5 class="card-title">{{trans("language.children")}}</h5>--}}
                    <a href="{{url("admin/users/$user->id/children")}}" class="btn btn-info">
                        <h5 style="color: white ">{{trans('language.add') . ' ' . trans('language.children')}}</h5>
                    </a>
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>{{trans('language.image')}}</th>
                                <th>{{trans('language.name')}}</th>
                                <th>{{trans('language.mobile')}}</th>
                                <th>{{trans('language.school')}}</th>
                                <th>{{trans('language.plan')}}</th>
                                <th>{{trans('language.birthday')}}</th>
                                <th>{{trans('language.status')}}</th>
                                <th>{{trans('language.settings')}}</th>
                            </tr>
                            </thead>
                            <tbody>
                            @foreach($children as $item)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td> @includeIf("admin.components.image.index" , ["url" => $item->serv_image])</td>
                                    <td>{{$item->name}}</td>
                                    <td>{{$item->mobile}}</td>
                                    <td>{{optional($item->school_time->schoolLevel->school)->dash_name ? : trans('language.notSelected')  }}</td>

                                    <td>{{optional($item->order_plan_details())->dash_name ? : trans('language.notSelected')  }}</td>


                                    <td>{{$item->dash_birthday}}</td>
                                    <td>
                                        {{$item->dash_status_name}}
                                        <br>
                                        <br>
                                        @if($item->is_deleted == 0)
                                            <a class="btn btn-primary"
                                               href="{{url("admin/delete_account/$item->id")}}"> {{trans('language.remove')}} {{trans('language.account')}} </a>
                                        @else
                                            <a class="btn btn-primary"
                                               href="{{url("admin/retuen_account/$item->id")}}"> {{trans('language.retuen')}} {{trans('language.account')}} </a>
                                        @endif
                                    </td>
                                    <td>
                                        @includeIf("admin.components.buttons.delete",["message" =>  "($item->name)" ,  "action" => url("admin/users/$item->user_id/children/$item->id")])
                                        @includeIf("admin.components.buttons.custom" , ["href" => "afterSchool/$item->id", 'class' => 'default' , 'title'=> trans('language.after_school'), 'icon' => 'fa fa-list',"feather"=>'list'])
                                    </td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

@endsection
