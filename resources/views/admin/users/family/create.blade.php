@extends('admin.layout.forms.add.index')
@section('action' , "users/$user->id/familyMembers")
@section('title' , trans('language.add'))
@section('page-title',trans('language.familyMembers'))
@section('form-groups')

    @includeIf('admin.components.form.add.select', ['label' => trans("language.relations"),'name'=>'user_relation_id', 'items'=> \App\User_relation::all() , 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-image','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-pencil','label' => trans('language.name'),'name'=>'name', 'placeholder'=>trans('language.name')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-email','label' => trans('language.email'),'name'=>'email', 'placeholder'=>trans('language.email')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-mobile','label' => trans('language.mobile'),'name'=>'mobile', 'placeholder'=>trans('language.mobile')])

@endsection
@section('submit-button-title' ,trans('language.add'))

