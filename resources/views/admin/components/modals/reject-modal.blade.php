<div class="modal fade bs-example-modal-md rejectModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel" aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4>{{trans('language.reject')}}</h4>
{{--                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
            </div>
            <div class="modal-body">
                <div class="modal-body">
                    {{trans('language.delete_aleart')}}<h4 style="display: inline;color: red;" class="rejectMessage"></h4>
                </div>
            </div>
            <div class="modal-footer">
                <form class=" rejectForm" method="post">
                    <input class="rejectId" type="hidden" name="id">
                    @csrf
                    <button type="button" class="btn btn-default waves-effect" data-dismiss="modal">{{trans('language.close')}}</button>
                    <button type="submit" class="btn btn-danger waves-effect waves-light">{{trans('language.reject')}} </button>
                </form>
            </div>
        </div>
    </div>
</div>
