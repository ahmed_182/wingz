<div class="modal fade bs-example-modal-md showModal" tabindex="-1" role="dialog" aria-labelledby="mySmallModalLabel"
     aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-md">
        <div class="modal-content">
            <div class="modal-header">
                <h4>{{trans('language.show')}}</h4>
                {{--                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>--}}
            </div>
            <div class="modal-body">
                <div class="modal-body">
                    <p style="display: inline;color: black;font-size: 16px" class="showMessage"></p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-default waves-effect"
                        data-dismiss="modal">{{trans('language.close')}}</button>
            </div>
        </div>
    </div>
</div>
