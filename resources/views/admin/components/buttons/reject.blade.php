<a href="javascript:void(0)"
   class="btn btn-pure btn-outline rejectButton"
   data-original-title="Reject"
   data-message="{{$message}}"
   data-action="{{$action}}"
   data-toggle="modal"
   data-target=".rejectModal"
    title="{{trans('language.reject')}}">
    <i class="fa fa-ban" aria-hidden="true"></i>
</a>
