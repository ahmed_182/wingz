<div class="form-group">
    <label for="exampleInputuname">{{$label}}</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text"><i class="{{$icon}}"></i></span>
        </div>
      {{--  <input required name="{{$name}}" type="number" value="{{request()->filter ? request()->filter : ''}}"
               class="form-control" placeholder="{{$placeholder}}">--}}
        <input name="{{$name}}"
               value="{{$item["$name"]}}"
               placeholder="{{$placeholder}}"
               type="number" class="form-control" >
    </div>
</div>
