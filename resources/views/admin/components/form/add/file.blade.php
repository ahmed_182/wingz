<div class="form-group">
    <label for="exampleInputuname">{{$label}}</label>
    <div class="input-group">
        <input id="input-file-events" type="file" name="{{$name}}" class="dropify"
               data-max-file-size="{{$max}}M"/>
    </div>
</div>
