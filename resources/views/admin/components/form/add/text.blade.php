<div class="form-group">
    <label for="exampleInputuname">{{$label}}</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="link-icon" data-feather="edit"></i>
            </span>
        </div>
        <input style="height: 50px" required name="{{$name}}" type="text"
               value="{{request()->filter ? request()->filter : ''}}"
               class="form-control" placeholder="{{$placeholder}}"  >
    </div>
</div>




