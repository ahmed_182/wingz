<div class="form-group ">
    <label>{{$label}}</label>
    <div class="input-group">
        <div class="input-group-prepend">
            <span class="input-group-text">
              <i class="link-icon" data-feather="list"></i>
            </span>
        </div>
        <select required class="form-control" name="{{$name}}" style="height: 47px !important;">
            @foreach($items as $selectItem)
                <option value="{{$selectItem->id}}">{{$selectItem->dash_name}}</option>
            @endforeach
        </select>
    </div>
</div>
