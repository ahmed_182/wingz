@extends('admin.layout.forms.edit.index')
@section('action' , "voucher/$item->id")
@section('title' , trans('language.add'))
@section('page-title',trans('language.voucher'))
@section('form-groups')
    @includeIf('admin.components.form.add.select', ['label' => trans("language.type"),'name'=>'type_id', 'items'=> \App\VoucherType::all() , 'icon' => 'fa fa-list',])

    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.code'),'name'=>'code', 'placeholder'=>trans('language.code')])
    @includeIf('admin.components.form.edit.textarea', ['icon' => 'fa fa-user','label' => trans('language.description'),'name'=>'description', 'placeholder'=>trans('language.description')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.discount'),'name'=>'discount', 'placeholder'=>trans('language.discount')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.maximum_discount'),'name'=>'maximum_discount', 'placeholder'=>trans('language.maximum_discount')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.uses'),'name'=>'uses', 'placeholder'=>trans('language.uses')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.max_uses'),'name'=>'max_uses', 'placeholder'=>trans('language.max_uses')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.max_uses_user'),'name'=>'max_uses_user', 'placeholder'=>trans('language.max_uses_user')])

    @includeIf('admin.components.form.edit.date', ['icon' => 'fa fa-user','label' => trans('language.starts_at'),'name'=>'start_date', 'placeholder'=>trans('language.starts_at')])
    @includeIf('admin.components.form.edit.date', ['icon' => 'fa fa-user','label' => trans('language.expired_date'),'name'=>'expires_at', 'placeholder'=>trans('language.expired_date')])

@endsection
@section('submit-button-title', trans('web.edit'))
