@extends('admin.layout.table.index')
@section('page-title',trans('language.clients'))

@section('buttons')
    {{--@if($categories)--}}
    @includeIf("admin.components.buttons.addbtn" , ["href" => "user/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
{{--
    @endif
--}}
@stop

@section('thead')
    <th>#</th>
    <th>{{trans('language.Voucher_User')}}</th>
    <th>{{trans('language.email')}}</th>
    <th>{{trans('language.mobile')}}</th>
    <th>{{trans('language.number_of_use')}}</th>
   <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>

            <td>{{$item->dash_user}}</td>
            <td>{{$item->dash_email}}</td>
            <td>{{$item->dash_mobile}}</td>
            <td>{{$item->number_of_use}}</td>

            <td>
               @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_user)" ,  "action" => url("admin/voucher/$voucher_id/user/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection

