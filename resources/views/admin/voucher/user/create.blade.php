@extends('admin.layout.forms.add.index')
@section('action' , "voucher/$voucher->id/user")
@section('title' , trans('language.add'))
@section('page-title',trans('language.voucher'))
@section('form-groups')

    @foreach($users as $user)

        @includeIf('admin.components.form.add.checklist', ['id'=>$user->id,'name'=>'user_id','value'=>$user->id,'class'=>'default','customClass'=> '' ,'label'=>"$user->dash_name"." "])

    @endforeach

@endsection

@section('submit-button-title' ,trans('web.add'))

