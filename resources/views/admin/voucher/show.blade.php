@extends('admin.layout.index')
@section('content')
    <div class="col-12">
        <div class="card">
            <div class="card-body">
                <h1> {{ trans('language.voucher-details') }}</h1>
            </div>
        </div>
        <div class="col-lg-12 col-12 animated bounce delay-2s">
            <div class="card pull-up">
                <div class="card-content">
                    <div class="card-body ">
                            <div class="media d-flex">
                                <div class="media-body text-left">
                                    <h1 class="text-muted">{{trans('language.code')}} :{{$item->code}} </h1>
                                    <br>
                                    <h1 class="text-muted">{{trans('language.discount')}} :{{$item->discount}} </h1>
                                    <br>
                                    <h1 class="text-muted">{{trans('language.maximum_discount')}} :{{$item->maximum_discount}} </h1>
                                    <br>
                                    <h1 class="text-muted">{{trans('language.uses')}} :{{$item->uses}} </h1>
                                    <br>
                                    <h1 class="text-muted">{{trans('language.max_uses')}} :{{$item->max_uses}} </h1>
                                    <br>
                                    <h1 class="text-muted">{{trans('language.max_uses_user')}} :{{$item->max_uses_user}} </h1>
                                    <br>
                                    <h1 class="text-muted">{{trans('language.description')}} :{{$item->description}}</h1>
                                </div>
                                <div class="align-self-center">

                                </div>
                            </div>


                    </div>
                </div>
            </div>
        </div>
    </div>




@endsection
