@extends('admin.layout.forms.add.index')
@section('action' , "voucher")
@section('title' , trans('language.add'))
@section('page-title',trans('language.voucher'))
@section('form-groups')
    @includeIf('admin.components.form.add.select', ['label' => trans("language.type"),'name'=>'type_id', 'items'=> \App\VoucherType::all() , 'icon' => 'fa fa-list',])

    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.code'),'name'=>'code', 'placeholder'=>trans('language.code')])
    @includeIf('admin.components.form.add.textarea', ['icon' => 'fa fa-user','label' => trans('language.description'),'name'=>'description', 'placeholder'=>trans('language.description')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.discount') . " % ",'name'=>'discount', 'placeholder'=>trans('language.discount'). " % "])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.maximum_discount'),'name'=>'maximum_discount', 'placeholder'=>trans('language.maximum_discount')])
    {{-- @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.uses'),'name'=>'uses', 'placeholder'=>trans('language.uses')]) --}}
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('Maximum uses '),'name'=>'max_uses', 'placeholder'=>trans('Maximum uses per user ')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans(' Maximum uses per user '),'name'=>'max_uses_user', 'placeholder'=>trans(' Maximum uses per user ')])

    @includeIf('admin.components.form.add.date', ['icon' => 'fa fa-user','label' => trans('language.start_date'),'name'=>'starts_at', 'placeholder'=>trans('language.starts_at')])
    @includeIf('admin.components.form.add.date', ['icon' => 'fa fa-user','label' => trans('language.expired_date'),'name'=>'expires_at', 'placeholder'=>trans('language.expired_date')])

@endsection
@section('submit-button-title', trans('web.add'))
