@extends('admin.layout.table.index')
@section('page-title',trans('language.voucher'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/voucher/create"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.voucher')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.code')}}</th>
    <th>{{trans('language.description')}}</th>
    <th>{{trans('language.type')}}</th>
    <th>{{trans('language.new_user')}}</th>
    <th>{{trans('language.starts_at')}}</th>
    <th>{{trans('language.expires_at')}}</th>
    <th>{{trans('language.voucher_plans')}}</th>
    <th>{{trans('language.voucher_user')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td><a href="{{url("admin/voucher/$item->id")}}">{{ $loop->iteration }}</a></td>
            <td><a href="{{url("admin/voucher/$item->id")}}">{{$item->code}}</a></td>
            <td>{{$item->description}}</td>
            <td>{{$item->dash_type}}</td>
            <td>{{$item->dash_new_user_status_name}}</td>
            <td>{{$item->dash_start}}</td>
            <td>{{$item->dash_expire}}</td>
            <td>
                @includeIf("admin.components.buttons.custom" , ["href" => "voucher/$item->id/plans/", 'class' => 'default' , 'title'=> "plans", 'icon' => 'fa fa-list',"feather"=>'list'])

            </td>
            <td>
                @includeIf("admin.components.buttons.custom" , ["href" => "voucher/$item->id/user/", 'class' => 'default' , 'title'=> "users", 'icon' => 'fa fa-list',"feather"=>'user'])

            </td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "voucher/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" => "($item->code)" ,  "action" => url("admin/voucher/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


