@extends('admin.layout.forms.add.index')
@section('action' , "voucher/$voucher->id/plans")
@section('title' , trans('language.add'))
@section('page-title',trans('language.plans'))
@section('form-groups')

@foreach($plans as $plan)

    @includeIf('admin.components.form.add.checklist', [ 'customClass' => '','id'=>$plan->id,'name'=>'plan_id[]','value'=>$plan->id,'class'=>'default','label'=>$plan->dash_name])

@endforeach

@endsection

@section('submit-button-title' ,trans('web.add'))

