@extends('admin.layout.table.index')
@section('page-title',trans('language.plans'))
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item"><a
                href="{{url("admin/voucher")}}">  {{trans('language.voucher')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.plans')}}</li>
    </ol>
@endsection
@section('buttons')
    {{--@if($categories)--}}
    @includeIf("admin.components.buttons.addbtn" , ["href" => "plans/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
{{--
    @endif
--}}
@stop

@section('thead')
    <th>#</th>
    <th>{{trans('language.plan')}}</th>
   <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)

        <tr>
            <td>{{ $loop->iteration }}</td>

            <td>{{$item->dash_name}}</td>

            <td>
               @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/voucher/$voucher_id/plans/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection

