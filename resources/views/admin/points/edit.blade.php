@extends('admin.layout.forms.edit.index')
@section('action' , "points/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.points'))
@section('form-groups')

    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.value'),'name'=>'value', 'placeholder'=>trans('language.value')])

@endsection
@section('submit-button-title' ,trans('language.edit'))
