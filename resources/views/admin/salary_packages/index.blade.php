@extends('admin.layout.table.index')
@section('page-title',trans('language.salary_packages'))
@section('buttons')

@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.description')}}</th>
    <th>{{trans('language.value')}}</th>

    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->image])</td>
            <td>{{$item->dash_name}}</td>
            <td>{{$item->dash_description}}</td>
            <td>{{$item->value}}</td>

            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "salary_packages/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/salary_packages/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


