@extends('admin.layout.forms.add.index')
@section('action' , "salary_packages")
@section('title' , trans('language.add'))
@section('page-title',trans('language.salary_packages'))
@section('form-groups')

    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])

    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.description_ar'),'name'=>'description_ar', 'placeholder'=>trans('language.description_ar')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.description_en'),'name'=>'description_en', 'placeholder'=>trans('language.description_en')])

    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.value'),'name'=>'value', 'placeholder'=>trans('language.value')])


@endsection
@section('submit-button-title' , trans('language.add'))
