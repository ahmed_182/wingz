@extends('admin.layout.table.index')
@section('page-title',trans('language.tax_type'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/tax_type/create"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.tax_type')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>

    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
   @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_name}}</td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "tax_type/$item->id/edit"])

                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/tax_type/$item->id")])

            </td>
        </tr>
    @endforeach
@endsection


