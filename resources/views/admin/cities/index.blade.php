@extends('admin.layout.table.index')
@section('page-title',trans('language.cities'))
@section('buttons')
<br>
    @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/cou/cities"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.cities')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.country')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_name}}</td>
            <td>{{$item->dash_country_name}}</td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "cities/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/cities/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form method="get" action="{{url("/admin/cities/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control" name="name" placeholder="{{trans('language.name')}}"  >
            </div>
            <div class="col-md-3">
                <select class="form-control" name="country_id">
                    <option value="">{{trans('language.countries')}}</option>
                    @foreach(\App\Country::all() as $selectItem)
                        <option value="{{$selectItem->id}}">{{$selectItem->dash_name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-2">
                <input type="submit" class="btn btn-primary " value="{{trans('language.filter')}}">
            </div>
        </div>
    </form>
@stop


