@extends('admin.layout.forms.edit.index')
@section('action' , "weather_messages/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.weather_messages'))
@section('form-groups')
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])

    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.start_at_day")}}</label>
        <input type="time" class="form-control" name="start_at"
               value="{{$item->format_start_time}}"  placeholder="start_at"></div>

    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.end_at_day")}}</label>
        <input type="time" class="form-control" name="end_at"
               value="{{$item->format_end_time}}"  placeholder="end_at"></div>


@endsection
@section('submit-button-title' ,trans('language.edit'))
