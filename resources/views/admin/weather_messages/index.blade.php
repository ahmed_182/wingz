@extends('admin.layout.table.index')
@section('page-title',trans('language.weather_messages'))
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.weather_messages')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>

    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.start_at_day')}}</th>
    <th>{{trans('language.end_at_day')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
   @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_name}}</td>
            <td>{{$item->dash_start_at}}</td>
            <td>{{$item->dash_end_at}}</td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "weather_messages/$item->id/edit"])

                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/weather_messages/$item->id")])

            </td>
        </tr>
    @endforeach
@endsection


