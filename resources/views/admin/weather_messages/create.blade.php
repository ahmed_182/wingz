@extends('admin.layout.forms.add.index')
@section('action' , "weather_messages")
@section('title' , trans('language.add'))
@section('page-title',trans('language.weather_messages'))
@section('form-groups')
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.start_at_day")}}</label>
        <input type="time" class="form-control" name="start_at"
               value=""  placeholder="start_at"></div>

    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.end_at_day")}}</label>
        <input type="time" class="form-control" name="end_at"
               value=""  placeholder="end_at"></div>
@endsection
@section('submit-button-title' ,trans('language.add'))
