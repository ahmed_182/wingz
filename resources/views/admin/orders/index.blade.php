@extends('admin.layout.index')
@section('content')
    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.requests')}}</h4>

                <ul class="nav nav-pills m-t-30 m-b-30">
                    <li class=" nav-item"><a href="#navpills-1" class="nav-link active" data-toggle="tab"
                                             aria-expanded="false">{{trans('language.pending')}} @if(count($items_pending) > 0 )
                                ( {{count($items_pending)}} ) @endif </a></li>
                    {{--                    <li class="nav-item"><a href="#navpills-2" class="nav-link" data-toggle="tab"--}}
                    {{--                                            aria-expanded="false">Accepted @if(count($items_accepted) > 0 )--}}
                    {{--                                ( {{count($items_accepted)}} ) @endif </a></li>--}}
                    <li class="nav-item"><a href="#navpills-3" class="nav-link" data-toggle="tab"
                                            aria-expanded="false">{{trans('language.rejected')}} @if(count($items_rejected) > 0 )
                                ( {{count($items_rejected)}} ) @endif </a></li>
                </ul>
                <br>
                <form method="get" action="{{url("/admin/orders/")}}">


                    <div style="display: flex">
                        <div class="col-md-3">
                            <select class="form-control" name="status">
                                <option value="">{{trans('language.order_status')}}</option>
                                @foreach(\App\Order_status::all() as $selectItem)
                                    <option
                                        @if(request()->payment_status == $selectItem->id) selected @endif
                                    value="{{$selectItem->id}}">{{$selectItem->dash_name}}</option>
                                @endforeach
                            </select>
                        </div>

                        <div class="col-md-3">
                            <select class="form-control" name="payment_status">
                                <option value="">{{trans('language.order_status')}}</option>
                                <option
                                    value="{{\App\ModulesConst\PaymentStatus::active}}"> {{__('language.active')}}</option>
                                <option
                                    value="{{\App\ModulesConst\PaymentStatus::not_active}}">{{__('language.not_actived')}}</option>

                            </select>
                        </div>

                        <div class="col-md-2">
                            <input type="submit" class="btn btn-success " value="{{__('language.filter')}}">
                        </div>
                    </div>
                </form>
                <br>
                <div class="tab-content br-n pn col-md-12">
                    <div id="navpills-1" class="tab-pane active">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>

                                    <th>{{trans('language.user')}}</th>
                                    <th>{{trans('language.children')}}</th>
                                    <th>{{trans('language.schools')}}</th>
                                    <th>{{trans('language.place')}}</th>
                                    <th>{{trans('language.distance')}}</th>
                                    <th>{{trans('language.order_car_type')}}</th>
                                    <th>{{trans('language.kilo_price')}}</th>
                                    <th>{{trans('language.total_price')}}</th>
                                    <th>{{trans('language.client_plan')}}</th>
                                    <th>{{trans('language.status')}}</th>
                                    <th>{{trans('language.payment_status')}}</th>
                                    <th>{{trans('language.client_drivers_responded')}}</th>
                                    <th>{{trans('language.settings')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items_pending as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{$item->dash_user_name}}</td>
                                        <td>{{$item->dash_child_name}}</td>
                                        <td>{{$item->dash_school_name}}</td>
                                        <td>{{$item->dash_place_name}}</td>
                                        <td>{{$item->distance}} {{trans('language.km')}} </td>
                                        <td>{{$item->order_car_type['dash_name']}}</td>
                                        <td>{{$item->kilo_price}} {{trans('language.km_hour')}} </td>
                                        <td>{{$item->Subscription()->total_price}}</td>
                                        <td class="all">{{$item->dash_plan_name}} <br>
                                            ( {{$item->dash_plan_days}} {{trans('language.day')}} )
                                        </td>
                                        <td style="width: 137px !important;">{{$item->serv_type_name}}</td>
                                        <td><span class="label label-primary">{{$item->dash_payment_status_name}}</span>
                                        </td>
                                        <td>
                                            @includeIf("admin.components.buttons.custom" , ["href" => "order_client_drivers_responded/$item->id", 'class' => 'default' , 'title'=> trans('language.client_drivers_responded'), 'icon' => 'fa fa-map','feather'=>'map-pin'])

                                        </td>
                                        <td>
                                            @includeIf("admin.components.buttons.custom" , ["href" => "orders/$item->id", 'class' => 'default' , 'title'=> trans('language.location'), 'icon' => 'fa fa-map' ,'feather'=>'map-pin'])
                                            @includeIf("admin.components.buttons.custom" , ["href" => "attach/$item->id/school/$item->school_id", 'class' => 'default' , 'title'=> trans('language.attach'), 'icon' => 'fa fa-plus-circle','feather'=>'plus-circle'])
                                            <br>
                                            @includeIf("admin.components.buttons.custom" , ["href" => "orders/$item->id/reject", 'class' => 'default' , 'title'=> trans('language.reject'), 'icon' => 'fa fa-ban','feather'=>'list'])
                                            @includeIf("admin.components.buttons.delete",["message" => "" ,  "action" => url("admin/orders/$item->id")])
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <div id="navpills-3" class="tab-pane">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>

                                    <th>{{trans('language.user')}}</th>
                                    <th>{{trans('language.children')}}</th>
                                    <th>{{trans('language.schools')}}</th>
                                    <th>{{trans('language.place')}}</th>
                                    <th>{{trans('language.distance')}}</th>
                                    <th>{{trans('language.order_car_type')}}</th>
                                    <th>{{trans('language.kilo_price')}}</th>
                                    <th>{{trans('language.total_price')}}</th>
                                    <th>{{trans('language.client_plan')}}</th>
                                    <th>{{trans('language.status')}}</th>
                                    <th>{{trans('language.rejected_reason')}}</th>
                                    <th>{{trans('language.payment_status')}}</th>
                                    <th>{{trans('language.settings')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items_rejected as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{$item->dash_user_name}}</td>
                                        <td>{{$item->dash_child_name}}</td>
                                        <td>{{$item->dash_school_name}}</td>
                                        <td>{{$item->dash_place_name}}</td>
                                        <td>{{$item->distance}} {{trans('language.km')}} </td>
                                        <td>{{$item->order_car_type['dash_name']}}</td>
                                        <td>{{$item->kilo_price}} {{trans('language.km_hour')}} </td>
                                        <td>{{$item->total_price}}</td>
                                        <td class="all">{{$item->dash_plan_name}} <br>
                                            ( {{$item->dash_plan_days}} {{trans('language.day')}} )
                                        </td>
                                        <td><span class="label label-info">{{$item->serv_type_name}}</span></td>
                                        <td> {{$item->rejected_reason}}</td>
                                        <td><span class="label label-primary">{{$item->dash_payment_status_name}}</span>
                                        </td>
                                        <td>
                                            @includeIf("admin.components.buttons.delete",["message" => "" ,  "action" => url("admin/orders/$item->id")])
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection



