@extends('admin.layout.forms.add.index')
@section('action' , "orders/$item->id/reject")
@section('title' , trans('language.add'))
@section('page-title',trans('language.children'))
@section('form-groups')

    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.rejected_reason'),'name'=>'rejected_reason', 'placeholder'=>trans('language.rejected_reason')])

@endsection
@section('submit-button-title' ,trans('language.reject'))

