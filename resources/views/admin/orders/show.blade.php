@extends('admin.layout.index')
@section('content')

    {{--   <div class="col-12">
           <div class="card">
               <div class="card-body">
                   <h2  style="color: #Fff">{{ trans('language.location') }}</h2>
                   <h1 style="color: #Fff"> ({{$item->pickup_name}}  )</h1>

                   <h4 style="color: #Fff" > # {{$item->pickup_address}}</h4>


               </div>
           </div>
       </div>--}}

    <div id="googleMap" style="width:100%;height:800px;"></div>

    <script>
        var placeIconUrl = "{{asset('assets/admin/images/marker.png')}}"
        var schoolIconUrl = "{{asset('assets/admin/images/redMarker.png')}}"
    </script>

    <script>

        function myMap() {

            var place_lat = parseFloat("{{$item->favourite_place->lat}}");
            var place_lng = parseFloat("{{$item->favourite_place->lng}}");

            var school_lat = parseFloat("{{$item->school->lat}}");
            var school_lng = parseFloat("{{$item->school->lng}}");

            var mapProp = {
                center: new google.maps.LatLng(place_lat, place_lng),
                zoom: 11,
            };

            var geocoder = new google.maps.Geocoder;
            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

            var place_goldenGatePosition = {lat: place_lat, lng: place_lng};
            var place_marker = new google.maps.Marker({
                position: place_goldenGatePosition,
                map: map,
                title: '{{$item->favourite_place->name}}',
                icon: placeIconUrl
            });

            var school_goldenGatePosition = {lat: school_lat, lng: school_lng};
            var school_marker = new google.maps.Marker({
                position: school_goldenGatePosition,
                map: map,
                title: '{{$item->school->dash_name}}',
                icon: schoolIconUrl
            });

            // draw path ..
            var place_address = geocodeLatLng(geocoder, map, school_goldenGatePosition);


            var directionsRenderer = new google.maps.DirectionsRenderer;
            var directionsService = new google.maps.DirectionsService;
            directionsRenderer.setMap(map);
            var selectedMode = 'DRIVING';
            directionsService.route({
                origin: {lat: place_lat, lng: place_lng},  // Haight.
                destination: {lat: school_lat, lng: school_lng},  //
                provideRouteAlternatives: false,
                // Note that Javascript allows us to access the constant
                // using square brackets and a string value as its
                // "property."
                travelMode: google.maps.TravelMode[selectedMode]
            }, function (response, status) {
                if (status == 'OK') {
                    directionsRenderer.setDirections(response);
                } else {
                    window.alert('Directions request failed due to ' + status);
                }
            });

        }
    </script>


    <script>
        function geocodeLatLng(geocoder, map, latlng) {
            geocoder.geocode({'location': latlng}, function (results, status) {
                if (status === 'OK') {
                    console.log(results[0])
                    if (results[0]) {
                        address = results[0].formatted_address;
                        return address
                    } else {

                    }
                } else {

                }
            });
        }
    </script>



@endsection
