@extends('admin.layout.table.index')
@section('page-title',trans('language.order_car_types'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/order_car_types/create"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.order_car_types')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.description')}}</th>
    <th>{{trans('language.price_per_kilo')}}</th>
    <th>{{trans('language.price_per_kilo_after_school')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
   @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->image])</td>
            <td>{{$item->dash_name}}</td>
            <td>{{$item->dash_description}}</td>
            <td>{{$item->price_per_kilo}}</td>
            <td>{{$item->price_per_kilo_after_school}}</td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "order_car_types/$item->id/edit"])

                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/order_car_types/$item->id")])

            </td>
        </tr>
    @endforeach
@endsection


