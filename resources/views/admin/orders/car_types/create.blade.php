@extends('admin.layout.forms.add.index')
@section('action' , "order_car_types")
@section('title' , trans('language.add'))
@section('page-title',trans('language.order_car_types'))
@section('form-groups')
    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.description_ar'),'name'=>'description_ar', 'placeholder'=>trans('language.description_ar')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.description_en'),'name'=>'description_en', 'placeholder'=>trans('language.description_en')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.kilo_price'),'name'=>'price_per_kilo', 'placeholder'=>trans('language.kilo_price')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.price_per_kilo_after_school'),'name'=>'price_per_kilo_after_school', 'placeholder'=>trans('language.price_per_kilo_after_school')])


@endsection
@section('submit-button-title' ,trans('language.add'))
