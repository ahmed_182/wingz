
@extends('admin.layout.forms.add.index')
@section('action' , "users/$user->id/commissaries")
@section('title' , trans('language.add'))
@section('page-title',trans('language.commissaries'))
@section('form-groups')

    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.amount'),'name'=>'amount', 'placeholder'=>trans('language.amount')])
    @includeIf('admin.components.form.add.select', ['label' => trans("language.commissary_reason"),'name'=>'commissary_reason_id', 'items'=> \App\Commissary_reasons::all() , 'icon' => 'fa fa-list',])

    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.request_date")}}</label>
        <input type="date" class="form-control" name="request_date"
               value="" placeholder=" "></div>

@endsection
@section('submit-button-title' ,trans('language.add'))

