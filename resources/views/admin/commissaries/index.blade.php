@extends('admin.layout.table.index')
@section('page-title',trans('language.commissaries'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/users/$user_id/commissaries/create"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.commissary_reason')}}</th>
    <th>{{trans('language.amount')}}</th>
    <th>{{trans('language.request_date')}}</th>
    <th>{{trans('language.confirm_date')}}</th>

@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_commissary_reason}}</td>
            <td>{{$item->amount}}</td>
            <td>{{$item->dash_request_date}}</td>
            @if($item->dash_confirm_date)
                <td>{{$item->dash_confirm_date}}</td>
            @else
                <td>
                    <a class="btn btn-info"
                       href="{{url("admin/ConfirmCommissary/$item->id")}}"> {{trans('language.ConfirmCommissary')}} </a>
                </td>
            @endif

        </tr>
    @endforeach
@endsection


