@extends('admin.layout.forms.edit.index')
@section('action' , "terms/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.termes'))
@section('form-groups')

    <div class="row">
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{trans('language.terms_ar')}}</h4>
                        <textarea id="mymce" name="name_ar">{{$item->name_ar}}</textarea>
                </div>
            </div>
        </div>
        <div class="col-12">
            <div class="card">
                <div class="card-body">
                    <h4 class="card-title">{{trans('language.terms_en')}}</h4>

                        <textarea id="mymce" name="name_en">{{$item->name_en}}</textarea>

                </div>
            </div>
        </div>
    </div>

@endsection
@section('submit-button-title', trans('web.edit'))
@section('extra_js')
    <!-- wysuhtml5 Plugin JavaScript -->
    <script src="{{asset("assets/admin/")}}/js/tinymce/tinymce.min.js"></script>
    <script>
        $(document).ready(function () {

            if ($("#mymce").length > 0) {
                tinymce.init({
                    selector: "textarea#mymce",
                    theme: "modern",
                    height: 300,
                    plugins: [
                        "advlist autolink link image lists charmap print preview hr anchor pagebreak spellchecker",
                        "searchreplace wordcount visualblocks visualchars code fullscreen insertdatetime media nonbreaking",
                        "save table contextmenu directionality emoticons template paste textcolor"
                    ],
                    toolbar: "insertfile undo redo | styleselect | bold italic | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | l      ink image | print preview media fullpage | forecolor backcolor emoticons",

                });
            }
        });
    </script>


@endsection
