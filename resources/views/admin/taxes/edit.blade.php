@extends('admin.layout.forms.edit.index')
@section('action' , "taxes/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.edit'))
@section('form-groups')
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
    @includeIf('admin.components.form.edit.select', ['label' => trans("language.taxes"),'name'=>'tax_type_id', 'items'=> \App\Tax_type::all() , 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-info','label' => trans('language.value'),'name'=>'value', 'placeholder'=>trans('language.value')])

    <div class="form-group">
        <label>{{trans('language.select')}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-info"></i></span>
            </div>
            <select required class="form-control" name="is_enable">

                <option value="0"
                        @if($item['is_enable'] == 0) selected @endif >{{trans('language.not_enable')}}</option>
                <option value="1" @if($item['is_enable'] == 1) selected @endif >{{trans('language.enable')}}</option>

            </select>
        </div>
    </div>

    <div class="form-group">
        <label>{{trans('language.tax_for')}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-info"></i></span>
            </div>
            <select required class="form-control" name="type">
                <option value="1" @if($item['type'] == 1) selected @endif >{{trans('language.system')}}</option>
                <option value="2" @if($item['type'] == 2) selected @endif >{{trans('language.client')}}</option>

            </select>
        </div>
    </div>


@endsection
@section('submit-button-title' , trans('web.edit'))
