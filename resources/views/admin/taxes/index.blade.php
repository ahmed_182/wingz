@extends('admin.layout.table.index')
@section('page-title',trans('language.taxes'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/taxes/create"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.taxes')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.title')}}</th>
    <th>{{trans('language.tax_type')}}</th>
    <th>{{trans('language.value')}}</th>
    <th>{{trans('language.status')}}</th>
    <th>{{trans('language.tax_for')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_name}}</td>
            <td>{{$item->dash_type_name}}</td>
            <td>{{$item->value}}</td>
            <td>{{$item->dash_status_name}}</td>
            <td>{{$item->dash_kind_name}}</td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "taxes/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->value)" ,  "action" => url("admin/taxes/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


