@extends('admin.layout.forms.add.index')
@section('action' , "taxes")
@section('title' , trans('language.add'))
@section('page-title',trans('language.taxes'))
@section('form-groups')

    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
    @includeIf('admin.components.form.add.select', ['label' => trans("language.taxes"),'name'=>'tax_type_id', 'items'=> \App\Tax_type::all() , 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-info','label' => trans('language.value'),'name'=>'value', 'placeholder'=>trans('language.value')])

    <div class="form-group">
        <label>{{trans('language.select')}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-info"></i></span>
            </div>
            <select required class="form-control" name="is_enable">

                <option value="0">{{trans('language.not_enable')}}</option>
                <option value="1">{{trans('language.enable')}}</option>

            </select>
        </div>
    </div>
    <div class="form-group">
        <label>{{trans('language.tax_for')}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-info"></i></span>
            </div>
            <select required class="form-control" name="type">

                <option value="1">{{trans('language.system')}}</option>
                <option value="2">{{trans('language.client')}}</option>

            </select>
        </div>
    </div>
@endsection
@section('submit-button-title' , trans('web.add'))
