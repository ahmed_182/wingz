@extends('admin.layout.forms.add.index')
@section('action' , "addTripsWithDrivers")
@section('title' , trans('language.add'))
@section('page-title',trans('language.trips'))
@section('form-groups')

    <input type="hidden" name="order_id" value="{{$order->id}}">
    @forelse ($drivers as $driver)
        <div class="col-md-6 col-lg-6 col-xlg-4">
            <div class="card card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="box-title m-b-0">{{$driver->dash_name}}  </h3>
                    </div>
                    <div class="col-md-12">
                        <input type="checkbox" id="driver{{$driver->id}}" name="driver_id[]" value="{{$driver->id}}">
                        <label
                                for="driver{{$driver->id}}"> @if(count(\App\Trip::where('driver_id',$driver->id)->get())) {{count(\App\Trip::where('driver_id',$driver->id)->get())}}  {{trans('language.trips')}}  @else {{trans('language.no_trips')}} @endif </label><br>
                    </div>

                </div>
            </div>
        </div>
    @empty
        <div class="col-md-12">
            <div class="card card-body">
                <div class="row">
                    <div class="col-md-12">
                        <h3 class="box-title m-b-0">{{trans('language.no_drivers_exist')}}  </h3>
                    </div>
                </div>
            </div>
        </div>


    @endforelse



@endsection

@section('submit-button-title', trans('web.add'))
