@extends('admin.layout.table.index')
@section('page-title',trans('language.trips'))
@section('buttons')

@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.driver')}}</th>
    <th>{{trans('language.schools')}}</th>
    <th>{{trans('language.children')}}</th>
    {{--    <th>{{trans('language.status')}}</th>--}}
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_driver_name}}</td>
            <td>{{$item->dash_school_name}}</td>
            <td>{{$item->dash_children_count}}</td>
            {{--            <td>{{$item->dash_status_name}}</td>--}}
            <td>
                @includeIf("admin.components.buttons.custom" , ["href" => "order/$order->id/attach/$item->id/store", 'class' => 'default' , 'title'=> trans('language.apply'), 'icon' => 'fa fa-check-square lg',"feather" =>'list'])
                {{--                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/trips/$item->id")])--}}
            </td>
        </tr>
    @endforeach
@endsection

