@extends('admin.layout.table.index')
@section('page-title',trans('language.taxes'))
@section('buttons')

@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.receipt_no')}}</th>
    <th>{{trans('language.user_name')}}</th>
    <th>{{trans('language.representative_name')}}</th>
    <th>{{trans('language.receipt_cost')}}</th>
    <th>{{trans('language.receipt_status')}}</th>
    <th>{{trans('language.receipt_due_date')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->id}}</td>
            <td>{{$item->user->name}}</td>
            <td>
                @if($item->representative)
                    {{$item->representative->name}}
                @else
                    <a class="btn btn-info"   href="{{url("/admin/applyRepresentative/$item->id")}}"> اسناد
                        مندوب </a>
                @endif
            </td>
            <td>{{$item->price_after_tax}}</td>
            <td>{{$item->status_name() }}</td>
            <td>{{\Carbon\Carbon::createFromFormat('Y-m-d H:i:s', $item->due_date)->format('Y-m-d')}}</td>

        </tr>
    @endforeach
@endsection


