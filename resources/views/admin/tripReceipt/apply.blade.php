@extends('admin.layout.forms.add.index')
@section('action' , "saveApplyRepresentative")
@section('title' , trans('language.receipts'))
@section('submit-button-title' , trans('language.send'))
@section('form-groups')

    <input type="hidden" name="receipt_id" value="{{$receipt->id}}">
    <div class="col-md-12 col-lg-12 col-xlg-12">
        <div class="card card-body">
            <div class="row">
                <div class="col-md-3">
                    @foreach($items as $item)
                        <div class="col-md-12">
                            <input type="radio" id="rec{{$item->id}}" name="representative_id" value="{{$item->id}}">
                            <label for="rec{{$item->id}}">  {{$item->name}}</label><br>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>

@endsection




