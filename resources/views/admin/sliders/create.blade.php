@extends('admin.layout.forms.add.index')
@section('action' , "sliders")
@section('title' , trans('language.add'))
@section('page-title',trans('language.sliders'))
@section('form-groups')

    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.link'),'name'=>'link', 'placeholder'=>trans('language.link')])
    <div class="form-group">
        <label>{{trans('language.types')}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-list"></i></span>
            </div>
            <select required class="form-control" name="type">
                    <option value="1">{{trans('language.users')}}</option>
                    <option value="2">{{trans('language.drivers')}}</option>
            </select>
        </div>
    </div>


@endsection
@section('submit-button-title' , trans('language.add'))
