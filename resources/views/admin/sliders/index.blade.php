@extends('admin.layout.index')

@section('content')

    <div class="col-md-12">
        @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/sliders/create"),'class' => 'btn btn-primary' , 'title'=> trans('language.add_slider_image'), ])
        <br>
        <br>


        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.sliders')}} </h4>
                <ul class="nav nav-pills m-t-30 m-b-30">
                    <li class=" nav-item"><a href="#navpills-1" class="nav-link active" data-toggle="tab"
                                             aria-expanded="false">{{trans('language.user_slider')}}</a></li>
                    <li class="nav-item"><a href="#navpills-2" class="nav-link" data-toggle="tab"
                                            aria-expanded="false">{{trans('language.driver_slider')}}</a></li>
                </ul>
                <div class="tab-content br-n pn col-md-12">
                    <div id="navpills-1" class="tab-pane active">
                        {{--                        <h5 class="card-title">{{trans("language.pending")}}  </h5>--}}
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('language.image')}}</th>
                                    <th>{{trans('language.link')}}</th>
                                    <th>{{trans('language.count')}}</th>
                                    <th>{{trans('language.settings')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items_user as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td> @includeIf("admin.components.image.index" , ["url" => $item->image])</td>
                                        <td><a class="btn btn-info" target="_blank" href="{{$item->link}}"> {{trans('language.link')}} </a></td>
                                        <td> {{$item->dash_click_count}} </td>

                                        <td>
                                            @includeIf("admin.components.buttons.edit" , ["href" => "sliders/$item->id/edit"])
                                             @includeIf("admin.components.buttons.delete",["message" => trans('language.slider_image')  ,  "action" => url("admin/user_slider/$item->id")])
                                            @includeIf("admin.components.buttons.custom" , ["href" => "sliders/$item->id/click/", 'class' => 'default' , 'title'=> trans('language.users'), 'feather' => 'list'])
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div id="navpills-2" class="tab-pane">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('language.image')}}</th>
                                    <th>{{trans('language.link')}}</th>
                                    <th>{{trans('language.count')}}</th>
                                    <th>{{trans('language.settings')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items_driver as $item)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td> @includeIf("admin.components.image.index" , ["url" => $item->image])</td>
                                        <td><a class="btn btn-info"  target="_blank" href="{{$item->link}}"> LINK </a></td>
                                        <td> {{$item->dash_click_count}} </td>

                                        <td>
                                             @includeIf("admin.components.buttons.delete",["message" => trans('language.slider_image')  ,  "action" => url("admin/user_slider/$item->id")])
                                            @includeIf("admin.components.buttons.custom" , ["href" => "sliders/$item->id/click/", 'class' => 'default' , 'title'=> trans('language.users'), 'feather' => 'list'])
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div>

            </div>
        </div>
    </div>

@endsection
