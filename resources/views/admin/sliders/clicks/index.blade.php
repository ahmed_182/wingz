@extends('admin.layout.table.index')
@section('page-title',"data")
@section('buttons')

@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.created_at')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_user_name}}</td>
            <td>{{$item->dash_created}}</td>
        </tr>
    @endforeach
@endsection


