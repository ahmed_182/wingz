@extends('admin.layout.forms.add.index')
@section('action' , "districts")
@section('title' , trans('language.add'))
@section('page-title',trans('language.districts'))
@section('form-groups')

    <div class="form-group">
        <label>{{trans("language.countries")}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class='fa fa-list'></i></span>
            </div>
            <select required class="form-control country_id_list country_id" id="country_id"
                    name="main_category_id">
                <option value="0"> {{trans('language.countries')}}</option>
                @foreach(\App\Country::get() as $country)
                    <option name="country_id" value="{{$country->id}}">{{$country->dash_name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group  cities_List">
        <label>{{trans("language.cities")}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class='fa fa-list'></i></span>
            </div>
            <select id="city_id" name="city_id"
                    class=" form-control city_id_list city_id">
                <option name="city_id" class="basicOption" value="0">{{trans('language.cities')}}</option>
            </select>

        </div>
    </div>
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])

    <input type="hidden" class="location" id="location" name="location" value="">
    <div class="col-xs-12 text-center margin-bottom-40">
        <div class="map" id="map" style="width:100%;height:600px;"></div>
    </div>
    <hr>

@endsection
@section('submit-button-title' , trans('web.add'))
@section('extra_js')

    <script>
        $(document).ready(function () {
            $('.country_id').on('change', function () {
                var country_id = $('.country_id').val();
                $.post("{{url("/getCities")}}",
                    {
                        country_id: country_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {
                        console.log(data)
                        if (data.status == true) {
                            $('.city_id_list').html('');
                            if (data.status == true) {
                                $(".city_id_list").prepend('' +
                                    '<option  value="0"> {{trans('language.cities')}} </option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {
                                    $(".city_id_list").prepend('' +
                                        '<option name="city_id"  value="' + item.id + '">' + item.name + '</option>');
                                });
                            });
                        } else {
                            $('.city_id_list').html('');
                            $(".city_id_list").prepend('' +
                                '<option name="city_id"  value="0">لا توجد مدن  </option>');
                        }
                    });
            });
        });
    </script>

    <script>
        var iconUrl = "{{asset('assets/admin/images/marker.png')}}"
        lat = 30.006735;
        lng = 31.428937;
    </script>
    <script type="text/javascript" src="{{asset('assets/admin/js/map/setMapCreate.js')}}"></script>


@endsection
