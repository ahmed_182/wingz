@extends('admin.layout.table.index')
@section('page-title',trans('language.rates'))
@section('buttons')

@stop
@section('thead')
    <th>#</th>

    <th>{{trans('language.rate')}}</th>
    <th>{{trans('language.comment')}}</th>

@endsection
@section('tbody')
   @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->rate}}</td>
            <td>{{$item->feedBack}}</td>
        </tr>
    @endforeach
@endsection


