<script src="{{asset("assets/admin/")}}/js/jquery.min.js"></script>

<script
    src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBX0XkxJ6iX1N9FSjDH6U92O5BVKIZr4dA&libraries&callback=myMap"></script>

<!-- core:js -->
<script src="{{asset("assets/admin/")}}/vendors/core/core.js"></script>
<!-- endinject -->
<!-- plugin js for this page -->
<script src="{{asset("assets/admin/")}}/vendors/chartjs/Chart.min.js"></script>
<script src="{{asset("assets/admin/")}}/vendors/jquery.flot/jquery.flot.js"></script>
<script src="{{asset("assets/admin/")}}/vendors/jquery.flot/jquery.flot.resize.js"></script>
<script src="{{asset("assets/admin/")}}/vendors/bootstrap-datepicker/bootstrap-datepicker.min.js"></script>
<script src="{{asset("assets/admin/")}}/vendors/apexcharts/apexcharts.min.js"></script>
<script src="{{asset("assets/admin/")}}/vendors/progressbar.js/progressbar.min.js"></script>
<!-- end plugin js for this page -->
<!-- inject:js -->
<script src="{{asset("assets/admin/")}}/vendors/feather-icons/feather.min.js"></script>
<!-- plugin js for this page -->

<!-- end plugin js for this page -->
<script src="{{asset("assets/admin/")}}/js/template.js"></script>
<!-- endinject -->
<!-- custom js for this page -->
<script src="{{asset("assets/admin/")}}/js/dashboard.js"></script>
<script src="{{asset("assets/admin/")}}/js/datepicker.js"></script>
<script src="{{asset("assets/admin/")}}/js/data-table.js"></script>
<!-- end custom js for this page -->

<!-- plugin js for this page -->
<script src="{{asset("assets/admin/")}}/vendors/datatables.net/jquery.dataTables.js"></script>
<script src="{{asset("assets/admin/")}}/vendors/datatables.net-bs4/dataTables.bootstrap4.js"></script>
<script src="{{asset('assets/admin/')}}/js/setMap.js"></script>

<script type="text/javascript" src="{{asset('assets/admin/js/map/setMapCreate.js')}}"></script>
<script src="{{asset("assets/admin/")}}/js/dropify.min.js"></script>

<script src="{{asset('assets/admin/')}}/js/open-delete-modal.js"></script>

<script>
    $(document).ready(function () {
        $('.dropify').dropify();
        var drEvent = $('#input-file-events').dropify();
        drEvent.on('dropify.beforeClear', function (event, element) {
            return confirm("هل تريد الغاء تحميل الصورة ؟");
        });
        drEvent.on('dropify.afterClear', function (event, element) {
            alert('تم الغاء عمليه تحميل الصوره .');
        });
        drEvent.on('dropify.errors', function (event, element) {
            console.log('هناك خطاء اثناء تحميل الصوره');
        });
        var drDestroy = $('#input-file-to-destroy').dropify();
        drDestroy = drDestroy.data('dropify')
        $('#toggleDropify').on('click', function (e) {
            e.preventDefault();
            if (drDestroy.isDropified()) {
                drDestroy.destroy();
            } else {
                drDestroy.init();
            }
        })
    });
</script>
<script type="text/javascript">
    $('#cp2').colorpicker();
</script>
<script>
    $(function () {
        setTimeout(function () {
            $('.fade-message').slideUp();
        }, 2000);
    });
</script>


{{--<link href="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.css" rel="stylesheet">--}}
{{--<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote-bs4.min.js"></script>--}}
<link href="{{asset("assets/admin/")}}/js/summernote-bs4.min.css" rel="stylesheet">
<link href="{{asset("assets/admin/")}}/js/summernote-bs4.css" rel="stylesheet">
<script src="{{asset("assets/admin/")}}/js/summernote-bs4.min.js"></script>
<script src="{{asset("assets/admin/")}}/js/summernote-bs4.js"></script>
<script src="{{asset("assets/admin/")}}/js/summernote.ttf"></script>
<script src="{{asset("assets/admin/")}}/js/summernote.woff"></script>
<script src="{{asset("assets/admin/")}}/js/summernote.woff2"></script>

<script>
    $(document).ready(function () {
        $('#summernote_ar').summernote();
    });
    $(document).ready(function () {
        $('#summernote_en').summernote();
    });
    $(document).ready(function () {
        $('#summernote').summernote();
    });
</script>


<script>
    $(".nav-link")
        .hover(function () {
            $(this).css("color", "#2176B7");
            $(this).find(".i_link").css("color", "#2176B7");
        })
        .mouseleave(function () {
            $(this).css("color", "inherit");
            $(this).find(".i_link").css("color", "inherit");
        });
</script>

<!-- end plugin js for this page -->

@yield('extra_js')

