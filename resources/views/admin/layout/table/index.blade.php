@extends('admin.layout.index')
@section('content')

        <nav class="page-breadcrumb">
            @yield('nav')

        </nav>
        <div class="row">
        <div class="col-md-12 grid-margin stretch-card">
            <div class="card">
                <div class="card-body">

                @yield('taps')
             @yield('filters')
                @yield('buttons')
                <div class="table-responsive">
                    <table  {{--id="dataTableExample"--}} class="table">
                        <thead>
                        @yield('thead')
                        </thead>
                        <tbody>
                        @yield('tbody')
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="10">
                                <div style="display: flex; justify-content: center;" class="text-center">
                                    {{$items->appends(request()->input())->links()}}
                                </div>
                            </td>
                        </tr>
                        </tfoot>
                    </table>
                    @yield('submit')
                </div>

                </div>
            </div>
        </div>
        </div>
@endsection
