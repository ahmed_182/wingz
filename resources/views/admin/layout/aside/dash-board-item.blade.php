<div class="clearfix"></div>
<li class="nav-devider"></li>
<li style="position: relative">
    <a style="padding-bottom: 10px; padding-top: 10px;color: #4fc3f7!important;border: none "
       class=" waves-effect waves-dark"
       data-toggle="collapse" href="{{$href}}" role="button" aria-expanded="false">
        <i style="color: #4fc3f7!important; font-size: 20px;" data-toggle="tooltip"
           data-placement="right" data-original-title="DashBoard" class="{{$class}}"></i>
        <span class="hide-menu">{{$name}}</span>

    </a>
</li>


