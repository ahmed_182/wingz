<a style="color: black" class="nav-link" data-toggle="collapse" href="{{$href}}" role="button" aria-expanded="false"
   aria-controls="emails">
    <i class="link-icon i_link " style="color: black" data-feather="{{$feather}}"></i>
    <span class="link-title">{{$title}}</span>
    <i class="link-arrow" data-feather="chevron-down"></i>
</a>
