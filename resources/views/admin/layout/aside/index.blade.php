<nav class="sidebar">
    <div class="sidebar-header">
        <a href="{{url("admin/")}}" class="sidebar-brand">
            {{trans('language.yakoot')}}
        </a>
        <div class="sidebar-toggler not-active">
            <span></span>
            <span></span>
            <span></span>
        </div>
    </div>
    <div class="sidebar-body">
        <ul class="nav">

            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#trips" , "icon"=>"user", "title" => trans('language.trips') , "description" => trans('web.list'), "feather"=>"activity"])

                <br>
                <div class="collapse" id="trips">
                    <div class="sub-items">
                        <div class="nav-item">
                            @includeIf("admin.layout.aside.main-item" ,["href" => "#pending_children" , "icon"=>"user", "title" => trans('language.pending_children') , "description" => trans('web.list'), "feather"=>"align-justify"])
                            <div class="collapse" id="pending_children">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "pending_children" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#trips_2" , "icon"=>"user", "title" => trans('language.trips') , "description" => trans('web.list'), "feather"=>"award"])

                            <div class="collapse" id="trips_2">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "trips" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "trips/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#trips_ride_status" , "icon"=>"user", "title" => trans('language.trips_ride_status') , "description" => trans('web.list'), "feather"=>"align-left"])

                            <div class="collapse" id="trips_ride_status">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "trips_ride_status" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "trips_ride_status/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#meeting_requests" , "icon"=>"user", "title" => trans('language.meeting_requests') , "description" => trans('web.list'), "feather"=>"list"])

                            <div class="collapse" id="meeting_requests">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "meeting_requests" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "meeting_requests/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </li>
            <br>

            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#members_lis" , "icon"=>"user", "title" => trans('language.members') , "description" => trans('web.list'), "feather"=>"users"])

                <br>
                <div class="collapse" id="members_lis">
                    <div class="sub-items">

                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#users" , "icon"=>"user", "title" => trans('language.users') , "description" => trans('web.list'), "feather"=>"user-x"])

                            <div class="collapse" id="users">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "users" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "users/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#drivers" , "icon"=>"user", "title" => trans('language.drivers') , "description" => trans('web.list'), "feather"=>"compass"])

                            <div class="collapse" id="drivers">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "active_drivers" , "title" => trans('language.active_drivers') , "tooltip" => trans('language.active_drivers') , "class" => "mdi mdi-view-list"])
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "waiting_drivers" , "title" => trans('language.waiting_drivers') , "tooltip" => trans('language.waiting_drivers') , "class" => "mdi mdi-view-list"])
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "block_drivers" , "title" => trans('language.block_drivers') , "tooltip" => trans('language.block_drivers') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "drivers/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#representative" , "icon"=>"user", "title" => trans('language.representatives') , "description" => trans('web.list'), "feather"=>"briefcase"])

                            <div class="collapse" id="representative">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "representative" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "representative/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>

                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#users_not_answer_drivers_request" , "icon"=>"user", "title" => trans('language.users_not_answer_drivers_request') , "description" => trans('web.list'), "feather"=>"briefcase"])

                            <div class="collapse" id="users_not_answer_drivers_request">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "users_not_answer_drivers_request" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                    {{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "representative/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>



                        <br>

                        <br>
                        <br>
                    </div>

                </div>
            </li>



{{--            <li class="nav-item">--}}

{{--                @includeIf("admin.layout.aside.main-item" ,["href" => "#schools_lis" , "icon"=>"user", "title" => trans('language.schools') , "description" => trans('web.list'), "feather"=>"list"])--}}

{{--                <br>--}}
{{--                <div class="collapse" id="schools_lis">--}}
{{--                    <div class="sub-items">--}}
{{--                        <div class="nav-item">--}}

{{--                            @includeIf("admin.layout.aside.main-item" ,["href" => "#education_types" , "icon"=>"user", "title" => trans('language.education_types') , "description" => trans('web.list'), "feather"=>"book-open"])--}}

{{--                            <div class="collapse" id="education_types">--}}
{{--                                <ul class="nav sub-menu">--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "education_types" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "education_types/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}

{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <br>--}}
{{--                        <div class="nav-item">--}}

{{--                            @includeIf("admin.layout.aside.main-item" ,["href" => "#schools" , "icon"=>"schools", "title" => trans('language.schools') , "description" => trans('web.list'), "feather"=>"book"])--}}

{{--                            <div class="collapse" id="schools">--}}
{{--                                <ul class="nav sub-menu">--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "schools" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "schools/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <br>--}}

{{--                        <div class="nav-item">--}}

{{--                            @includeIf("admin.layout.aside.main-item" ,["href" => "#levels" , "icon"=>"user", "title" => trans('language.levels') , "description" => trans('web.list'), "feather"=>"bookmark"])--}}

{{--                            <div class="collapse" id="levels">--}}
{{--                                <ul class="nav sub-menu">--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "levels" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "levels/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}

{{--                        <br>--}}
{{--                        <div class="nav-item">--}}

{{--                            @includeIf("admin.layout.aside.main-item" ,["href" => "#stages" , "icon"=>"user", "title" => trans('language.stages') , "description" => trans('web.list'), "feather"=>"layers"])--}}

{{--                            <div class="collapse" id="stages">--}}
{{--                                <ul class="nav sub-menu">--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "stages" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "stages/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <br>--}}
{{--                        <br>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </li>--}}
            <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#orders_lis" , "icon"=>"user", "title" => trans('language.orders') , "description" => trans('web.list'), "feather"=>"shopping-bag"])

                <br>
                <div class="collapse" id="orders_lis">
                    <div class="sub-items">
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#orders" , "icon"=>"user", "title" => trans('language.requests') , "description" => trans('web.list'), "feather"=>"list"])

                            <div class="collapse" id="orders">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "orders" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])

                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#order_status" , "icon"=>"schools", "title" => trans('language.order_status') , "description" => trans('web.list'), "feather"=>"box"])

                            <div class="collapse" id="order_status">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "order_status" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "order_status/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>

                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#order_car_types" , "icon"=>"user", "title" => trans('language.order_car_types') , "description" => trans('web.list'), "feather"=>"cast"])

                            <div class="collapse" id="order_car_types">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "order_car_types" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "order_car_types/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>

                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#tripReceipt" , "icon"=>"user", "title" => trans('language.tripReceipt') , "description" => trans('web.list'), "feather"=>"pie-chart"])

                            <div class="collapse" id="tripReceipt">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "tripReceipt" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>

                </div>
            </li>
            <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#plans_lis" , "icon"=>"user", "title" => trans('language.plans') , "description" => trans('web.list'), "feather"=>"bookmark"])

                <br>
                <div class="collapse" id="plans_lis">
                    <div class="sub-items">

                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#plans" , "icon"=>"schools", "title" => trans('language.plans') , "description" => trans('web.list'), "feather"=>"home"])

                            <div class="collapse" id="plans">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "plans" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "plans/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>

                </div>
            </li>
            <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#voucher_lis" , "icon"=>"user", "title" => trans('language.voucher') , "description" => trans('web.list'), "feather"=>"gift"])

                <br>
                <div class="collapse" id="voucher_lis">
                    <div class="sub-items">

                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#voucher-type" , "icon"=>"schools", "title" => trans('language.voucher-type') , "description" => trans('web.list'), "feather"=>"package"])

                            <div class="collapse" id="voucher-type">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "voucher-type" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "voucher-type/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#voucher" , "icon"=>"schools", "title" => trans('language.voucher') , "description" => trans('web.list'), "feather"=>"octagon"])

                            <div class="collapse" id="voucher">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "voucher" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "voucher/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>

                </div>
            </li>

            <br>
{{--            <li class="nav-item">--}}

{{--                @includeIf("admin.layout.aside.main-item" ,["href" => "#car_types_lis" , "icon"=>"user", "title" => trans('language.car_types') , "description" => trans('web.list'), "feather"=>"key"])--}}

{{--                <br>--}}
{{--                <div class="collapse" id="car_types_lis">--}}
{{--                    <div class="sub-items">--}}

{{--                        <div class="nav-item">  --}}

{{--                            @includeIf("admin.layout.aside.main-item" ,["href" => "#car_types" , "icon"=>"schools", "title" => trans('language.car_types') , "description" => trans('web.list'), "feather"=>"inbox"])--}}

{{--                            <div class="collapse" id="car_types">--}}
{{--                                <ul class="nav sub-menu">--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "car_types" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "car_types/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <br>--}}
{{--                        <div class="nav-item">--}}

{{--                            @includeIf("admin.layout.aside.main-item" ,["href" => "#car_names" , "icon"=>"schools", "title" => trans('language.car_names') , "description" => trans('web.list'), "feather"=>"hexagon"])--}}

{{--                            <div class="collapse" id="car_names">--}}
{{--                                <ul class="nav sub-menu">--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "car_names" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "car_names/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <br>--}}
{{--                        <div class="nav-item">--}}

{{--                            @includeIf("admin.layout.aside.main-item" ,["href" => "#manufacturing_years" , "icon"=>"schools", "title" => trans('language.manufacturing_years') , "description" => trans('web.list'), "feather"=>"trello"])--}}

{{--                            <div class="collapse" id="manufacturing_years">--}}
{{--                                <ul class="nav sub-menu">--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "manufacturing_years" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "manufacturing_years/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <br>--}}
{{--                        <br>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </li>--}}

{{--            <br>--}}
{{--            <li class="nav-item">--}}

{{--                @includeIf("admin.layout.aside.main-item" ,["href" => "#taxes_lis" , "icon"=>"user", "title" => trans('language.taxes') , "description" => trans('web.list'), "feather"=>"grid"])--}}

{{--                <br>--}}
{{--                <div class="collapse" id="taxes_lis">--}}
{{--                    <div class="sub-items">--}}

{{--                        <div class="nav-item">  --}}

{{--                            @includeIf("admin.layout.aside.main-item" ,["href" => "#tax_type" , "icon"=>"schools", "title" => trans('language.tax_type') , "description" => trans('web.list'), "feather"=>"tablet"])--}}

{{--                            <div class="collapse" id="tax_type">--}}
{{--                                <ul class="nav sub-menu">--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "tax_type" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "tax_type/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <br>--}}
{{--                        <div class="nav-item">--}}

{{--                            @includeIf("admin.layout.aside.main-item" ,["href" => "#taxes" , "icon"=>"schools", "title" => trans('language.taxes') , "description" => trans('web.list'), "feather"=>"shield"])--}}

{{--                            <div class="collapse" id="taxes">--}}
{{--                                <ul class="nav sub-menu">--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "taxes" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])--}}
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "taxes/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
{{--                                </ul>--}}
{{--                            </div>--}}
{{--                        </div>--}}
{{--                        <br>--}}
{{--                        <br>--}}
{{--                    </div>--}}

{{--                </div>--}}
{{--            </li>--}}

            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#points_lis" , "icon"=>"user", "title" => trans('language.points') , "description" => trans('web.list'), "feather"=>"gift"])

                <br>
                <div class="collapse" id="points_lis">
                    <div class="sub-items">

                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#points" , "icon"=>"schools", "title" => trans('language.points') , "description" => trans('web.list'), "feather"=>"file-plus"])

                            <div class="collapse" id="points">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "points" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "points/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#redeems" , "icon"=>"schools", "title" => trans('language.redeems') , "description" => trans('web.list'), "feather"=>"credit-card"])

                            <div class="collapse" id="redeems">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "redeems" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "redeems/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>

                </div>
            </li>
            <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#places_lis" , "icon"=>"user", "title" => trans('language.places') , "description" => trans('web.list'), "feather"=>"map-pin"])

                <br>
                <div class="collapse" id="places_lis">
                    <div class="sub-items">

                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#countries" , "icon"=>"schools", "title" => trans('language.countries') , "description" => trans('web.list'), "feather"=>"database"])

                            <div class="collapse" id="countries">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "countries" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "countries/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#cities" , "icon"=>"schools", "title" => trans('language.cities') , "description" => trans('web.list'), "feather"=>"cpu"])

                            <div class="collapse" id="cities">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "cities" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "cities/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#districts" , "icon"=>"schools", "title" => trans('language.districts') , "description" => trans('web.list'), "feather"=>"crosshair"])

                            <div class="collapse" id="districts">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "districts" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "districts/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <br>
                    </div>

                </div>
            </li>
            <br>
            <li class="nav-item">

                @includeIf("admin.layout.aside.main-item" ,["href" => "#settings_lis" , "icon"=>"user", "title" => trans('language.settings') , "description" => trans('web.list'), "feather"=>"settings"])

                <br>
                <div class="collapse" id="settings_lis">
                    <div class="sub-items">


                        <div class="nav-item">
                            @includeIf("admin.layout.aside.main-item" ,["href" => "#relations" , "icon"=>"schools", "title" => trans('language.relations') , "description" => trans('web.list'), "feather"=>"monitor"])
                            <div class="collapse" id="relations">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "relations" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div><br>


                        <div class="nav-item">
                            @includeIf("admin.layout.aside.main-item" ,["href" => "#education_types" , "icon"=>"schools", "title" => trans('language.education_types') , "description" => trans('web.list'), "feather"=>"monitor"])
                            <div class="collapse" id="education_types">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "education_types" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                 </ul>
                            </div>
                        </div><br>
                        <div class="nav-item">
                            @includeIf("admin.layout.aside.main-item" ,["href" => "#schools" , "icon"=>"schools", "title" => trans('language.schools') , "description" => trans('web.list'), "feather"=>"monitor"])
                            <div class="collapse" id="schools">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "schools" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div><br>
                        <div class="nav-item">
                            @includeIf("admin.layout.aside.main-item" ,["href" => "#levels" , "icon"=>"schools", "title" => trans('language.levels') , "description" => trans('web.list'), "feather"=>"monitor"])
                            <div class="collapse" id="levels">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "levels" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div><br>
                        <div class="nav-item">
                            @includeIf("admin.layout.aside.main-item" ,["href" => "#stages" , "icon"=>"schools", "title" => trans('language.stages') , "description" => trans('web.list'), "feather"=>"monitor"])
                            <div class="collapse" id="stages">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "stages" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div><br>


                        <div class="nav-item">
                            @includeIf("admin.layout.aside.main-item" ,["href" => "#stages" , "icon"=>"car_types", "title" => trans('language.car_types') , "description" => trans('web.list'), "feather"=>"monitor"])
                            <div class="collapse" id="car_types">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "car_types" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div><br>
                        <div class="nav-item">
                            @includeIf("admin.layout.aside.main-item" ,["href" => "#car_names" , "icon"=>"schools", "title" => trans('language.car_names') , "description" => trans('web.list'), "feather"=>"monitor"])
                            <div class="collapse" id="car_names">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "car_names" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div><br>
                        <div class="nav-item">
                            @includeIf("admin.layout.aside.main-item" ,["href" => "#manufacturing_years" , "icon"=>"manufacturing_years", "title" => trans('language.manufacturing_years') , "description" => trans('web.list'), "feather"=>"monitor"])
                            <div class="collapse" id="manufacturing_years">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "manufacturing_years" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div><br>


                        <div class="nav-item">
                            @includeIf("admin.layout.aside.main-item" ,["href" => "#tax_type" , "icon"=>"car_types", "title" => trans('language.tax_type') , "description" => trans('web.list'), "feather"=>"monitor"])
                            <div class="collapse" id="tax_type">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "tax_type" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div><br>
                        <div class="nav-item">
                            @includeIf("admin.layout.aside.main-item" ,["href" => "#taxes" , "icon"=>"car_types", "title" => trans('language.taxes') , "description" => trans('web.list'), "feather"=>"monitor"])
                            <div class="collapse" id="taxes">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "taxes" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div><br>


                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#openScreens" , "icon"=>"schools", "title" => trans('language.openScreens') , "description" => trans('web.list'), "feather"=>"monitor"])

                            <div class="collapse" id="openScreens">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "openScreens" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "openScreens/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#contacts" , "icon"=>"schools", "title" => trans('language.contacts') , "description" => trans('web.list'), "feather"=>"phone-outgoing"])

                            <div class="collapse" id="contacts">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "contacts" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#brands" , "icon"=>"schools", "title" => trans('language.brands') , "description" => trans('web.list'), "feather"=>"shopping-bag"])

                            <div class="collapse" id="brands">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "brands" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "brands/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#terms" , "icon"=>"schools", "title" => trans('language.termes') , "description" => trans('web.list'), "feather"=>"octagon"])

                            <div class="collapse" id="terms">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "terms" , "title" => trans('web.list') , "tooltip" => trans('language.show') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#sliders" , "icon"=>"schools", "title" => trans('language.sliders') , "description" => trans('web.list'), "feather"=>"columns"])

                            <div class="collapse" id="sliders">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "sliders" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "sliders/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#salary_packages" , "icon"=>"schools", "title" => trans('language.salary_packages') , "description" => trans('web.list'), "feather"=>"folder"])

                            <div class="collapse" id="salary_packages">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "salary_packages" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "salary_packages/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#commissary_reasons" , "icon"=>"schools", "title" => trans('language.commissary_reasons') , "description" => trans('web.list'), "feather"=>"info"])

                            <div class="collapse" id="commissary_reasons">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "commissary_reasons" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "commissary_reasons/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#notifications" , "icon"=>"schools", "title" => trans('language.notifications') , "description" => trans('web.list'), "feather"=>"bell"])

                            <div class="collapse" id="notifications">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "notifications" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#weather_messages" , "icon"=>"schools", "title" => trans('language.weather_messages') , "description" => trans('web.list'), "feather"=>"sunrise"])

                            <div class="collapse" id="weather_messages">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "weather_messages" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "weather_messages/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>
                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#payment_types" , "icon"=>"schools", "title" => trans('language.payment_types') , "description" => trans('web.list'), "feather"=>"dollar-sign"])

                            <div class="collapse" id="payment_types">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "payment_types" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
{{--                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "payment_types/create" , "title" => trans('web.add') , "tooltip" => trans('web.add') , "class" => "mdi mdi-view-plus"])--}}
                                </ul>
                            </div>
                        </div>
                        <br>

                        <div class="nav-item">

                            @includeIf("admin.layout.aside.main-item" ,["href" => "#setting_section" , "icon"=>"schools", "title" => trans('language.settings') , "description" => trans('web.list'), "feather"=>"settings"])

                            <div class="collapse" id="setting_section">
                                <ul class="nav sub-menu">
                                    @includeIf("admin.layout.aside.sub-item" ,["href" => "setting" , "title" => trans('web.list') , "tooltip" => trans('web.list') , "class" => "mdi mdi-view-list"])
                                </ul>
                            </div>
                        </div>


                        <br>
                        <br>
                    </div>

                </div>
            </li>


        </ul>

    </div>
</nav>
<nav class="settings-sidebar">
    <div class="sidebar-body">
        <a href="#" class="settings-sidebar-toggler">
            <i data-feather="settings"></i>
        </a>
        <h6 class="text-muted">Sidebar:</h6>
        <div class="form-group border-bottom">
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="sidebarThemeSettings" id="sidebarLight"
                           value="sidebar-light" checked>
                    Light
                </label>
            </div>
            <div class="form-check form-check-inline">
                <label class="form-check-label">
                    <input type="radio" class="form-check-input" name="sidebarThemeSettings" id="sidebarDark"
                           value="sidebar-dark">
                    Dark
                </label>
            </div>
        </div>
        <div class="theme-wrapper">
            <h6 class="text-muted mb-2">Light Theme:</h6>
            <a class="theme-item active" href="demo_1/dashboard-one.html">
                <img src="assets/images/screenshots/light.jpg" alt="light theme">
            </a>
            <h6 class="text-muted mb-2">Dark Theme:</h6>
            <a class="theme-item" href="demo_2/dashboard-one.html">
                <img src="assets/images/screenshots/dark.jpg" alt="light theme">
            </a>
        </div>
    </div>
</nav>
