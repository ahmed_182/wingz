@extends('admin.layout.table.index')
@section('page-title',trans('language.meeting_requests'))
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.meeting_requests')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.user')}}</th>
    <th>{{trans('language.child_name')}}</th>
    <th>{{trans('language.driver')}}</th>
    <th>{{trans('language.trip')}}  </th>
    <th>{{trans('language.status')}}  </th>

@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->user['name'] }}</td>
            <td>{{ $item->child['name'] }}</td>
            <td>{{ $item->trip->driver['name'] }}</td>
            <td> {{trans('language.number')}} {{ $item->trip->id }}</td>
            <td>  {{ $item->status_name }}</td>
        </tr>
    @endforeach
@endsection


