@extends('admin.layout.forms.add.index')
@section('action' , "openScreens")
@section('title' , trans('language.add'))
@section('page-title',trans('language.openScreens'))
@section('form-groups')

    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])

    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])

     @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.title_ar'),'name'=>'body_ar', 'placeholder'=>trans('language.title_ar')])
     @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.title_en'),'name'=>'body_en', 'placeholder'=>trans('language.title_en')])

@endsection
@section('submit-button-title' , trans('language.add'))
