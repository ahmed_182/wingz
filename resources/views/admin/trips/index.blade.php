@extends('admin.layout.table.index')
@section('page-title',trans('language.trips'))
@section('buttons')
<br>
    @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/trips/create"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop

@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.trips')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>


    <th>{{trans('language.driver')}}</th>
    <th>{{trans('language.schools')}}</th>
    <th>{{trans('language.status')}}</th>
    <th>{{trans('language.children')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_driver_name}}</td>
            <td>{{$item->dash_school_name}}</td>
            <td>{{$item->dash_status_name}} </td>
            <td>
                <a class="btn btn-info"
                   href="{{url("admin/trips/$item->id/children/")}}"> {{trans('language.children')}} </a>
            </td>
            <td>
                @includeIf("admin.components.buttons.delete",["message" => "" ,  "action" => url("admin/trips/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


