@extends('admin.layout.table.index')
@section('page-title',trans('language.rides'))
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item"><a
                href="{{url("admin/trips")}}">  {{trans('language.trips')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.rides')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.schools')}}</th>
    <th>{{trans('language.status')}}</th>
    <th>{{trans('language.created_at')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->trip->school['dash_name']}}</td>
            <td>{{$item->trip->dash_status_name}}</td>
            <td>{{$item->trip_status['created_at']}} </td>
        </tr>
    @endforeach
@endsection


