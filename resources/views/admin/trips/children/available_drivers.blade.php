@extends('admin.layout.forms.add.index')
@section('action' , "child_assign_drivers")
@section('title' , trans('language.drivers'))
@section('submit-button-title' , trans('language.send_notification'))
@section('form-groups')

    <input type="hidden" name="child_id" value="{{$child->id}}">
    @foreach($items as $driver)
        <div class="col-md-6 col-lg-6 col-xlg-4">
            <div class="card card-body">
                <div class="row">
                    <div class="col-md-4 col-lg-3 text-center">
                        <a href="{{$driver->dash_image}}" target="_blank"><img style="height: 70px;width: 70px"
                                                                               src="{{$driver->dash_image}}"
                                                                               alt="user"
                                                                               class="img-circle img-responsive"></a>
                    </div>
                    <div class="col-md-8 col-lg-9">
                        <h3 class="box-title m-b-0">{{$driver->dash_name}}</h3>
                        <address>
                            <i class="fa fa-phone"></i> \ {{$driver->dash_mobile}}
                        </address>
                    </div>
                    <div class="col-md-12">
                        <br>
                        @foreach(\App\Trip::where('driver_id',$driver->id)->get() as $trip)
                            <input type="checkbox" id="trip{{$trip->id}}" name="trip_id[]" value="{{$trip->id}}"
                                   @if(\App\User_assigin_trips::where('child_id', $child->id)->where('trip_id', $trip->id)->first()) checked
                                   disabled @endif>
                            children : (
                            @foreach($trip->children as $child)
                                {{@$child->child->name}} ,
                            @endforeach
                    ) -  School : ( {{$trip->school->dash_name}} ) .

                            </label><br>
                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    @endforeach
@endsection




