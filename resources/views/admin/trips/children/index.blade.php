@extends('admin.layout.table.index')
@section('page-title',trans('language.children'))
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item"><a
                href="{{url("admin/trips")}}">  {{trans('language.trips')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.children')}}</li>
    </ol>
@endsection

@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => "children/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.birthday')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->serv_image])</td>
            <td>{{$item->name}}</td>
            <td>{{$item->dash_birthday}}</td>
            <td>
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->name)" ,  "action" => url("admin/trips/$trip->id/children/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


