@extends('admin.layout.table.index')
@section('page-title',trans('language.pending_children'))
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.pending_children')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.mobile')}}</th>
    {{--    <th>{{trans('language.school')}}</th>--}}
    <th>{{trans('language.plan')}}</th>
    <th>{{trans('language.birthday')}}</th>
    <th>{{trans('language.status')}}</th>
    <th>{{trans('language.location')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->serv_image])</td>
            <td>{{$item->name}}</td>
            <td>{{$item->mobile}}</td>
            {{--            <td>{{optional($item->school_time->schoolLevel->school)->dash_name ? : trans('language.notSelected')  }}</td>--}}

            <td>{{optional($item->order_plan_details())->dash_name ? : trans('language.notSelected')  }}</td>


            <td>{{$item->birthday}}</td>
            <td>
                {{$item->dash_status_name}}
                <br>
                <br>
                @if($item->is_deleted == 0)
                    <a class="btn btn-primary"
                       href="{{url("admin/delete_account/$item->id")}}"> {{trans('language.remove')}} {{trans('language.account')}} </a>
                @else
                    <a class="btn btn-primary"
                       href="{{url("admin/retuen_account/$item->id")}}"> {{trans('language.retuen')}} {{trans('language.account')}} </a>
                @endif
            </td>
            <td>
                <a href="{{url("/admin/pending_child_map_location/$item->id")}}" class="btn btn-info"> <i
                        class="fa fa-map"></i> {{trans('language.location')}}  </a>
            </td>
            <td>
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->name)" ,  "action" => url("admin/users/$item->user_id/children/$item->id")])
                @includeIf("admin.components.buttons.custom" , ["href" => "afterSchool/$item->id", 'class' => 'default' , 'title'=> trans('language.after_school'), 'icon' => 'fa fa-list','feather' => 'list'])
                @includeIf("admin.components.buttons.custom" , ["href" => "assign_drivers/$item->id", 'class' => 'default' , 'title'=> trans('language.assignDriver'), 'icon' => 'fa fa-send-o' , 'feather' => 'plus-circle'])
            </td>
        </tr>
    @endforeach
@endsection
@section("filters")
    <form method="get" action="{{url("/admin/pending_children/")}}">

        <div style="display: flex">
            <div class="col-md-3">
                <input type="text" class="form-control" name="name" placeholder="{{trans('language.name')}}">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" name="mobile" placeholder="{{trans('language.mobile')}}">
            </div>
            <div class="col-md-3">
                <select id="" class="form-control" name="user_id">
                    <option value="0">{{trans('language.users')}}  </option>
                    @foreach(\App\User::where('user_type_id',\App\ModulesConst\UserTyps::user)->get() as $user)
                        <option value="{{$user->id}}"
                        >{{$user->dash_name}}</option>
                    @endforeach
                </select>
            </div>
            <div class="col-md-2">
                <input type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
            </div>
        </div>
    </form>
@stop


