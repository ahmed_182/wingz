@extends('admin.layout.index')
@section('content')

    <div class="col-md-12">
        <div id="googleMap" style="width:100%;height:750px;"></div>
    </div>

@endsection
@section("extra_js")
    <script>

        var lat = "{{$child->user->lat}}";
        var lng = "{{$child->user->lng}}";

        var orange_lats = @json($orange_lats);
        var orange_lngs = @json($orange_lngs);

        var green_lats = @json($orange_lats);
        var green_lngs = @json($orange_lngs);

        var mapProp = {
            center: new google.maps.LatLng(lat, lng),
            zoom: 9,
        };

        var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);
        marker_of_child = new google.maps.Marker({
            position: new google.maps.LatLng(lat, lng),
            map: map,
            animation: google.maps.Animation.DROP,
            icon: "{{asset('assets/admin/images/student_red.png')}}",
        });

        // draw other children pins on map Green in trip ) . :-
        for (i = 0; i < green_lats.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(green_lats[i], green_lngs[i]),
                map: map,
                animation: google.maps.Animation.DROP,
                icon: "{{asset('assets/admin/images/student_green.png')}}",
            });
        }
        // draw other children pins on map Orange not  in trip ) . :-
        for (i = 0; i < orange_lats.length; i++) {
            marker = new google.maps.Marker({
                position: new google.maps.LatLng(orange_lats[i], orange_lngs[i]),
                map: map,
                animation: google.maps.Animation.DROP,
                icon: "{{asset('assets/admin/images/student_orange.png')}}",
            });
        }


    </script>
@endsection
