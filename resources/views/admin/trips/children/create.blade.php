@extends('admin.layout.forms.add.index')
@section('action' , "trips/$trip->id/children")
@section('title' , trans('language.add'))
@section('page-title',trans('language.children'))
@section('form-groups')

    @includeIf('admin.components.form.add.select', ['label' => trans("language.children"),'name'=>'child_id', 'items'=> $children, 'icon' => 'fa fa-list',])

@endsection
@section('submit-button-title' ,trans('language.add'))

