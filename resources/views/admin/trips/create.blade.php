@extends('admin.layout.forms.add.index')
@section('action' , "trips")
@section('title' , trans('language.add'))
@section('page-title',trans('language.trips'))
@section('form-groups')


    @includeIf('admin.components.form.add.select', ['label' => trans("language.schools"),'name'=>'school_id', 'items'=> $schools, 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.add.select', ['label' => trans("language.drivers"),'name'=>'driver_id', 'items'=> $drivers, 'icon' => 'fa fa-list',])

@endsection

@section('submit-button-title', trans('web.add'))


