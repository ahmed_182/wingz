@extends('admin.layout.forms.edit.index')
@section('action' , "stages/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.stages'))
@section('form-groups')
    @includeIf('admin.components.form.edit.select', ['label' => trans("language.levels"),'name'=>'level_id', 'items'=> \App\Level::all() , 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])

@endsection
@section('submit-button-title' ,trans('language.edit'))
