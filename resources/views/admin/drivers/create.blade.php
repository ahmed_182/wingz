@extends('admin.layout.forms.add.index')
@section('action' , "drivers")
@section('title' , trans('language.add'))
@section('page-title',trans('language.drivers'))
@section('form-groups')
    <input type="hidden" class="location" id="location" name="location" value="">
    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])
    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.car_photo'),'name'=>'car_photo', 'max'=>'2'])
    @includeIf('admin.components.form.add.select', ['label' => trans("language.country"),'name'=>'country_id', 'items'=> \App\Country::all() , 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name'),'name'=>'name', 'placeholder'=>trans('language.name')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-phone','label' => trans('language.mobile'),'name'=>'mobile', 'placeholder'=>trans('language.mobile')])
    @includeIf('admin.components.form.add.password', ['icon' => 'fa fa-key','label' => trans('language.password'),'name'=>'password', 'placeholder'=>trans('language.password')])

    @includeIf('admin.components.form.add.select', ['label' => trans("language.car_names"),'name'=>'cat_name_id', 'items'=> \App\Car_name::all() , 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.add.select', ['label' => trans("language.car_types"),'name'=>'cat_type_id', 'items'=> \App\Car_type::all() , 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.add.select', ['label' => trans("language.manufacturing_years"),'name'=>'manufacturing_year_id', 'items'=> \App\Manufacturing_year::all() , 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-list','label' => trans('language.plate_number'),'name'=>'plate_number', 'placeholder'=>trans('language.plate_number')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-list','label' => trans('language.driving_license'),'name'=>'driving_license', 'placeholder'=>trans('language.driving_license')])


    <div class="form-group">
        <label>{{trans('language.driver_types')}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-list"></i></span>
            </div>
            <select required class="form-control" name="driver_type">
                <option value="1">{{trans('language.captain_driver')}}</option>
                <option value="2">{{trans('language.share_driver')}}</option>
            </select>
        </div>
    </div>


    <div class="col-xs-12 text-center margin-bottom-40">
        <div class="map" id="map" style="width:100%;height:600px;"></div>
    </div>
    <br>
    <br>
    <br>

    <script>
        var iconUrl = "{{asset('assets/admin/images/marker.png')}}"
    </script>

@endsection
@section('submit-button-title' , trans('web.add'))
@section('extra_js')

    <script>

        lat = 30.006735;
        lng = 31.428937;

            var myLatlng = new google.maps.LatLng(lat, lng);
            var mapOptions = {
                zoom: 11,
                center: myLatlng
            }
            var geocoder = new google.maps.Geocoder;
            var map = new google.maps.Map(document.getElementById("map"), mapOptions);

            var marker = new google.maps.Marker({
                position: myLatlng,
                map: map,
                animation: google.maps.Animation.DROP
            });

            // To add the marker to the map, call setMap();
            marker.setMap(map);

            google.maps.event.addListener(map, 'click', function (event) {
                placeMarker(event.latLng);
            });

            function placeMarker(location) {
                marker.setPosition(location);
                console.log(location)
                geocodeLatLng(geocoder, map, location);
                latLngLocation = location.toString().slice(1, -1); // remove ()
                $('#location').val(latLngLocation);
            }

            function geocodeLatLng(geocoder, map, latlng) {
                geocoder.geocode({'location': latlng}, function (results, status) {
                    if (status === 'OK') {
                        if (results[0]) {
                            address = results[0].formatted_address;

                            $('#pac-input').val(address);
                        } else {
                            window.alert('No results found');
                        }
                    } else {
                        window.alert('Geocoder failed due to: ' + status);
                    }
                });
        }

    </script>
    <!--Load the API from the specified URL
    * The async attribute allows the browser to render the page while the API loads
    * The key parameter will contain your own API key (which is not needed for this tutorial)
    * The callback parameter executes the initMap() function
    -->
{{--    <script async defer--}}
{{--            src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBvZ8TXE8hvtoapELAaHIjb9NC78KOq9Xo&callback=initMap">--}}
{{--    </script>--}}

@endsection
