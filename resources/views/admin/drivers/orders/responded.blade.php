@extends('admin.layout.table.index')
@section('page-title',trans('language.trips'))
@section('buttons')

@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.driver')}}</th>
    <th>{{trans('language.status')}}</th>

@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_driver_name}} </td>
            <td>{{$item->status_name}} </td>
        </tr>
    @endforeach
@endsection

