@extends('admin.layout.table.index')
@section('page-title',trans('language.trips'))
@section('buttons')

@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.driver')}}</th>
    <th>{{trans('language.schools')}}</th>
    <th>{{trans('language.status')}}</th>
    <th>{{trans('language.children')}}</th>
    <th>{{trans('language.rides')}}</th>

@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_driver_name}}</td>
            <td>{{$item->dash_school_name}}</td>
            <td>{{$item->dash_status_name}} </td>
            <td>
                <a class="btn btn-info"
                   href="{{url("admin/trips/$item->id/children/")}}"> {{trans('language.children')}} </a>
            </td>
            <td>
                <a class="btn btn-primary"
                   href="{{url("admin/trips/$item->id/rides/")}}"> {{trans('language.rides')}} </a>
            </td>

        </tr>
    @endforeach
@endsection


