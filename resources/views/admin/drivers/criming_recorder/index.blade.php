@extends('admin.layout.table.index')
@section('page-title',trans('language.criming_recorder'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => "criming_recorder/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->data])</td>
            <td>
                 @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/drivers/$item->driver_id/criming_recorder/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


