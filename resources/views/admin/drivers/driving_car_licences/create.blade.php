@extends('admin.layout.forms.add.index')
@section('action' , "drivers/$driver->id/driving_car_licences")
@section('title' , trans('language.add'))
@section('page-title',trans('language.driving_car_licences'))
@section('form-groups')
    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'data', 'max'=>'2'])
@endsection
@section('submit-button-title' , trans('web.add'))
