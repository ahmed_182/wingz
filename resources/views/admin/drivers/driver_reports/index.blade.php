@extends('admin.layout.table.index')
@section('page-title',trans('language.driver_reports'))
@section('buttons')
{{--    @includeIf("admin.components.buttons.addbtn" , ["href" => "driver_reports/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])--}}
@stop
@section('thead')
    <th>#</th>
    <th>{{trans('language.rate')}}</th>
    <th>{{trans('language.text')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{ $item->rate }}</td>
            <td>{{ $item->report }}</td>
            <td>
                 @includeIf("admin.components.buttons.delete",["message" =>  "($item->dash_name)" ,  "action" => url("admin/drivers/$item->driver_id/driver_reports/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


