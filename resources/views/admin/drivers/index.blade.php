@extends('admin.layout.index')

@section('content')


    <div class="col-md-12">
        <div class="card">
            <div class="card-body">
                <h4 class="card-title">{{trans('language.drivers')}}</h4>

                <ul class="nav nav-pills m-t-30 m-b-30">
                    <li class=" nav-item navTap-1"><a href="#navpills-1"
                                                      class="nav-link @if($tapType == 'navTap-1' ) active @endif"
                                                      data-toggle="tab"
                                                      aria-expanded="false">{{trans('language.driver')}}
                        </a></li>
                    <li class="nav-item navTap-2"><a href="#navpills-2"
                                                     class="nav-link @if($tapType == 'navTap-2' ) active @endif"
                                                     data-toggle="tab"
                                                     aria-expanded="false">{{trans('language.driver_wings')}} </a></li>
                </ul>

                <br>
                

                <form method="get" action="{{url("/admin/active_drivers/")}}">

                    <div style="display: flex">
                        <div class="col-md-2">
                            <input type="text" class="form-control" name="name"
                                   placeholder="{{trans('language.name')}}">
                        </div>
                        <div class="col-md-3">
                            <input type="text" class="form-control" name="mobile"
                                   placeholder="{{trans('language.mobile')}}">
                        </div>

                    {{--                        <div class="col-md-3">--}}
                    {{--                            <select class="form-control" name="district_id">--}}
                    {{--                                @foreach(\App\District::all() as $selectItem)--}}
                    {{--                                    <option value="">{{trans('language.districts')}}</option>--}}
                    {{--                                    <option value="{{$selectItem->id}}">{{$selectItem->dash_name}}</option>--}}
                    {{--                                @endforeach--}}
                    {{--                            </select>--}}
                    {{--                        </div>--}}

                    <!-- send tap type in form to store it in session -->
                        <input type="hidden" name="tapType" value="navTap-1" class="tapType">

                        <div class="col-md-2">
                            <input type="submit" class="btn btn-success " value="{{trans('language.filter')}}">
                        </div>
                    </div>
                </form>
                <br>
    @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/drivers/create"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
                <div class="tab-content br-n pn col-md-12">

                    <!-- Normal Drivers -->
                    <div id="navpills-1" class="tab-pane @if($tapType == 'navTap-1' ) active @endif">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('language.image')}}</th>
                                    <th>{{trans('language.driver')}}</th>
                                    <th>{{trans('language.mobile')}}</th>
                                    <th>{{trans('language.orders')}}</th>
                                    <th>{{trans('language.settings')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    @if($item->driver)
                                        @if($item->driver->driver_type == \App\ModulesConst\DriverTyps::captain)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td> @includeIf("admin.components.image.index" , ["url" => $item->dash_image])</td>
                                                <td>{{$item->dash_name}}</td>
                                                <td>{{$item->dash_mobile}}</td>
                                                <td>
                                                    <a class="btn btn-primary"
                                                       href="{{url("admin/drivers/$item->id/orders")}}"> {{trans('language.orders')}}   </a>
                                                </td>
                                                <td>


                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/districts/", 'class' => 'default' , 'title'=> trans('language.districts'), 'icon' => 'fa fa-building','feather' => 'list'])
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/criming_recorder/", 'class' => 'default' , 'title'=> trans('language.criming_recorder'), 'icon' => 'fa fa-bullseye','feather' => 'feather'])
                                                    <br>
                                                    <br>
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driving_licences/", 'class' => 'default' , 'title'=> trans('language.driving_licences'), 'icon' => 'fa fa-database','feather' => 'database'])
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driving_car_licences/", 'class' => 'default' , 'title'=> trans('language.driving_car_licences'), 'icon' => 'fa fa-car','feather' => 'grid'])
                                                    <br>
                                                    <br>
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driving_car_images/", 'class' => 'default' , 'title'=> trans('language.driving_car_images'), 'icon' => 'fa fa-image','feather' => 'image'])
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driver_identities/", 'class' => 'default' , 'title'=> trans('language.driver_identities'), 'icon' => 'fa fa-ils','feather' => 'link'])
                                                    <br>
                                                    <br>
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driver_reports/", 'class' => 'default' , 'title'=> trans('language.driver_reports'), 'icon' => 'fa fa-repeat','feather' => 'book'])
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers_driving_license_block/$item->id", 'class' => 'default' , 'title'=> trans('language.driving_license_block'), 'icon' => 'fa fa-ban','feather' => 'shield'])
                                                    <br>
                                                    <br>
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "getReviews/$item->id", 'class' => 'default' , 'title'=> trans('language.rates'), 'icon' => 'fa fa-comment','feather' => 'folder'])
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/single_rides", 'class' => 'default' , 'title'=> trans('language.single_rides'), 'icon' => 'fa fa-road','feather' => 'key'])
                                                    <br>
                                                    <br>

                                                    @includeIf("admin.components.buttons.edit" , ["href" => "drivers/$item->id/edit"])
                                                    @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/drivers/$item->id")])
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                    <!-- wings Drivers -->
                    <div id="navpills-2" class="tab-pane @if($tapType == 'navTap-2' ) active @endif ">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>
                                <tr>
                                    <th>#</th>
                                    <th>{{trans('language.image')}}</th>
                                    <th>{{trans('language.driver')}}</th>
                                    <th>{{trans('language.mobile')}}</th>

                                    <th>{{trans('language.settings')}}</th>
                                </tr>
                                </thead>
                                <tbody>
                                @foreach($items as $item)
                                    @if($item->driver)
                                        @if($item->driver->driver_type == \App\ModulesConst\DriverTyps::share)
                                            <tr>
                                                <td>{{ $loop->iteration }}</td>
                                                <td> @includeIf("admin.components.image.index" , ["url" => $item->dash_image])</td>
                                                <td>{{$item->dash_name}}</td>
                                                <td>{{$item->dash_mobile}}</td>

                                                <td>

                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/districts/", 'class' => 'default' , 'title'=> trans('language.districts'), 'icon' => 'fa fa-building','feather' => 'list'])
                                                    <br>
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/criming_recorder/", 'class' => 'default' , 'title'=> trans('language.criming_recorder'), 'icon' => 'fa fa-bullseye','feather' => 'feather'])
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driving_licences/", 'class' => 'default' , 'title'=> trans('language.driving_licences'), 'icon' => 'fa fa-database' ,'feather' => 'database'])
                                                    <br>
                                                    <br>
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driving_car_licences/", 'class' => 'default' , 'title'=> trans('language.driving_car_licences'), 'icon' => 'fa fa-car' ,'feather' => 'grid'])
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driving_car_images/", 'class' => 'default' , 'title'=> trans('language.driving_car_images'), 'icon' => 'fa fa-image','feather' => 'image'])
                                                    <br>
                                                    <br>
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driver_identities/", 'class' => 'default' , 'title'=> trans('language.driver_identities'), 'icon' => 'fa fa-ils','feather' => 'link'])
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driver_reports/", 'class' => 'default' , 'title'=> trans('language.driver_reports'), 'icon' => 'fa fa-repeat','feather' => 'book'])
                                                    <br>
                                                    <br>

                                                    @includeIf("admin.components.buttons.custom" , ["href" => "drivers_driving_license_block/$item->id", 'class' => 'default' , 'title'=> trans('language.driving_license_block'), 'icon' => 'fa fa-ban','feather' => 'shield'])
                                                    @includeIf("admin.components.buttons.custom" , ["href" => "getReviews/$item->id", 'class' => 'default' , 'title'=> trans('language.rates'), 'icon' => 'fa fa-comment','feather' => 'folder'])

                                                    <br>
                                                    <br>
                                                    @includeIf("admin.components.buttons.edit" , ["href" => "drivers/$item->id/edit"])
                                                    @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/drivers/$item->id")])
                                                </td>
                                            </tr>
                                        @endif
                                    @endif
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </div>

@endsection

@section('extra_js')
    <script>

        // when page reload ... update the type of tap
        var type = "{{$tapType}}";
        $('.tapType').val(type)

        // navTap-1
        $(".navTap-1").click(function () {
            $('.tapType').val('navTap-1')
        });

        // navTap-2
        $(".navTap-2").click(function () {
            $('.tapType').val('navTap-2')
        });

        // navTap-3
        $(".navTap-3").click(function () {
            $('.tapType').val('navTap-3')
        });

    </script>
@stop

