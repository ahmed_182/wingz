@extends('admin.layout.forms.add.index')
@section('action' , "drivers/$driver->id/driver_identities")
@section('title' , trans('language.add'))
@section('page-title',trans('language.driver_identities'))
@section('form-groups')
    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'data', 'max'=>'2'])
@endsection
@section('submit-button-title' , trans('web.add'))
