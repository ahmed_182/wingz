@extends('admin.layout.table.index')
@section('page-title',trans('language.drivers'))
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.waiting_drivers')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.driver')}}</th>
    <th>{{trans('language.email')}}</th>
    <th>{{trans('language.mobile')}}</th>
    <th>{{trans('language.active')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->dash_image])</td>
            <td>{{$item->dash_name}}</td>
            <td>{{$item->dash_email}}</td>
            <td>{{$item->dash_mobile}}</td>
            {{--<td>
                <a class="btn btn-primary"
                   href="{{url("admin/active_account/$item->id")}}"> {{trans('language.active')}} {{trans('language.account')}} </a>
            </td>--}}
            <td>

                <strong style="color: #0c5460;font-weight: bold">{{$item->dash_status_name}}</strong>
                <br>
                <br>
                @if($item->userVerify == \App\ModulesConst\UserVerify::no)
                    <a class="btn btn-success "
                       href="{{url("admin/active_account/$item->id")}}" title="{{trans('language.active')}} {{trans('language.account')}}"> <i class="" data-feather="check" ></i> </a>
                @else
                    <a class="btn btn-danger "
                       href="{{url("admin/drActive_account/$item->id")}}" title="{{trans('language.deActive')}} {{trans('language.account')}}"> <i class="" data-feather="x" ></i> </a>
                @endif
            </td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "drivers/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/drivers/$item->id")])
                @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/districts/", 'class' => 'default' , 'title'=> trans('language.districts'), 'icon' => 'fa fa-building','feather' => 'list'])
                <br>
                <br>
                @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/criming_recorder/", 'class' => 'default' , 'title'=> trans('language.criming_recorder'), 'icon' => 'fa fa-bullseye','feather' => 'align-right'])
                @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driving_licences/", 'class' => 'default' , 'title'=> trans('language.driving_licences'), 'icon' => 'fa fa-database','feather' => 'database'])
                @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driving_car_licences/", 'class' => 'default' , 'title'=> trans('language.driving_car_licences'), 'icon' => 'fa fa-car','feather' => 'database'])
                <br>
                <br>
                @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driving_car_images/", 'class' => 'default' , 'title'=> trans('language.driving_car_images'), 'icon' => 'fa fa-image','feather' => 'image'])
                @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driver_identities/", 'class' => 'default' , 'title'=> trans('language.driver_identities'), 'icon' => 'fa fa-ils','feather' => 'database'])
                @includeIf("admin.components.buttons.custom" , ["href" => "drivers/$item->id/driver_reports/", 'class' => 'default' , 'title'=> trans('language.driver_reports'), 'icon' => 'fa fa-repeat','feather' => 'user'])
                <br>
                <br>
                @includeIf("admin.components.buttons.custom" , ["href" => "drivers_driving_license_block/$item->id", 'class' => 'default' , 'title'=> trans('language.driving_license_block'), 'icon' => 'fa fa-ban','feather' => 'list'])
                @includeIf("admin.components.buttons.custom" , ["href" => "getReviews/$item->id", 'class' => 'default' , 'title'=> trans('language.rates'), 'icon' => 'fa fa-comment','feather' => 'list'])

            </td>
        </tr>
    @endforeach
@endsection


@section("filters")
    <form  method="get" action="{{url("/admin/waiting_drivers/")}}" >

        <div style="display: flex">
            <div class="col-md-2" >
                <input type="text" class="form-control" name="name" placeholder="Name"  >
            </div>
            <div class="col-md-3" >
                <input type="text" class="form-control" name="email" placeholder="Email">
            </div>
            <div class="col-md-3">
                <input type="text" class="form-control" name="mobile" placeholder="Phone">
            </div>

{{--            <div class="col-md-3" >--}}
{{--                <select  class="form-control" name="district_id">--}}
{{--                    @foreach(\App\District::all() as $selectItem)--}}
{{--                        <option value="">{{trans('language.districts')}}</option>--}}
{{--                        <option value="{{$selectItem->id}}">{{$selectItem->dash_name}}</option>--}}
{{--                    @endforeach--}}
{{--                </select>--}}
{{--            </div>--}}

            <div class="col-md-2">
                <input type="submit" class="btn btn-success " value="filter">
            </div>
        </div>
    </form>
@stop

