@extends('admin.layout.forms.edit.index')
@section('action' , "countries/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.countries'))
@section('form-groups')
    @includeIf('admin.components.form.edit.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])

    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
{{--
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.curreny_ar'),'name'=>'curreny_ar', 'placeholder'=>trans('language.curreny_ar')])
--}}
{{--
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.curreny_en'),'name'=>'curreny_en', 'placeholder'=>trans('language.curreny_en')])
--}}
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.code'),'name'=>'code', 'placeholder'=>trans('language.code')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.number_count'),'name'=>'number_count', 'placeholder'=>trans('language.number_count')])


@endsection
@section('submit-button-title' , trans('language.edit'))
