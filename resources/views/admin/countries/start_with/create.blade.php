@extends('admin.layout.forms.add.index')
@section('action' , "countries/$country->id/start_with")
@section('title' , trans('language.add'))
@section('page-title',trans('language.start_with'))
@section('form-groups')
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.start_with'),'name'=>'start_with', 'placeholder'=>trans('language.start_with')])
@endsection
@section('submit-button-title' ,trans('language.add'))

