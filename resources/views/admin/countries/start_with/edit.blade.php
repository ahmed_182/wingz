@extends('admin.layout.forms.edit.index')
@section('action' , "countries/$item->country_id/start_with/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.start_with'))
@section('form-groups')
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.start_with'),'name'=>'start_with', 'placeholder'=>trans('language.start_with')])
@endsection
@section('submit-button-title' , trans('language.edit'))
