@extends('admin.layout.table.index')
@section('page-title',trans('language.contact_us'))
@section('buttons')

@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.contact_us')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
{{--    <th>{{trans('language.name')}}</th>--}}
{{--    <th>{{trans('language.mobile')}}</th>--}}
{{--    <th>{{trans('language.subject')}}</th>--}}
    <th>{{trans('language.text')}}</th>
    <th>{{trans('language.created_at')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
{{--            <td>{{$item->name}}</td>--}}
{{--            <td>{{$item->mobile}}</td>--}}
{{--            <td>{{$item->subject}}</td>--}}
            <td>{{$item->message}}</td>
            <td>{{$item->dash_created}}</td>
            <td>
                @includeIf("admin.components.buttons.delete",["message" =>  "($item->message)" ,  "action" => url("admin/contacts/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


