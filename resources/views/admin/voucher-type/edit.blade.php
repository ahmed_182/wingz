@extends('admin.layout.forms.edit.index')
@section('action' , "voucher-type/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.voucher-type'))
@section('form-groups')

    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
@endsection
@section('submit-button-title', trans('web.edit'))
