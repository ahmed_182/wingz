@extends('admin.layout.table.index')
@section('page-title',trans('language.voucher-type'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/voucher-type/create"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.voucher-type')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>
    <th>{{trans('language.type')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_name}}</td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "voucher-type/$item->id/edit"])
                @includeIf("admin.components.buttons.delete",["message" => "($item->voucher_type)" ,  "action" => url("admin/voucher-type/$item->id")])
            </td>
        </tr>
    @endforeach
@endsection


