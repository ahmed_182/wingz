@extends('admin.layout.table.index')
@section('page-title',trans('language.schools'))
@section('root' , "schools")
@section('buttons')
<br>
    @includeIf("admin.components.buttons.addbtn" , ["href" => url("admin/schools/create"),'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.schools')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>

    <th>{{trans('language.image')}}</th>
    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.education_types')}}</th>
    <th>{{trans('language.address')}}</th>
    <th>{{trans('language.city')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td> @includeIf("admin.components.image.index" , ["url" => $item->dash_image])</td>
            <td>{{$item->dash_name}}</td>
            <td>{{$item->dash_education_type_name}}</td>
            <td>{{$item->address}}</td>
            <td>{{$item->dash_district_name}}</td>
            <td>


                @includeIf("admin.components.buttons.custom" , ["href" => "schoolsAddress/$item->id", 'class' => 'default' , 'title'=> trans('language.location'), 'icon' => 'fa fa-map','feather'=>'map-pin'])

                @includeIf("admin.components.buttons.custom" , ["href" => "schools/$item->id/levels/", 'class' => 'default' , 'title'=> trans('language.levels'), 'icon' => 'fa fa-list','feather'=>'list'])
                <br>
                <br>
                @includeIf("admin.components.buttons.edit" , ["href" => "schools/$item->id/edit"])

                @includeIf("admin.components.buttons.delete",["message" => "($item->dash_name)" ,  "action" => url("admin/schools/$item->id")])


            </td>
        </tr>
    @endforeach
@endsection

@section("filters")
    <form  method="get" action="{{url("/admin/schools/")}}" >

        <div style="display: flex">
            <div class="col-md-3" >
                <input type="text" class="form-control" name="name" placeholder="{{trans('language.name')}}"  >
            </div>
            <div class="col-md-3" >
                <select  class="form-control" name="district_id">
                    <option value="">{{trans('language.countries')}}</option>
                    @foreach(\App\District::all() as $selectItem)
                        <option value="{{$selectItem->id}}">{{$selectItem->dash_name}}</option>
                    @endforeach
                </select>
            </div>

            <div class="col-md-2">
                <input type="submit" class="btn btn-primary " value="{{trans('language.filter')}}">
            </div>
        </div>
    </form>
@stop



