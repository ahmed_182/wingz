@extends('admin.layout.table.index')
@section('page-title',trans('language.holidays'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => "holidays/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('thead')
    <th>#</th>

    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.school_name')}}</th>
    <th>{{trans('language.start_at')}}</th>
    <th>{{trans('language.end_at')}}</th>
    <th>{{trans('language.status')}}</th>
    <th>{{trans('language.settings')}}</th>

@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_stage_name}}</td>
            <td>{{$item->dash_school_name}}</td>
            <td>{{$item->start_at}}</td>
            <td>{{$item->end_at}}</td>
            <td>{{$item->dash_status_name}}</td>
            <td>
{{--                @includeIf("admin.components.buttons.edit" , ["href" => "school_times_stages/$item->school_time_stage_id/holidays/$item->id/edit"])--}}

                @includeIf("admin.components.buttons.delete",["message" =>  "($item->serv_stage_name)" ,  "action" => url("admin/school_times_stages/$item->school_time_stage_id/holidays/$item->id")])


                @includeIf("admin.components.buttons.custom" , ["href" => "stage_holidays/$item->id/days", 'class' => 'default' , 'title'=> trans('language.days'), 'icon' => 'fa fa-list-ul'])

            </td>
        </tr>
    @endforeach
@endsection


