@extends('admin.layout.table.index')
@section('page-title',trans('language.holidays'))
{{--@section('buttons')--}}
{{--    @includeIf("admin.components.buttons.addbtn" , ["href" => "holidays/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])--}}
{{--@stop--}}
@section('thead')
    <th>#</th>
    <th>{{trans('language.day')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->day_stamp}}</td>
        </tr>
    @endforeach
@endsection


