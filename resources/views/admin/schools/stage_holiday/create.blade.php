@extends('admin.layout.forms.add.index')
@section('action' , "school_times_stages/$school_time->id/holidays")
@section('title' , trans('language.add'))
@section('page-title',trans('language.holidays'))
@section('form-groups')


    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.start_at")}}</label>
        <input class="form-control" type="date" data-date="" name="start_at" data-date-format="DD MMMM YYYY"
               value="2020-01-01">
    </div>


    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.end_at")}}</label>
        <input class="form-control" type="date" data-date="" name="end_at" data-date-format="DD MMMM YYYY"
               value="2020-01-01">
    </div>

    <div class="form-group">
        <label>{{trans('language.status')}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class="fa fa-list"></i></span>
            </div>
            <select required class="form-control" name="paid_status">
                <option value="1">{{trans('language.paid')}}</option>
                <option value="0">{{trans('language.free')}}</option>
            </select>
        </div>
    </div>

@endsection
@section('submit-button-title' ,trans('language.add'))
@section('extra_css')
    <style>
        input {
            position: relative;
            width: 150px;
            height: 25px;
            color: white;
        }

        input:before {
            position: absolute;
            top: 5px;
            left: 3px;
            content: attr(data-date);
            display: inline-block;
            color: black;
        }

        input::-webkit-datetime-edit, input::-webkit-inner-spin-button, input::-webkit-clear-button {
            display: none;
        }

        input::-webkit-calendar-picker-indicator {
            position: absolute;
            top: 3px;
            right: 0;
            color: black;
            opacity: 1;
        }
    </style>
@endsection
@section('extra_js')

    <script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.10.3/moment.min.js"></script>


    <script>
        $("input").on("change", function () {
            this.setAttribute(
                "data-date",
                moment(this.value, "YYYY-MM-DD")
                    .format(this.getAttribute("data-date-format"))
            )
        }).trigger("change")

    </script>
@endsection

