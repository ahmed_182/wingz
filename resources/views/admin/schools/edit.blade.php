@extends('admin.layout.forms.edit.index')
@section('action' , "schools/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.schools'))
@section('form-groups')
    <input type="hidden" class="location" id="location" name="location" value="">
    @includeIf('admin.components.form.edit.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
    @includeIf('admin.components.form.edit.text', ['icon' => 'fa fa-location-arrow','label' => trans('language.address'),'name'=>'address', 'placeholder'=>trans('language.address')])
    @includeIf('admin.components.form.edit.select', ['label' => trans("language.education_types"),'name'=>'education_type_id', 'items'=> \App\Education_type::all() , 'icon' => 'fa fa-list',])
    @includeIf('admin.components.form.add.select', ['label' => trans("language.districts"),'name'=>'district_id', 'items'=> \App\District::all() , 'icon' => 'fa fa-list',])
    <div id="googleMap" style="width:100%;height:700px;"></div>
    <br>10إلى تاريخ

@endsection
@section('submit-button-title' ,trans('language.edit'))


@section('extra_css')
    <style>
        /* Set the size of the div element that contains the map */
        #map {
            height: 400px; /* The height is 400 pixels */
            width: 100%; /* The width is the width of the web page */
        }
    </style>
@endsection
@section('extra_js')
    <script>
        var iconUrl = "{{asset('assets/admin/images/marker.png')}}"
    </script>

    <script>

            var lat = parseFloat("{{$item->lat}}");
            var lng = parseFloat("{{$item->lng}}");

            var mapProp = {
                center: new google.maps.LatLng(lat, lng),
                zoom: 8,
            };

            var map = new google.maps.Map(document.getElementById("googleMap"), mapProp);

            var goldenGatePosition = {lat: lat, lng: lng};
            var marker = new google.maps.Marker({
                position: goldenGatePosition,
                map: map,
                title: '{{$item->name}}',
                icon: iconUrl
            });
            google.maps.event.addListener(map, 'click', function (event) {
                placeMarker(event.latLng);
            });

            function placeMarker(location) {
                marker.setPosition(location);
                latLngLocation = location.toString().slice(1, -1);
                $('#location').val(latLngLocation);
            }

    </script>
@endsection