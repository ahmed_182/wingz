@extends('admin.layout.forms.add.index')
@section('action' , "schools")
@section('title' , trans('language.add'))
@section('page-title',trans('language.schools'))
@section('form-groups')
    <input type="hidden" class="location" id="location" name="location" value="">
    @includeIf('admin.components.form.add.file', ['icon' => 'fa fa-check','label' => trans('language.image'),'name'=>'image', 'max'=>'2'])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_ar'),'name'=>'name_ar', 'placeholder'=>trans('language.name_ar')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-user','label' => trans('language.name_en'),'name'=>'name_en', 'placeholder'=>trans('language.name_en')])
    @includeIf('admin.components.form.add.text', ['icon' => 'fa fa-location-arrow','label' => trans('language.address'),'name'=>'address', 'placeholder'=>trans('language.address')])
    @includeIf('admin.components.form.add.select', ['label' => trans("language.education_types"),'name'=>'education_type_id', 'items'=> \App\Education_type::all() , 'icon' => 'fa fa-list',])

    <div class="form-group">
        <label>{{trans("language.countries")}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class='fa fa-list'></i></span>
            </div>
            <select required class="form-control country_id_list country_id" id="country_id"
                    name="main_category_id">
                <option value="0"> {{trans('language.countries')}}</option>
                @foreach(\App\Country::get() as $country)
                    <option name="country_id" value="{{$country->id}}">{{$country->dash_name}}</option>
                @endforeach
            </select>
        </div>
    </div>
    <div class="form-group  cities_List">
        <label>{{trans("language.cities")}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class='fa fa-list'></i></span>
            </div>
            <select id="city_id" name="city_id"
                    class=" form-control city_id_list city_id">
                <option name="city_id" class="basicOption" value="0">{{trans('language.cities')}}</option>
            </select>

        </div>
    </div>
    <div class="form-group  districts_List">
        <label>{{trans("language.districts")}}</label>
        <div class="input-group">
            <div class="input-group-prepend">
                <span class="input-group-text"><i class='fa fa-list'></i></span>
            </div>
            <select id="district_id" name="district_id"
                    class=" form-control district_id_list district_id">
                <option name="district_id" class="basicOption" value="0">{{trans('language.districts')}}</option>
            </select>

        </div>
    </div>


    <div class="col-xs-12 text-center margin-bottom-40">
        <div class="map" id="map" style="width:100%;height:600px;"></div>
    </div>

    <script>
        var iconUrl = "{{asset('assets/admin/images/marker.png')}}"
    </script>


    <hr>

@endsection
@section('submit-button-title' ,trans('language.add'))
@section('extra_js')

    <script>
        $(document).ready(function () {
            $('.country_id').on('change', function () {
                var country_id = $('.country_id').val();
                $.post("{{url("/getCities")}}",
                    {
                        country_id: country_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {
                        console.log(data)
                        if (data.status == true) {
                            $('.city_id_list').html('');
                            if (data.status == true) {
                                $(".city_id_list").prepend('' +
                                    '<option  value="0"> {{trans('language.cities')}} </option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {
                                    $(".city_id_list").prepend('' +
                                        '<option name="city_id"  value="' + item.id + '">' + item.name + '</option>');
                                });
                            });
                        } else {
                            $('.city_id_list').html('');
                            $(".city_id_list").prepend('' +
                                '<option name="city_id"  value="0">لا توجد مدن  </option>');
                        }
                    });
            });
            $('.city_id').on('change', function () {
                var city_id = $('.city_id').val();
                $.post("{{url("/getDistracts")}}",
                    {
                        city_id: city_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {
                        console.log(data)
                        if (data.status == true) {
                            $('.district_id_list').html('');
                            if (data.status == true) {
                                $(".district_id_list").prepend('' +
                                    '<option  value="0"> {{trans('language.districts')}} </option>');
                            }
                            $.each(data, function () {
                                $.each(this, function (index, item) {
                                    $(".district_id_list").prepend('' +
                                        '<option name="district_id"  value="' + item.id + '">' + item.name + '</option>');
                                });
                            });
                        } else {
                            $('.district_id_list').html('');
                            $(".district_id_list").prepend('' +
                                '<option name="district_id"  value="0">لا توجد احياء  </option>');
                        }
                    });
            });
            $('.district_id').on('change', function () {
                var district_id = $('.district_id').val();
                $.post("{{url("/getDistractDetails")}}",
                    {
                        district_id: district_id,
                        _token: "{{csrf_token()}}"
                    },
                    function (data, status) {
                        if (data.status == true) {
                            lat = data.distract['lat'];
                            lng = data.distract['lng'];
                        } else {
                            lat = 30.006735;
                            lng = 31.428937;
                        }
                        $(document).ready(function () {


                            latLng = {lat: lat, lng: lng};
                            var marker;
                            var geocoder = new google.maps.Geocoder;
                            markerUrl = window.location.host + 'wings/assets/admin/images/marker.png';
                            mapProp = {
                                center: new google.maps.LatLng(lat, lng),
                                zoom: 13,
                                gestureHandling: 'greedy'
                            };

                            map = new google.maps.Map(document.getElementById("map"), mapProp);
                            marker = new google.maps.Marker({
                                map: map,
                                animation: google.maps.Animation.DROP,
                                icon: iconUrl
                            });


                            google.maps.event.addListener(map, 'click', function (event) {
                                placeMarker(event.latLng);
                            });

                            function placeMarker(location) {
                                marker.setPosition(location);
                                geocodeLatLng(geocoder, map, location);
                                latLngLocation = location.toString().slice(1, -1); // remove ()
                                $('#location').val(latLngLocation);
                            }

                            function geocodeLatLng(geocoder, map, latlng) {
                                geocoder.geocode({'location': latlng}, function (results, status) {
                                    if (status === 'OK') {
                                        if (results[0]) {
                                            address = results[0].formatted_address;

                                            $('#pac-input').val(address);
                                        } else {
                                            window.alert('No results found');
                                        }
                                    } else {
                                        window.alert('Geocoder failed due to: ' + status);
                                    }
                                });
                            }

                            // Create the search box and link it to the UI element.
                            var input = document.getElementById('pac-input');
                            var searchBox = new google.maps.places.SearchBox(input);
                            // map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

                            // Bias the SearchBox results towards current map's viewport.
                            map.addListener('bounds_changed', function () {
                                searchBox.setBounds(map.getBounds());
                            });

                            var markers = [];
                            // Listen for the event fired when the user selects a prediction and retrieve
                            // more details for that place.
                            searchBox.addListener('places_changed', function () {
                                var places = searchBox.getPlaces();

                                if (places.length == 0) {
                                    return;
                                }

                                // Clear out the old markers.
                                markers.forEach(function (marker) {
                                    marker.setMap(null);
                                });
                                markers = [];

                                // For each place, get the icon, name and location.
                                var bounds = new google.maps.LatLngBounds();
                                places.forEach(function (place) {
                                    if (!place.geometry) {
                                        console.log("Returned place contains no geometry");
                                        return;
                                    }
                                    var icon = {
                                        url: place.icon,
                                        size: new google.maps.Size(71, 71),
                                        origin: new google.maps.Point(0, 0),
                                        anchor: new google.maps.Point(17, 34),
                                        scaledSize: new google.maps.Size(25, 25)
                                    };

                                    if (place.geometry.viewport) {
                                        // Only geocodes have viewport.
                                        bounds.union(place.geometry.viewport);
                                    } else {
                                        bounds.extend(place.geometry.location);
                                    }
                                });
                                map.fitBounds(bounds);
                            });

                        });
                    });
            });
        });
    </script>



@endsection
