@extends('admin.layout.forms.add.index')
@section('action' , "schools/$school->id/levels")
@section('title' , trans('language.add'))
@section('page-title',trans('language.car_types'))
@section('form-groups')
    @includeIf('admin.components.form.add.select', ['label' => trans("language.levels"),'name'=>'level_id', 'items'=> $levels , 'icon' => 'fa fa-list',])
@endsection
@section('submit-button-title' ,trans('language.add'))

