@extends('admin.layout.table.index')
@section('page-title',trans('language.levels'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => "levels/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item"><a href="{{url("admin/schools")}}">  {{trans('language.schools')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.levels')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>

    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.settings')}}</th>
@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->dash_level_name}}</td>
            <td>
                @includeIf("admin.components.buttons.custom" , ["href" => "schoolLevels/$item->id/school_times", 'class' => 'default' , 'title'=> trans('language.stages'), 'icon' => 'fa fa-list-ul','feather'=>'list'])
            </td>
        </tr>

    @endforeach
@endsection






