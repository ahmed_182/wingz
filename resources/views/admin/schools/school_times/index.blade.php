@extends('admin.layout.table.index')
@section('page-title',trans('language.stages'))
@section('buttons')
    @includeIf("admin.components.buttons.addbtn" , ["href" => "school_times/create",'class' => 'btn btn-success' , 'title'=> trans('web.add'), ])
@stop
@section('nav')
    <ol class="breadcrumb">
        <li class="breadcrumb-item"><a href="{{url("admin/")}}">  {{trans('language.home')}}</a></li>
        <li class="breadcrumb-item"><a href="{{url("admin/schools")}}">  {{trans('language.schools')}}</a></li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.levels')}}</li>
        <li class="breadcrumb-item active" aria-current="page">{{trans('language.school_times')}}</li>
    </ol>
@endsection
@section('thead')
    <th>#</th>

    <th>{{trans('language.name')}}</th>
    <th>{{trans('language.start_at_day')}}</th>
    <th>{{trans('language.end_at_day')}}</th>
    <th>{{trans('language.year_start_time')}}</th>
    <th>{{trans('language.year_end_time')}}</th>
    <th>{{trans('language.settings')}}</th>

@endsection
@section('tbody')
    @foreach($items as $item)
        <tr>
            <td>{{ $loop->iteration }}</td>
            <td>{{$item->serv_stage_name}}</td>
            <td>{{$item->dash_start_at}}</td>
            <td>{{$item->dash_end_at}}</td>
            <td>{{$item->getYearStartTimeAttribute()}}</td>
            <td>{{$item->getYearEndTimeAttribute()}}</td>
            <td>
                @includeIf("admin.components.buttons.edit" , ["href" => "schoolLevels/$item->school_level_id/school_times/$item->id/edit"])

                @includeIf("admin.components.buttons.delete",["message" =>  "($item->serv_stage_name)" ,  "action" => url("admin/schoolLevels/$item->school_level_id/school_times/$item->id")])

                @includeIf("admin.components.buttons.custom" , ["href" => "school_times_stages/$item->id/holidays", 'class' => 'default' , 'title'=> trans('language.holidays'), 'icon' => 'fa fa-list-ul','feather'=>'list'])
            </td>
        </tr>
    @endforeach
@endsection


