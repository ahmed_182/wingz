@extends('admin.layout.forms.edit.index')
@section('action' , "schoolLevels/$item->school_level_id/school_times/$item->id")
@section('title' , trans('language.edit'))
@section('page-title',trans('language.school_times'))
@section('form-groups')

    @includeIf('admin.components.form.edit.select', ['label' => trans("language.stages"),'name'=>'stage_id', 'items'=> \App\Stage::all() , 'icon' => 'fa fa-list',])


    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.start_at_day")}}</label>
        <input type="time" class="form-control" name="start_at"
               value="{{$item->format_start_time}}"  placeholder="start_at"></div>

    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.end_at_day")}}</label>
        <input type="time" class="form-control" name="end_at"
               value="{{$item->format_end_time}}"  placeholder="end_at"></div>

    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.year_start_time")}}</label>
        <input type="date" class="form-control" name="year_start_at"
               value="{{$item->format_start_time_year}}"  placeholder="year_start_at"></div>

    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.year_end_time")}}</label>
        <input type="date" class="form-control" name="year_end_at"
               value="{{$item->format_end_time_year}}"  placeholder="year_end_at"></div>






@endsection
@section('submit-button-title' ,trans('language.edit'))
