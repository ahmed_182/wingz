@extends('admin.layout.forms.add.index')
@section('action' , "schoolLevels/$school_level->id/school_times")
@section('title' , trans('language.add'))
@section('page-title',trans('language.school_times'))
@section('form-groups')

    @includeIf('admin.components.form.add.select', ['label' => trans("language.stages"),'name'=>'stage_id', 'items'=> \App\Stage::where("level_id",$school_level->level_id)->get() , 'icon' => 'fa fa-list',])


    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.start_at_day")}}</label>
        <input type="time" class="form-control" name="start_at"
               value=""  placeholder="start_at"></div>

    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.end_at_day")}}</label>
        <input type="time" class="form-control" name="end_at"
               value=""  placeholder="end_at"></div>

    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.year_start_time")}}</label>
        <input type="date" class="form-control" name="year_start_at"
               value=""  placeholder="year_start_at"></div>

    <div class="col-md-12 m-b-20">
        <label for="">{{trans("language.year_end_time")}}</label>
        <input type="date" class="form-control" name="year_end_at"
               value=""  placeholder="year_end_at"></div>

@endsection
@section('submit-button-title' ,trans('language.add'))

